package ch.diso.llqa.controller;

import ch.diso.llqa.dto.UserDto;
import ch.diso.llqa.dto.UsersFullDto;
import ch.diso.llqa.dto.UsersShortlistDto;
import ch.diso.llqa.model.Grant;
import ch.diso.llqa.model.User;
import ch.diso.llqa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

// TODO: @CrossOrigins is set to test from frontend (localhost:4567) to backend (localhost:8080) otherwise the preflight
// TODO: returns forbidden (403)
@CrossOrigin(origins = {"http://localhost:4567", "http://localhost:8080"}, maxAge = 3600)
@RestController
@RequestMapping("/api")
public class UserController {

  private final UserService userService;

  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  /**
   * Route USER_1
   */
  @GetMapping("/users/shortlist")
  public ResponseEntity<UsersShortlistDto> usersShortlist() {
    return new ResponseEntity<>(userService.getUsersShortlist(), HttpStatus.OK);
  }

  /**
   * Route USER_2
   */
  @GetMapping("/users/full")
  public ResponseEntity<UsersFullDto> usersFull() {
    return new ResponseEntity<>(userService.getUsersFull(), HttpStatus.OK);
  }

  /**
   * Route USER_3
   */
  @PutMapping("/users")
  public ResponseEntity<Void> usersUpdate(@RequestBody UserDto userDto) {
    userService.update(userDto);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /**
   * Route USER_3_2
   */
  @PostMapping("/users")
  public ResponseEntity<UserDto> usersCreate(@RequestBody UserDto userDto) {
    return new ResponseEntity<>(userService.create(userDto), HttpStatus.OK);
  }

  /**
   * Route USER_8
   */
  @PostMapping("/users/{userId}/grants")
  public ResponseEntity<List<Grant>> usersGrantsPost(@RequestBody ArrayList<Long> privilegeIds, @PathVariable long userId) {
    return new ResponseEntity<>(userService.createGrant(privilegeIds, userId), HttpStatus.OK);
  }

  /**
   * Route USER_8_2
   */
  @DeleteMapping("/users/{userId}/grants/{privilegeId}")
  public ResponseEntity<Void> usersGrantsDelete(@PathVariable long userId, @PathVariable long privilegeId) {
    userService.deleteGrant(userId, privilegeId);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
