package ch.diso.llqa.controller;

import ch.diso.llqa.dto.UsergroupDto;
import ch.diso.llqa.model.Grant;
import ch.diso.llqa.model.Usergroup;
import ch.diso.llqa.service.UsergroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UsergroupController {

  @Autowired
  private UsergroupService usergroupService;

  /**
   * Route USER_4
   */
  @PutMapping("/groups")
  public ResponseEntity<UsergroupDto> updateUsergroup(@RequestBody UsergroupDto usergroupDto) {
    return new ResponseEntity<>(usergroupService.update(usergroupDto), HttpStatus.OK);
  }

  /**
   * Route USER_4_2
   */
  @PostMapping("/groups")
  public ResponseEntity<UsergroupDto> createUsergroup(@RequestBody UsergroupDto usergroupDto) {
    return new ResponseEntity<>(usergroupService.create(usergroupDto), HttpStatus.OK);
  }

  /**
   * Route USER_6
   */
  @DeleteMapping("/affiliations/{userId}/group/{usergroupId}")
  public ResponseEntity<Void> deleteAffiliations(@PathVariable long userId, @PathVariable long usergroupId) {
    usergroupService.deleteAffiliation(userId, usergroupId);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /**
   * Route USER_7
   */
  @PostMapping("/affiliations")
  public ResponseEntity<Void> createAffiliations(@RequestBody ArrayList<HashMap<String, Long>> affiliations) {
    usergroupService.createAffiliation(affiliations);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /**
   * Route USER_9
   */
  @PostMapping("/groups/{groupId}/grants")
  public ResponseEntity<List<Grant>> usersGrantsPost(@RequestBody ArrayList<Long> privilegeIds, @PathVariable long groupId) {
    return new ResponseEntity<>(usergroupService.createGrant(privilegeIds, groupId), HttpStatus.OK);
  }

  /**
   * Route USER_9_2
   */
  @DeleteMapping("/groups/{groupId}/grants/{privilegeId}")
  public ResponseEntity<Void> usersGrantsDelete(@PathVariable long groupId, @PathVariable long privilegeId) {
    usergroupService.deleteGrant(groupId, privilegeId);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
