package ch.diso.llqa.dto;


import ch.diso.llqa.model.Affiliation;
import ch.diso.llqa.service.TransactionService;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * AffiliationDto
 */
public class AffiliationDto extends BaseDto {

  private long id;

  @JsonProperty(value = "user_id", access = JsonProperty.Access.READ_WRITE)
  private Long userId;

  @JsonProperty(value = "usergroup_id", access = JsonProperty.Access.READ_WRITE)
  private Long usergroupId;

  public AffiliationDto() {
  }

  public AffiliationDto(Affiliation affiliation) {
    this.id = affiliation.getId();
    this.userId = affiliation.getUserId();
    this.usergroupId = affiliation.getUsergroupId();
    this.createdAt = TransactionService.dateToString(affiliation.getCreatedAt());
    this.updatedAt = TransactionService.dateToString(affiliation.getUpdatedAt());
    this.createdBy = TransactionService.stringIdToLong(affiliation.getCreatedBy());
    this.updatedBy = TransactionService.stringIdToLong(affiliation.getUpdatedBy());
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getUsergroupId() {
    return usergroupId;
  }

  public void setUsergroupId(Long usergroupId) {
    this.usergroupId = usergroupId;
  }
}