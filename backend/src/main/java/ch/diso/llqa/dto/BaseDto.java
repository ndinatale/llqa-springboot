package ch.diso.llqa.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by ndinatale on 02.06.17.
 */
public abstract class BaseDto {

  @JsonProperty(value = "created_at", access = JsonProperty.Access.READ_WRITE)
  protected String createdAt;

  @JsonProperty(value = "updated_at", access = JsonProperty.Access.READ_WRITE)
  protected String updatedAt;

  @JsonProperty(value = "created_by", access = JsonProperty.Access.READ_WRITE)
  protected Long createdBy;

  @JsonProperty(value = "updated_by", access = JsonProperty.Access.READ_WRITE)
  protected Long updatedBy;

  public String getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(String createdAt) {
    this.createdAt = createdAt;
  }

  public String getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(String updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
  }

  public Long getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(Long updatedBy) {
    this.updatedBy = updatedBy;
  }
}
