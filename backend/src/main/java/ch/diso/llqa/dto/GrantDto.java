package ch.diso.llqa.dto;


import ch.diso.llqa.model.Grant;
import ch.diso.llqa.service.TransactionService;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * GrantDto
 */
public class GrantDto extends BaseDto {

  private long id;

  @JsonProperty(value = "grantee_id", access = JsonProperty.Access.READ_WRITE)
  private Long granteeId;

  @JsonProperty(value = "grantee_type", access = JsonProperty.Access.READ_WRITE)
  private String granteeType;

  @JsonProperty(value = "privilege_id", access = JsonProperty.Access.READ_WRITE)
  private Long privilegeId;

  public GrantDto() {
  }

  public GrantDto(Grant grant) {
    this.id = grant.getId();
    this.granteeId = grant.getGranteeId();
    this.granteeType = grant.getGranteeType();
    this.privilegeId = grant.getPrivilegeId();
    this.createdAt = TransactionService.dateToString(grant.getCreatedAt());
    this.updatedAt = TransactionService.dateToString(grant.getUpdatedAt());
    this.createdBy = TransactionService.stringIdToLong(grant.getCreatedBy());
    this.updatedBy = TransactionService.stringIdToLong(grant.getUpdatedBy());
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Long getGranteeId() {
    return granteeId;
  }

  public void setGranteeId(Long granteeId) {
    this.granteeId = granteeId;
  }

  public String getGranteeType() {
    return granteeType;
  }

  public void setGranteeType(String granteeType) {
    this.granteeType = granteeType;
  }

  public Long getPrivilegeId() {
    return privilegeId;
  }

  public void setPrivilegeId(Long privilegeId) {
    this.privilegeId = privilegeId;
  }
}