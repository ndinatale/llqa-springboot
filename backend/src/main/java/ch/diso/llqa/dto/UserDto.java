package ch.diso.llqa.dto;


import ch.diso.llqa.model.User;
import ch.diso.llqa.service.TransactionService;

import java.util.ArrayList;
import java.util.Map;

/**
 * UserDto
 */
public class UserDto extends BaseDto {

  private long id;
  private String username;
  private String passhash;
  private String realname;
  private String comment;
  private Map userinfo;
  private Long status;
  private Map metadata;
  private ArrayList dashboardinfo;


  public UserDto() {
  }

  public UserDto(User user) {
    this.id = user.getId();
    this.username = user.getUsername();
    this.passhash = user.getPasshash();
    this.realname = user.getRealname();
    this.comment = user.getComment();
    this.userinfo = TransactionService.deserializeYamlToMap(user.getUserinfo());
    this.status = user.getStatus();
    this.createdAt = TransactionService.dateToString(user.getCreatedAt());
    this.updatedAt = TransactionService.dateToString(user.getUpdatedAt());
    this.createdBy = TransactionService.stringIdToLong(user.getCreatedBy());
    this.updatedBy = TransactionService.stringIdToLong(user.getUpdatedBy());
    this.metadata = TransactionService.deserializeYamlToMap(user.getMetadata());
    this.dashboardinfo = TransactionService.deserializeYamlToArrayList(user.getDashboardinfo());
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPasshash() {
    return passhash;
  }

  public void setPasshash(String passhash) {
    this.passhash = passhash;
  }

  public String getRealname() {
    return realname;
  }

  public void setRealname(String realname) {
    this.realname = realname;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public Map getUserinfo() {
    return userinfo;
  }

  public void setUserinfo(Map userinfo) {
    this.userinfo = userinfo;
  }

  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }

  public Map getMetadata() {
    return metadata;
  }

  public void setMetadata(Map metadata) {
    this.metadata = metadata;
  }

  public ArrayList getDashboardinfo() {
    return dashboardinfo;
  }

  public void setDashboardinfo(ArrayList dashboardinfo) {
    this.dashboardinfo = dashboardinfo;
  }

}