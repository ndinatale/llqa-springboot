package ch.diso.llqa.dto;

import ch.diso.llqa.model.Usergroup;
import ch.diso.llqa.service.TransactionService;

/**
 * UsergroupDto
 */
public class UsergroupDto extends BaseDto  {

  private long id;
  private String name;
  private Long level;
  private String description;
  private Boolean deleted;

  public UsergroupDto() {
  }

  public UsergroupDto(Usergroup usergroup) {
    this.id = usergroup.getId();
    this.name = usergroup.getName();
    this.level = usergroup.getLevel();
    this.description = usergroup.getDescription();
    this.createdAt = TransactionService.dateToString(usergroup.getCreatedAt());
    this.updatedAt = TransactionService.dateToString(usergroup.getUpdatedAt());
    this.createdBy = TransactionService.stringIdToLong(usergroup.getCreatedBy());
    this.updatedBy = TransactionService.stringIdToLong(usergroup.getUpdatedBy());
    this.deleted = usergroup.getDeleted();
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getLevel() {
    return level;
  }

  public void setLevel(Long level) {
    this.level = level;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Boolean getDeleted() {
    return deleted;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }
}