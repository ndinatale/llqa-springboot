package ch.diso.llqa.dto;

import java.util.List;

/**
 * UsersFullDto
 */
public class UsersFullDto {

    private List<UsersFullDtoUsergroupsDto> groups;
    private List<UsersFullDtoUsersDto> users;
    private List<UsersFullDtoPrivilegesDto> privileges;

    public UsersFullDto() {
    }

    public UsersFullDto(List<UsersFullDtoUsergroupsDto> groups, List<UsersFullDtoUsersDto> users, List<UsersFullDtoPrivilegesDto> privileges) {
        this.groups = groups;
        this.users = users;
        this.privileges = privileges;
    }

    public List<UsersFullDtoUsergroupsDto> getGroups() {
        return groups;
    }

    public void setGroups(List<UsersFullDtoUsergroupsDto> groups) {
        this.groups = groups;
    }

    public List<UsersFullDtoUsersDto> getUsers() {
        return users;
    }

    public void setUsers(List<UsersFullDtoUsersDto> users) {
        this.users = users;
    }

    public List<UsersFullDtoPrivilegesDto> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(List<UsersFullDtoPrivilegesDto> privileges) {
        this.privileges = privileges;
    }
}
