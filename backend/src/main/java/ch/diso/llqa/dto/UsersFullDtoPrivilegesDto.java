package ch.diso.llqa.dto;


import ch.diso.llqa.model.Privilege;
import ch.diso.llqa.service.TransactionService;

/**
 * UsersFullDtoPrivilegesDto
 */
public class UsersFullDtoPrivilegesDto extends BaseDto  {

  private long id;
  private String name;
  private String description;
  private String code;

  public UsersFullDtoPrivilegesDto(Privilege privilege) {
    this.id = privilege.getId();
    this.name = privilege.getName();
    this.description = privilege.getDescription();
    this.code = privilege.getCode();
    this.createdAt = TransactionService.dateToString(privilege.getCreatedAt());
    this.updatedAt = TransactionService.dateToString(privilege.getUpdatedAt());
    this.createdBy = TransactionService.stringIdToLong(privilege.getCreatedBy());
    this.updatedBy = TransactionService.stringIdToLong(privilege.getUpdatedBy());
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}