package ch.diso.llqa.dto;

import java.util.List;

/**
 * UsersShortlistDto
 */
public class UsersShortlistDto {

    private List<UsersShortlistDtoUsergroupsDto> groups;
    private List<UsersShortlistDtoUsersDto> users;

    public UsersShortlistDto() {
    }

    public UsersShortlistDto(List<UsersShortlistDtoUsergroupsDto> groups, List<UsersShortlistDtoUsersDto> users) {
        this.groups = groups;
        this.users = users;
    }

    public List<UsersShortlistDtoUsergroupsDto> getUserShortlistUsergroupsDtos() {
        return groups;
    }

    public void setUserShortlistUsergroupsDtos(List<UsersShortlistDtoUsergroupsDto> usersShortlistDtoUsergroupsDtos) {
        this.groups = usersShortlistDtoUsergroupsDtos;
    }

    public List<UsersShortlistDtoUsersDto> getUserShortlistUsersDtos() {
        return users;
    }

    public void setUserShortlistUsersDtos(List<UsersShortlistDtoUsersDto> usersShortlistDtoUsersDtos) {
        this.users = usersShortlistDtoUsersDtos;
    }
}
