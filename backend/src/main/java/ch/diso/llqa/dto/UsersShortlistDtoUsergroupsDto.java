package ch.diso.llqa.dto;


import ch.diso.llqa.model.Usergroup;

/**
 * UsersShortlistDtoUsergroupsDto
 */
public class UsersShortlistDtoUsergroupsDto {

    private long id;
    private String name;
    private Long level;

    public UsersShortlistDtoUsergroupsDto() {
    }

    public UsersShortlistDtoUsergroupsDto(Usergroup usergroup) {
        this.id = usergroup.getId();
        this.name = usergroup.getName();
        this.level = usergroup.getLevel();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }
}
