package ch.diso.llqa.dto;

import ch.diso.llqa.model.User;
import java.util.ArrayList;

/**
 * UsersShortlistDtoUsersDto
 */
public class UsersShortlistDtoUsersDto {

    private long id;
    private String username;
    private String realname;
    private ArrayList<Long> groupIds;

    public UsersShortlistDtoUsersDto() {
    }

    public UsersShortlistDtoUsersDto(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.realname = user.getRealname();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public ArrayList<Long> getGroupIds() {
        return groupIds;
    }

    public void setGroupIds(ArrayList<Long> groupIds) {
        this.groupIds = groupIds;
    }
}
