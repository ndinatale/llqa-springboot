package ch.diso.llqa.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * AccessNotAllowedException
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
public class AccessNotAllowedException extends RuntimeException {

  public AccessNotAllowedException(long currentUserId) {
    super("access not allowed by user with id: " + currentUserId);
  }

}
