package ch.diso.llqa.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * AffiliationNotFoundException
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class AffiliationNotFoundException extends RuntimeException {

  public AffiliationNotFoundException(long userId, long usergroupId) {
    super("affiliation not found with userId: " + userId + " usergroupId: " + usergroupId);
  }

}
