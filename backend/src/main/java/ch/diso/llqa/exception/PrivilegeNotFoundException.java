package ch.diso.llqa.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * PrivilegeNotFoundException
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class PrivilegeNotFoundException extends RuntimeException {

  public PrivilegeNotFoundException(long grantId) {
    super("privilege not found: " + grantId);
  }

}
