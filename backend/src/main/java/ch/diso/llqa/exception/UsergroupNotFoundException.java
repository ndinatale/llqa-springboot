package ch.diso.llqa.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * UsergroupNotFoundException
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class UsergroupNotFoundException extends RuntimeException {

  public UsergroupNotFoundException(long usergroupId) {
    super("usergroup not found: " + usergroupId);
  }

}
