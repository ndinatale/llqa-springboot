package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "activechecktypes", schema = "public", catalog = "spring")
public class Activechecktype extends BaseEntity {

  private long id;
  private Long modelId;
  private Long checktypeId;
  private Model modelsByModelId;
  private Checktype checktypesByChecktypeId;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_activechecktypes", sequenceName="sq_activechecktypes", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_activechecktypes")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "model_id", nullable = true)
  public Long getModelId() {
    return modelId;
  }

  public void setModelId(Long modelId) {
    this.modelId = modelId;
  }

  @Basic
  @Column(name = "checktype_id", nullable = true)
  public Long getChecktypeId() {
    return checktypeId;
  }

  public void setChecktypeId(Long checktypeId) {
    this.checktypeId = checktypeId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Activechecktype that = (Activechecktype) o;

    if (id != that.id) return false;
    if (modelId != null ? !modelId.equals(that.modelId) : that.modelId != null) return false;
    if (checktypeId != null ? !checktypeId.equals(that.checktypeId) : that.checktypeId != null) return false;
    if (createdAt != null ? !createdAt.equals(that.createdAt) : that.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(that.updatedAt) : that.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(that.updatedBy) : that.updatedBy != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (modelId != null ? modelId.hashCode() : 0);
    result = 31 * result + (checktypeId != null ? checktypeId.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    return result;
  }

  @ManyToOne
  @JoinColumn(name = "model_id", referencedColumnName = "id", insertable=false, updatable=false)
  public Model getModelsByModelId() {
    return modelsByModelId;
  }

  public void setModelsByModelId(Model modelsByModelId) {
    this.modelsByModelId = modelsByModelId;
  }

  @ManyToOne
  @JoinColumn(name = "checktype_id", referencedColumnName = "id", insertable=false, updatable=false)
  public Checktype getChecktypesByChecktypeId() {
    return checktypesByChecktypeId;
  }

  public void setChecktypesByChecktypeId(Checktype checktypesByChecktypeId) {
    this.checktypesByChecktypeId = checktypesByChecktypeId;
  }
}
