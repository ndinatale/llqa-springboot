package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "activeprocedures", schema = "public", catalog = "spring")
public class Activeprocedure extends BaseEntity {

  private long id;
  private Long modelId;
  private Long procedureId;
  private String flowcontrol;
  private long seqnum;
  private Long enforce;
  private Model modelsByModelId;
  private Procedure proceduresByProcedureId;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_activeprocedures", sequenceName="sq_activeprocedures", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_activeprocedures")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "model_id", nullable = true)
  public Long getModelId() {
    return modelId;
  }

  public void setModelId(Long modelId) {
    this.modelId = modelId;
  }

  @Basic
  @Column(name = "procedure_id", nullable = true)
  public Long getProcedureId() {
    return procedureId;
  }

  public void setProcedureId(Long procedureId) {
    this.procedureId = procedureId;
  }

  @Basic
  @Column(name = "flowcontrol", nullable = true, length = -1)
  public String getFlowcontrol() {
    return flowcontrol;
  }

  public void setFlowcontrol(String flowcontrol) {
    this.flowcontrol = flowcontrol;
  }

  @Basic
  @Column(name = "created_at", nullable = true)
  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  @Basic
  @Column(name = "updated_at", nullable = true)
  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Timestamp updatedAt) {
    this.updatedAt = updatedAt;
  }

  @Basic
  @Column(name = "updated_by", nullable = false, length = 255)
  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  @Basic
  @Column(name = "created_by", nullable = false, length = 255)
  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String measure) {
    createdBy = measure;
  }

  @Basic
  @Column(name = "seqnum", nullable = false)
  public long getSeqnum() {
    return seqnum;
  }

  public void setSeqnum(long seqnum) {
    this.seqnum = seqnum;
  }

  @Basic
  @Column(name = "enforce", nullable = true)
  public Long getEnforce() {
    return enforce;
  }

  public void setEnforce(Long enforce) {
    this.enforce = enforce;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Activeprocedure that = (Activeprocedure) o;

    if (id != that.id) return false;
    if (seqnum != that.seqnum) return false;
    if (modelId != null ? !modelId.equals(that.modelId) : that.modelId != null) return false;
    if (procedureId != null ? !procedureId.equals(that.procedureId) : that.procedureId != null) return false;
    if (flowcontrol != null ? !flowcontrol.equals(that.flowcontrol) : that.flowcontrol != null) return false;
    if (createdAt != null ? !createdAt.equals(that.createdAt) : that.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(that.updatedAt) : that.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(that.updatedBy) : that.updatedBy != null) return false;
    if (enforce != null ? !enforce.equals(that.enforce) : that.enforce != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (modelId != null ? modelId.hashCode() : 0);
    result = 31 * result + (procedureId != null ? procedureId.hashCode() : 0);
    result = 31 * result + (flowcontrol != null ? flowcontrol.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    result = 31 * result + (int) (seqnum ^ (seqnum >>> 32));
    result = 31 * result + (enforce != null ? enforce.hashCode() : 0);
    return result;
  }

  @ManyToOne
  @JoinColumn(name = "model_id", referencedColumnName = "id", insertable=false, updatable=false)
  public Model getModelsByModelId() {
    return modelsByModelId;
  }

  public void setModelsByModelId(Model modelsByModelId) {
    this.modelsByModelId = modelsByModelId;
  }

  @ManyToOne
  @JoinColumn(name = "procedure_id", referencedColumnName = "id", insertable=false, updatable=false)
  public Procedure getProceduresByProcedureId() {
    return proceduresByProcedureId;
  }

  public void setProceduresByProcedureId(Procedure proceduresByProcedureId) {
    this.proceduresByProcedureId = proceduresByProcedureId;
  }
}
