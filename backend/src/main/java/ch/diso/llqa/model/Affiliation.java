package ch.diso.llqa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "affiliations", schema = "public", catalog = "spring")
public class Affiliation extends BaseEntity {

  private long id;

  @JsonProperty(value = "user_id", access = JsonProperty.Access.READ_WRITE)
  private Long userId;

  @JsonProperty(value = "usergroup_id", access = JsonProperty.Access.READ_WRITE)
  private Long usergroupId;

  private User usersByUserId;
  private Usergroup usergroupsByUsergroupId;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_affiliations", sequenceName="sq_affiliations", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_affiliations")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "user_id", nullable = true)
  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  @Basic
  @Column(name = "usergroup_id", nullable = true)
  public Long getUsergroupId() {
    return usergroupId;
  }

  public void setUsergroupId(Long usergroupId) {
    this.usergroupId = usergroupId;
  }

  @Basic
  @Column(name = "created_at", nullable = true)
  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  @Basic
  @Column(name = "updated_at", nullable = true)
  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Timestamp updatedAt) {
    this.updatedAt = updatedAt;
  }

  @Basic
  @Column(name = "created_by", nullable = false, length = 255)
  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Basic
  @Column(name = "updated_by", nullable = false, length = 255)
  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Affiliation that = (Affiliation) o;

    if (id != that.id) return false;
    if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
    if (usergroupId != null ? !usergroupId.equals(that.usergroupId) : that.usergroupId != null) return false;
    if (createdAt != null ? !createdAt.equals(that.createdAt) : that.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(that.updatedAt) : that.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(that.updatedBy) : that.updatedBy != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (userId != null ? userId.hashCode() : 0);
    result = 31 * result + (usergroupId != null ? usergroupId.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    return result;
  }

  @ManyToOne
  @JoinColumn(name = "user_id", referencedColumnName = "id", insertable=false, updatable=false)
  @JsonIgnore
  public User getUsersByUserId() {
    return usersByUserId;
  }

  public void setUsersByUserId(User usersByUserId) {
    this.usersByUserId = usersByUserId;
  }

  @ManyToOne
  @JoinColumn(name = "usergroup_id", referencedColumnName = "id", insertable=false, updatable=false)
  @JsonIgnore
  public Usergroup getUsergroupsByUsergroupId() {
    return usergroupsByUsergroupId;
  }

  public void setUsergroupsByUsergroupId(Usergroup usergroupsByUsergroupId) {
    this.usergroupsByUsergroupId = usergroupsByUsergroupId;
  }
}
