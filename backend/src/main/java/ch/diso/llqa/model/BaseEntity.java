package ch.diso.llqa.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.sql.Timestamp;

/**
 * BaseEntity
 */
@MappedSuperclass
public abstract class BaseEntity {

  @Basic
  @Column(name = "created_at", nullable = true)
  @JsonProperty(value = "created_at", access = JsonProperty.Access.READ_WRITE)
  protected Timestamp createdAt;

  @Basic
  @Column(name = "updated_at", nullable = true)
  @JsonProperty(value = "updated_at", access = JsonProperty.Access.READ_WRITE)
  protected Timestamp updatedAt;

  @Basic
  @Column(name = "created_by", nullable = true)
  @JsonProperty(value = "created_by", access = JsonProperty.Access.READ_WRITE)
  protected String createdBy;

  @Basic
  @Column(name = "updated_by", nullable = true)
  @JsonProperty(value = "updated_by", access = JsonProperty.Access.READ_WRITE)
  protected String updatedBy;

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Timestamp updatedAt) {
    this.updatedAt = updatedAt;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }
}
