package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "binaryfiles", schema = "public", catalog = "spring")
public class Binaryfile extends BaseEntity {
  private long id;
  private String filename;
  private String originalFilename;
  private String hexhash;
  private String mimetype;
  private Long size;
  private String metadata;
  private Collection<Document> documentsById;
  private Collection<Image> imagesById;
  private Collection<Image> imagesById_0;
  private Collection<Image> imagesById_1;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_binaryfiles", sequenceName="sq_binaryfiles", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_binaryfiles")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "filename", nullable = true, length = 255)
  public String getFilename() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  @Basic
  @Column(name = "original_filename", nullable = true, length = 255)
  public String getOriginalFilename() {
    return originalFilename;
  }

  public void setOriginalFilename(String originalFilename) {
    this.originalFilename = originalFilename;
  }

  @Basic
  @Column(name = "hexhash", nullable = true, length = 255)
  public String getHexhash() {
    return hexhash;
  }

  public void setHexhash(String hexhash) {
    this.hexhash = hexhash;
  }

  @Basic
  @Column(name = "mimetype", nullable = true, length = 255)
  public String getMimetype() {
    return mimetype;
  }

  public void setMimetype(String mimetype) {
    this.mimetype = mimetype;
  }

  @Basic
  @Column(name = "size", nullable = true)
  public Long getSize() {
    return size;
  }

  public void setSize(Long size) {
    this.size = size;
  }

  @Basic
  @Column(name = "metadata", nullable = true, length = -1)
  public String getMetadata() {
    return metadata;
  }

  public void setMetadata(String metadata) {
    this.metadata = metadata;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Binaryfile that = (Binaryfile) o;

    if (id != that.id) return false;
    if (filename != null ? !filename.equals(that.filename) : that.filename != null) return false;
    if (originalFilename != null ? !originalFilename.equals(that.originalFilename) : that.originalFilename != null)
      return false;
    if (hexhash != null ? !hexhash.equals(that.hexhash) : that.hexhash != null) return false;
    if (mimetype != null ? !mimetype.equals(that.mimetype) : that.mimetype != null) return false;
    if (size != null ? !size.equals(that.size) : that.size != null) return false;
    if (metadata != null ? !metadata.equals(that.metadata) : that.metadata != null) return false;
    if (createdAt != null ? !createdAt.equals(that.createdAt) : that.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(that.updatedAt) : that.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(that.updatedBy) : that.updatedBy != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (filename != null ? filename.hashCode() : 0);
    result = 31 * result + (originalFilename != null ? originalFilename.hashCode() : 0);
    result = 31 * result + (hexhash != null ? hexhash.hashCode() : 0);
    result = 31 * result + (mimetype != null ? mimetype.hashCode() : 0);
    result = 31 * result + (size != null ? size.hashCode() : 0);
    result = 31 * result + (metadata != null ? metadata.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    return result;
  }

  @OneToMany(mappedBy = "binaryfilesByBinaryfileId")
  public Collection<Document> getDocumentsById() {
    return documentsById;
  }

  public void setDocumentsById(Collection<Document> documentsById) {
    this.documentsById = documentsById;
  }

  @OneToMany(mappedBy = "binaryfilesByFullimageId")
  public Collection<Image> getImagesById() {
    return imagesById;
  }

  public void setImagesById(Collection<Image> imagesById) {
    this.imagesById = imagesById;
  }

  @OneToMany(mappedBy = "binaryfilesByPreviewId")
  public Collection<Image> getImagesById_0() {
    return imagesById_0;
  }

  public void setImagesById_0(Collection<Image> imagesById_0) {
    this.imagesById_0 = imagesById_0;
  }

  @OneToMany(mappedBy = "binaryfilesByThumbnailId")
  public Collection<Image> getImagesById_1() {
    return imagesById_1;
  }

  public void setImagesById_1(Collection<Image> imagesById_1) {
    this.imagesById_1 = imagesById_1;
  }
}
