package ch.diso.llqa.model;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "checks", schema = "public", catalog = "spring")
public class Check extends BaseEntity {
  private long id;
  private long unitId;
  private long modelId;
  private long checktypeId;
  private String comment;
  private Date scheduled;
  private Date dueby;
  private Date started;
  private Date finished;
  private Long status;
  private String metadata;
  private String checkdata;
  private BigInteger progressFinished;
  private BigInteger progressError;
  private String assignData;
  private Unit unitsByUnitId;
  private Model modelsByModelId;
  private Checktype checktypesByChecktypeId;
  private Collection<Measurement> measurementsById;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_checks", sequenceName="sq_checks", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_checks")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "unit_id", nullable = false)
  public long getUnitId() {
    return unitId;
  }

  public void setUnitId(long unitId) {
    this.unitId = unitId;
  }

  @Basic
  @Column(name = "model_id", nullable = false)
  public long getModelId() {
    return modelId;
  }

  public void setModelId(long modelId) {
    this.modelId = modelId;
  }

  @Basic
  @Column(name = "checktype_id", nullable = false)
  public long getChecktypeId() {
    return checktypeId;
  }

  public void setChecktypeId(long checktypeId) {
    this.checktypeId = checktypeId;
  }

  @Basic
  @Column(name = "comment", nullable = true, length = -1)
  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  @Basic
  @Column(name = "scheduled", nullable = true)
  public Date getScheduled() {
    return scheduled;
  }

  public void setScheduled(Date scheduled) {
    this.scheduled = scheduled;
  }

  @Basic
  @Column(name = "dueby", nullable = true)
  public Date getDueby() {
    return dueby;
  }

  public void setDueby(Date dueby) {
    this.dueby = dueby;
  }

  @Basic
  @Column(name = "started", nullable = true)
  public Date getStarted() {
    return started;
  }

  public void setStarted(Date started) {
    this.started = started;
  }

  @Basic
  @Column(name = "finished", nullable = true)
  public Date getFinished() {
    return finished;
  }

  public void setFinished(Date finished) {
    this.finished = finished;
  }

  @Basic
  @Column(name = "status", nullable = true)
  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }

  @Basic
  @Column(name = "metadata", nullable = true, length = -1)
  public String getMetadata() {
    return metadata;
  }

  public void setMetadata(String metadata) {
    this.metadata = metadata;
  }

  @Basic
  @Column(name = "checkdata", nullable = true, length = -1)
  public String getCheckdata() {
    return checkdata;
  }

  public void setCheckdata(String checkdata) {
    this.checkdata = checkdata;
  }

  @Basic
  @Column(name = "progress_finished", nullable = true, precision = 0)
  public BigInteger getProgressFinished() {
    return progressFinished;
  }

  public void setProgressFinished(BigInteger progressFinished) {
    this.progressFinished = progressFinished;
  }

  @Basic
  @Column(name = "progress_error", nullable = true, precision = 0)
  public BigInteger getProgressError() {
    return progressError;
  }

  public void setProgressError(BigInteger progressError) {
    this.progressError = progressError;
  }

  @Basic
  @Column(name = "assign_data", nullable = true, length = -1)
  public String getAssignData() {
    return assignData;
  }

  public void setAssignData(String assignData) {
    this.assignData = assignData;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Check check = (Check) o;

    if (id != check.id) return false;
    if (unitId != check.unitId) return false;
    if (modelId != check.modelId) return false;
    if (checktypeId != check.checktypeId) return false;
    if (comment != null ? !comment.equals(check.comment) : check.comment != null) return false;
    if (scheduled != null ? !scheduled.equals(check.scheduled) : check.scheduled != null) return false;
    if (dueby != null ? !dueby.equals(check.dueby) : check.dueby != null) return false;
    if (started != null ? !started.equals(check.started) : check.started != null) return false;
    if (finished != null ? !finished.equals(check.finished) : check.finished != null) return false;
    if (status != null ? !status.equals(check.status) : check.status != null) return false;
    if (metadata != null ? !metadata.equals(check.metadata) : check.metadata != null) return false;
    if (checkdata != null ? !checkdata.equals(check.checkdata) : check.checkdata != null) return false;
    if (createdAt != null ? !createdAt.equals(check.createdAt) : check.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(check.updatedAt) : check.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(check.createdBy) : check.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(check.updatedBy) : check.updatedBy != null) return false;
    if (progressFinished != null ? !progressFinished.equals(check.progressFinished) : check.progressFinished != null)
      return false;
    if (progressError != null ? !progressError.equals(check.progressError) : check.progressError != null) return false;
    if (assignData != null ? !assignData.equals(check.assignData) : check.assignData != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (int) (unitId ^ (unitId >>> 32));
    result = 31 * result + (int) (modelId ^ (modelId >>> 32));
    result = 31 * result + (int) (checktypeId ^ (checktypeId >>> 32));
    result = 31 * result + (comment != null ? comment.hashCode() : 0);
    result = 31 * result + (scheduled != null ? scheduled.hashCode() : 0);
    result = 31 * result + (dueby != null ? dueby.hashCode() : 0);
    result = 31 * result + (started != null ? started.hashCode() : 0);
    result = 31 * result + (finished != null ? finished.hashCode() : 0);
    result = 31 * result + (status != null ? status.hashCode() : 0);
    result = 31 * result + (metadata != null ? metadata.hashCode() : 0);
    result = 31 * result + (checkdata != null ? checkdata.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    result = 31 * result + (progressFinished != null ? progressFinished.hashCode() : 0);
    result = 31 * result + (progressError != null ? progressError.hashCode() : 0);
    result = 31 * result + (assignData != null ? assignData.hashCode() : 0);
    return result;
  }

  @ManyToOne
  @JoinColumn(name = "unit_id", referencedColumnName = "id", nullable = false, insertable=false, updatable=false)
  public Unit getUnitsByUnitId() {
    return unitsByUnitId;
  }

  public void setUnitsByUnitId(Unit unitsByUnitId) {
    this.unitsByUnitId = unitsByUnitId;
  }

  @ManyToOne
  @JoinColumn(name = "model_id", referencedColumnName = "id", nullable = false, insertable=false, updatable=false)
  public Model getModelsByModelId() {
    return modelsByModelId;
  }

  public void setModelsByModelId(Model modelsByModelId) {
    this.modelsByModelId = modelsByModelId;
  }

  @ManyToOne
  @JoinColumn(name = "checktype_id", referencedColumnName = "id", nullable = false, insertable=false, updatable=false)
  public Checktype getChecktypesByChecktypeId() {
    return checktypesByChecktypeId;
  }

  public void setChecktypesByChecktypeId(Checktype checktypesByChecktypeId) {
    this.checktypesByChecktypeId = checktypesByChecktypeId;
  }

  @OneToMany(mappedBy = "checksByCheckId")
  public Collection<Measurement> getMeasurementsById() {
    return measurementsById;
  }

  public void setMeasurementsById(Collection<Measurement> measurementsById) {
    this.measurementsById = measurementsById;
  }
}
