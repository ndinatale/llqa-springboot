package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "checktypes", schema = "public", catalog = "spring")
public class Checktype extends BaseEntity
{
  private long id;
  private String code;
  private String title;
  private String description;
  private String metadata;
  private Boolean deleted;
  private Collection<Activechecktype> activechecktypesById;
  private Collection<Check> checksById;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_checktypes", sequenceName="sq_checktypes", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_checktypes")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "code", nullable = false, length = 255)
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  @Basic
  @Column(name = "title", nullable = false, length = -1)
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Basic
  @Column(name = "description", nullable = true, length = -1)
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Basic
  @Column(name = "metadata", nullable = true, length = -1)
  public String getMetadata() {
    return metadata;
  }

  public void setMetadata(String metadata) {
    this.metadata = metadata;
  }

  @Basic
  @Column(name = "deleted", nullable = true)
  public Boolean getDeleted() {
    return deleted;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Checktype checktype = (Checktype) o;

    if (id != checktype.id) return false;
    if (code != null ? !code.equals(checktype.code) : checktype.code != null) return false;
    if (title != null ? !title.equals(checktype.title) : checktype.title != null) return false;
    if (description != null ? !description.equals(checktype.description) : checktype.description != null) return false;
    if (metadata != null ? !metadata.equals(checktype.metadata) : checktype.metadata != null) return false;
    if (deleted != null ? !deleted.equals(checktype.deleted) : checktype.deleted != null) return false;
    if (createdAt != null ? !createdAt.equals(checktype.createdAt) : checktype.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(checktype.updatedAt) : checktype.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(checktype.createdBy) : checktype.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(checktype.updatedBy) : checktype.updatedBy != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (code != null ? code.hashCode() : 0);
    result = 31 * result + (title != null ? title.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (metadata != null ? metadata.hashCode() : 0);
    result = 31 * result + (deleted != null ? deleted.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    return result;
  }

  @OneToMany(mappedBy = "checktypesByChecktypeId")
  public Collection<Activechecktype> getActivechecktypesById() {
    return activechecktypesById;
  }

  public void setActivechecktypesById(Collection<Activechecktype> activechecktypesById) {
    this.activechecktypesById = activechecktypesById;
  }

  @OneToMany(mappedBy = "checktypesByChecktypeId")
  public Collection<Check> getChecksById() {
    return checksById;
  }

  public void setChecksById(Collection<Check> checksById) {
    this.checksById = checksById;
  }
}
