package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "configentries", schema = "public", catalog = "spring")
public class Configentry extends BaseEntity {
  private long id;
  private Long codeId;
  private Long configtableId;
  private Boolean active;
  private Boolean deleted;
  private String col1;
  private String col2;
  private String col3;
  private String col4;
  private String col5;
  private String col6;
  private String timeline;
  private String blockedBy;
  private Boolean blocked;
  private Configtable configtablesByConfigtableId;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_configentries", sequenceName="sq_configentries", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_configentries")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "code_id", nullable = true)
  public Long getCodeId() {
    return codeId;
  }

  public void setCodeId(Long codeId) {
    this.codeId = codeId;
  }

  @Basic
  @Column(name = "configtable_id", nullable = true)
  public Long getConfigtableId() {
    return configtableId;
  }

  public void setConfigtableId(Long configtableId) {
    this.configtableId = configtableId;
  }

  @Basic
  @Column(name = "active", nullable = true)
  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  @Basic
  @Column(name = "deleted", nullable = true)
  public Boolean getDeleted() {
    return deleted;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }

  @Basic
  @Column(name = "col1", nullable = true, length = -1)
  public String getCol1() {
    return col1;
  }

  public void setCol1(String col1) {
    this.col1 = col1;
  }

  @Basic
  @Column(name = "col2", nullable = true, length = -1)
  public String getCol2() {
    return col2;
  }

  public void setCol2(String col2) {
    this.col2 = col2;
  }

  @Basic
  @Column(name = "col3", nullable = true, length = -1)
  public String getCol3() {
    return col3;
  }

  public void setCol3(String col3) {
    this.col3 = col3;
  }

  @Basic
  @Column(name = "col4", nullable = true, length = -1)
  public String getCol4() {
    return col4;
  }

  public void setCol4(String col4) {
    this.col4 = col4;
  }

  @Basic
  @Column(name = "col5", nullable = true, length = -1)
  public String getCol5() {
    return col5;
  }

  public void setCol5(String col5) {
    this.col5 = col5;
  }

  @Basic
  @Column(name = "col6", nullable = true, length = -1)
  public String getCol6() {
    return col6;
  }

  public void setCol6(String col6) {
    this.col6 = col6;
  }

  @Basic
  @Column(name = "timeline", nullable = true, length = -1)
  public String getTimeline() {
    return timeline;
  }

  public void setTimeline(String timeline) {
    this.timeline = timeline;
  }

  @Basic
  @Column(name = "blocked_by", nullable = true, length = -1)
  public String getBlockedBy() {
    return blockedBy;
  }

  public void setBlockedBy(String blockedBy) {
    this.blockedBy = blockedBy;
  }

  @Basic
  @Column(name = "blocked", nullable = true)
  public Boolean getBlocked() {
    return blocked;
  }

  public void setBlocked(Boolean blocked) {
    this.blocked = blocked;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Configentry that = (Configentry) o;

    if (id != that.id) return false;
    if (codeId != null ? !codeId.equals(that.codeId) : that.codeId != null) return false;
    if (configtableId != null ? !configtableId.equals(that.configtableId) : that.configtableId != null) return false;
    if (active != null ? !active.equals(that.active) : that.active != null) return false;
    if (deleted != null ? !deleted.equals(that.deleted) : that.deleted != null) return false;
    if (col1 != null ? !col1.equals(that.col1) : that.col1 != null) return false;
    if (col2 != null ? !col2.equals(that.col2) : that.col2 != null) return false;
    if (col3 != null ? !col3.equals(that.col3) : that.col3 != null) return false;
    if (col4 != null ? !col4.equals(that.col4) : that.col4 != null) return false;
    if (col5 != null ? !col5.equals(that.col5) : that.col5 != null) return false;
    if (col6 != null ? !col6.equals(that.col6) : that.col6 != null) return false;
    if (timeline != null ? !timeline.equals(that.timeline) : that.timeline != null) return false;
    if (createdAt != null ? !createdAt.equals(that.createdAt) : that.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(that.updatedAt) : that.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(that.updatedBy) : that.updatedBy != null) return false;
    if (blockedBy != null ? !blockedBy.equals(that.blockedBy) : that.blockedBy != null) return false;
    if (blocked != null ? !blocked.equals(that.blocked) : that.blocked != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (codeId != null ? codeId.hashCode() : 0);
    result = 31 * result + (configtableId != null ? configtableId.hashCode() : 0);
    result = 31 * result + (active != null ? active.hashCode() : 0);
    result = 31 * result + (deleted != null ? deleted.hashCode() : 0);
    result = 31 * result + (col1 != null ? col1.hashCode() : 0);
    result = 31 * result + (col2 != null ? col2.hashCode() : 0);
    result = 31 * result + (col3 != null ? col3.hashCode() : 0);
    result = 31 * result + (col4 != null ? col4.hashCode() : 0);
    result = 31 * result + (col5 != null ? col5.hashCode() : 0);
    result = 31 * result + (col6 != null ? col6.hashCode() : 0);
    result = 31 * result + (timeline != null ? timeline.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    result = 31 * result + (blockedBy != null ? blockedBy.hashCode() : 0);
    result = 31 * result + (blocked != null ? blocked.hashCode() : 0);
    return result;
  }

  @ManyToOne
  @JoinColumn(name = "configtable_id", referencedColumnName = "id", insertable=false, updatable=false)
  public Configtable getConfigtablesByConfigtableId() {
    return configtablesByConfigtableId;
  }

  public void setConfigtablesByConfigtableId(Configtable configtablesByConfigtableId) {
    this.configtablesByConfigtableId = configtablesByConfigtableId;
  }
}
