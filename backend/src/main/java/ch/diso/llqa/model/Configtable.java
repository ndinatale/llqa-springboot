package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "configtables", schema = "public", catalog = "spring")
public class Configtable extends BaseEntity
{
  private long id;
  private String title;
  private String parent;
  private Long parentid;
  private String parentcode;
  private String colheader1;
  private String colheader2;
  private String colheader3;
  private String colheader4;
  private String colheader5;
  private String colheader6;
  private String timeline;
  private Boolean editable;
  private Boolean deleted;
  private Collection<Configentry> configentriesById;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_configtables", sequenceName="sq_configtables", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_configtables")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "title", nullable = true, length = 255)
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Basic
  @Column(name = "parent", nullable = true, length = 255)
  public String getParent() {
    return parent;
  }

  public void setParent(String parent) {
    this.parent = parent;
  }

  @Basic
  @Column(name = "parentid", nullable = true)
  public Long getParentid() {
    return parentid;
  }

  public void setParentid(Long parentid) {
    this.parentid = parentid;
  }

  @Basic
  @Column(name = "parentcode", nullable = true, length = 255)
  public String getParentcode() {
    return parentcode;
  }

  public void setParentcode(String parentcode) {
    this.parentcode = parentcode;
  }

  @Basic
  @Column(name = "colheader1", nullable = true, length = 255)
  public String getColheader1() {
    return colheader1;
  }

  public void setColheader1(String colheader1) {
    this.colheader1 = colheader1;
  }

  @Basic
  @Column(name = "colheader2", nullable = true, length = 255)
  public String getColheader2() {
    return colheader2;
  }

  public void setColheader2(String colheader2) {
    this.colheader2 = colheader2;
  }

  @Basic
  @Column(name = "colheader3", nullable = true, length = 255)
  public String getColheader3() {
    return colheader3;
  }

  public void setColheader3(String colheader3) {
    this.colheader3 = colheader3;
  }

  @Basic
  @Column(name = "colheader4", nullable = true, length = 255)
  public String getColheader4() {
    return colheader4;
  }

  public void setColheader4(String colheader4) {
    this.colheader4 = colheader4;
  }

  @Basic
  @Column(name = "colheader5", nullable = true, length = 255)
  public String getColheader5() {
    return colheader5;
  }

  public void setColheader5(String colheader5) {
    this.colheader5 = colheader5;
  }

  @Basic
  @Column(name = "colheader6", nullable = true, length = 255)
  public String getColheader6() {
    return colheader6;
  }

  public void setColheader6(String colheader6) {
    this.colheader6 = colheader6;
  }

  @Basic
  @Column(name = "timeline", nullable = true, length = -1)
  public String getTimeline() {
    return timeline;
  }

  public void setTimeline(String timeline) {
    this.timeline = timeline;
  }

  @Basic
  @Column(name = "editable", nullable = true)
  public Boolean getEditable() {
    return editable;
  }

  public void setEditable(Boolean editable) {
    this.editable = editable;
  }

  @Basic
  @Column(name = "deleted", nullable = true)
  public Boolean getDeleted() {
    return deleted;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Configtable that = (Configtable) o;

    if (id != that.id) return false;
    if (title != null ? !title.equals(that.title) : that.title != null) return false;
    if (parent != null ? !parent.equals(that.parent) : that.parent != null) return false;
    if (parentid != null ? !parentid.equals(that.parentid) : that.parentid != null) return false;
    if (parentcode != null ? !parentcode.equals(that.parentcode) : that.parentcode != null) return false;
    if (colheader1 != null ? !colheader1.equals(that.colheader1) : that.colheader1 != null) return false;
    if (colheader2 != null ? !colheader2.equals(that.colheader2) : that.colheader2 != null) return false;
    if (colheader3 != null ? !colheader3.equals(that.colheader3) : that.colheader3 != null) return false;
    if (colheader4 != null ? !colheader4.equals(that.colheader4) : that.colheader4 != null) return false;
    if (colheader5 != null ? !colheader5.equals(that.colheader5) : that.colheader5 != null) return false;
    if (colheader6 != null ? !colheader6.equals(that.colheader6) : that.colheader6 != null) return false;
    if (timeline != null ? !timeline.equals(that.timeline) : that.timeline != null) return false;
    if (editable != null ? !editable.equals(that.editable) : that.editable != null) return false;
    if (deleted != null ? !deleted.equals(that.deleted) : that.deleted != null) return false;
    if (createdAt != null ? !createdAt.equals(that.createdAt) : that.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(that.updatedAt) : that.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(that.updatedBy) : that.updatedBy != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (title != null ? title.hashCode() : 0);
    result = 31 * result + (parent != null ? parent.hashCode() : 0);
    result = 31 * result + (parentid != null ? parentid.hashCode() : 0);
    result = 31 * result + (parentcode != null ? parentcode.hashCode() : 0);
    result = 31 * result + (colheader1 != null ? colheader1.hashCode() : 0);
    result = 31 * result + (colheader2 != null ? colheader2.hashCode() : 0);
    result = 31 * result + (colheader3 != null ? colheader3.hashCode() : 0);
    result = 31 * result + (colheader4 != null ? colheader4.hashCode() : 0);
    result = 31 * result + (colheader5 != null ? colheader5.hashCode() : 0);
    result = 31 * result + (colheader6 != null ? colheader6.hashCode() : 0);
    result = 31 * result + (timeline != null ? timeline.hashCode() : 0);
    result = 31 * result + (editable != null ? editable.hashCode() : 0);
    result = 31 * result + (deleted != null ? deleted.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    return result;
  }

  @OneToMany(mappedBy = "configtablesByConfigtableId")
  public Collection<Configentry> getConfigentriesById() {
    return configentriesById;
  }

  public void setConfigentriesById(Collection<Configentry> configentriesById) {
    this.configentriesById = configentriesById;
  }
}
