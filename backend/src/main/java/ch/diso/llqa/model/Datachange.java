package ch.diso.llqa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "datachanges", schema = "public", catalog = "spring")
public class Datachange {
  private int id;
  private Integer sourceId;
  private String sourceType;
  private String subobject;
  private String subobjtype;
  private String changetype;
  private String changedetails;
  private Integer user;
  private Integer timestamp;
  private Boolean postFinalize;
  private Boolean dirty;
  private User usersByUser;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_datachanges", sequenceName="sq_datachanges", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_datachanges")
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Basic
  @Column(name = "source_id", nullable = true)
  public Integer getSourceId() {
    return sourceId;
  }

  public void setSourceId(Integer sourceId) {
    this.sourceId = sourceId;
  }

  @Basic
  @Column(name = "source_type", nullable = true, length = 255)
  public String getSourceType() {
    return sourceType;
  }

  public void setSourceType(String sourceType) {
    this.sourceType = sourceType;
  }

  @Basic
  @Column(name = "subobject", nullable = false, length = 255)
  public String getSubobject() {
    return subobject;
  }

  public void setSubobject(String subobject) {
    this.subobject = subobject;
  }

  @Basic
  @Column(name = "subobjtype", nullable = false, length = 255)
  public String getSubobjtype() {
    return subobjtype;
  }

  public void setSubobjtype(String subobjtype) {
    this.subobjtype = subobjtype;
  }

  @Basic
  @Column(name = "changetype", nullable = false, length = 255)
  public String getChangetype() {
    return changetype;
  }

  public void setChangetype(String changetype) {
    this.changetype = changetype;
  }

  @Basic
  @Column(name = "changedetails", nullable = true, length = -1)
  public String getChangedetails() {
    return changedetails;
  }

  public void setChangedetails(String changedetails) {
    this.changedetails = changedetails;
  }

  @Basic
  @Column(name = "user", nullable = true)
  public Integer getUser() {
    return user;
  }

  public void setUser(Integer user) {
    this.user = user;
  }

  @Basic
  @Column(name = "timestamp", nullable = true)
  public Integer getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Integer timestamp) {
    this.timestamp = timestamp;
  }

  @Basic
  @Column(name = "post_finalize", nullable = true)
  public Boolean getPostFinalize() {
    return postFinalize;
  }

  public void setPostFinalize(Boolean postFinalize) {
    this.postFinalize = postFinalize;
  }

  @Basic
  @Column(name = "dirty", nullable = true)
  public Boolean getDirty() {
    return dirty;
  }

  public void setDirty(Boolean dirty) {
    this.dirty = dirty;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Datachange that = (Datachange) o;

    if (id != that.id) return false;
    if (sourceId != null ? !sourceId.equals(that.sourceId) : that.sourceId != null) return false;
    if (sourceType != null ? !sourceType.equals(that.sourceType) : that.sourceType != null) return false;
    if (subobject != null ? !subobject.equals(that.subobject) : that.subobject != null) return false;
    if (subobjtype != null ? !subobjtype.equals(that.subobjtype) : that.subobjtype != null) return false;
    if (changetype != null ? !changetype.equals(that.changetype) : that.changetype != null) return false;
    if (changedetails != null ? !changedetails.equals(that.changedetails) : that.changedetails != null) return false;
    if (user != null ? !user.equals(that.user) : that.user != null) return false;
    if (timestamp != null ? !timestamp.equals(that.timestamp) : that.timestamp != null) return false;
    if (postFinalize != null ? !postFinalize.equals(that.postFinalize) : that.postFinalize != null) return false;
    if (dirty != null ? !dirty.equals(that.dirty) : that.dirty != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + (sourceId != null ? sourceId.hashCode() : 0);
    result = 31 * result + (sourceType != null ? sourceType.hashCode() : 0);
    result = 31 * result + (subobject != null ? subobject.hashCode() : 0);
    result = 31 * result + (subobjtype != null ? subobjtype.hashCode() : 0);
    result = 31 * result + (changetype != null ? changetype.hashCode() : 0);
    result = 31 * result + (changedetails != null ? changedetails.hashCode() : 0);
    result = 31 * result + (user != null ? user.hashCode() : 0);
    result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
    result = 31 * result + (postFinalize != null ? postFinalize.hashCode() : 0);
    result = 31 * result + (dirty != null ? dirty.hashCode() : 0);
    return result;
  }

  @ManyToOne
  @JoinColumn(name = "user", referencedColumnName = "id", insertable=false, updatable=false)
  @JsonIgnore
  public User getUsersByUser() {
    return usersByUser;
  }

  public void setUsersByUser(User usersByUser) {
    this.usersByUser = usersByUser;
  }
}
