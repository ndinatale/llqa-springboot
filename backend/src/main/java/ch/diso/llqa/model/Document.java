package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "documents", schema = "public", catalog = "spring")
public class Document extends BaseEntity {
  private long id;
  private Long ownerId;
  private String ownerType;
  private Long binaryfileId;
  private String caption;
  private Long doctype;
  private long seqnum;
  private Binaryfile binaryfilesByBinaryfileId;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_documents", sequenceName="sq_documents", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_documents")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "owner_id", nullable = true)
  public Long getOwnerId() {
    return ownerId;
  }

  public void setOwnerId(Long ownerId) {
    this.ownerId = ownerId;
  }

  @Basic
  @Column(name = "owner_type", nullable = true, length = 255)
  public String getOwnerType() {
    return ownerType;
  }

  public void setOwnerType(String ownerType) {
    this.ownerType = ownerType;
  }

  @Basic
  @Column(name = "binaryfile_id", nullable = true)
  public Long getBinaryfileId() {
    return binaryfileId;
  }

  public void setBinaryfileId(Long binaryfileId) {
    this.binaryfileId = binaryfileId;
  }

  @Basic
  @Column(name = "caption", nullable = true, length = 255)
  public String getCaption() {
    return caption;
  }

  public void setCaption(String caption) {
    this.caption = caption;
  }

  @Basic
  @Column(name = "doctype", nullable = true)
  public Long getDoctype() {
    return doctype;
  }

  public void setDoctype(Long doctype) {
    this.doctype = doctype;
  }

  @Basic
  @Column(name = "seqnum", nullable = false)
  public long getSeqnum() {
    return seqnum;
  }

  public void setSeqnum(long seqnum) {
    this.seqnum = seqnum;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Document document = (Document) o;

    if (id != document.id) return false;
    if (seqnum != document.seqnum) return false;
    if (ownerId != null ? !ownerId.equals(document.ownerId) : document.ownerId != null) return false;
    if (ownerType != null ? !ownerType.equals(document.ownerType) : document.ownerType != null) return false;
    if (binaryfileId != null ? !binaryfileId.equals(document.binaryfileId) : document.binaryfileId != null)
      return false;
    if (caption != null ? !caption.equals(document.caption) : document.caption != null) return false;
    if (doctype != null ? !doctype.equals(document.doctype) : document.doctype != null) return false;
    if (createdAt != null ? !createdAt.equals(document.createdAt) : document.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(document.updatedAt) : document.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(document.createdBy) : document.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(document.updatedBy) : document.updatedBy != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (ownerId != null ? ownerId.hashCode() : 0);
    result = 31 * result + (ownerType != null ? ownerType.hashCode() : 0);
    result = 31 * result + (binaryfileId != null ? binaryfileId.hashCode() : 0);
    result = 31 * result + (caption != null ? caption.hashCode() : 0);
    result = 31 * result + (doctype != null ? doctype.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    result = 31 * result + (int) (seqnum ^ (seqnum >>> 32));
    return result;
  }

  @ManyToOne
  @JoinColumn(name = "binaryfile_id", referencedColumnName = "id", insertable=false, updatable=false)
  public Binaryfile getBinaryfilesByBinaryfileId() {
    return binaryfilesByBinaryfileId;
  }

  public void setBinaryfilesByBinaryfileId(Binaryfile binaryfilesByBinaryfileId) {
    this.binaryfilesByBinaryfileId = binaryfilesByBinaryfileId;
  }
}
