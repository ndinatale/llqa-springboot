package ch.diso.llqa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "grants", schema = "public", catalog = "spring")
public class Grant extends BaseEntity {

  private long id;

  @JsonProperty(value = "grantee_id", access = JsonProperty.Access.READ_WRITE)
  private Long granteeId;

  @JsonProperty(value = "grantee_type", access = JsonProperty.Access.READ_WRITE)
  private String granteeType;

  @JsonProperty(value = "privilege_id", access = JsonProperty.Access.READ_WRITE)
  private Long privilegeId;

  private Privilege privilegesByPrivilegeId;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_grants", sequenceName="sq_grants", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_grants")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "grantee_id", nullable = true)
  public Long getGranteeId() {
    return granteeId;
  }

  public void setGranteeId(Long granteeId) {
    this.granteeId = granteeId;
  }

  @Basic
  @Column(name = "grantee_type", nullable = true, length = 255)
  public String getGranteeType() {
    return granteeType;
  }

  public void setGranteeType(String granteeType) {
    this.granteeType = granteeType;
  }

  @Basic
  @Column(name = "privilege_id", nullable = true)
  public Long getPrivilegeId() {
    return privilegeId;
  }

  public void setPrivilegeId(Long privilegeId) {
    this.privilegeId = privilegeId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Grant grant = (Grant) o;

    if (id != grant.id) return false;
    if (granteeId != null ? !granteeId.equals(grant.granteeId) : grant.granteeId != null) return false;
    if (granteeType != null ? !granteeType.equals(grant.granteeType) : grant.granteeType != null) return false;
    if (privilegeId != null ? !privilegeId.equals(grant.privilegeId) : grant.privilegeId != null) return false;
    if (createdAt != null ? !createdAt.equals(grant.createdAt) : grant.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(grant.updatedAt) : grant.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(grant.createdBy) : grant.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(grant.updatedBy) : grant.updatedBy != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (granteeId != null ? granteeId.hashCode() : 0);
    result = 31 * result + (granteeType != null ? granteeType.hashCode() : 0);
    result = 31 * result + (privilegeId != null ? privilegeId.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    return result;
  }

  @ManyToOne
  @JoinColumn(name = "privilege_id", referencedColumnName = "id", insertable=false, updatable=false)
  @JsonIgnore
  public Privilege getPrivilegesByPrivilegeId() {
    return privilegesByPrivilegeId;
  }

  public void setPrivilegesByPrivilegeId(Privilege privilegesByPrivilegeId) {
    this.privilegesByPrivilegeId = privilegesByPrivilegeId;
  }
}
