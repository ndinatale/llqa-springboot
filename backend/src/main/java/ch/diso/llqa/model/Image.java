package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "images", schema = "public", catalog = "spring")
public class Image extends BaseEntity {

  private long id;
  private Long ownerId;
  private String ownerType;
  private Long fullimageId;
  private Long previewId;
  private Long thumbnailId;
  private String caption;
  private long seqnum;
  private Binaryfile binaryfilesByFullimageId;
  private Binaryfile binaryfilesByPreviewId;
  private Binaryfile binaryfilesByThumbnailId;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_images", sequenceName="sq_images", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_images")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "owner_id", nullable = true)
  public Long getOwnerId() {
    return ownerId;
  }

  public void setOwnerId(Long ownerId) {
    this.ownerId = ownerId;
  }

  @Basic
  @Column(name = "owner_type", nullable = true, length = 255)
  public String getOwnerType() {
    return ownerType;
  }

  public void setOwnerType(String ownerType) {
    this.ownerType = ownerType;
  }

  @Basic
  @Column(name = "fullimage_id", nullable = true)
  public Long getFullimageId() {
    return fullimageId;
  }

  public void setFullimageId(Long fullimageId) {
    this.fullimageId = fullimageId;
  }

  @Basic
  @Column(name = "preview_id", nullable = true)
  public Long getPreviewId() {
    return previewId;
  }

  public void setPreviewId(Long previewId) {
    this.previewId = previewId;
  }

  @Basic
  @Column(name = "thumbnail_id", nullable = true)
  public Long getThumbnailId() {
    return thumbnailId;
  }

  public void setThumbnailId(Long thumbnailId) {
    this.thumbnailId = thumbnailId;
  }

  @Basic
  @Column(name = "caption", nullable = true, length = 255)
  public String getCaption() {
    return caption;
  }

  public void setCaption(String caption) {
    this.caption = caption;
  }

  @Basic
  @Column(name = "seqnum", nullable = false)
  public long getSeqnum() {
    return seqnum;
  }

  public void setSeqnum(long seqnum) {
    this.seqnum = seqnum;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Image image = (Image) o;

    if (id != image.id) return false;
    if (seqnum != image.seqnum) return false;
    if (ownerId != null ? !ownerId.equals(image.ownerId) : image.ownerId != null) return false;
    if (ownerType != null ? !ownerType.equals(image.ownerType) : image.ownerType != null) return false;
    if (fullimageId != null ? !fullimageId.equals(image.fullimageId) : image.fullimageId != null) return false;
    if (previewId != null ? !previewId.equals(image.previewId) : image.previewId != null) return false;
    if (thumbnailId != null ? !thumbnailId.equals(image.thumbnailId) : image.thumbnailId != null) return false;
    if (caption != null ? !caption.equals(image.caption) : image.caption != null) return false;
    if (createdAt != null ? !createdAt.equals(image.createdAt) : image.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(image.updatedAt) : image.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(image.createdBy) : image.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(image.updatedBy) : image.updatedBy != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (ownerId != null ? ownerId.hashCode() : 0);
    result = 31 * result + (ownerType != null ? ownerType.hashCode() : 0);
    result = 31 * result + (fullimageId != null ? fullimageId.hashCode() : 0);
    result = 31 * result + (previewId != null ? previewId.hashCode() : 0);
    result = 31 * result + (thumbnailId != null ? thumbnailId.hashCode() : 0);
    result = 31 * result + (caption != null ? caption.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    result = 31 * result + (int) (seqnum ^ (seqnum >>> 32));
    return result;
  }

  @ManyToOne
  @JoinColumn(name = "fullimage_id", referencedColumnName = "id", insertable=false, updatable=false)
  public Binaryfile getBinaryfilesByFullimageId() {
    return binaryfilesByFullimageId;
  }

  public void setBinaryfilesByFullimageId(Binaryfile binaryfilesByFullimageId) {
    this.binaryfilesByFullimageId = binaryfilesByFullimageId;
  }

  @ManyToOne
  @JoinColumn(name = "preview_id", referencedColumnName = "id", insertable=false, updatable=false)
  public Binaryfile getBinaryfilesByPreviewId() {
    return binaryfilesByPreviewId;
  }

  public void setBinaryfilesByPreviewId(Binaryfile binaryfilesByPreviewId) {
    this.binaryfilesByPreviewId = binaryfilesByPreviewId;
  }

  @ManyToOne
  @JoinColumn(name = "thumbnail_id", referencedColumnName = "id", insertable=false, updatable=false)
  public Binaryfile getBinaryfilesByThumbnailId() {
    return binaryfilesByThumbnailId;
  }

  public void setBinaryfilesByThumbnailId(Binaryfile binaryfilesByThumbnailId) {
    this.binaryfilesByThumbnailId = binaryfilesByThumbnailId;
  }
}
