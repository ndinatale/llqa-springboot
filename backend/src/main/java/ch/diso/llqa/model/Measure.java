package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "measures", schema = "public", catalog = "spring")
public class Measure extends BaseEntity {
  private long id;
  private long stepId;
  private Long tooltypeId;
  private String code;
  private String title;
  private String description;
  private long measuretype;
  private String calculation;
  private String metadata;
  private String flowcontrol;
  private long seqnum;
  private long enforce;
  private Step stepsByStepId;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_measures", sequenceName="sq_measures", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_measures")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "step_id", nullable = false)
  public long getStepId() {
    return stepId;
  }

  public void setStepId(long stepId) {
    this.stepId = stepId;
  }

  @Basic
  @Column(name = "tooltype_id", nullable = true)
  public Long getTooltypeId() {
    return tooltypeId;
  }

  public void setTooltypeId(Long tooltypeId) {
    this.tooltypeId = tooltypeId;
  }

  @Basic
  @Column(name = "code", nullable = false, length = 255)
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  @Basic
  @Column(name = "title", nullable = false, length = -1)
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Basic
  @Column(name = "description", nullable = true, length = -1)
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Basic
  @Column(name = "measuretype", nullable = false)
  public long getMeasuretype() {
    return measuretype;
  }

  public void setMeasuretype(long measuretype) {
    this.measuretype = measuretype;
  }

  @Basic
  @Column(name = "calculation", nullable = false, length = -1)
  public String getCalculation() {
    return calculation;
  }

  public void setCalculation(String calculation) {
    this.calculation = calculation;
  }

  @Basic
  @Column(name = "metadata", nullable = true, length = -1)
  public String getMetadata() {
    return metadata;
  }

  public void setMetadata(String metadata) {
    this.metadata = metadata;
  }

  @Basic
  @Column(name = "flowcontrol", nullable = true, length = -1)
  public String getFlowcontrol() {
    return flowcontrol;
  }

  public void setFlowcontrol(String flowcontrol) {
    this.flowcontrol = flowcontrol;
  }

  @Basic
  @Column(name = "seqnum", nullable = false)
  public long getSeqnum() {
    return seqnum;
  }

  public void setSeqnum(long seqnum) {
    this.seqnum = seqnum;
  }

  @Basic
  @Column(name = "enforce", nullable = false)
  public long getEnforce() {
    return enforce;
  }

  public void setEnforce(long enforce) {
    this.enforce = enforce;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Measure measure = (Measure) o;

    if (id != measure.id) return false;
    if (stepId != measure.stepId) return false;
    if (measuretype != measure.measuretype) return false;
    if (seqnum != measure.seqnum) return false;
    if (enforce != measure.enforce) return false;
    if (tooltypeId != null ? !tooltypeId.equals(measure.tooltypeId) : measure.tooltypeId != null) return false;
    if (code != null ? !code.equals(measure.code) : measure.code != null) return false;
    if (title != null ? !title.equals(measure.title) : measure.title != null) return false;
    if (description != null ? !description.equals(measure.description) : measure.description != null) return false;
    if (calculation != null ? !calculation.equals(measure.calculation) : measure.calculation != null) return false;
    if (metadata != null ? !metadata.equals(measure.metadata) : measure.metadata != null) return false;
    if (flowcontrol != null ? !flowcontrol.equals(measure.flowcontrol) : measure.flowcontrol != null) return false;
    if (createdAt != null ? !createdAt.equals(measure.createdAt) : measure.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(measure.updatedAt) : measure.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(measure.createdBy) : measure.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(measure.updatedBy) : measure.updatedBy != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (int) (stepId ^ (stepId >>> 32));
    result = 31 * result + (tooltypeId != null ? tooltypeId.hashCode() : 0);
    result = 31 * result + (code != null ? code.hashCode() : 0);
    result = 31 * result + (title != null ? title.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (int) (measuretype ^ (measuretype >>> 32));
    result = 31 * result + (calculation != null ? calculation.hashCode() : 0);
    result = 31 * result + (metadata != null ? metadata.hashCode() : 0);
    result = 31 * result + (flowcontrol != null ? flowcontrol.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    result = 31 * result + (int) (seqnum ^ (seqnum >>> 32));
    result = 31 * result + (int) (enforce ^ (enforce >>> 32));
    return result;
  }

  @ManyToOne
  @JoinColumn(name = "step_id", referencedColumnName = "id", nullable = false, insertable=false, updatable=false)
  public Step getStepsByStepId() {
    return stepsByStepId;
  }

  public void setStepsByStepId(Step stepsByStepId) {
    this.stepsByStepId = stepsByStepId;
  }
}
