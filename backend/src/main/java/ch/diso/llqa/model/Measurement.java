package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "measurements", schema = "public", catalog = "spring")
public class Measurement extends BaseEntity {

  private long id;
  private long checkId;
  private long measureId;
  private Long toolunitId;
  private String rawvalues;
  private String value;
  private String comment;
  private Long status;
  private String metadata;
  private Long savedon;
  private Long savedby;
  private Check checksByCheckId;
  private Toolunit toolunitsByToolunitId;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_measurements", sequenceName="sq_measurements", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_measurements")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "check_id", nullable = false)
  public long getCheckId() {
    return checkId;
  }

  public void setCheckId(long checkId) {
    this.checkId = checkId;
  }

  @Basic
  @Column(name = "measure_id", nullable = false)
  public long getMeasureId() {
    return measureId;
  }

  public void setMeasureId(long measureId) {
    this.measureId = measureId;
  }

  @Basic
  @Column(name = "toolunit_id", nullable = true)
  public Long getToolunitId() {
    return toolunitId;
  }

  public void setToolunitId(Long toolunitId) {
    this.toolunitId = toolunitId;
  }

  @Basic
  @Column(name = "rawvalues", nullable = true, length = -1)
  public String getRawvalues() {
    return rawvalues;
  }

  public void setRawvalues(String rawvalues) {
    this.rawvalues = rawvalues;
  }

  @Basic
  @Column(name = "value", nullable = true, length = 255)
  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @Basic
  @Column(name = "comment", nullable = true, length = 1000)
  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  @Basic
  @Column(name = "status", nullable = false)
  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }

  @Basic
  @Column(name = "metadata", nullable = true, length = -1)
  public String getMetadata() {
    return metadata;
  }

  public void setMetadata(String metadata) {
    this.metadata = metadata;
  }

  @Basic
  @Column(name = "savedon", nullable = true)
  public Long getSavedon() {
    return savedon;
  }

  public void setSavedon(Long savedon) {
    this.savedon = savedon;
  }

  @Basic
  @Column(name = "savedby", nullable = true)
  public Long getSavedby() {
    return savedby;
  }

  public void setSavedby(Long savedby) {
    this.savedby = savedby;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Measurement that = (Measurement) o;

    if (id != that.id) return false;
    if (checkId != that.checkId) return false;
    if (measureId != that.measureId) return false;
    if (status != that.status) return false;
    if (toolunitId != null ? !toolunitId.equals(that.toolunitId) : that.toolunitId != null) return false;
    if (rawvalues != null ? !rawvalues.equals(that.rawvalues) : that.rawvalues != null) return false;
    if (value != null ? !value.equals(that.value) : that.value != null) return false;
    if (comment != null ? !comment.equals(that.comment) : that.comment != null) return false;
    if (createdAt != null ? !createdAt.equals(that.createdAt) : that.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(that.updatedAt) : that.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(that.updatedBy) : that.updatedBy != null) return false;
    if (metadata != null ? !metadata.equals(that.metadata) : that.metadata != null) return false;
    if (savedon != null ? !savedon.equals(that.savedon) : that.savedon != null) return false;
    if (savedby != null ? !savedby.equals(that.savedby) : that.savedby != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (int) (checkId ^ (checkId >>> 32));
    result = 31 * result + (int) (measureId ^ (measureId >>> 32));
    result = 31 * result + (toolunitId != null ? toolunitId.hashCode() : 0);
    result = 31 * result + (rawvalues != null ? rawvalues.hashCode() : 0);
    result = 31 * result + (value != null ? value.hashCode() : 0);
    result = 31 * result + (comment != null ? comment.hashCode() : 0);
    result = 31 * result + (int) (status ^ (status >>> 32));
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    result = 31 * result + (metadata != null ? metadata.hashCode() : 0);
    result = 31 * result + (savedon != null ? savedon.hashCode() : 0);
    result = 31 * result + (savedby != null ? savedby.hashCode() : 0);
    return result;
  }

  @ManyToOne
  @JoinColumn(name = "check_id", referencedColumnName = "id", nullable = false, insertable=false, updatable=false)
  public Check getChecksByCheckId() {
    return checksByCheckId;
  }

  public void setChecksByCheckId(Check checksByCheckId) {
    this.checksByCheckId = checksByCheckId;
  }

  @ManyToOne
  @JoinColumn(name = "toolunit_id", referencedColumnName = "id", insertable=false, updatable=false)
  public Toolunit getToolunitsByToolunitId() {
    return toolunitsByToolunitId;
  }

  public void setToolunitsByToolunitId(Toolunit toolunitsByToolunitId) {
    this.toolunitsByToolunitId = toolunitsByToolunitId;
  }
}
