package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "models", schema = "public", catalog = "spring")
public class Model extends BaseEntity {

  private long id;
  private long devicetypeId;
  private String code;
  private String title;
  private String description;
  private String metadata;
  private long realid;
  private Long version;
  private Boolean disabled;
  private Collection<Activechecktype> activechecktypesById;
  private Collection<Activeprocedure> activeproceduresById;
  private Collection<Check> checksById;
  private Model modelsByDevicetypeId;
  private Collection<Model> modelsById;
  private Collection<Unit> unitsById;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_models", sequenceName="sq_models", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_models")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "devicetype_id", nullable = false)
  public long getDevicetypeId() {
    return devicetypeId;
  }

  public void setDevicetypeId(long devicetypeId) {
    this.devicetypeId = devicetypeId;
  }

  @Basic
  @Column(name = "code", nullable = false, length = 255)
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  @Basic
  @Column(name = "title", nullable = false, length = -1)
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Basic
  @Column(name = "description", nullable = true, length = -1)
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Basic
  @Column(name = "metadata", nullable = true, length = -1)
  public String getMetadata() {
    return metadata;
  }

  public void setMetadata(String metadata) {
    this.metadata = metadata;
  }

  @Basic
  @Column(name = "realid", nullable = false)
  public long getRealid() {
    return realid;
  }

  public void setRealid(long realid) {
    this.realid = realid;
  }

  @Basic
  @Column(name = "version", nullable = true)
  public Long getVersion() {
    return version;
  }

  public void setVersion(Long version) {
    this.version = version;
  }

  @Basic
  @Column(name = "disabled", nullable = true)
  public Boolean getDisabled() {
    return disabled;
  }

  public void setDisabled(Boolean disabled) {
    this.disabled = disabled;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Model model = (Model) o;

    if (id != model.id) return false;
    if (devicetypeId != model.devicetypeId) return false;
    if (realid != model.realid) return false;
    if (code != null ? !code.equals(model.code) : model.code != null) return false;
    if (title != null ? !title.equals(model.title) : model.title != null) return false;
    if (description != null ? !description.equals(model.description) : model.description != null) return false;
    if (metadata != null ? !metadata.equals(model.metadata) : model.metadata != null) return false;
    if (version != null ? !version.equals(model.version) : model.version != null) return false;
    if (disabled != null ? !disabled.equals(model.disabled) : model.disabled != null) return false;
    if (createdAt != null ? !createdAt.equals(model.createdAt) : model.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(model.updatedAt) : model.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(model.createdBy) : model.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(model.updatedBy) : model.updatedBy != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (int) (devicetypeId ^ (devicetypeId >>> 32));
    result = 31 * result + (code != null ? code.hashCode() : 0);
    result = 31 * result + (title != null ? title.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (metadata != null ? metadata.hashCode() : 0);
    result = 31 * result + (int) (realid ^ (realid >>> 32));
    result = 31 * result + (version != null ? version.hashCode() : 0);
    result = 31 * result + (disabled != null ? disabled.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    return result;
  }

  @OneToMany(mappedBy = "modelsByModelId")
  public Collection<Activechecktype> getActivechecktypesById() {
    return activechecktypesById;
  }

  public void setActivechecktypesById(Collection<Activechecktype> activechecktypesById) {
    this.activechecktypesById = activechecktypesById;
  }

  @OneToMany(mappedBy = "modelsByModelId")
  public Collection<Activeprocedure> getActiveproceduresById() {
    return activeproceduresById;
  }

  public void setActiveproceduresById(Collection<Activeprocedure> activeproceduresById) {
    this.activeproceduresById = activeproceduresById;
  }

  @OneToMany(mappedBy = "modelsByModelId")
  public Collection<Check> getChecksById() {
    return checksById;
  }

  public void setChecksById(Collection<Check> checksById) {
    this.checksById = checksById;
  }

  @ManyToOne
  @JoinColumn(name = "devicetype_id", referencedColumnName = "id", nullable = false, insertable=false, updatable=false)
  public Model getModelsByDevicetypeId() {
    return modelsByDevicetypeId;
  }

  public void setModelsByDevicetypeId(Model modelsByDevicetypeId) {
    this.modelsByDevicetypeId = modelsByDevicetypeId;
  }

  @OneToMany(mappedBy = "modelsByDevicetypeId")
  public Collection<Model> getModelsById() {
    return modelsById;
  }

  public void setModelsById(Collection<Model> modelsById) {
    this.modelsById = modelsById;
  }

  @OneToMany(mappedBy = "modelsByModelId")
  public Collection<Unit> getUnitsById() {
    return unitsById;
  }

  public void setUnitsById(Collection<Unit> unitsById) {
    this.unitsById = unitsById;
  }
}
