package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "notices", schema = "public", catalog = "spring")
public class Notice extends BaseEntity {

  private long id;
  private String text;
  private long categoryId;
  private String path;
  private String artno;
  private String artdesc;
  private Long timeloss;
  private Long status;
  private String timeline;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_notices", sequenceName="sq_notices", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_notices")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "text", nullable = true, length = 2000)
  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  @Basic
  @Column(name = "category_id", nullable = false)
  public long getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(long categoryId) {
    this.categoryId = categoryId;
  }

  @Basic
  @Column(name = "path", nullable = true, length = -1)
  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  @Basic
  @Column(name = "artno", nullable = true, length = 255)
  public String getArtno() {
    return artno;
  }

  public void setArtno(String artno) {
    this.artno = artno;
  }

  @Basic
  @Column(name = "artdesc", nullable = true, length = 255)
  public String getArtdesc() {
    return artdesc;
  }

  public void setArtdesc(String artdesc) {
    this.artdesc = artdesc;
  }

  @Basic
  @Column(name = "timeloss", nullable = true)
  public Long getTimeloss() {
    return timeloss;
  }

  public void setTimeloss(Long timeloss) {
    this.timeloss = timeloss;
  }

  @Basic
  @Column(name = "status", nullable = true)
  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }

  @Basic
  @Column(name = "timeline", nullable = true, length = -1)
  public String getTimeline() {
    return timeline;
  }

  public void setTimeline(String timeline) {
    this.timeline = timeline;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Notice notice = (Notice) o;

    if (id != notice.id) return false;
    if (categoryId != notice.categoryId) return false;
    if (text != null ? !text.equals(notice.text) : notice.text != null) return false;
    if (path != null ? !path.equals(notice.path) : notice.path != null) return false;
    if (artno != null ? !artno.equals(notice.artno) : notice.artno != null) return false;
    if (artdesc != null ? !artdesc.equals(notice.artdesc) : notice.artdesc != null) return false;
    if (timeloss != null ? !timeloss.equals(notice.timeloss) : notice.timeloss != null) return false;
    if (status != null ? !status.equals(notice.status) : notice.status != null) return false;
    if (timeline != null ? !timeline.equals(notice.timeline) : notice.timeline != null) return false;
    if (createdAt != null ? !createdAt.equals(notice.createdAt) : notice.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(notice.updatedAt) : notice.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(notice.createdBy) : notice.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(notice.updatedBy) : notice.updatedBy != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (text != null ? text.hashCode() : 0);
    result = 31 * result + (int) (categoryId ^ (categoryId >>> 32));
    result = 31 * result + (path != null ? path.hashCode() : 0);
    result = 31 * result + (artno != null ? artno.hashCode() : 0);
    result = 31 * result + (artdesc != null ? artdesc.hashCode() : 0);
    result = 31 * result + (timeloss != null ? timeloss.hashCode() : 0);
    result = 31 * result + (status != null ? status.hashCode() : 0);
    result = 31 * result + (timeline != null ? timeline.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    return result;
  }
}
