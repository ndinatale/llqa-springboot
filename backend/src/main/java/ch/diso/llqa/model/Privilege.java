package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "privileges", schema = "public", catalog = "spring")
public class Privilege extends BaseEntity {
  private long id;
  private String name;
  private String description;
  private String code;
  private Collection<Grant> grantsById;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_privileges", sequenceName="sq_privileges", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_privileges")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "name", nullable = false, length = 255)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Basic
  @Column(name = "description", nullable = true, length = -1)
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Basic
  @Column(name = "code", nullable = false, length = 255)
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Privilege privilege = (Privilege) o;

    if (id != privilege.id) return false;
    if (name != null ? !name.equals(privilege.name) : privilege.name != null) return false;
    if (description != null ? !description.equals(privilege.description) : privilege.description != null) return false;
    if (code != null ? !code.equals(privilege.code) : privilege.code != null) return false;
    if (createdAt != null ? !createdAt.equals(privilege.createdAt) : privilege.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(privilege.updatedAt) : privilege.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(privilege.createdBy) : privilege.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(privilege.updatedBy) : privilege.updatedBy != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (code != null ? code.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    return result;
  }

  @OneToMany(mappedBy = "privilegesByPrivilegeId")
  public Collection<Grant> getGrantsById() {
    return grantsById;
  }

  public void setGrantsById(Collection<Grant> grantsById) {
    this.grantsById = grantsById;
  }
}
