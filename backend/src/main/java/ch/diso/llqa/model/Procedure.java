package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "procedures", schema = "public", catalog = "spring")
public class Procedure extends BaseEntity {

  private long id;
  private String code;
  private String title;
  private String description;
  private String metadata;
  private String flowcontrol;
  private long realid;
  private Long version;
  private boolean disabled;
  private String tag1;
  private String tag2;
  private String tag3;
  private String tag4;
  private String tag5;
  private String tag6;
  private Collection<Activeprocedure> activeproceduresById;
  private Collection<Step> stepsById;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_procedures", sequenceName="sq_procedures", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_procedures")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "code", nullable = false, length = 255)
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  @Basic
  @Column(name = "title", nullable = false, length = -1)
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Basic
  @Column(name = "description", nullable = true, length = -1)
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Basic
  @Column(name = "metadata", nullable = true, length = -1)
  public String getMetadata() {
    return metadata;
  }

  public void setMetadata(String metadata) {
    this.metadata = metadata;
  }

  @Basic
  @Column(name = "flowcontrol", nullable = true, length = -1)
  public String getFlowcontrol() {
    return flowcontrol;
  }

  public void setFlowcontrol(String flowcontrol) {
    this.flowcontrol = flowcontrol;
  }

  @Basic
  @Column(name = "realid", nullable = false)
  public long getRealid() {
    return realid;
  }

  public void setRealid(long realid) {
    this.realid = realid;
  }

  @Basic
  @Column(name = "version", nullable = true)
  public Long getVersion() {
    return version;
  }

  public void setVersion(Long version) {
    this.version = version;
  }

  @Basic
  @Column(name = "disabled", nullable = false)
  public boolean isDisabled() {
    return disabled;
  }

  public void setDisabled(boolean disabled) {
    this.disabled = disabled;
  }

  @Basic
  @Column(name = "tag_1", nullable = true, length = -1)
  public String getTag1() {
    return tag1;
  }

  public void setTag1(String tag1) {
    this.tag1 = tag1;
  }

  @Basic
  @Column(name = "tag_2", nullable = true, length = -1)
  public String getTag2() {
    return tag2;
  }

  public void setTag2(String tag2) {
    this.tag2 = tag2;
  }

  @Basic
  @Column(name = "tag_3", nullable = true, length = -1)
  public String getTag3() {
    return tag3;
  }

  public void setTag3(String tag3) {
    this.tag3 = tag3;
  }

  @Basic
  @Column(name = "tag_4", nullable = true, length = -1)
  public String getTag4() {
    return tag4;
  }

  public void setTag4(String tag4) {
    this.tag4 = tag4;
  }

  @Basic
  @Column(name = "tag_5", nullable = true, length = -1)
  public String getTag5() {
    return tag5;
  }

  public void setTag5(String tag5) {
    this.tag5 = tag5;
  }

  @Basic
  @Column(name = "tag_6", nullable = true, length = -1)
  public String getTag6() {
    return tag6;
  }

  public void setTag6(String tag6) {
    this.tag6 = tag6;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Procedure procedure = (Procedure) o;

    if (id != procedure.id) return false;
    if (realid != procedure.realid) return false;
    if (disabled != procedure.disabled) return false;
    if (code != null ? !code.equals(procedure.code) : procedure.code != null) return false;
    if (title != null ? !title.equals(procedure.title) : procedure.title != null) return false;
    if (description != null ? !description.equals(procedure.description) : procedure.description != null) return false;
    if (metadata != null ? !metadata.equals(procedure.metadata) : procedure.metadata != null) return false;
    if (flowcontrol != null ? !flowcontrol.equals(procedure.flowcontrol) : procedure.flowcontrol != null) return false;
    if (createdAt != null ? !createdAt.equals(procedure.createdAt) : procedure.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(procedure.updatedAt) : procedure.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(procedure.createdBy) : procedure.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(procedure.updatedBy) : procedure.updatedBy != null) return false;
    if (version != null ? !version.equals(procedure.version) : procedure.version != null) return false;
    if (tag1 != null ? !tag1.equals(procedure.tag1) : procedure.tag1 != null) return false;
    if (tag2 != null ? !tag2.equals(procedure.tag2) : procedure.tag2 != null) return false;
    if (tag3 != null ? !tag3.equals(procedure.tag3) : procedure.tag3 != null) return false;
    if (tag4 != null ? !tag4.equals(procedure.tag4) : procedure.tag4 != null) return false;
    if (tag5 != null ? !tag5.equals(procedure.tag5) : procedure.tag5 != null) return false;
    if (tag6 != null ? !tag6.equals(procedure.tag6) : procedure.tag6 != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (code != null ? code.hashCode() : 0);
    result = 31 * result + (title != null ? title.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (metadata != null ? metadata.hashCode() : 0);
    result = 31 * result + (flowcontrol != null ? flowcontrol.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    result = 31 * result + (int) (realid ^ (realid >>> 32));
    result = 31 * result + (version != null ? version.hashCode() : 0);
    result = 31 * result + (disabled ? 1 : 0);
    result = 31 * result + (tag1 != null ? tag1.hashCode() : 0);
    result = 31 * result + (tag2 != null ? tag2.hashCode() : 0);
    result = 31 * result + (tag3 != null ? tag3.hashCode() : 0);
    result = 31 * result + (tag4 != null ? tag4.hashCode() : 0);
    result = 31 * result + (tag5 != null ? tag5.hashCode() : 0);
    result = 31 * result + (tag6 != null ? tag6.hashCode() : 0);
    return result;
  }

  @OneToMany(mappedBy = "proceduresByProcedureId")
  public Collection<Activeprocedure> getActiveproceduresById() {
    return activeproceduresById;
  }

  public void setActiveproceduresById(Collection<Activeprocedure> activeproceduresById) {
    this.activeproceduresById = activeproceduresById;
  }

  @OneToMany(mappedBy = "proceduresByProcedureId")
  public Collection<Step> getStepsById() {
    return stepsById;
  }

  public void setStepsById(Collection<Step> stepsById) {
    this.stepsById = stepsById;
  }
}
