//package ch.diso.llqa.model;
//
//import javax.persistence.Basic;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Table;
//
///**
// * Created by ndinatale on 19.05.17.
// */
//@Entity
//@Table(name = "schema_migrations", schema = "public", catalog = "spring")
//public class SchemaMigration {
//  private String version;
//
//  @Basic
//  @Column(name = "version", nullable = false, length = 255)
//  public String getVersion() {
//    return version;
//  }
//
//  public void setVersion(String version) {
//    this.version = version;
//  }
//
//  @Override
//  public boolean equals(Object o) {
//    if (this == o) return true;
//    if (o == null || getClass() != o.getClass()) return false;
//
//    SchemaMigration that = (SchemaMigration) o;
//
//    if (version != null ? !version.equals(that.version) : that.version != null) return false;
//
//    return true;
//  }
//
//  @Override
//  public int hashCode() {
//    return version != null ? version.hashCode() : 0;
//  }
//}
