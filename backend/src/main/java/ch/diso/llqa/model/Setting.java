package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "settings", schema = "public", catalog = "spring")
public class Setting extends BaseEntity {

  private long id;
  private String key;
  private String value;
  private Long seqnum;
  private String subValues;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_settings", sequenceName="sq_settings", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_settings")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "key", nullable = true, length = 255)
  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  @Basic
  @Column(name = "value", nullable = true, length = 255)
  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @Basic
  @Column(name = "seqnum", nullable = true)
  public Long getSeqnum() {
    return seqnum;
  }

  public void setSeqnum(Long seqnum) {
    this.seqnum = seqnum;
  }

  @Basic
  @Column(name = "sub_values", nullable = true, length = 255)
  public String getSubValues() {
    return subValues;
  }

  public void setSubValues(String subValues) {
    this.subValues = subValues;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Setting setting = (Setting) o;

    if (id != setting.id) return false;
    if (key != null ? !key.equals(setting.key) : setting.key != null) return false;
    if (value != null ? !value.equals(setting.value) : setting.value != null) return false;
    if (seqnum != null ? !seqnum.equals(setting.seqnum) : setting.seqnum != null) return false;
    if (createdAt != null ? !createdAt.equals(setting.createdAt) : setting.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(setting.updatedAt) : setting.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(setting.createdBy) : setting.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(setting.updatedBy) : setting.updatedBy != null) return false;
    if (subValues != null ? !subValues.equals(setting.subValues) : setting.subValues != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (key != null ? key.hashCode() : 0);
    result = 31 * result + (value != null ? value.hashCode() : 0);
    result = 31 * result + (seqnum != null ? seqnum.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    result = 31 * result + (subValues != null ? subValues.hashCode() : 0);
    return result;
  }
}
