package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "snippets", schema = "public", catalog = "spring")
public class Snippet {

  private long id;
  private String category;
  private String text;
  private Long status;
  private Timestamp createdAt;
  private Timestamp updatedAt;
  private String createdBy;
  private String updatedBy;
  private long seqnum;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_snippets", sequenceName="sq_snippets", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_snippets")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "category", nullable = true, length = 255)
  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  @Basic
  @Column(name = "text", nullable = true, length = 1000)
  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  @Basic
  @Column(name = "status", nullable = true)
  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }

  @Basic
  @Column(name = "created_at", nullable = true)
  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  @Basic
  @Column(name = "updated_at", nullable = true)
  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Timestamp updatedAt) {
    this.updatedAt = updatedAt;
  }

  @Basic
  @Column(name = "created_by", nullable = false, length = 255)
  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Basic
  @Column(name = "updated_by", nullable = false, length = 255)
  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  @Basic
  @Column(name = "seqnum", nullable = false)
  public long getSeqnum() {
    return seqnum;
  }

  public void setSeqnum(long seqnum) {
    this.seqnum = seqnum;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Snippet snippet = (Snippet) o;

    if (id != snippet.id) return false;
    if (seqnum != snippet.seqnum) return false;
    if (category != null ? !category.equals(snippet.category) : snippet.category != null) return false;
    if (text != null ? !text.equals(snippet.text) : snippet.text != null) return false;
    if (status != null ? !status.equals(snippet.status) : snippet.status != null) return false;
    if (createdAt != null ? !createdAt.equals(snippet.createdAt) : snippet.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(snippet.updatedAt) : snippet.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(snippet.createdBy) : snippet.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(snippet.updatedBy) : snippet.updatedBy != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (category != null ? category.hashCode() : 0);
    result = 31 * result + (text != null ? text.hashCode() : 0);
    result = 31 * result + (status != null ? status.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    result = 31 * result + (int) (seqnum ^ (seqnum >>> 32));
    return result;
  }
}
