package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "steps", schema = "public", catalog = "spring")
public class Step extends BaseEntity {

  private long id;
  private long procedureId;
  private String code;
  private String title;
  private String description;
  private String metadata;
  private String flowcontrol;
  private long seqnum;
  private long enforce;
  private Long steptype;
  private Collection<Measure> measuresById;
  private Procedure proceduresByProcedureId;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_steps", sequenceName="sq_steps", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_steps")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "procedure_id", nullable = false)
  public long getProcedureId() {
    return procedureId;
  }

  public void setProcedureId(long procedureId) {
    this.procedureId = procedureId;
  }

  @Basic
  @Column(name = "code", nullable = false, length = 255)
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  @Basic
  @Column(name = "title", nullable = false, length = -1)
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Basic
  @Column(name = "description", nullable = true, length = -1)
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Basic
  @Column(name = "metadata", nullable = true, length = -1)
  public String getMetadata() {
    return metadata;
  }

  public void setMetadata(String metadata) {
    this.metadata = metadata;
  }

  @Basic
  @Column(name = "flowcontrol", nullable = true, length = -1)
  public String getFlowcontrol() {
    return flowcontrol;
  }

  public void setFlowcontrol(String flowcontrol) {
    this.flowcontrol = flowcontrol;
  }

  @Basic
  @Column(name = "seqnum", nullable = false)
  public long getSeqnum() {
    return seqnum;
  }

  public void setSeqnum(long seqnum) {
    this.seqnum = seqnum;
  }

  @Basic
  @Column(name = "enforce", nullable = false)
  public long getEnforce() {
    return enforce;
  }

  public void setEnforce(long enforce) {
    this.enforce = enforce;
  }

  @Basic
  @Column(name = "steptype", nullable = true)
  public Long getSteptype() {
    return steptype;
  }

  public void setSteptype(Long steptype) {
    this.steptype = steptype;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Step step = (Step) o;

    if (id != step.id) return false;
    if (procedureId != step.procedureId) return false;
    if (seqnum != step.seqnum) return false;
    if (enforce != step.enforce) return false;
    if (code != null ? !code.equals(step.code) : step.code != null) return false;
    if (title != null ? !title.equals(step.title) : step.title != null) return false;
    if (description != null ? !description.equals(step.description) : step.description != null) return false;
    if (metadata != null ? !metadata.equals(step.metadata) : step.metadata != null) return false;
    if (flowcontrol != null ? !flowcontrol.equals(step.flowcontrol) : step.flowcontrol != null) return false;
    if (createdAt != null ? !createdAt.equals(step.createdAt) : step.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(step.updatedAt) : step.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(step.createdBy) : step.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(step.updatedBy) : step.updatedBy != null) return false;
    if (steptype != null ? !steptype.equals(step.steptype) : step.steptype != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (int) (procedureId ^ (procedureId >>> 32));
    result = 31 * result + (code != null ? code.hashCode() : 0);
    result = 31 * result + (title != null ? title.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (metadata != null ? metadata.hashCode() : 0);
    result = 31 * result + (flowcontrol != null ? flowcontrol.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    result = 31 * result + (int) (seqnum ^ (seqnum >>> 32));
    result = 31 * result + (int) (enforce ^ (enforce >>> 32));
    result = 31 * result + (steptype != null ? steptype.hashCode() : 0);
    return result;
  }

  @OneToMany(mappedBy = "stepsByStepId")
  public Collection<Measure> getMeasuresById() {
    return measuresById;
  }

  public void setMeasuresById(Collection<Measure> measuresById) {
    this.measuresById = measuresById;
  }

  @ManyToOne
  @JoinColumn(name = "procedure_id", referencedColumnName = "id", nullable = false, insertable=false, updatable=false)
  public Procedure getProceduresByProcedureId() {
    return proceduresByProcedureId;
  }

  public void setProceduresByProcedureId(Procedure proceduresByProcedureId) {
    this.proceduresByProcedureId = proceduresByProcedureId;
  }
}
