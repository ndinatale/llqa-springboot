package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "toolunits", schema = "public", catalog = "spring")
public class Toolunit extends BaseEntity {

  private long id;
  private long tooltypeId;
  private String code;
  private String comment;
  private String metadata;
  private Boolean deleted;
  private Collection<Measurement> measurementsById;
  private Tooltype tooltypesByTooltypeId;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_toolunits", sequenceName="sq_toolunits", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_toolunits")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "tooltype_id", nullable = false)
  public long getTooltypeId() {
    return tooltypeId;
  }

  public void setTooltypeId(long tooltypeId) {
    this.tooltypeId = tooltypeId;
  }

  @Basic
  @Column(name = "code", nullable = false, length = 255)
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  @Basic
  @Column(name = "comment", nullable = true, length = -1)
  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  @Basic
  @Column(name = "metadata", nullable = true, length = -1)
  public String getMetadata() {
    return metadata;
  }

  public void setMetadata(String metadata) {
    this.metadata = metadata;
  }

  @Basic
  @Column(name = "deleted", nullable = true)
  public Boolean getDeleted() {
    return deleted;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Toolunit toolunit = (Toolunit) o;

    if (id != toolunit.id) return false;
    if (tooltypeId != toolunit.tooltypeId) return false;
    if (code != null ? !code.equals(toolunit.code) : toolunit.code != null) return false;
    if (comment != null ? !comment.equals(toolunit.comment) : toolunit.comment != null) return false;
    if (metadata != null ? !metadata.equals(toolunit.metadata) : toolunit.metadata != null) return false;
    if (deleted != null ? !deleted.equals(toolunit.deleted) : toolunit.deleted != null) return false;
    if (createdAt != null ? !createdAt.equals(toolunit.createdAt) : toolunit.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(toolunit.updatedAt) : toolunit.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(toolunit.createdBy) : toolunit.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(toolunit.updatedBy) : toolunit.updatedBy != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (int) (tooltypeId ^ (tooltypeId >>> 32));
    result = 31 * result + (code != null ? code.hashCode() : 0);
    result = 31 * result + (comment != null ? comment.hashCode() : 0);
    result = 31 * result + (metadata != null ? metadata.hashCode() : 0);
    result = 31 * result + (deleted != null ? deleted.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    return result;
  }

  @OneToMany(mappedBy = "toolunitsByToolunitId")
  public Collection<Measurement> getMeasurementsById() {
    return measurementsById;
  }

  public void setMeasurementsById(Collection<Measurement> measurementsById) {
    this.measurementsById = measurementsById;
  }

  @ManyToOne
  @JoinColumn(name = "tooltype_id", referencedColumnName = "id", nullable = false, insertable=false, updatable=false)
  public Tooltype getTooltypesByTooltypeId() {
    return tooltypesByTooltypeId;
  }

  public void setTooltypesByTooltypeId(Tooltype tooltypesByTooltypeId) {
    this.tooltypesByTooltypeId = tooltypesByTooltypeId;
  }
}
