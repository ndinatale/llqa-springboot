package ch.diso.llqa.model;

import com.fasterxml.jackson.databind.ser.Serializers;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "units", schema = "public", catalog = "spring")
public class Unit extends BaseEntity {

  private long id;
  private long modelId;
  private String code;
  private String customer;
  private String comment;
  private Date commissioned;
  private Date finished;
  private Date delivered;
  private Date approved;
  private Long status;
  private String metadata;
  private Collection<Check> checksById;
  private Model modelsByModelId;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_units", sequenceName="sq_units", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_units")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "model_id", nullable = false)
  public long getModelId() {
    return modelId;
  }

  public void setModelId(long modelId) {
    this.modelId = modelId;
  }

  @Basic
  @Column(name = "code", nullable = false, length = 255)
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  @Basic
  @Column(name = "customer", nullable = true, length = 255)
  public String getCustomer() {
    return customer;
  }

  public void setCustomer(String customer) {
    this.customer = customer;
  }

  @Basic
  @Column(name = "comment", nullable = true, length = -1)
  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  @Basic
  @Column(name = "commissioned", nullable = true)
  public Date getCommissioned() {
    return commissioned;
  }

  public void setCommissioned(Date commissioned) {
    this.commissioned = commissioned;
  }

  @Basic
  @Column(name = "finished", nullable = true)
  public Date getFinished() {
    return finished;
  }

  public void setFinished(Date finished) {
    this.finished = finished;
  }

  @Basic
  @Column(name = "delivered", nullable = true)
  public Date getDelivered() {
    return delivered;
  }

  public void setDelivered(Date delivered) {
    this.delivered = delivered;
  }

  @Basic
  @Column(name = "approved", nullable = true)
  public Date getApproved() {
    return approved;
  }

  public void setApproved(Date approved) {
    this.approved = approved;
  }

  @Basic
  @Column(name = "status", nullable = true)
  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }

  @Basic
  @Column(name = "metadata", nullable = true, length = -1)
  public String getMetadata() {
    return metadata;
  }

  public void setMetadata(String metadata) {
    this.metadata = metadata;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Unit unit = (Unit) o;

    if (id != unit.id) return false;
    if (modelId != unit.modelId) return false;
    if (code != null ? !code.equals(unit.code) : unit.code != null) return false;
    if (customer != null ? !customer.equals(unit.customer) : unit.customer != null) return false;
    if (comment != null ? !comment.equals(unit.comment) : unit.comment != null) return false;
    if (commissioned != null ? !commissioned.equals(unit.commissioned) : unit.commissioned != null) return false;
    if (finished != null ? !finished.equals(unit.finished) : unit.finished != null) return false;
    if (delivered != null ? !delivered.equals(unit.delivered) : unit.delivered != null) return false;
    if (approved != null ? !approved.equals(unit.approved) : unit.approved != null) return false;
    if (status != null ? !status.equals(unit.status) : unit.status != null) return false;
    if (metadata != null ? !metadata.equals(unit.metadata) : unit.metadata != null) return false;
    if (createdAt != null ? !createdAt.equals(unit.createdAt) : unit.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(unit.updatedAt) : unit.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(unit.createdBy) : unit.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(unit.updatedBy) : unit.updatedBy != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (int) (modelId ^ (modelId >>> 32));
    result = 31 * result + (code != null ? code.hashCode() : 0);
    result = 31 * result + (customer != null ? customer.hashCode() : 0);
    result = 31 * result + (comment != null ? comment.hashCode() : 0);
    result = 31 * result + (commissioned != null ? commissioned.hashCode() : 0);
    result = 31 * result + (finished != null ? finished.hashCode() : 0);
    result = 31 * result + (delivered != null ? delivered.hashCode() : 0);
    result = 31 * result + (approved != null ? approved.hashCode() : 0);
    result = 31 * result + (status != null ? status.hashCode() : 0);
    result = 31 * result + (metadata != null ? metadata.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    return result;
  }

  @OneToMany(mappedBy = "unitsByUnitId")
  public Collection<Check> getChecksById() {
    return checksById;
  }

  public void setChecksById(Collection<Check> checksById) {
    this.checksById = checksById;
  }

  @ManyToOne
  @JoinColumn(name = "model_id", referencedColumnName = "id", nullable = false, insertable=false, updatable=false)
  public Model getModelsByModelId() {
    return modelsByModelId;
  }

  public void setModelsByModelId(Model modelsByModelId) {
    this.modelsByModelId = modelsByModelId;
  }
}
