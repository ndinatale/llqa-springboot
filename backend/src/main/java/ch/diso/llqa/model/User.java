package ch.diso.llqa.model;

import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "users", schema = "public", catalog = "spring")
public class User extends BaseEntity {
  private long id;
  private String username;
  private String passhash;
  private String realname;
  private String comment;
  private String userinfo;
  private Long status;
  private String metadata;
  private String dashboardinfo;
  private List<Affiliation> affiliationsById;
  private List<Datachange> datachangesById;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_users", sequenceName="sq_users", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_users")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "username", nullable = false, length = 255)
  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  @Basic
  @Column(name = "passhash", nullable = false, length = 255)
  public String getPasshash() {
    return passhash;
  }

  public void setPasshash(String passhash) {
    this.passhash = passhash;
  }

  @Basic
  @Column(name = "realname", nullable = false, length = -1)
  public String getRealname() {
    return realname;
  }

  public void setRealname(String realname) {
    this.realname = realname;
  }

  @Basic
  @Column(name = "comment", nullable = true, length = -1)
  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  @Basic
  @Column(name = "userinfo", nullable = true, length = -1)
  public String getUserinfo() {
    return userinfo;
  }

  public void setUserinfo(String userinfo) {
    this.userinfo = userinfo;
  }

  @Basic
  @Column(name = "status", nullable = false)
  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }

  @Basic
  @Column(name = "metadata", nullable = true, length = -1)
  public String getMetadata() {
    return metadata;
  }

  public void setMetadata(String metadata) {
    this.metadata = metadata;
  }

  @Basic
  @Column(name = "dashboardinfo", nullable = true, length = -1)
  public String getDashboardinfo() {
    return dashboardinfo;
  }

  public void setDashboardinfo(String dashboardinfo) {
    this.dashboardinfo = dashboardinfo;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    User user = (User) o;

    if (id != user.id) return false;
    if (status != user.status) return false;
    if (username != null ? !username.equals(user.username) : user.username != null) return false;
    if (passhash != null ? !passhash.equals(user.passhash) : user.passhash != null) return false;
    if (realname != null ? !realname.equals(user.realname) : user.realname != null) return false;
    if (comment != null ? !comment.equals(user.comment) : user.comment != null) return false;
    if (userinfo != null ? !userinfo.equals(user.userinfo) : user.userinfo != null) return false;
    if (createdAt != null ? !createdAt.equals(user.createdAt) : user.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(user.updatedAt) : user.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(user.createdBy) : user.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(user.updatedBy) : user.updatedBy != null) return false;
    if (metadata != null ? !metadata.equals(user.metadata) : user.metadata != null) return false;
    if (dashboardinfo != null ? !dashboardinfo.equals(user.dashboardinfo) : user.dashboardinfo != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (username != null ? username.hashCode() : 0);
    result = 31 * result + (passhash != null ? passhash.hashCode() : 0);
    result = 31 * result + (realname != null ? realname.hashCode() : 0);
    result = 31 * result + (comment != null ? comment.hashCode() : 0);
    result = 31 * result + (userinfo != null ? userinfo.hashCode() : 0);
    result = 31 * result + (int) (status ^ (status >>> 32));
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    result = 31 * result + (metadata != null ? metadata.hashCode() : 0);
    result = 31 * result + (dashboardinfo != null ? dashboardinfo.hashCode() : 0);
    return result;
  }

  @OneToMany(mappedBy = "usersByUserId")
  @OrderBy("id ASC")
  public List<Affiliation> getAffiliationsById() {
    return affiliationsById;
  }

  public void setAffiliationsById(List<Affiliation> affiliationsById) {
    this.affiliationsById = affiliationsById;
  }

  @OneToMany(mappedBy = "usersByUser")
  public List<Datachange> getDatachangesById() {
    return datachangesById;
  }

  public void setDatachangesById(List<Datachange> datachangesById) {
    this.datachangesById = datachangesById;
  }
}
