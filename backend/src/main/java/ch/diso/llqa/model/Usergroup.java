package ch.diso.llqa.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by ndinatale on 19.05.17.
 */
@Entity
@Table(name = "usergroups", schema = "public", catalog = "spring")
public class Usergroup extends BaseEntity {

  private long id;
  private String name;
  private Long level;
  private String description;
  private Boolean deleted;
  private Collection<Affiliation> affiliationsById;

  @Id
  @Column(name = "id", nullable = false)
  @SequenceGenerator(name="sq_usergroups", sequenceName="sq_usergroups", allocationSize=1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sq_usergroups")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Basic
  @Column(name = "name", nullable = false, length = 255)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Basic
  @Column(name = "level", nullable = true)
  public Long getLevel() {
    return level;
  }

  public void setLevel(Long level) {
    this.level = level;
  }

  @Basic
  @Column(name = "description", nullable = true, length = -1)
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Timestamp updatedAt) {
    this.updatedAt = updatedAt;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
  }

  @Basic
  @Column(name = "deleted", nullable = true)
  public Boolean getDeleted() {
    return deleted;
  }

  public void setDeleted(Boolean deleted) {
    this.deleted = deleted;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Usergroup usergroup = (Usergroup) o;

    if (id != usergroup.id) return false;
    if (name != null ? !name.equals(usergroup.name) : usergroup.name != null) return false;
    if (level != null ? !level.equals(usergroup.level) : usergroup.level != null) return false;
    if (description != null ? !description.equals(usergroup.description) : usergroup.description != null) return false;
    if (createdAt != null ? !createdAt.equals(usergroup.createdAt) : usergroup.createdAt != null) return false;
    if (updatedAt != null ? !updatedAt.equals(usergroup.updatedAt) : usergroup.updatedAt != null) return false;
    if (createdBy != null ? !createdBy.equals(usergroup.createdBy) : usergroup.createdBy != null) return false;
    if (updatedBy != null ? !updatedBy.equals(usergroup.updatedBy) : usergroup.updatedBy != null) return false;
    if (deleted != null ? !deleted.equals(usergroup.deleted) : usergroup.deleted != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = (int) (id ^ (id >>> 32));
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (level != null ? level.hashCode() : 0);
    result = 31 * result + (description != null ? description.hashCode() : 0);
    result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
    result = 31 * result + (updatedAt != null ? updatedAt.hashCode() : 0);
    result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
    result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
    result = 31 * result + (deleted != null ? deleted.hashCode() : 0);
    return result;
  }

  @OneToMany(mappedBy = "usergroupsByUsergroupId")
  public Collection<Affiliation> getAffiliationsById() {
    return affiliationsById;
  }

  public void setAffiliationsById(Collection<Affiliation> affiliationsById) {
    this.affiliationsById = affiliationsById;
  }
}
