package ch.diso.llqa.repository;

import ch.diso.llqa.model.Affiliation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AffiliationRepository extends CrudRepository<Affiliation, Long> {

  Affiliation findByUserIdAndUsergroupId(long userId, long usergroupId);

}
