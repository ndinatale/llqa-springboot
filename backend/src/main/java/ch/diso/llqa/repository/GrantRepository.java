package ch.diso.llqa.repository;

import ch.diso.llqa.model.Grant;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GrantRepository extends CrudRepository<Grant, Long>{

    List<Grant> findByGranteeIdAndGranteeType(Long id, String granteeType);

    Grant findByPrivilegeIdAndGranteeIdAndGranteeType(Long privilegeId, Long granteeId, String granteeType);
}
