package ch.diso.llqa.repository;

import ch.diso.llqa.model.Privilege;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PrivilegeRepository extends CrudRepository<Privilege, Long>{

    List<Privilege> findAllByOrderByCode();
}
