package ch.diso.llqa.repository;

import ch.diso.llqa.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User findOne(long id);

    List<User> findAllByStatusOrderByUsernameAsc(Long status);

    List<User> findByOrderByUsernameAsc();


}
