package ch.diso.llqa.repository;

import ch.diso.llqa.model.Usergroup;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsergroupRepository extends CrudRepository<Usergroup, Long> {

    Usergroup findOne(long id);

    List<Usergroup> findAllByOrderByLevelAscNameAsc();
}
