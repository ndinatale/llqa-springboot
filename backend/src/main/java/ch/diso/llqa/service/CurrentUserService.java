package ch.diso.llqa.service;

import ch.diso.llqa.model.Affiliation;
import ch.diso.llqa.model.Grant;
import ch.diso.llqa.model.Privilege;
import ch.diso.llqa.model.User;
import ch.diso.llqa.repository.GrantRepository;
import ch.diso.llqa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * CurrentUserService
 *
 * Information about the logged in user
 */
@Service
public class CurrentUserService {

  private final GrantRepository grantRepository;
  private final UserRepository userRepository;

  @Autowired
  public CurrentUserService(GrantRepository grantRepository, UserRepository userRepository) {
    this.grantRepository = grantRepository;
    this.userRepository = userRepository;
  }

  /**
   * hasPrivilege()
   *
   * @param privileges Array of privilege codes
   * @return true if privilege was found, false if not
   */
//  boolean hasPrivilege(ArrayList<String> privileges) {
  public boolean hasPrivilege(String... privileges) {

    // get the current user object
    User user = userRepository.findOne(getCurrentUserId());

    // no current user was found
    if (user == null) {
      return false;
    }

    // create hash with code of privilege as key, and privilege object as value
    HashMap<String, Privilege> privilegeHashMap = getAllPrivileges(user);

    // return true if passed privilege exists in privilege hash from above
    for (String privilege : privileges) {
      if (privilegeHashMap.get(privilege) != null) {
        return true;
      }

      // Also return true if the privilege for all subprivileges exists. For instance:
      // If the privileg WFLREG is needed and the user has the privileg WFLALL, then he is
      // authorized, because all privileges of the category WFL is in WFLALL
      if (privilegeHashMap.get(privilege.substring(0, 3) + "ALL") != null) {
        return true;
      }
    }

    // User is not authorized
    return false;
  }

  /**
   * Gets all privileges from the user and also all privileges from the usergroups which the user
   * is in.
   *
   * @param user record object
   * @return HashMap of privileges combined from user-privileges and usergroup-privileges
   */
  public HashMap<String, Privilege> getAllPrivileges(User user) {

    HashMap<String, Privilege> privilegeHashMap = new HashMap<>();

    // add code of privilege to the hash map as key and the privilege object as value (privileges are from
    // the current user)
    for (Grant grant : grantRepository.findByGranteeIdAndGranteeType(user.getId(), "User")) {
      Privilege privilege = grant.getPrivilegesByPrivilegeId();
      privilegeHashMap.put(privilege.getCode(), privilege);

    }

    // add code of privilege to the hash map as key and the privilege object as value (privileges are from the
    // usergroup of the current user)
    for (Affiliation affiliation : user.getAffiliationsById()) {
      for (Grant grant : grantRepository.findByGranteeIdAndGranteeType(affiliation.getUsergroupId(), "Usergroup")) {
        Privilege privilege = grant.getPrivilegesByPrivilegeId();
        privilegeHashMap.put(privilege.getCode(), privilege);
      }
    }

    return privilegeHashMap;
  }

  /**
   * getCurrentUserId
   * <p>
   * TODO | can't get the current user yet. returning constant id (9, which is root) below so far
   * TODO | get_current_user() and get_current_user_id() are same except default return value!
   * TODO | see: server/lib/utils.rb:get_current_user (line 42)
   * TODO | so leaving out get_current_user() so far
   *
   * @return current user id
   */
  public long getCurrentUserId() {

    return 9L;

// TODO    return "??"; for get_current_user()
// TODO    return -1L; for get_current_user_id()

  }

  /**
   * Get the highest rank of the current user
   *
   * @return highest rank of the usergroups which the current user is in
   */
  public Long getRank() {

    // default rank
    Long rank = 1L;

    // get the current user object
    User user = userRepository.findOne(getCurrentUserId());
    if (user == null) {
      return -1L;
    }

    // get usergroups of the current user by affiliations and find the highest
    // rank of the usergroups which the current user is in
    for (Affiliation affiliation : user.getAffiliationsById()) {
      if (affiliation.getUsergroupsByUsergroupId().getLevel() > rank) {
        rank = affiliation.getUsergroupsByUsergroupId().getLevel();
      }
    }

    return rank;
  }
}
