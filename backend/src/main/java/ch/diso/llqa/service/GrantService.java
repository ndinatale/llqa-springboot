package ch.diso.llqa.service;

import ch.diso.llqa.exception.PrivilegeNotFoundException;
import ch.diso.llqa.exception.UserNotFoundException;
import ch.diso.llqa.model.Grant;
import ch.diso.llqa.model.Privilege;
import ch.diso.llqa.model.User;
import ch.diso.llqa.repository.GrantRepository;
import ch.diso.llqa.repository.PrivilegeRepository;
import ch.diso.llqa.repository.UsergroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * GrantService
 */
@Service
public class GrantService {

  private final GrantRepository grantRepository;
  private final PrivilegeRepository privilegeRepository;

  @Autowired
  public GrantService(GrantRepository grantRepository, PrivilegeRepository privilegeRepository) {
    this.grantRepository = grantRepository;
    this.privilegeRepository = privilegeRepository;
  }

  /**
   *
   * @param granteeType
   * @param privilegeIds
   * @param id
   * @return
   */
  public List<Grant> create(String granteeType, ArrayList<Long> privilegeIds, long id) {

    // get the current grants from the object
    List<Grant> grants = grantRepository.findByGranteeIdAndGranteeType(id, granteeType);

    // add the grants
    for (Privilege privilege : getPrivilegesToAddOrRemove(grants, privilegeIds, "create")) {

      Grant grant = new Grant();
      grant.setPrivilegeId(privilege.getId());
      grant.setGranteeId(id);
      grant.setGranteeType(granteeType);
      TransactionService.setCreatedValues(grant, id);

      grants.add(grant);
    }

    grantRepository.save(grants);

    return grants;
  }

  /**
   *
   * @param id
   * @param privilegeId
   */
  public void deleteGrant(String granteeType, long id, long privilegeId) {

    List<Grant> grants = grantRepository.findByGranteeIdAndGranteeType(id, granteeType);

    ArrayList<Long> privilegeIds = new ArrayList<>();
    privilegeIds.add(privilegeId);

    // delete the grants
    for (Privilege privilege : getPrivilegesToAddOrRemove(grants, privilegeIds, "delete")) {
      grantRepository.delete(grantRepository.findByPrivilegeIdAndGranteeIdAndGranteeType(privilege.getId(), id, granteeType));
    }
  }

  /**
   *
   * @param grants
   * @return
   */
  private List<Privilege> getPrivilegesToAddOrRemove(List<Grant> grants, ArrayList<Long> privilegeIds, String action) {

    // Create empty List of Privileges
    List<Privilege> privilegesToAddOrRemove = new ArrayList<>();

    // Find all passed privileges
    for (Long privilegeId : privilegeIds) {

      Privilege privilege = privilegeRepository.findOne(privilegeId);

      if (privilege == null) {
        throw new PrivilegeNotFoundException(privilegeId);
      }

      boolean hasGrant = false;
      for (Grant userGrant : grants) {
        if (Objects.equals(userGrant.getPrivilegeId(), privilege.getId())) {
          hasGrant = true;
          break;
        }
      }

      if (hasGrant && action.equals("create") || (!hasGrant && action.equals("delete"))) {
        continue;
      }

      privilegesToAddOrRemove.add(privilege);
    }

    return privilegesToAddOrRemove;
  }


}
