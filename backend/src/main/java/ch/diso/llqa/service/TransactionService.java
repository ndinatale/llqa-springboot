package ch.diso.llqa.service;

import ch.diso.llqa.model.BaseEntity;
import org.yaml.snakeyaml.Yaml;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * TransactionService
 */
public class TransactionService {

  /**
   * Deserialize YAML to Map
   *
   * @param yamlString string in YAML format
   * @return Map of the YAML data or empty Map if yamlString was null
   */
  public static Map deserializeYamlToMap(String yamlString) {

    if (yamlString != null) {
      Yaml yaml = new Yaml();
      return (Map) yaml.load(yamlString);
    }

    return Collections.emptyMap();
  }

  /**
   * Deserialize YAML to ArrayList
   *
   * @param yamlString string in YAML format
   * @return ArrayList of the YAML data or empty ArrayList if yamlString was null
   */
  public static ArrayList deserializeYamlToArrayList(String yamlString) {

    if (yamlString != null) {
      Yaml yaml = new Yaml();
      return (ArrayList) yaml.load(yamlString);
    }

    return new ArrayList();
  }

  /**
   * Serialize Object(s) to YAML
   *
   * @param map which should be converted to YAML
   * @return string in YAML format
   */
  public static String serializeMapToYaml(Map map) {

    if (map != null) {
      Yaml yaml = new Yaml();
      return yaml.dump(map);
    }

    return null;
  }

  /**
   * Serialize Object(s) to YAML
   *
   * @param arrayList which should be converted to YAML
   * @return string in YAML format
   */
  public static String serializeArrayListToYaml(ArrayList arrayList) {

    if (arrayList != null) {
      Yaml yaml = new Yaml();
      return yaml.dump(arrayList);
    }

    return null;
  }

  /**
   * Formats given date to string
   *
   * @param date to be converted to string
   * @return string of format '2017-01-10 08:01:50 UTF'
   */
  public static String dateToString(Timestamp date) {

    if (date != null) {
      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
      return dateFormat.format(date);
    }

    return null;
  }

  /**
   * Formats given string to Date (Timestamp)
   *
   * @param dateString to be converted to date
   * @return date in milliseconds
   */
  public static Timestamp stringToDate(String dateString) {

    if (dateString != null) {
      try {
      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
      Date parsedDate;
        parsedDate = dateFormat.parse(dateString);
      return new Timestamp(parsedDate.getTime());
      } catch (ParseException e) {
        e.printStackTrace();
      }
    }

    return null;
  }

  /**
   * Converts the given String id to a Long. Since there are values of "??" saved
   * in the DB, we need to catch the exception if thrown.
   *
   * @param id String ID
   * @return parsed Long
   */
  public static Long stringIdToLong(String id) {

    try {
      return Long.valueOf(id);
    } catch (NumberFormatException e) {
      // e.printStackTrace();
      return -1L;
    }
  }

  /**
   * Sets the suiting values for an updated object
   *
   * @param object        updated object object
   * @param id id of the object who updated the record
   */
  public static void setUpdatedValues(BaseEntity object, long id) {
    object.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
    object.setUpdatedBy(Long.toString(id));
  }

  /**
   * Sets the suiting values for a created object
   *
   * @param object        created object object
   * @param id id of the object who created the record
   */
  public static void setCreatedValues(BaseEntity object, long id) {

    // set created_at and created_by
    object.setCreatedAt(new Timestamp(System.currentTimeMillis()));
    object.setCreatedBy(Long.toString(id));

    // updated_at and updated_by will be set the same
    setUpdatedValues(object, id);
  }

}
