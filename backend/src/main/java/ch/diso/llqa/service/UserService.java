package ch.diso.llqa.service;

import ch.diso.llqa.dto.*;
import ch.diso.llqa.exception.AccessNotAllowedException;
import ch.diso.llqa.exception.UserNotFoundException;
import ch.diso.llqa.model.*;
import ch.diso.llqa.repository.GrantRepository;
import ch.diso.llqa.repository.PrivilegeRepository;
import ch.diso.llqa.repository.UserRepository;
import ch.diso.llqa.repository.UsergroupRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * UserService
 */
@Service
public class UserService {

  private final String granteeType = "User";

  private final UserRepository userRepository;
  private final UsergroupRepository usergroupRepository;
  private final GrantRepository grantRepository;
  private final PrivilegeRepository privilegeRepository;
  private final CurrentUserService currentUserService;
  private final GrantService grantService;

  @Autowired
  public UserService(UserRepository userRepository, UsergroupRepository usergroupRepository, GrantRepository grantRepository, PrivilegeRepository privilegeRepository, CurrentUserService currentUserService, GrantService grantService) {
    this.userRepository = userRepository;
    this.usergroupRepository = usergroupRepository;
    this.grantRepository = grantRepository;
    this.privilegeRepository = privilegeRepository;
    this.currentUserService = currentUserService;
    this.grantService = grantService;
  }

  /**
   * Gets the Users Shortlist
   *
   * @return UsersShortlistDto with Users and Usergroups
   */
  public UsersShortlistDto getUsersShortlist() {

    // Find all usergroups ordered by Level, Name
    List<UsersShortlistDtoUsergroupsDto> usergroups = usergroupRepository.findAllByOrderByLevelAscNameAsc()
        .stream()
        .map(usergroup -> new UsersShortlistDtoUsergroupsDto(usergroup))
        .collect(Collectors.toList());

    // Find all users with Status = 1 (active) ordered by Username
    List<UsersShortlistDtoUsersDto> users = userRepository.findAllByStatusOrderByUsernameAsc(1L)
        .stream()
        .map(user -> {

          // Get the affiliations for the user
          List<Affiliation> userAffiliations = user.getAffiliationsById();

          // iterate through the affiliations and save the IDs in an ArrayList (which are
          // the groupIds of the user)
          ArrayList<Long> groupIds = new ArrayList<>();
          for (Affiliation userAffiliation : userAffiliations) {
            groupIds.add(userAffiliation.getId());
          }

          // Create the usersDto for the ShortlistDto and set the property groupIds as
          // the created ArrayList with the groupIds above
          UsersShortlistDtoUsersDto usersShortlistDtoUsersDto = new UsersShortlistDtoUsersDto(user);
          usersShortlistDtoUsersDto.setGroupIds(groupIds);
          return usersShortlistDtoUsersDto;

        })
        .collect(Collectors.toList());

    return new UsersShortlistDto(usergroups, users);
  }

  /**
   * Gets the full list of Users
   *
   * @return UsersFullDto with Users, Usergroups and Privileges
   */
  public UsersFullDto getUsersFull() {

    // Check if user has permissions to get this Route
    if (!currentUserService.hasPrivilege("MNGUSR")) {
      throw new AccessNotAllowedException(currentUserService.getCurrentUserId());
    }

    // Find all usergroups ordered by Level, Name
    List<UsersFullDtoUsergroupsDto> usergroups = usergroupRepository.findAllByOrderByLevelAscNameAsc()
        .stream()
        .map(usergroup -> {

          // Add list of Grants to the created DTO
          UsersFullDtoUsergroupsDto usersFullDtoUsergroupsDto = new UsersFullDtoUsergroupsDto(usergroup);
          usersFullDtoUsergroupsDto.setGrants(grantRepository.findByGranteeIdAndGranteeType(usergroup.getId(), "Usergroup")
              .stream()
              .map(groupgrant -> new GrantDto(groupgrant))
              .collect(Collectors.toList()));

          return usersFullDtoUsergroupsDto;

        }).collect(Collectors.toList());

    // Find all users ordered by Username
    List<UsersFullDtoUsersDto> users = userRepository.findByOrderByUsernameAsc()
        .stream()
        .map(user -> {

          // Add list of Grants and Affiliations to the created DTO
          UsersFullDtoUsersDto usersFullDtoUsersDto = new UsersFullDtoUsersDto(user);
          usersFullDtoUsersDto.setGrants(grantRepository.findByGranteeIdAndGranteeType(user.getId(), granteeType)
              .stream()
              .map(userGrant -> new GrantDto(userGrant))
              .collect(Collectors.toList()));

          // Set the rank
          usersFullDtoUsersDto.setRank(getRank(user));

          // Set affiliations
          usersFullDtoUsersDto.setAffiliations(
              user.getAffiliationsById()
              .stream()
              .map(affiliation -> new AffiliationDto(affiliation))
              .collect(Collectors.toList())
          );

          return usersFullDtoUsersDto;

        }).collect(Collectors.toList());

    // Find all privileges ordered by Code
    List<UsersFullDtoPrivilegesDto> privileges = privilegeRepository.findAllByOrderByCode()
        .stream()
        .map(privilege -> new UsersFullDtoPrivilegesDto(privilege))
        .collect(Collectors.toList());

    return new UsersFullDto(usergroups, users, privileges);
  }

  /**
   * Updates the user record with the given id if the user
   * is allowed to do so
   *
   * @param userDto includes the data to create
   */
  @Transactional
  public void update(UserDto userDto) {

    // check if user is allowed to update
    if (!accessAllowed(userDto)) {
      throw new AccessNotAllowedException(currentUserService.getCurrentUserId());
    }

    /*
     * find the user and set the changed data
     */
    User user = userRepository.findOne(userDto.getId());

    // set data if user was found
    if (user != null) {
      if (userDto.getUsername() != null) {
        user.setUsername(userDto.getUsername());
      }
      if (userDto.getRealname() != null) {
        user.setRealname(userDto.getRealname());
      }
      if (userDto.getPasshash() != null) {
        user.setPasshash(userDto.getPasshash());
      }
      if (userDto.getComment() != null) {
        user.setComment(userDto.getComment());
      }
      if (userDto.getStatus() != null) {
        user.setStatus(userDto.getStatus());
      }

      // set updated_at and updated_by
      TransactionService.setUpdatedValues(user, currentUserService.getCurrentUserId());

      // save the record to the db
      userRepository.save(user);
    } else {
      throw new UserNotFoundException(userDto.getId());
    }

    // null if user was not found
//    return user;
  }

  /**
   * Creates a userDto and persists it in the DB
   *
   * @param userDto RequestBody
   * @return saved UserDto
   */
  @Transactional
  public UserDto create(UserDto userDto) {

    // check if userDto is allowed to create
    if (!accessAllowed(userDto)) {
      throw new AccessNotAllowedException(currentUserService.getCurrentUserId());
    }

    Long currentUserId = currentUserService.getCurrentUserId();

    // Copy data to the entity object and work with that from now on
    User user = new User();
    BeanUtils.copyProperties(userDto, user);

    // set created_at and updated_at and save record
    TransactionService.setCreatedValues(user, currentUserId);

    // create metadata with key _commentby as Map and serialize to YAML
    Map<String, Long> metadataMap = new HashMap<>();
    metadataMap.put("_commentby", currentUserId);
    user.setMetadata(TransactionService.serializeMapToYaml(metadataMap));

    // TODO: column is NOT NULL and has a DEFAULT VALUE. Still, if I pass NULL, it throws a not-null constraint instead
    // TODO: of putting the default value. Wrokaround, set the value here and "simulate" the DEFAULT VALUE...
    userDto.setStatus(1L);

    // save userDto and create the UserDto to ship it to the frontend
    return new UserDto(userRepository.save(user));
  }

  /**
   * Checks if the given user is allowed to access
   *
   * @param userDto user to check access
   * @return true if user is allowed to acces, false otherwise
   */
  private boolean accessAllowed(UserDto userDto) {

    // The user may change (only) his own password, so check if the user record belongs to the
    // logged in user and if the user changes his own password
    if (userDto.getId() == currentUserService.getCurrentUserId()) {
      if (userDto.getPasshash() != null) {
        return true;
      }
    }

    // if access isn't allowed already, check if the user has the privileges to add or change
    // the user record
    return currentUserService.hasPrivilege("USRMGA", "USRMGO");
  }

  /**
   * Creates grant
   *
   * @param privilegeIds affected privilege IDs
   * @param userId affecter user ID
   * @return created Grants
   */
  public List<Grant> createGrant(ArrayList<Long> privilegeIds, long userId) {

    // find the user
    User user = userRepository.findOne(userId);

    if (user == null) {
      throw new UserNotFoundException(userId);
    }

    return grantService.create(granteeType, privilegeIds, userId);
  }

  /**
   * Deletes grant
   *
   * @param privilegeId affected privilege ID
   * @param userId affected user ID
   */
  public void deleteGrant(long userId, long privilegeId) {

    User user = userRepository.findOne(userId);

    if (user == null) {
      throw new UserNotFoundException(userId);
    }

    grantService.deleteGrant(granteeType, userId, privilegeId);
  }

  /**
   * Gets the highest rank the user has
   *
   * @param user affected user
   * @return highest rank the user has
   */
  private Long getRank(User user) {

    // default rank
    Long rank = 1L;

    // get usergroups of the current user by affiliations and find the highest
    // rank of the usergroups which the current user is in
    for (Affiliation affiliation : user.getAffiliationsById()) {
      if (affiliation.getUsergroupsByUsergroupId().getLevel() > rank) {
        rank = affiliation.getUsergroupsByUsergroupId().getLevel();
      }
    }

    return rank;
  }
}
