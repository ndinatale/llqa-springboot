package ch.diso.llqa.service;

import ch.diso.llqa.dto.UsergroupDto;
import ch.diso.llqa.exception.*;
import ch.diso.llqa.model.*;
import ch.diso.llqa.repository.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * UsergroupService
 */
@Service
public class UsergroupService {

  private final CurrentUserService currentUserService;
  private final UsergroupRepository usergroupRepository;
  private final AffiliationRepository affiliationRepository;
  private final GrantService grantService;

  private final String granteeType = "Usergroup";

  @Autowired
  public UsergroupService(CurrentUserService currentUserService, UsergroupRepository usergroupRepository, AffiliationRepository affiliationRepository, GrantService grantService) {
    this.currentUserService = currentUserService;
    this.usergroupRepository = usergroupRepository;
    this.affiliationRepository = affiliationRepository;
    this.grantService = grantService;
  }

  /**
   *
   * Creates the usergroupDto record filled with the given data
   *
   * @param usergroupDto which contains the data to the usergroupDto that should be created
   * @return the UsergroupDto of the created Usergroup
   */
  public UsergroupDto create(UsergroupDto usergroupDto) {

    // check if user is allowed to create
    if (!hasPrivilege()) {
      throw new AccessNotAllowedException(currentUserService.getCurrentUserId());
    }

    // Copy data to the entity object and work with that from now on
    Usergroup usergroup = new Usergroup();
    BeanUtils.copyProperties(usergroupDto, usergroup);

    // set created information
    TransactionService.setCreatedValues(usergroup, currentUserService.getCurrentUserId());

    // TODO: column is NOT NULL and has a DEFAULT VALUE. Still, if I pass NULL, it throws a not-null constraint instead
    // TODO: of putting the default value. Wrokaround, set the value here and "simulate" the DEFAULT VALUE...
    usergroup.setDeleted(false);

    return new UsergroupDto(usergroupRepository.save(usergroup));
  }

  /**
   * Updates the usergroup record with the given id
   *
   * @param usergroupDto includes the data to update
   * @return the updated UsergroupDto
   */
  @Transactional
  public UsergroupDto update(UsergroupDto usergroupDto) {

    // check if user is allowed to do changes
    if (!hasPrivilege()) {
      throw new AccessNotAllowedException(currentUserService.getCurrentUserId());
    }

    /*
     * find the usergroup and set the changed data
     */
    Usergroup usergroup = usergroupRepository.findOne(usergroupDto.getId());

    if (usergroup == null) {
      throw new UsergroupNotFoundException(usergroupDto.getId());
    }

    // set data if user was found, otherwise
    if (usergroupDto.getName() != null) {
      usergroup.setName(usergroupDto.getName());
    }
    if (usergroupDto.getLevel() != null) {
      usergroup.setLevel(usergroupDto.getLevel());
    }
    if (usergroupDto.getDescription() != null) {
      usergroup.setDescription(usergroupDto.getDescription());
    }
    if (usergroupDto.getDeleted() != null) {

      usergroup.setDeleted(usergroupDto.getDeleted());

      if (usergroupDto.getDeleted()) {

        // get the affiliations for this usergroup and delete them
        Collection<Affiliation> affiliations = usergroup.getAffiliationsById();
        affiliationRepository.delete(affiliations);
      }

    }

    // set updated_at and updated_by
    TransactionService.setUpdatedValues(usergroup, currentUserService.getCurrentUserId());

    // save the record to the db
    return new UsergroupDto(usergroupRepository.save(usergroup));

  }

  /**
   * Deletes the affiliation with the passed userId and usergroupId
   *
   * @param userId      of the affected user
   * @param usergroupId of the affected usergroup
   */
  public void deleteAffiliation(long userId, long usergroupId) {

    // check if user is allowed to do changes
    if (!hasPrivilege()) {
      throw new AccessNotAllowedException(currentUserService.getCurrentUserId());
    }

    Affiliation affiliation = affiliationRepository.findByUserIdAndUsergroupId(userId, usergroupId);

    if (affiliation == null) {
      throw new AffiliationNotFoundException(userId, usergroupId);
    }

    if (currentUserService.getRank() < affiliation.getUsergroupsByUsergroupId().getLevel()) {
      if (!currentUserService.hasPrivilege("USRMGA")) {
        throw new AccessNotAllowedException(currentUserService.getCurrentUserId());
      }
    }

    affiliationRepository.delete(affiliation);
  }

  /**
   * Creates Grants
   *
   * @param privilegeIds affected privilege ID
   * @param usergroupId affected usergroup ID
   * @return List of created Grants
   */
  public List<Grant> createGrant(ArrayList<Long> privilegeIds, long usergroupId) {

    // find the usergroup
    Usergroup usergroup = usergroupRepository.findOne(usergroupId);

    if (usergroup == null) {
      throw new UsergroupNotFoundException(usergroupId);
    }

    return grantService.create(granteeType, privilegeIds, usergroupId);
  }

  /**
   * Deletes Grants
   *
   * @param privilegeId affected privilege ID
   * @param usergroupId affected usergroup ID
   */
  public void deleteGrant(long usergroupId, long privilegeId) {

    Usergroup usergroup = usergroupRepository.findOne(usergroupId);

    if (usergroup == null) {
      throw new UsergroupNotFoundException(usergroupId);
    }

    grantService.deleteGrant(granteeType, usergroupId, privilegeId);
  }

  /**
   * Checks if user has privileges, if so, pass data to the AffiliationService to create
   * them
   */
  public void createAffiliation(ArrayList<HashMap<String, Long>> affiliations) {

    // check if user is allowed to do changes
    if (!hasPrivilege()) {
      throw new AccessNotAllowedException(currentUserService.getCurrentUserId());
    }

    // Get the passed user_id and usergroup_id and create the affiliation
    for (HashMap<String, Long> affiliationIds : affiliations) {

      Affiliation affiliation = new Affiliation();
      affiliation.setUsergroupId(affiliationIds.get("gid"));
      affiliation.setUserId(affiliationIds.get("uid"));

      TransactionService.setCreatedValues(affiliation, currentUserService.getCurrentUserId());
      affiliationRepository.save(affiliation);
    }
  }

  /**
   * Check if the current user has privileges to do actions affecting usergroups
   *
   * @return true if current user is allowed to do the requested action, false if otherwise
   */
  private boolean hasPrivilege() {
    return currentUserService.hasPrivilege("USRMGA", "USRMGO");
  }
}
