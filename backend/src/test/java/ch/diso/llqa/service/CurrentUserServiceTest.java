package ch.diso.llqa.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import ch.diso.llqa.model.*;
import ch.diso.llqa.repository.GrantRepository;
import ch.diso.llqa.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;


/**
 * Created by ndinatale on 19.05.17.
 */
@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class CurrentUserServiceTest {

  @InjectMocks
  CurrentUserService currentUserService;

  @Mock
  UserRepository userRepository;
  GrantRepository grantRepository;

  @Test
  public void testHasPrivilege() {

//    Grant grant1 = new Grant();
//    grant1.setGranteeType("User");
//    grant1.setGranteeId(9L);
//    grant1.setPrivilegeId(1L);
//
//    Grant grant2 = new Grant();
//    grant2.setGranteeType("User");
//    grant2.setGranteeId(9L);
//    grant2.setPrivilegeId(2L);
//
//    Grant grant3 = new Grant();
//    grant3.setGranteeType("User");
//    grant3.setGranteeId(9L);
//    grant3.setPrivilegeId(3L);
//
//    Grant grant4 = new Grant();
//    grant4.setGranteeType("User");
//    grant4.setGranteeId(8L);
//    grant4.setPrivilegeId(4L);
//
//    Grant grant5 = new Grant();
//    grant5.setGranteeType("User");
//    grant5.setGranteeId(10L);
//    grant5.setPrivilegeId(5L);
//
//    Grant grant6 = new Grant();
//    grant6.setGranteeType("Usergroup");
//    grant6.setGranteeId(9L);
//    grant6.setPrivilegeId(6L);
//
//    Grant grant7 = new Grant();
//    grant6.setGranteeType("Usergroup");
//    grant6.setGranteeId(9L);
//    grant6.setPrivilegeId(7L);
//
//    List<Grant> grants1 = new ArrayList<>();
//    grants1.add(grant1);
//    grants1.add(grant2);
//    grants1.add(grant3);
//
//    List<Grant> grants2 = new ArrayList<>();
//    grants2.add(grant6);
//    grants2.add(grant7);
//
//    List<Grant> grants3 = new ArrayList<>();
//    grants3.add(grant5);
//    grants3.add(grant6);
//
//    Privilege privilege1 = new Privilege();
//    privilege1.setCode("PRIV1");
//    privilege1.setGrantsById(grants1);
//
//    Privilege privilege2 = new Privilege();
//    privilege1.setCode("PRIV2");
//    privilege1.setGrantsById(grants2);
//
//    Privilege privilege3 = new Privilege();
//    privilege1.setCode("PRIV3");
//    privilege1.setGrantsById(grants3);
//
//    User user = new User();
//    user.setId(9L);
//    user.setPasshash("123456789");
//    user.setRealname("Kevin Müller");
//    user.setUsername("kmueller");
//
//    Affiliation affiliation = new Affiliation();
//    affiliation.setId(1L);
//    affiliation.setUsergroupId(99L);
//    affiliation.setUserId(9L);
//
//    Affiliation affiliation2 = new Affiliation();
//    affiliation2.setId(2L);
//    affiliation2.setUsergroupId(99L);
//    affiliation2.setUserId(9L);
//
//    List<Affiliation> affiliations = new ArrayList<>();
//    affiliations.add(affiliation);
//    affiliations.add(affiliation2);
//
//    Usergroup usergroup = new Usergroup();
//    usergroup.setId(99L);
//    usergroup.setName("Admins");
//    usergroup.setAffiliationsById(affiliations);
//
//    HashMap<String, Privilege> privilegeHashMap = new HashMap<>();
//    privilegeHashMap.put(privilege1.getCode(), privilege1);
//    privilegeHashMap.put(privilege2.getCode(), privilege2);
//    privilegeHashMap.put(privilege3.getCode(), privilege3);
//
//    when(userRepository.findOne(9L)).thenReturn(user);
//    when(grantRepository.findByGranteeIdAndGranteeType(user.getId(), "User")).thenReturn(grants1);
//    when(user.getAffiliationsById()).thenReturn(affiliations);
//    when(grantRepository.findByGranteeIdAndGranteeType(affiliation.getUsergroupId(), "Usergroup")).thenReturn(grants2);
//    when(currentUserService.getAllPrivileges(user)).thenReturn(privilegeHashMap);
//
//    boolean result = currentUserService.hasPrivilege();
//
//    assertEquals(result, true);
//    verify(userRepository, times(1)).findOne(9L);
//    verify(currentUserService, times(1)).getAllPrivileges(user);
  }

  @Test
  public void getRank() {

    /*
     * Usergroups
     */
    Usergroup usergroup1 = new Usergroup();
    usergroup1.setId(99L);
    usergroup1.setName("Admins");
    usergroup1.setLevel(5L);

    Usergroup usergroup2 = new Usergroup();
    usergroup2.setId(88L);
    usergroup2.setName("Arbeiter");
    usergroup2.setLevel(2L);

    /*
     * Affiliations
     */
    Affiliation affiliation1 = new Affiliation();
    affiliation1.setId(1L);
    affiliation1.setUserId(9L);
    affiliation1.setUsergroupId(99L);
    affiliation1.setUsergroupsByUsergroupId(usergroup1);

    Affiliation affiliation2 = new Affiliation();
    affiliation2.setId(1L);
    affiliation2.setUserId(9L);
    affiliation2.setUsergroupId(88L);
    affiliation2.setUsergroupsByUsergroupId(usergroup2);

    Affiliation affiliation3 = new Affiliation();
    affiliation3.setId(2L);
    affiliation3.setUserId(1L);
    affiliation3.setUsergroupId(88L);
    affiliation3.setUsergroupsByUsergroupId(usergroup2);

    List<Affiliation> affiliations1 = new ArrayList<>();
    affiliations1.add(affiliation1);
    affiliations1.add(affiliation2);

    List<Affiliation> affiliations2 = new ArrayList<>();
    affiliations2.add(affiliation3);

    /*
     * Users
     */
    User user1 = new User();
    user1.setId(9L);
    user1.setPasshash("123456789");
    user1.setRealname("Kevin Müller");
    user1.setUsername("kmueller");
    user1.setAffiliationsById(affiliations1);

    User user2 = new User();
    user2.setId(1L);
    user2.setPasshash("987654321");
    user2.setRealname("Krasser Typ");
    user2.setUsername("ktyp");

//    when(currentUserService.getCurrentUserId()).thenReturn(9L);
    when(userRepository.findOne(9L)).thenReturn(user1);

    long result = currentUserService.getRank();

    assertEquals(result, 5L);
  }
}
