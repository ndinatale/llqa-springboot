# Developer guide

Dieser Developer Guide soll einen grundlegenden Einstieg in die Applikation aus Entwicklersicht bieten und dabei unter anderem die Themengebiete Weiterentwicklung, Inbetriebnahme etwas erläutern.

## Architektur

### Aufbau

Grundsätzlich ist die Applikation aufgeteilt in folgende Teile:

* **public**: Frontend in AngularJS
* **server**: Backend in Ruby on Rails, inkl. REST-API, Routes und DB
* **util**: Konfigurationsdateien und utilities
* **tests**: Tests

### Datenbank

Schema der Datenbank:

![Database Schema](schema.png)

### Fachliches

Dieser Abschnitt soll nicht das Benutzerhandbuch ersetzen, dort finden sich weit mehr Infos.
Es soll lediglich als kurze Übersicht (und begriffliche Klärung) dienen, welche Objekttypen wie verwendet werden:

* Modell - Einheit - Prüfung (device - unit - check): Ein Modell bildet die Grundlage oder Blaupause, wovon dann Einheiten "instanziiert" werden. Diese können dann bestimmte Prüfungen zugewiesen haben.
* Prozedur - Schritte - Messungen (process - step - measure): Eine Prozedur bildet den
* Werkzeugtypen - Werkzeugeinheiten (tooltype - toolunit): Hilfswerkzeuge -typen, zur Durchführung der Messungen.
* Benutzer, Gruppe, Privileg (user, group, privilege)
* Fehlerberichte, Textvorschläge (notices, snippets)
* Konfigurationstabelle, -Eintrag: Erster Schritt einer angedachten SAP-Schnittstelle


## Start

### Applikation

Start der Applikation:

    ruby server.rb

### E2E-Tests

Aus dem Verzeichnis tests/e2e den Protractor aufrufen:

    protractor conf.js


## Server-Installation

Inbetriebnahme LLQA (aus LLQA-90):

### Als user root

    curl -sL https://deb.nodesource.com/setup_5.x | sudo -E bash -
    apt-get install nodejs
    apt-get update
    apt-get install git
    apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev imagemagick
    adduser lladm
    adduser lladm sudo

Passwort für lladm festlegen und notieren.

### Als user lladm:

Ruby installieren:
    
    su lladm # User wechseln
    cd
    git clone https://github.com/rbenv/rbenv.git ~/.rbenv
    echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
    echo 'eval "$(rbenv init -)"' >> ~/.bashrc
    exec $SHELL

    git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
    echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
    exec $SHELL

    git clone https://github.com/rbenv/rbenv-gem-rehash.git ~/.rbenv/plugins/rbenv-gem-rehash

    rbenv install 2.3.0 # dauert etwas...
    rbenv global 2.3.0
    ruby -v # sollte sowas wie ruby 2.3.0p0 (2015-12-25 revision 53290) [x86_64-linux] ausgeben

Git konfigurieren:

    git config --global color.ui true
    git config --global user.name "YOUR NAME"
    git config --global user.email "YOUR@EMAIL.com"
    ssh-keygen -t rsa -b 4096 -C "YOUR@EMAIL.com"

    cat ~/.ssh/id_rsa.pub # > add to bitbucket

Applikation:

    cd
    git clone git@bitbucket.org:diso/leanlogic-qa-prototype.git llqa # app klonen aus git
    cd llqa
    # npm install # -> schlägt fehl, Paket "pg" (postgres) aus packages.json entfernen...
    nano packages.json
    npm install
    npm install -g bower
    bower install
    cd server
    rm Gemfile.lock
    nano Gemfile # ruby 2.0.0 -> 2.3.0
    # bundle # -> schlägt fehl
    nano /home/lladm/llqa/server/.ruby-version # -> 2.3.0
    rbenv versions # zeigt auf 2.3.0, korrekt
    gem install bundler
    bundle install # installiert puma etc.
    # DB und uploads kopieren
    ruby server.rb

### IPtables:

    sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080
    sudo iptables -t nat -A PREROUTING -p tcp --dport 81 -j REDIRECT --to-port 8081


## Entwicklung

### DB-Migration

Neues, leeres Migrationsfile erstellen:

    bundle exec rake db:create_migration NAME=create_config_table

Danach können die Änderungen eingepflegt werden, die in dieser Version anfallen sollen.
Ein solches File kann z.B. so aussehen:
```ruby
class CreateConfigTable < ActiveRecord::Migration
  def up
    Configtable.create_table self, :configtable
  end

  def down
    drop_table :configtable
  end
end
```
Dabei entsprechen die beiden Methoden `up` und `down` einem Upgrade bzw. Downgrade.

Migrationsfile ausführen:

    bundle exec rake db:migrate

### Report, PDF-Generierung

Mit dem File `measurestatistics.rb` (API `/servlet/pdf/msrstatistics`) wird der PDF-Report für die Messungen generiert.

Hier werden die Daten zuerst aufbereitet und dann mittels folgender Befehle in den Report befüllt:

    pdf_text pdf, 25, 30, 170, 8, lcat.pdf_stat_msr1 % [msr[:code], msr[:title]], false, fontsize: 14, autofit: true, inline_format: true

Diese Funktionen `lcat.pdf_stat_msr{x}` zum Einsetzen definierter Textbausteine findet sich dabei direkt im Übersetzungfile unter:
  
  PDF.STAT.MSR{X}

### Globale Suche

Für globale Suche muss User das Privileg GLSRCH haben.

Um ein Objekt für die globale Suche bereitzustellen, muss folgendes im entsprechden Model eingebunden werden:

    class Configtable < ActiveRecord::Base
        include Searching
   
Und dazu eine Angabe, wo gesucht werden darf:

    # -----
    # Global search
    # -----
    
    def self.searchable_fields
      {
          title: :text
      }
    end

Die Suche wird aus dem Menü gestartet (wenn Privileg vorhanden) und in `AppCtrl.js` gesteuert, über einen mehrstufigen Wizard.
Interessant sind die Methoden `globalSearchView()` und `searchWizardStep1()`.

Schlussendlich wird die Suche über den Server abgewickelt, über die folgenden beiden Routen:

* MISC_14: GET `/api/globalsearch`
* MISC_15: POST `/api/globalsearch`

Die Texte für den Wizard müssen in GSEARCH.TYPE und GSEARCH.FIELD hinterlegt werden, wenn nicht automatisch erkannt.

Die Verlinkung muss im `GlobalSearchViewCtrl.js` verdrahtet werden:

    $scope.goto = function(item) {
        ...    
        if (item.type == 'Configtable') $modalInstance.close({ action: 'url', url: '/configmgt/view/'+item.object.id });
        ....
    }
    
