// Karma configuration
var _ = require('lodash');
//var grunt = require('grunt'); // not sure if we need this like it's written here

module.exports = function (config) {

  var karmaResources = [];

  // AngularJS & additional Angular scripts
  karmaResources.push('components/angular/angular.js');
  karmaResources.push('components/angular-mocks/angular-mocks.js');
  karmaResources.push('components/sprintf/dist/angular-sprintf.min.js');
  karmaResources.push('components/sprintf/dist/sprintf.min.js');
  karmaResources.push('components/lodash/lodash.js');
  karmaResources.push('components/angular-bootstrap/ui-bootstrap.js');
  karmaResources.push('components/restangular/dist/restangular.js');
  karmaResources.push('components/angular-translate/angular-translate.js');

  // project JS files
  karmaResources.push('public/js/app.js');
  karmaResources.push('public/js/controllers/*.js');
  karmaResources.push('public/js/directives/*.js');
  karmaResources.push('public/js/filters/*.js');
  karmaResources.push('public/js/services/*.js');

  // tests
  karmaResources.push('tests/angular/controllers/*.spec.js');
  karmaResources.push('tests/angular/directives/*.spec.js');
  karmaResources.push('tests/angular/filters/*.spec.js');
  karmaResources.push('tests/angular/services/*.spec.js');

  config.set({

    // base path, that will be used to resolve files and exclude
    basePath: '',
    frameworks: ['jasmine'],
    files: karmaResources,
    exclude: [],

    // TODO need this?
    reporters: ['progress', 'coverage'],

    plugins : ['karma-jasmine', 'karma-phantomjs-launcher', 'karma-coverage'],

    junitReporter: {
      outputDir: 'target/surefire-reports/',
      outputFile: 'TEST-karma-results.xml'
    },

    coverageReporter: {
      type : 'html',
      dir : 'tests/coverage/'
    },

    preprocessors : {
      '**/public/js/*.js' : 'coverage',
      '**/public/js/**/*.js' : 'coverage'
    },

    port: 9877,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    logLevel: config.LOG_DEBUG, // LOG_DEBUG, LOG_INFO

    // enable / disable watching file and executing tests whenever any file changes
    //autoWatch: true,

    // Start these browsers, currently available: Chrome, Firefox, Opera, Safari, PhantomJS, IE
    browsers: ['PhantomJS'],

    // If browser does not capture in given timeout [ms], kill it
    captureTimeout: 60000,

    // to avoid DISCONNECTED messages
    browserDisconnectTimeout: 10000, // default 2000
    browserDisconnectTolerance: 1, // default 0
    browserNoActivityTimeout: 60000 //default 10000
  });
};
