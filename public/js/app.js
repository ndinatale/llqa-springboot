'use strict';

angular.module('leanLogic', [
    'leanLogic.controllers',
    'leanLogic.filters',
    'leanLogic.services',
    'leanLogic.directives',
    'leanLogic.constants',
    'ngSanitize',
    'ngCookies',
    'restangular',
    'ui.bootstrap',
    'ui.bootstrap.collapse',
    'ui.router',
    'pascalprecht.translate',
    'ui.router',
    'angularSpinner' ,
    'angularFileUpload',
    'ui.sortable',
    'ui.utils',
    'com.2fdevs.videogular',
    'com.2fdevs.videogular.plugins.controls',
    'com.2fdevs.videogular.plugins.buffering',
    'textAngular',
    'feature-flags',
    'ui.select',
    'ngTouch'
]).config(function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $translateProvider, $provide, RestangularProvider) {
        $translateProvider.useStaticFilesLoader({
            prefix: 'i18n/lang-',
            suffix: '.json'
        });
        $translateProvider.preferredLanguage('en');
        $translateProvider.fallbackLanguage('en');

        $urlRouterProvider.otherwise("/app/dashboard/0");

        $stateProvider
            .state('error', {
                url: '/app/error/:code',
                controller: 'ErrorMsgCtrl',
                templateUrl: '/templates/error.html'
            })
            .state('login', {
                url: '/app/login',
                controller: 'LoginCtrl',
                templateUrl: '/templates/login.html'
            })
            .state('dashboard', {
                url: '/app/dashboard/:dbnum',
                controller: 'DashboardCtrl',
                templateUrl: '/templates/dashboard.html'
            })
            .state('procedure', {
                url: '/app/procedures',
                controller: 'ProcedureListCtrl',
                templateUrl: '/templates/procedures/list.html'
            })
            .state('procedure.view', {
                url: '/view/:procid',
                controller: 'ProcedureViewCtrl',
                templateUrl: '/templates/procedures/view.html'
            })
            .state('procedure.edit', {
                url: '/edit/:procid',
                controller: 'ProcedureEditCtrl',
                templateUrl: '/templates/procedures/edit.html'
            })
            .state('step', {
                url: '/app/steps/:procid',
                controller: 'StepListCtrl',
                templateUrl: '/templates/steps/list.html'
            })
            .state('step.view', {
                url: '/view/:stepid',
                controller: 'StepViewCtrl',
                templateUrl: '/templates/steps/view.html'
            })
            .state('step.edit', {
                url: '/edit/:stepid',
                controller: 'StepEditCtrl',
                templateUrl: '/templates/steps/edit.html'
            })
            .state('model', {
                url: '/app/models',
                controller: 'ModelListCtrl',
                templateUrl: '/templates/models/list.html'
            })
            .state('model.view', {
                url: '/view/:modid',
                controller: 'ModelViewCtrl',
                templateUrl: '/templates/models/view.html'
            })
            .state('model.edit', {
                url: '/edit/:modid',
                controller: 'ModelEditCtrl',
                templateUrl: '/templates/models/edit.html'
            })
            .state('unit', {
                url: '/app/units/:modid',
                controller: 'UnitListCtrl',
                templateUrl: '/templates/units/list.html'
            })
            .state('unit.view', {
                url: '/view/:unitid',
                controller: 'UnitViewCtrl',
                templateUrl: '/templates/units/view.html'
            })
            .state('unit.edit', {
                url: '/edit/:unitid',
                controller: 'UnitEditCtrl',
                templateUrl: '/templates/units/edit.html'
            })
            .state('tool', {
                url: '/app/tools',
                controller: 'ToolListCtrl',
                templateUrl: '/templates/tools/list.html'
            })
            .state('tool.view', {
                url: '/view/:toolid',
                controller: 'ToolViewCtrl',
                templateUrl: '/templates/tools/view.html'
            })
            .state('tool.edit', {
                url: '/edit/:toolid',
                controller: 'ToolEditCtrl',
                templateUrl: '/templates/tools/edit.html'
            })
            .state('wfintro', {
                url: '/app/workflow/:checkid',
                controller: 'WorkflowIntroCtrl',
                templateUrl: '/templates/checks/intro.html'
            })
            .state('wfstep', {
                url: '/app/workflow/:checkid/:stepnum',
                controller: 'WorkflowStepCtrl',
                templateUrl: '/templates/checks/step.html'
            })
            .state('usermgt', {
                url: '/app/usermgt/:what/:id',
                controller: 'UserManagerCtrl',
                templateUrl: '/templates/users.html'
            })
            .state('noticemgt', {
                url: '/app/noticemgt',
                controller: 'NoticeViewCtrl',
                templateUrl: '/templates/noticeview.html'
            })
            .state('configmgt', {
                url: '/app/configmgt',
                controller: 'ConfigListCtrl',
                templateUrl: '/templates/configtable/list.html'
            })
            .state('configmgt.view', {
                url: '/view/:tableid',
                controller: 'ConfigViewCtrl',
                templateUrl: '/templates/configtable/view.html'
            });
    
        RestangularProvider.setBaseUrl('/api');
        RestangularProvider.setResponseInterceptor(function (response, operation, what, url, responsefull, deferred) {
            if (response.status === "error") {
                deferred.reject(response)
            } else {
                if (operation === 'getList') return response.data.rows;
                if (response.data) {
                    return response.data
                } else {
                  return response;
                }
            }
        });

        $provide.factory('llHttpInterceptor', function($q, $rootScope) {
            return {
                'request': function(config) {
                    $rootScope.incLoadingShadeStack(config.url);
                    return config
                },

                'requestError': function(rejection) {
                    $rootScope.incLoadingShadeStack(rejection.config.url);
                    return $q.reject(rejection)
                },

                'response': function(response) {
                    $rootScope.decLoadingShadeStack(response.config.url);
                    return response
                },

                'responseError': function(rejection) {
                    $rootScope.decLoadingShadeStack(rejection.config.url);
                    var status = rejection.status;
                    if (status == 403) {
                        if (rejection.data.type === 'SERVER.ERROR.ACCDENIED.TYPE')
                            $rootScope.showError("ACCDENY",JSON.stringify(rejection,null,4));
                        else
                            $rootScope.showError("TIMEOUT",JSON.stringify(rejection,null,4));
                        return $q.reject(null);
                    }
                    if (status == 404) {
                        $rootScope.showError("HTTP404",JSON.stringify(rejection,null,4));
                        return $q.reject(null);
                    }
                    if (status == 500) {
                        $rootScope.showError("HTTP500",JSON.stringify(rejection,null,4));
                        return $q.reject(null);
                    }
                    return $q.reject(rejection);
                }
            };
        });

        $httpProvider.interceptors.push('llHttpInterceptor');

        $locationProvider.html5Mode(true);

        $provide.decorator('taOptions', ['$delegate', function(taOptions) {
            taOptions.toolbar = [
                ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'pre', 'quote'],
                ['bold', 'italics', 'underline', 'ul', 'ol'], ['redo', 'undo'],
                ['justifyLeft','justifyCenter','justifyRight'],
                ['indent', 'outdent'],
                ['insertLink'], ['html', 'clear']
            ];
            taOptions.classes = {
                focussed: 'focussed ll-ta-focussed',
                toolbar: 'btn-toolbar ll-ta-btn-toolbar',
                toolbarGroup: 'btn-group ll-ta-btn-group',
                toolbarButton: 'btn btn-default ll-ta-btn',
                toolbarButtonActive: 'active ll-ta-btn-active',
                disabled: 'disabled ll-ta-disabled',
                textEditor: 'form-control ll-ta-text-editor',
                htmlEditor: 'form-control ll-ta-html-editor'
            };
            return taOptions;
        }]);

        //featureFlagsProvider.setInitialFlags([
        //    { key: 'customTags', active: false, name: 'custom procedure tags', description: 'custom procedure tags' }
        //])
    })
    .run(function ($window, $rootScope, $state, $stateParams, $modal, $cookies, i18n, $location, Restangular, BrowserService, featureFlags, $http) {
        FastClick.attach(document.body);

        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.appLoaded = false;

        if (!_.isUndefined($cookies.useMobile)) {
            $rootScope.isMobileApp = ($cookies.useMobile === '1')
        } else {
            $rootScope.isMobileApp = BrowserService.isMobile()
        }

        $rootScope.showLoadingShade = false;
        $rootScope.loadingShadeStack = false;

        $rootScope.incLoadingShadeStack = function(url) {
            $rootScope.loadingShadeStack++;
            $rootScope.showLoadingShade = $rootScope.loadingShadeStack > 0
        };
        $rootScope.decLoadingShadeStack = function(url) {
            $rootScope.loadingShadeStack--;
            $rootScope.showLoadingShade = $rootScope.loadingShadeStack > 0
        };

        $rootScope.errordetails = "";
        $rootScope.showError = function(code,details) {
            $rootScope.errordetails = (code == "TIMEOUT" || code == 'ACCDENY') ? null : details;
            $location.url("/app/error/"+code)
        };

        var oldopen = $modal.open;
        $rootScope.activeModalInstances = [];
        $modal.open = function(options) {
            var instance = oldopen(options);
            $rootScope.activeModalInstances.push(instance);
            var olddismiss = instance.dismiss;
            instance.dismiss = function(reason) {
                $rootScope.activeModalInstances = _.without($rootScope.activeModalInstances,instance);
                olddismiss(reason)
            };
            var oldclose = instance.close;
            instance.close = function(result) {
                $rootScope.activeModalInstances = _.without($rootScope.activeModalInstances,instance);
                oldclose(result)
            };
            return instance
        };
        $modal.closeall = function() {
            var list = [];
            _.each($rootScope.activeModalInstances,function(i) { list.push(i) });
            _.each(list,function(i) { i.dismiss() })
        };

        // Calling route SESSION_6
        Restangular.one('applicationproperties').get().then(function(data) {
            $rootScope.appProperties = data;
            $rootScope.appversion = $rootScope.appProperties.appversion;
            if ($rootScope.appProperties.testenvironment) {
              $rootScope.appversion += " - Test";
            }
            $rootScope.appLoaded = true;
        });

        // configure feature flags
        featureFlags.set($http.get('/api/customerfeatures'));
    });

var cmod = angular.module('leanLogic.controllers', []);
var fmod = angular.module('leanLogic.filters', []);
var smod = angular.module('leanLogic.services', []);
var dmod = angular.module('leanLogic.directives', []);

// Elemente aus den Verzeichnissen controllers, directives, filters und services werden hier angehängt
