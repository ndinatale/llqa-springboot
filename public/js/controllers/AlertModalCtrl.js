cmod.controller('AlertModalCtrl', function ($scope, $modalInstance, type, head, intro, reasons) {
  $scope.type = type;
  $scope.head = head;
  $scope.reasons = reasons;
  $scope.intro = intro;

  $scope.ok = function () {
    $modalInstance.close();
  };

  $scope.cancel = function () {
    $modalInstance.dismiss();
  };

  $scope.gettypestr = function () {
    if ($scope.type == 1) {
      return "primary";
    }
    else if ($scope.type == 2) {
      return "danger";
    }
    else if ($scope.type == 3) {
      return "warning";
    }
    else if ($scope.type == 4) {
      return "success";
    }
    else {
      return "primary";
    }
  };
});
