cmod.controller('AppCtrl', function ($rootScope, $scope, $modal, $timeout, $location, $cookies, $window, i18n, AuthService, AlertService, UsersService, Restangular, ModalEditorService, SnippetEditorService) {
  $scope.logout = function () {
    AuthService.logout();
  };

  $rootScope.sessiondata = {};

  $rootScope.i18n = i18n;

  $scope.contains = _.includes;
  $scope.userinfo = {};

  $scope.isMobileApp = $rootScope.isMobileApp;

  $rootScope.refreshTitleData = function () {
    UsersService.addGrantMethods($scope).then(function (info) {
      $scope.userinfo = info
    })
  };
  $rootScope.refreshTitleData();

  $scope.switchToMobile = function () {
    $cookies.useMobile = 1;
    $window.location.reload()
  };
  $scope.switchToDesktop = function () {
    $cookies.useMobile = 0;
    $window.location.reload()
  };

  $scope.deviceTypeEditor = function () {
    $modal.open({
      templateUrl: 'templates/modal/typelisteditor.html',
      controller: 'TypeListEditorCtrl',
      //size: 'lg',
      keyboard: false,
      backdrop: 'static',
      resolve: {
        list: function () {
          return 'devicetype'
        },
        param: function () {
          return {title: 'FRAME.EDITDTYPE.TITLE', text: 'FRAME.EDITDTYPE.TEXT', media: true}
        }
      }
    })
  };

  $scope.checkTypeEditor = function () {
    $modal.open({
      templateUrl: 'templates/modal/typelisteditor.html',
      controller: 'TypeListEditorCtrl',
      //size: 'lg',
      keyboard: false,
      backdrop: 'static',
      resolve: {
        list: function () {
          return 'checktype'
        },
        param: function () {
          return {title: 'FRAME.EDITCTYPE.TITLE', text: 'FRAME.EDITCTYPE.TEXT', media: false}
        }
      }
    })

  };

  $scope.settingsEditor = function () {
    $modal.open({
      templateUrl: 'templates/modal/settingseditor.html',
      controller: 'SettingsEditorCtrl',
      controllerAs: 'ctrl',
      //size: 'lg',
      keyboard: false,
      backdrop: 'static',
      resolve: {
        list: function () {
          return 'setting'
        },
        param: function () {
          return {title: 'FRAME.EDITCTYPE.TITLE', text: 'FRAME.EDITCTYPE.TEXT', media: false}
        }
      }
    });
  };

  $scope.changePassword = function () {
    UsersService.addGrantMethods($scope).then(function (info) {
      $scope.userinfo = info;
      ModalEditorService.showTextLineEditor("FRAME.CHPWD.TITLE", "FRAME.CHPWD.PW1", null).result.then(function (pwd1) {
        if (_.isNull(pwd1)) pwd1 = "";
        ModalEditorService.showTextLineEditor("FRAME.CHPWD.TITLE", "FRAME.CHPWD.PW2", null).result.then(function (pwd2) {
          if (_.isNull(pwd2)) pwd2 = "";
          if (pwd1 != pwd2) {
            AlertService.createErrorMessage("FRAME.CHPWD.TITLE", "FRAME.CHPWD.NOMATCH", null).show();
            return
          }
          if (pwd1.length < 3) {
            AlertService.createErrorMessage("FRAME.CHPWD.TITLE", "FRAME.CHPWD.TOOSHORT", null).show();
            return
          }
          // Calling route USER_3
          Restangular.all('users').post({
            id: $scope.userinfo.userid,
            passhash: CryptoJS.SHA1(pwd1) + ""
          }).then(function () {
            AlertService.createSuccessMessage("FRAME.CHPWD.TITLE", "FRAME.CHPWD.OK", null).show()
          }, AlertService.showRESTError)
        })
      })
    })
  };

  $scope.searchWizardStep1 = function () {
    ModalEditorService.showTextLineEditor("GSEARCH.WIZARD.TITLE", "GSEARCH.WIZARD.TEXT1", null).result.then(function (searchstring) {
      $timeout(function () {
        $scope.searchWizardStep2(searchstring)
      }, 200)
    })
  };

  $scope.searchWizardStep2 = function (searchstring) {
    // Calling route MISC_14
    Restangular.one('globalsearch').get().then(function (result) {
      var blocks = [];
      var curitems = null;
      _.each(result.searchable, function (type) {
        curitems = [];
        blocks.push({name: "GSEARCH.TYPE." + type.model.toUpperCase() + ".SELECT", items: curitems});
        _.each(type.fields, function (field) {
          curitems.push({id: type.model + "/" + field, name: "GSEARCH.FIELD." + field.toUpperCase()})
        })
      });
      ModalEditorService.showMultiPicker('GSEARCH.WIZARD.TITLE', 'GSEARCH.WIZARD.TEXT2', null, blocks, {
        blocks: true,
        style: 2
      }).result.then(function (res) {
        if (res.length < 1) return;
        var searchIn = [];
        var curmodel = '';
        var curfields = null;
        _.each(res, function (picked) {
          picked = picked.split("/");
          if (curmodel !== picked[0]) {
            curfields = [];
            curmodel = picked[0];
            searchIn.push({type: curmodel, fields: curfields})
          }
          curfields.push(picked[1])
        });
        $scope.searchWizardStep3(searchstring, searchIn)
      })
    }, AlertService.showRESTError)
  };

  $scope.searchWizardStep3 = function (searchstring, searchIn) {
    var blocks = [];
    var presel = [];
    _.each(i18n.languages, function (lang) {
      blocks.push({name: lang.name, id: lang.code});
      presel.push(lang.code)
    });
    if (blocks.length > 1) {
      ModalEditorService.showMultiPicker('GSEARCH.WIZARD.TITLE', 'GSEARCH.WIZARD.TEXT3', presel, blocks, {
        blocks: false,
        style: 2
      }).result.then(function (res) {
        if (res.length < 1) return;
        $scope.searchWizardStep4(searchstring, searchIn, res)
      })
    } else {
      $scope.searchWizardStep4(searchstring, searchIn, [i18n.languages[0].code])
    }
  };

  $scope.searchWizardStep4 = function (searchstring, searchIn, languages) {
    var blocks = [];
    _.each(['ALLALL', 'ALLRECENT', 'FINALL', 'FINRECENT', 'LATESTV', 'EDITV'], function (type) {
      blocks.push({id: type, name: 'GSEARCH.SCOPE.' + type + '.TEXT', ttip: 'GSEARCH.SCOPE.' + type + '.TTIP'})
    });
    ModalEditorService.showSinglePicker('GSEARCH.WIZARD.TITLE', 'GSEARCH.WIZARD.TEXT4', "ALLRECENT", blocks, {
      blocks: false,
      style: 2
    }).result.then(function (scope) {
      $scope.searchWizardExecute(searchstring, searchIn, languages, scope)
    })
  };


    $scope.searchWizardExecute = function (searchstring, searchIn, languages, scope) {
    // Calling route MISC_15
    Restangular.all("globalsearch").post({
      string: searchstring,
      search_in: searchIn,
      languages: languages,
      max_results: 50,
      scope: scope
    }).then(function (response) {
      $rootScope.lastGlobalSearchResult = {
        string: searchstring, search_in: searchIn, languages: languages, max_results: 50, result: response.result
      };
      $scope.globalSearchView();
    });
    console.log(searchstring, searchIn, languages);
  };

  $scope.globalSearchView = function () {
    if (!_.isUndefined($rootScope.lastGlobalSearchResult)) {
      $modal.open({
        templateUrl: 'templates/modal/gsearchview.html',
        controller: 'GlobalSearchViewCtrl',
        size: 'lg',
        keyboard: false,
        backdrop: 'static'
      }).result.then(function (result) {
        console.log(result);
        if (result.action === "url") {
          $location.url("/app" + result.url);
          return
        }
        if (result.action === "ct") {
          $scope.checkTypeEditor();
          return
        }
        if (result.action === "dt") {
          $scope.deviceTypeEditor();
          return
        }
        if (result.action === "new") {
          $scope.searchWizardStep1();

        }
      })
    } else {
      $scope.searchWizardStep1();
      //$scope.testSearch();
    }
  };

  $scope.testSearch = function () {
    var where = [
      {type: "Check", fields: ["comment"]},
      {type: "Measurement", fields: ["comment"]},
      {type: "Checktype", fields: ["code", "title", "description"]},
      {type: "Devicetype", fields: ["code", "title", "description"]},
      {type: "Model", fields: ["code", "title", "description"]},
      {type: "Unit", fields: ["code", "customer", "comment"]},
      {type: "Measure", fields: ["code", "title", "description"]},
      {type: "Procedure", fields: ["code", "title", "description"]},
      {type: "Step", fields: ["code", "title", "description"]},
      {type: "Tooltype", fields: ["code", "title", "description"]},
      {type: "Toolunit", fields: ["code", "comment"]},
      {type: "User", fields: ["username", "realname", "comment"]}
    ];
    $scope.searchWizardExecute("NKS+!Lorem", where, ["en", "de", "cn", "fr"])
  };

  $scope.noticeButtonPath = null;

  $rootScope.activateNoticeButton = function (path) {
    $scope.noticeButtonPath = path
  };
  $rootScope.deactivateNoticeButton = function () {
    $scope.noticeButtonPath = null
  };
  $scope.showNoticeEditor = function () {
    $modal.open({
      templateUrl: 'templates/modal/editnotice.html',
      controller: 'EditNoticeCtrl',
      size: 'lg',
      resolve: {
        path: function () {
          return $scope.noticeButtonPath
        }
      }
    })
  };

  $scope.showtest = false;
  $scope.test = function () {
    SnippetEditorService.showSnippetEditor('notice_category', 'NOTICES.MODAL.SNIPPETCAT.TITLE', 'NOTICES.MODAL.SNIPPETCAT.SNIPPET', false);
    SnippetEditorService.showSnippetEditor('notice_textprop', 'NOTICES.MODAL.SNIPPETDESC.TITLE', 'NOTICES.MODAL.SNIPPETDESC.SNIPPET', true);
  }
});