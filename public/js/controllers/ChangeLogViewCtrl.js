cmod.controller('ChangeLogViewCtrl', function ($scope, $modalInstance, item, type, Restangular) {
  $scope.lines = [];
  $scope.hasDirty = false;
  $scope.hasPostFinalize = false;

  function refreshData() {
    $scope.lines = [];
    $scope.hasDirty = false;
    $scope.hasPostFinalize = false;

    // Calling route PSM_3(type='procedure'),MU_4('model')
    Restangular.one(type, item.id).one('changelog').get().then(function (response) {
      var lines = [];
      _.forEach(response.changelog, function (change) {
        var line = {
          itemtype: 'CHANGELOG.ITEM.' + change.subobjtype.toUpperCase(),
          item: change.subobject,
          description: 'CHANGELOG.TYPE.' + change.changetype.toUpperCase(),
          details: change.changedetails,
          user: change.user,
          timestamp: new Date(change.timestamp * 1000),
          marked: change.post_finalize ? 'postfin' : (change.dirty ? 'dirty' : 'normal')
        };
        lines.push(line);
        $scope.hasDirty |= change.dirty;
        $scope.hasPostFinalize |= change.post_finalize;
      });
      $scope.lines = lines;
      $scope.title = item.code;
    })
  }

  refreshData();

  $scope.close = function () {
    $modalInstance.close();
  };
});
