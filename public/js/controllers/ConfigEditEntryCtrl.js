cmod.controller('ConfigEditEntryCtrl', function ($scope, $q, $modalInstance, $rootScope, entry, mainscope, EditorManagementService, VersioningService, MeasureTypeService, ModalEditorService, Restangular, UsersService, i18n, $filter, AlertService, $modal) {
  EditorManagementService.installEditorManagement($scope, 'configentries', null)
  $scope.mainscope = mainscope;
  $scope.entry = entry;
  $scope.parent = mainscope.item;
  $scope.table = mainscope.table;

  $scope.refreshModalData = function (optfun) {
    if ($scope.entry < 0) {
      // add parent type attribute
      var entry = {code: "", configtable_id: $scope.table.id};
      entry.title = '';
      entry.description = '';
      $scope.editor.title = "CONFIGTABLE.EDITOR.TITLE.NEW";
      $scope.item = entry;
      $scope.entryEditLoaded = true;
    } else {
      // Calling route PSM_24
      Restangular.one('configentries', $scope.entry).get({foredit: true}).then(function mData(response) {
        var entry = response;
        entry.title = '';
        entry.description = '';
        $scope.editor.title = "CONFIGTABLE.EDITOR.TITLE.EDIT";
        $scope.item = entry;
        $scope.entryEditLoaded = true;
      }, AlertService.showSevereRESTError);
    }
  };

  $scope.cancel = function () {
    $modalInstance.dismiss();
  };

  $scope.continue = function () {
    //$scope.codeCheck($scope.item, function() {
    // Calling route PSM_27
    Restangular.all("configentries").post($scope.item).then(function (item) {
      $scope.entry = item.id;
      mainscope.refreshData(function () {
        $modalInstance.close()
      })
    }, AlertService.showRESTError);
    //})
    $modalInstance.dismiss();
  };

  $scope.refreshModalData();

});