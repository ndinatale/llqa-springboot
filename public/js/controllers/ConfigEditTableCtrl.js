cmod.controller('ConfigEditTableCtrl', function ($scope, $q, $modalInstance, $rootScope, mainscope, entry, EditorManagementService, VersioningService, MeasureTypeService, ModalEditorService, Restangular, UsersService, i18n, $filter, AlertService, $modal) {
  EditorManagementService.installEditorManagement($scope, 'configtables', null);
  $scope.mainscope = mainscope;
  $scope.table = mainscope.table;
  $scope.entry = entry;

  $scope.refreshModalData = function (optfun) {
    if ($scope.entry < 0) {
      $scope.editor.title = "CONFIGTABLE.EDITOR.TITLE.NEW";
      $scope.item = {title: "", parent: "", parentid: ""};
      $scope.tableEditLoaded = true;
    } else {
      Restangular.one('configtables', $scope.table.id).get({foredit: true}).then(function mData(response) {
        $scope.editor.title = "CONFIGTABLE.EDITOR.TITLE.EDIT";
        $scope.item = response;
        $scope.tableEditLoaded = true;
      }, AlertService.showSevereRESTError);
    }
  };

  $scope.cancel = function () {
    $modalInstance.dismiss();
  };

  $scope.continue = function () {
    // Calling route PSM_29
    Restangular.all("configtables").post($scope.item).then(function (item) {
      $scope.entry = item.id;
      mainscope.refreshData(function () {
        $modalInstance.close();
      })
    }, AlertService.showRESTError);
    $modalInstance.dismiss();
    mainscope.refreshData();
    mainscope.refreshListData();
  };

  $scope.refreshModalData();
});