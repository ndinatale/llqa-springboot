cmod.controller('ConfigListCtrl', function ($rootScope, $modal, $location, FileUploader, $scope, i18n, Restangular, UsersService, AlertService, ListManagementService, UploaderService, ModalEditorService) {
  ListManagementService.installListManagement($scope, 'config_filters');

  $scope.$on("$destroy", function () {
    $modal.closeall();
  });

  $scope.refreshData = function () {
    $scope.refreshListData();
  };

  $scope.refreshListData = function () {
    // Calling route MU_1
    Restangular.all('configtables').getList().then(function configtableList(response) {
      var configtables = response;
      $scope.realidstatus = ListManagementService.calcRealidStatus(configtables);
      $scope.items = configtables;
      $scope.configtableListLoaded = true;
    }, AlertService.showSevereRESTError)
  };

  $scope.goto = function (where) {
    $location.url("/app/configmgt/view/" + where);
  };

  $scope.newTable = function () {
    var modalInstance = $modal.open({
      templateUrl: 'templates/configtable/edittable.html',
      controller: 'ConfigEditTableCtrl',
      size: 'lg',
      resolve: {
        entry: function () {
          return -1
        },
        mainscope: function () {
          return $scope
        }
      }
    });
  };

  UsersService.addGrantMethods($scope).then(function () {
    $scope.loadFilterSettings();
    $scope.refreshListData();
  });

  UploaderService.installImportUploader($scope, "configtables", "/configmgt/view/", null);

  $scope.gotoModel = function (table) {
    $location.url("/app/models/view/" + table.parentid);
  };
});