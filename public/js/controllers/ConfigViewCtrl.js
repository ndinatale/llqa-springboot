cmod.controller('ConfigViewCtrl', function ($rootScope, $modal, $location, $stateParams, $filter, FileUploader, $scope, $window, i18n, VersioningService, Restangular, UsersService, AlertService, ModalEditorService, SnippetEditorService, ListManagementService, ViewerManagementService) {
  ListManagementService.installListManagement($scope, 'config_filters');
  ViewerManagementService.installViewerManagement($scope, null, 'configtable', '/cfgmgt');

  $scope.$on("$destroy", function () {
    $modal.closeall()
  });

  $scope.id = 0;
  $scope.table = [];
  $scope.allEntries = [];
  $scope.entries = [];
  $scope.sortingColumn = 'code_id';
  $scope.sortingReverse = false;

  $scope.refreshData = function (currNotice) {
    // get config table with headers
    Restangular.one('configtables', $stateParams.tableid).get().then(function (response) {
      $scope.table = response;
    }, AlertService.showSevereRESTError);

    // get all entries
    Restangular.one('configtables', $stateParams.tableid).all("entries").getList().then(function (response) {
      $scope.allEntries = response;
      $scope.filterEntrylist();
    }, AlertService.showSevereRESTError);
    $rootScope.selecteditem = {id: $stateParams.tableid};
  };

  UsersService.addGrantMethods($scope).then(function (sdata) {
    $scope.refreshData();
  });

  $scope.filterEntrylist = function () {
    // this will be set in ToolViewCtrl
    var showDeleted = $scope.showDeleted;
    $scope.entries = [];
    angular.forEach($scope.allEntries, function (entry) {
      if (!entry.deleted || showDeleted || $scope.table.deleted) {
        $scope.entries.push(entry);
      }
    });
  };

  $scope.newEntry = function () {
    var modalInstance = $modal.open({
      templateUrl: 'templates/configtable/editentry.html',
      controller: 'ConfigEditEntryCtrl',
      size: 'lg',
      resolve: {
        entry: function () {
          return -1;
        },
        mainscope: function () {
          return $scope;
        }
      }
    })
  };

  $scope.editEntry = function (entry) {
    var modalInstance = $modal.open({
      templateUrl: 'templates/configtable/editentry.html',
      controller: 'ConfigEditEntryCtrl',
      size: 'lg',
      resolve: {
        entry: function () {
          return entry.id;
        },
        mainscope: function () {
          return $scope;
        }
      }
    })
  };

  $scope.editTable = function () {
    var modalInstance = $modal.open({
      templateUrl: 'templates/configtable/edittable.html',
      controller: 'ConfigEditTableCtrl',
      size: 'lg',
      resolve: {
        entry: function () {
          return $scope.table;
        },
        mainscope: function () {
          return $scope;
        }
      }
    })
  };

  $scope.cloneTable = function (table) {
    var msg = AlertService.createConfirmMessage("CONFIGTABLE.CLONE.TITLE", "CONFIGTABLE.CLONE.TEXT", {title: table.title});
    msg.show().result.then(function () {
      // Calling route MISC_30
      Restangular.all('configtables').all(table.id).all('clone').post(table).then(function (data) {
        $scope.refreshListData();
      }, AlertService.showRESTError);
    });
  };

  $scope.deleteTable = function (table) {
    var msg = AlertService.createConfirmMessage("CONFIGTABLE.DELETE.TITLE", "CONFIGTABLE.DELETE.TEXT", {title: table.title});
    msg.show().result.then(function () {
      // Calling route MISC_30
      Restangular.one('configtables', table.id).remove().then(function () {
        $scope.refreshListData();
        $scope.refreshData();
      }, AlertService.showRESTError);
    });
  };

  $scope.reactivateTable = function (table) {
    Restangular.one('configtables', table.id).one('reactivate').post().then(function () {
      $scope.refreshListData();
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  $scope.disableEntry = function (entry) {
    var msg = AlertService.createConfirmMessage("CONFIGTABLE.ALERT.CONFHEADER", "CONFIGTABLE.ALERT.DELETE", {id: entry.id});
    msg.show().result.then(function () {
      Restangular.one("configentries", entry.id).remove().then(function () {
        $scope.refreshData();
      }, AlertService.showRESTError);
    });
  };

  $scope.enableEntry = function (entry) {
    entry.deleted = false;
    // update entry
    $scope.setActive(entry);
  };

  $scope.setActive = function (entry) {
    Restangular.all("configentries").post(entry).then(function (item) {
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  $scope.sortBy = function (sortingColumn) {
    $scope.sortingReverse = ($scope.sortingColumn === sortingColumn) ? !$scope.sortingReverse : false;
    $scope.sortingColumn = sortingColumn;
  };

  $scope.export = function (table) {
    $window.open("/servlet/export/configtables/" + table.id + "");
  };

});