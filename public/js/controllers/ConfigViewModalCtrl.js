cmod.controller('ConfigViewModalCtrl', function ($rootScope, $modal, $modalInstance, $location, $stateParams, $filter, FileUploader, $scope, $window, i18n, VersioningService, Restangular, UsersService, AlertService, ModalEditorService, SnippetEditorService, ListManagementService, ViewerManagementService, configtable_id, check_id) {
  UsersService.addGrantMethods($scope);

  $scope.$on("$destroy", function () {
    $modal.closeall();
  });

  $scope.configtable_id = configtable_id;
  $scope.check_id = check_id;
  $scope.table = [];
  $scope.entries = [];
  $scope.sortingColumn = 'code_id';
  $scope.sortingReverse = false;

  $scope.refreshData = function () {
    // get config table with headers
    Restangular.one('configtables', $scope.configtable_id).get().then(function (response) {
      $scope.table = response;
    }, AlertService.showSevereRESTError);
    Restangular.one('configtables', $scope.configtable_id).all("entries").getList().then(function (response) {
      $scope.entries = response;
    }, AlertService.showSevereRESTError);
  };

  $scope.setActive = function (entry) {
    Restangular.all("configentries").post(entry).then(function (item) {
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  $scope.sortBy = function (sortingColumn) {
    $scope.sortingReverse = ($scope.sortingColumn === sortingColumn) ? !$scope.sortingReverse : false;
    $scope.sortingColumn = sortingColumn;
  };

  $scope.close = function () {
    $modalInstance.dismiss();
  };

  $scope.revalidate = function () {
    Restangular.one('checks', $scope.check_id).one("revalidate").put().then(function (response) {
      // close modal and trigger check reload so that changes become visible
      $modalInstance.close();
    }, AlertService.showSevereRESTError);
  };

  $scope.refreshData();
});