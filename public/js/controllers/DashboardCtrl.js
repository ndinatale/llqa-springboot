dmod.directive('dashBlock', function () {
  var directiveDefinitionObject = {
    templateUrl: 'templates/plugins/dashblock.html',
    restrict: 'E',
    replace: true,
    scope: {
      data: '=data'
    }
  };
  return directiveDefinitionObject;
});

cmod.controller('DashboardCtrl', function ($rootScope, $scope, $modal, $q, $location, $translate, $stateParams, $filter, i18n, AuthService, UsersService, Restangular, AlertService, ModalEditorService) {
  $scope.blocktypes = [];

  $scope.isMobileApp = $rootScope.isMobileApp;

  $scope.numblocks = 0;
  $scope.dashblocks = [
    {num: 0, rows: [], title: '', linet: '', lineb: ''},
    {num: 1, rows: [], title: '', linet: '', lineb: ''},
    {num: 2, rows: [], title: '', linet: '', lineb: ''},
    {num: 3, rows: [], title: '', linet: '', lineb: ''}
  ];

  $scope.dashboardnum = 0;
  $scope.dashboardinfo = {};
  $scope.hasMoreDashboards = false;

  $scope.$on("$destroy", function () {
    $modal.closeall();
  });

  $scope.refreshData = function () {

    // Progress-Daten "normal" laden (nicht im Hintergrund, siehe unten)
    // LLQA-207
    $q.all(_.map($scope.dashboardinfo.blocks, function (block, index) {
      // Calling route MISC_17
      return Restangular.one("dashboard", $scope.dashboardnum).one('block', index).getList()
    })).then(function (blockdata) {
      var cnt = -1;
      var numblocks = _.isUndefined($scope.dashboardinfo.blocks) ? 0 : $scope.dashboardinfo.blocks.length;
      for (var i = 0; i < 4; i++) {
        if (i >= numblocks) {
          $scope.dashblocks[i] = {num: i, rows: [], title: '', line: '', filtered: 0};
        } else {
          var filtered = 0;
          _.each($scope.dashboardinfo.blocks[i].filters, function (filter) {
            if (filtered == 0) {
              filtered = 1;
            }
            if (!_.isNull(filter.val)) {
              filtered = 2;
            }
          });
          $scope.dashblocks[i] = {
            num: i,
            items: blockdata[i],
            title: $scope.dashboardinfo.blocks[i].name,
            line: 'DASHBOARD.TYPE.' + $scope.dashboardinfo.blocks[i].type + '.LINE',
            filtered: filtered
          }
        }
      }
      $scope.numblocks = $scope.dashboardinfo.blocks.length;
      $scope.dashLoaded = true;

      // Wenn dieser Call abgeschlossen ist, die Progress-Daten im Hintergrund holen
      // $scope.loadProgrDataInBg();

    }, AlertService.showSevereRESTError);


      // Progress-Daten wegen Performance-Probleme im Background laden
      // LLQA-207
    //   $scope.loadProgrDataInBg = function () {
    //     $q.all(_.map($scope.dashboardinfo.blocks, function (block, index) {
    //         // Calling route MISC_17
    //         return Restangular.one("dashboard", $scope.dashboardnum).one('block', index).one('inbackground', 'true').getList()
    //     })).then(function (blockdata) {
    //         var cnt = -1;
    //         var numblocks = _.isUndefined($scope.dashboardinfo.blocks) ? 0 : $scope.dashboardinfo.blocks.length;
    //         for (var i = 0; i < 4; i++) {
    //             if (i >= numblocks) {
    //                 $scope.dashblocks[i] = {num: i, rows: [], title: '', line: '', filtered: 0};
    //             } else {
    //                 var filtered = 0;
    //                 _.each($scope.dashboardinfo.blocks[i].filters, function (filter) {
    //                     if (filtered == 0) {
    //                         filtered = 1;
    //                     }
    //                     if (!_.isNull(filter.val)) {
    //                         filtered = 2;
    //                     }
    //                 });
    //                 $scope.dashblocks[i] = {
    //                     num: i,
    //                     items: blockdata[i],
    //                     title: $scope.dashboardinfo.blocks[i].name,
    //                     line: 'DASHBOARD.TYPE.' + $scope.dashboardinfo.blocks[i].type + '.LINE',
    //                     filtered: filtered
    //                 }
    //             }
    //         }
    //         $scope.numblocks = $scope.dashboardinfo.blocks.length;
    //         $scope.dashLoaded = true;
    //
    //     }, AlertService.showSevereRESTError);
    // };
  };

  $scope.hardRefreshData = function () {
    UsersService.addGrantMethods($scope).then(function (sdata) {
      $scope.dashboardnum = parseInt($stateParams.dbnum);
      $scope.username = sdata.username;
      $scope.realname = sdata.realname;
      $scope.userid = sdata.userid;
      $scope.groups = sdata.groups;
      var dashboardInfo = UsersService.getDashboardInfo();
      if (dashboardInfo.length > $scope.dashboardnum) {
        $scope.dashboardinfo = dashboardInfo[$scope.dashboardnum];
      } else {
        $scope.dashboardinfo = {
          name: 'Invalid Dashboard',
          blocks: []
        }
      }
      $scope.hasMoreDashboards = dashboardInfo.length > 1;
      $scope.refreshData();
    })
  };

  $scope.hardRefreshData();

  $scope.gotoItem = function (url) {
    $location.url("/app" + url);
  };

  $scope.closeBlock = function (num) {
    console.log("CLOSE " + num);
    $scope.dashboardinfo.blocks.splice(num, 1);
    UsersService.updateDashboardInfo().then(function () {
      $scope.refreshData();
    });
  };

  $scope.addBlock = function (num) {
    // Calling route MISC_16
    Restangular.all("dashboard").one("list").get().then(function (dtypes) {
      var vlist = [];
      _.each(dtypes.list, function (val) {
        var hasgrants = true;
        _.each(val.needgrant, function (grant) {
          if (!$scope.hasGrant(grant)) {
            hasgrants = false;
          }
        });
        if (hasgrants) {
          vlist.push({id: val, name: "DASHBOARD.TYPE." + val.type + ".TITLE"});
        }
      });
      ModalEditorService.showSinglePicker("DASHBOARD.NEWBLOCK.TITLE", "DASHBOARD.NEWBLOCK.TEXT", null, vlist).result.then(function (selv) {
        $scope.dashboardinfo.blocks.push({
          // LLQA-68: the title is translated in the template using translate filter. There is no need to do
          // that here...
          name: "DASHBOARD.TYPE." + selv.type + ".TITLE",
          //name: $filter('translate')("DASHBOARD.TYPE."+selv.type+".TITLE"),
          type: selv.type,
          filters: _.map(selv.filters, function (filter) {
            return {type: filter, val: null};
          })
        });
        UsersService.updateDashboardInfo().then(function () {
          $scope.refreshData();
        });
      });
    });
  };

  $scope.editBlockName = function (num) {
    ModalEditorService.showTextLineEditor("DASHBOARD.EDITBLKNAME.TITLE", "DASHBOARD.EDITBLKNAME.TEXT", $scope.dashboardinfo.blocks[num].name, null).result.then(function (title) {
      if (title.length >= 3) {
        $scope.dashboardinfo.blocks[num].name = title;
        UsersService.updateDashboardInfo().then(function () {
          $scope.refreshData();
        });
      }
    });
  };

  $scope.editFilterMODELSEL = function (blnum, idx, oldval) {
    // Calling route MU_1
    Restangular.all("models").getList({selector: true}).then(function (modlist) {
      var selectlist = [];
      _.each(modlist, function (model) {
        selectlist.push({id: model.realid, title: model.title});
      });
      ModalEditorService.showMultiPicker("DASHBOARD.FILTERS.TITLE", "DASHBOARD.FILTERS.TYPES.MODELSEL.TEXT", oldval, selectlist).result.then(function (newval) {
        $scope.dashboardinfo.blocks[blnum].filters[idx].val = newval;
        UsersService.updateDashboardInfo().then(function () {
          $scope.refreshData();
        });
      });
    }, AlertService.showSevereRESTError);
  };

  $scope.setFilterAction = function (blnum, idx, type, action) {
    if (action == 0) {
      $scope.dashboardinfo.blocks[blnum].filters[idx].val = null;
      UsersService.updateDashboardInfo().then(function () {
        $scope.refreshData();
      })
    } else {
      $scope['editFilter' + type](blnum, idx, $scope.dashboardinfo.blocks[blnum].filters[idx].val);
    }
  };

  $scope.setFilter = function (num) {
    var filters = $scope.dashboardinfo.blocks[num].filters;
    var select = [];
    if (filters.length < 1) return;
    _.each(filters, function (filter, idx) {
      console.log(JSON.stringify(filter, null, 4)); // TODO nisc: Remove!
      if (_.isNull(filter.val)) {
        select.push({
          id: {idx: idx, type: filter.type, action: 1},
          name: $filter('translate')('DASHBOARD.FILTERS.ACTENABLE') + " " + $filter('translate')('DASHBOARD.FILTERS.TYPES.' + filter.type + '.NAME')
        })
      } else {
        select.push({
          id: {idx: idx, type: filter.type, action: 1},
          name: $filter('translate')('DASHBOARD.FILTERS.ACTEDIT') + " " + $filter('translate')('DASHBOARD.FILTERS.TYPES.' + filter.type + '.NAME')
        });
        select.push({
          id: {idx: idx, type: filter.type, action: 0},
          name: $filter('translate')('DASHBOARD.FILTERS.ACTDISABLE') + " " + $filter('translate')('DASHBOARD.FILTERS.TYPES.' + filter.type + '.NAME')
        });
      }
    });
    if (select.length == 1) {
      $scope.setFilterAction(num, select[0].id.idx, select[0].id.type, select[0].id.action);
    } else {
      ModalEditorService.showSinglePicker("DASHBOARD.FILTERS.TITLE", "DASHBOARD.FILTERS.TEXT", null, select).result.then(function (selaction) {
        $scope.setFilterAction(num, selaction.idx, selaction.type, selaction.action);
      });
    }
  };

  $scope.addDashboard = function () {
    ModalEditorService.showTextLineEditor("DASHBOARD.ADDDB.TITLE", "DASHBOARD.ADDDB.TEXT", $filter('translate')('FRAME.DASHBOARD'), null).result.then(function (title) {
      if (title.length >= 3) {
        UsersService.getDashboardInfo().push({
          name: title,
          blocks: []
        });

        UsersService.updateDashboardInfo().then(function () {
          $scope.refreshData();
          $rootScope.refreshTitleData();
          $location.url("/app/dashboard/" + (UsersService.getDashboardInfo().length - 1));
        });
      }
    });
  };

  $scope.editDashboardName = function () {
    ModalEditorService.showTextLineEditor("DASHBOARD.EDITDBNAME.TITLE", "DASHBOARD.EDITDBNAME.TEXT", $scope.dashboardinfo.name, null).result.then(function (title) {
      if (title.length >= 3) {
        $scope.dashboardinfo.name = title;
        UsersService.updateDashboardInfo().then(function () {
          $scope.refreshData();
          $rootScope.refreshTitleData();
        });
      }
    });
  };

  $scope.removeDashboard = function () {
    AlertService.createConfirmMessage("DASHBOARD.DELETEDB.TITLE", "DASHBOARD.DELETEDB.TEXT", null).show().result.then(function () {
      UsersService.getDashboardInfo().splice($scope.dashboardnum, 1);
      UsersService.updateDashboardInfo();

      $rootScope.refreshTitleData();
      if ($scope.dashboardnum == 0) {
        $scope.hardRefreshData();
      } else {
        $location.url("/app/dashboard/" + ($scope.dashboardnum < 1 ? 0 : $scope.dashboardnum - 1));
      }
    })
  };

  $scope.rotateDashboard = function (direction, event) {
    var newdashbnum = $scope.dashboardnum + direction;
    if (newdashbnum > 0 && newdashbnum >= UsersService.getDashboardInfo().length) {
      newdashbnum = 0;
    }
    if (newdashbnum < 0) {
      newdashbnum = UsersService.getDashboardInfo().length - 1;
    }
    if (event.shiftKey || event.metaKey) {
      var olddashbnum = $scope.dashboardnum;
      var buffer = UsersService.getDashboardInfo()[olddashbnum];
      UsersService.getDashboardInfo()[olddashbnum] = UsersService.getDashboardInfo()[newdashbnum];
      UsersService.getDashboardInfo()[newdashbnum] = buffer;
      UsersService.updateDashboardInfo();
      $rootScope.refreshTitleData();
    }
    $location.url("/app/dashboard/" + newdashbnum);
  };

});