cmod.controller('EditNoticeCtrl', function ($scope, $q, $modalInstance, $rootScope, path, EditorManagementService, VersioningService, MeasureTypeService, ModalEditorService, Restangular, UsersService, i18n, $filter, AlertService, $modal) {

  $scope.refreshModalData = function (optfun) {
    // Calling route MISC_21
    Restangular.one('notices', 'editinfo').get().then(function procList(response) {
      $scope.select.textprop = response.textproposals;
      $scope.select.categories = response.categories;
      $scope.select.artdescprop = response.last_artdesc;
      $scope.select.artnoprop = response.last_artno;
      $scope.stepNEditLoaded = true;
    }, AlertService.showSevereRESTError);
  };

  $scope.stepNEditLoaded = false;

  $scope.notice = {
    text: '',
    category_id: null,
    path: path
  };

  $scope.select = {
    textprop_sel: null,
    textprop: [],
    categories: [],
    artdescprop: [],
    artnoprop: [],
    timeloss: [
      {min: 15, text: 'NOTICES.TIMELOSS.15'},
      {min: 30, text: 'NOTICES.TIMELOSS.30'},
      {min: 60, text: 'NOTICES.TIMELOSS.60'},
      {min: 90, text: 'NOTICES.TIMELOSS.90'},
      {min: 120, text: 'NOTICES.TIMELOSS.120'},
      {min: 180, text: 'NOTICES.TIMELOSS.180'},
      {min: 240, text: 'NOTICES.TIMELOSS.240'}
    ]
  };

  UsersService.addGrantMethods($scope).then(function () {
    MeasureTypeService.getMeasuretypes($scope).then(function () {
      $scope.refreshModalData();
      $('#n_text').focus();
    }, AlertService.showSevereRESTError);
  });

  $scope.useTextProposal = function () {
    if ($scope.select.textprop_sel) {
      _.each($scope.select.textprop, function (tp) {
        if (tp.id === $scope.select.textprop_sel) {
          $scope.notice.text = "" + tp.text;
        }
      });
    }
  };

  $scope.cancel = function () {
    $modalInstance.dismiss();
  };

  $scope.save = function () {
    if ($scope.notice.category_id === null) {
      AlertService.createErrorMessage("NOTICES.ALERT.CATMISS.TITLE", "NOTICES.ALERT.CATMISS.TEXT", null).show();
      return;
    }
    if ($scope.notice.text === null || $scope.notice.text.length < 3) {
      AlertService.createErrorMessage("NOTICES.ALERT.DESCMISS.TITLE", "NOTICES.ALERT.DESCMISS.TEXT", null).show();
      return;
    }
    Restangular.all('notices').post($scope.notice).then(function (response) {
      AlertService.createSuccessMessage("NOTICES.ALERT.THANKS.TITLE", "NOTICES.ALERT.THANKS.TEXT", null).show();
      $modalInstance.dismiss();
    }, AlertService.showRESTError);
  };

});