cmod.controller('ErrorMsgCtrl', function ($scope, $rootScope, $stateParams, $location) {
  $scope.leave = function () {
    $location.url("/app/login");
  };
  $scope.errCode = $stateParams.code;
});