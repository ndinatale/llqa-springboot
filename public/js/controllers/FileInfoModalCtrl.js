cmod.controller('FileInfoModalCtrl', function ($scope, $modalInstance, fileInfo) {
  $scope.fileInfo = fileInfo;

  $scope.ok = function () {
    $modalInstance.close();
  };
});
