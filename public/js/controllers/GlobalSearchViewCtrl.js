cmod.controller('GlobalSearchViewCtrl', function ($modalInstance, $rootScope, $scope) {
  $scope.searchstring = $rootScope.lastGlobalSearchResult.string;
  $scope.search_in = $rootScope.lastGlobalSearchResult.search_in;
  $scope.result = $rootScope.lastGlobalSearchResult.result;

  console.log($scope.result);

  $scope.close = function () {
    $modalInstance.close({action: 'none'})
  };

  $scope.newSearch = function () {
    $modalInstance.close({action: 'new'})
  };

  $scope.goto = function (item) {
    if (item.type == 'Check') {
      $modalInstance.close({
        action: 'url', url: '/workflow/' + item.object.id
      });
    }
    if (item.type == 'Measurement') {
      $modalInstance.close({
        action: 'url', url: '/workflow/' + item.object.check_id + "/MM" + item.object.id
      });
    }
    if (item.type == 'Checktype') {
      $modalInstance.close({
        action: 'ct'
      });
    }
    if (item.type == 'Devicetype') {
      $modalInstance.close({
        action: 'dt'
      });
    }
    if (item.type == 'Model') {
      $modalInstance.close({
        action: 'url', url: '/models/view/' + item.object.id
      });
    }
    if (item.type == 'Unit') {
      $modalInstance.close({
        action: 'url', url: '/units/' + item.object.model_id + '/view/' + item.object.id
      });
    }
    if (item.type == 'Measure') {
      $modalInstance.close({
        action: 'url', url: '/steps/' + item.object.step.procedure_id + '/view/' + item.object.step_id
      });
    }
    if (item.type == 'Procedure') {
      $modalInstance.close({
        action: 'url', url: '/procedures/view/' + item.object.id
      });
    }
    if (item.type == 'Step') {
      $modalInstance.close({
        action: 'url', url: '/steps/' + item.object.procedure_id + '/view/' + item.object.id
      });
    }
    if (item.type == 'Tooltype') {
      $modalInstance.close({
        action: 'url', url: '/tools/view/' + item.object.id
      });
    }
    if (item.type == 'Toolunit') {
      $modalInstance.close({
        action: 'url', url: '/tools/view/' + item.object.tooltype_id
      });
    }
    if (item.type == 'User') {
      $modalInstance.close({
        action: 'url', url: '/usermgt/user/' + item.object.id
      });
    }
    if (item.type == 'Configtable') {
      $modalInstance.close({
        action: 'url', url: '/configmgt/view/' + item.object.id
      });
    }
    if (item.type == 'Configentry') {
      $modalInstance.close({
        action: 'url', url: '/configmgt/view/' + item.object.configtable_id
      });
    }
  };
});
