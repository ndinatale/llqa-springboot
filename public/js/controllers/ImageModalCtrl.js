cmod.controller('ImageModalCtrl', function ($modalInstance, $rootScope, $scope, medium, disptype, MediaService) {
  $scope.image = null;
  $scope.disptype = disptype;
  $scope.medium = medium;
  $scope.selectedZoomImage = null;

  // {zoomType: "inner", scrollZoom: true, zoomContainerZLevel:1055 }

  $scope.setupZoomImage = function () {
    var image = MediaService.fitIntoBox(medium.fullimage, 870, 500, 2);
    image.path = "/media/" + medium.fullimage.filename;
    if (image.scale < 1) {
        $scope.zoomableImageOptions = {
            zoomWindowPosition: 1,
            zoomWindowHeight: 502,
            zoomWindowWidth: 870,
            zoomWindowOffetx: -2 - (image.width + image.padleft),
            zoomWindowOffety: -2,
            borderColour: '#005499',
            //scrollZoom: true,
            //easing:true,
            zoomContainerZLevel: 2005,
            lensBorder: 0,
            lensOpacity: 0,
            cursor: 'crosshair'
        };
       $("#zoombigimg").data('zoom-image', "/media/" + medium.fullimage.filename).elevateZoom($scope.zoomableImageOptions);
        $scope.selectedZoomImage = "/media/" + medium.fullimage.filename;
    }
    $scope.image = image;
    $scope.title = medium.caption ? medium.caption : medium.fullimage.original_filename;
  };

  $scope.setupPlainImage = function () {
    var image = MediaService.placeIntoBox(medium.fullimage, 870, 510, 2, null);
    image.path = "/media/" + medium.fullimage.filename;
    console.log(image);
    $scope.image = image;
    $scope.scalesteps = [];
    var curscale = image.maxscale;
    var factor = 1;
    $scope.scalesteps.push({scale: image.maxscale, name: "FULL", factor: null});
    if (image.fitscale < 1.0) $scope.scalesteps.push({scale: image.fitscale, name: "FIT", factor: null});
    while (curscale < 1.0) {
      if (factor > 1) $scope.scalesteps.push({scale: curscale, name: "STEP", factor: factor});
      curscale = curscale * 2;
      factor = factor * 2;
    }
    $scope.scalesteps.push({scale: 1.0, name: "MAX", factor: null});
    console.log($scope.scalesteps);
    $scope.title = medium.caption ? medium.caption : medium.fullimage.original_filename;
    if (image.scale < 1) {
      $scope.selectedZoomImage = "/media/" + medium.fullimage.filename;
    }
  };

  $rootScope.$on("toggleImage", function(event, dir){
      $scope.toggleImage(dir);
  });

  $scope.toggleImage = function (dir) {
    // left or right arrow key / button next or button previous
    if (event && (event.which == 37 || event.which == 39) || dir) {
      if (!dir) dir = event.which == 37 ? -1 : 1;
      medium.num = medium == null ? 0 : medium.num + dir;

      if (medium.num < 0) {
          medium.num = medium.imageList.length - 1;
      }
      if (medium.num >= medium.imageList.length) {
          medium.num = 0;
      }

      var num = medium.num;
      var imageList = medium.imageList;
      medium = medium.imageList[medium.num];
      medium.num = num;
      medium.imageList = imageList;
      $scope.medium = medium;
      if (disptype == 'zoom') MediaService.killZoomImage('zoombigimg');
      $scope.setupImage();
    } else if (event && event.which == 27) {
        // escape
        $scope.close();
    }
  };

  $scope.zoom = function (zlvl) {
    var image = MediaService.placeIntoBox(medium.fullimage, 870, 510, 2, zlvl);
    $scope.image.width = image.width;
    $scope.image.height = image.height;
    $scope.image.scale = image.scale;
  };

  $scope.setupImage = function () {
    if ($scope.disptype === 'zoom') {
        $scope.setupZoomImage();
    } else {
        $scope.setupPlainImage();
    }
  };

  $scope.setupImage();

  $rootScope.$on("close", function(){
      $scope.close();
  });

  $scope.close = function () {
    if (disptype === 'zoom') MediaService.killZoomImage('zoombigimg');
    $modalInstance.close();
  };

});