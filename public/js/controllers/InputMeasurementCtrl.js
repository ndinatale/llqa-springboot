cmod.controller('InputMeasurementCtrl', function ($scope, $rootScope, $modalInstance, $timeout, measure, mainscope, testmode, measurement, mtinfo, ConstantService, ModalEditorService, KeyboardShortcutService, EditorManagementService, Restangular, UsersService, i18n, $filter, AlertService, $modal) {
  EditorManagementService.installEditorManagement($scope, 'measures', null);
  $scope.measure = measure;
  $scope.mainscope = mainscope;
  $scope.testmode = testmode;
  $scope.mtinfo = mtinfo;

  var C = {};
  ConstantService.installConstants($scope, C, ['COMP_']);

  $scope.numkeyboard = $rootScope.isMobileApp;
  $scope.keyboard = {
    needed: false,
    modelkey: null,
    pressed: null,
    curpredot: '',
    curpostdot: '',
    curdot: false,
    curminus: false
  };

  $scope.valtimeconv = {
    date: "-",
    h: 0,
    m: 0,
    s: 0,
    raw: null,
    reduce: 0
  };

  function setValTimeConv(mseccnt) {
    var date = new Date(mseccnt);
    $scope.valtimeconv.date = $filter('date')(date, i18n.selectedLanguage['dformat']);
    $scope.valtimeconv.h = date.getHours();
    $scope.valtimeconv.m = date.getMinutes();
    $scope.valtimeconv.s = date.getSeconds();
    $scope.valtimeconv.raw = date;
  }

  setValTimeConv(new Date().getTime());

  function valTimeConvSave() {
    var date = $scope.valtimeconv.raw;
    date.setHours($scope.valtimeconv.h);
    date.setMinutes($scope.valtimeconv.m);
    date.setSeconds($scope.valtimeconv.s);
  }

  $scope.valTimeConvAddDay = function (toadd) {
    valTimeConvSave();
    var date = $scope.valtimeconv.raw;
    setValTimeConv(date.getTime() + (toadd * 86400000))
  };

  $scope.valTimeConvAddReduce = function (toadd) {
    var newred = $scope.valtimeconv.reduce + toadd;
    if (newred < 0) {
      newred = 0;
    }
    $scope.valtimeconv.reduce = newred;
  };

  $scope.sel = {
    hours: [],
    minutes: []
  };

  for (var i = 0; i < 24; i++) {
    $scope.sel.hours.push({id: i, disp: sprintf("%02d", i)})
  }

  for (i = 0; i < 60; i++) {
    $scope.sel.minutes.push({id: i, disp: sprintf("%02d", i)})
  }

  $scope.floatformat = $rootScope.appProperties.defaultfloatformat;
  if (!_.isUndefined($scope.measure) && !_.isUndefined($scope.measure.metadata.floatformat) && _.startsWith($scope.measure.metadata.floatformat, "%")) {
    $scope.floatformat = $scope.measure.metadata.floatformat;
  }

  $scope.sprintf = sprintf;

  var rows = [];
  for (i = 0; i < $scope.measure.calculation.mx_ysize; i++) {
    rows.push(i + 1);
  }
  var cols = [];
  for (i = 0; i < $scope.measure.calculation.mx_xsize; i++) {
    cols.push(i + 1);
  }
  $scope.rows = rows;
  $scope.cols = cols;
  $scope.yttlclass = "ll-ytitle" + $scope.measure.calculation.mx_ysize;

  $scope.refreshModalData = function () {
    if ($scope.testmode) {
      $scope.item = {
        comment: "",
        rawvalues: {},
        value: null,
        status: 0
      }
    } else {
      var item = {
        comment: "",
        rawvalues: {},
        value: null,
        status: 0
      };
      if (!_.isUndefined(measurement) && !_.isNull(measurement)) {
        if (!_.isUndefined(measurement.comment)) {
          item.comment = measurement.comment;
        }
        if (!_.isUndefined(measurement.rawvalues)) {
          item.rawvalues = measurement.rawvalues;
          if (_.startsWith($scope.mtinfo.input, 'timest') && _.isNumber(item.rawvalues.val)) {
            setValTimeConv(item.rawvalues.val * 1000);
            $scope.valtimeconv.reduce = item.rawvalues.reduce ? item.rawvalues.reduce : 0;
          }
        }
        if (!_.isUndefined(measurement.value)) {
          item.value = measurement.value;
        }
        if (!_.isUndefined(measurement.status)) {
          item.status = measurement.status;
        }
      }

      $scope.item = item;
    }
    // if list, set default to first value (if not yet set)
    if ($scope.mtinfo.input == 'list' && !$scope.item.rawvalues.val) {
      $scope.item.rawvalues.val = measure.calculation.t17_list[0];
    }

    $scope.wfInputLoaded = true;
  };

  UsersService.addGrantMethods($scope).then(function () {
    $scope.refreshModalData();
  });

  $scope.matrixTitle = function (what) {
    if (_.isUndefined($scope.measure.calculation["mx_" + what]) || _.isNull($scope.measure.calculation["mx_" + what]) || $scope.measure.calculation["mx_" + what] == "") {
      return " ";
    }
    return $scope.measure.calculation["mx_" + what]
  };

  $scope.matrixValModel = function (col, row) {
    return ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'][col - 1] + row
  };

  $scope.matrixUsedField = function (col, row) {
    return _.includes($scope.measure.calculation["mx_usedfields"], $scope.matrixValModel(col, row));
  };

  $scope.editComment = function () {
    ModalEditorService.showTextAreaEditor("MEASUREMENT.INPUT.COMMENT.TITLE", "MEASUREMENT.INPUT.COMMENT.TEXT", $scope.item.comment).result.then(function (val) {
      $scope.item.comment = val;
    });
  };

  $scope.contains = _.includes;

  function postprocess() {
    if (_.startsWith($scope.mtinfo.input, "timest")) {
      valTimeConvSave();
      var date = $scope.valtimeconv.raw;
      $scope.item.rawvalues.val = Math.round(date.getTime() / 1000);
      $scope.item.rawvalues.reduce = $scope.valtimeconv.reduce;
    }
  }

  $scope.hintGiven = function () {
    if (_.isUndefined($scope.measure.description) || _.isNull($scope.measure.description) || _.isUndefined($scope.measure.description[i18n.selectedLanguage.code]) || _.isNull($scope.measure.description[i18n.selectedLanguage.code])) {
      return false;
    }
    return true;
  };

  $scope.check = function () {
    postprocess();
    // Calling route CHECK_10
    Restangular.all('measurements').all('test').post({
      measure_id: $scope.measure.id,
      rawvalues: $scope.item.rawvalues
    }).then(function (result) {
      $scope.item.value = result.value;
      $scope.item.status = result.status;
    }, AlertService.showRESTError)
  };

  $scope.clearMatrix = function () {
    $scope.item.rawvalues = {}
  };

  $scope.close = function () {
    $modalInstance.dismiss()
  };

  $scope.continue = function () {
    postprocess();
    // Calling route CHECK_7
    Restangular.one('workflow', mainscope.step.check.id).one('step', mainscope.stepnum).all("action").post({
      action: 'savemeasure',
      measure_id: $scope.measure.id,
      rawvalues: $scope.item.rawvalues,
      comment: $scope.item.comment,
      userid: mainscope.userinfo.userid
    }).then(function () {
      $modalInstance.close();
    }, AlertService.showRESTError);
  };

  $scope.quickCheckAction = function () {
    if ($scope.testmode) {
      $scope.check();
    } else {
      $scope.continue();
    }
  };

  var firstTextBox = true;
  $scope.firstTextBox = function () {
    var ret = firstTextBox;
    firstTextBox = false;
    return ret;
  };

  $scope.setBool = function (val) {
    // for a yes/no question, set value on a Y / N keypress
    if (!$scope.item.rawvalues.skip && $scope.mtinfo.input == 'bool') {
      val ? $scope.item.rawvalues.val = 1 : $scope.item.rawvalues.val = 0;
    }
  };

  // helpers for setBool, so we can bind a function to a keypress below

  KeyboardShortcutService.installKeyboardShortcuts($scope, undefined, {
    13: $scope.quickCheckAction,
    89: function () {
      $scope.setBool(true);
    }, // Y
    78: function () {
      $scope.setBool(false);
    }, // N,
    37: function () {
      $scope.listKeySelect(false);
    },
    38: function () {
      $scope.listKeySelect(false);
    },
    39: function () {
      $scope.listKeySelect(true);
    },
    40: function () {
      $scope.listKeySelect(true);
    }
  });

  $scope.listKeySelect = function (goingdown) {
    if ($scope.mtinfo.input == 'list') {
      var idx = _.indexOf(measure.calculation.t17_list, $scope.item.rawvalues.val);
      var lastElement = measure.calculation.t17_list.length - 1;
      if (idx >= 0) {
        $scope.item.rawvalues.val = measure.calculation.t17_list[(goingdown ? Math.min(lastElement, idx + 1) : Math.max(0, idx - 1))];
      }
    }
  };

  $scope.activateKeyboardItem = function (modelkey, onlyIfNothingSelected) {
    if (!$scope.numkeyboard) {
      return;
    }
    if (onlyIfNothingSelected && !_.isNull($scope.keyboard.modelkey)) {
      return;
    }
    $scope.keyboard.needed = true;
    $scope.keyboard.modelkey = modelkey;
    $scope.keyboard.curdot = false;
    $scope.keyboard.curpostdot = '';
    $scope.keyboard.curpredot = '';
    $scope.keyboard.curminus = false;
    $scope.keyboard.preview = '';
  };

  $scope.keyboardAction = function (key) {
    $scope.keyboard.pressed = key;
    if (!_.isNull($scope.keyboard.keyrelease)) {
      $timeout.cancel($scope.keyboard.keyrelease);
    }
    $scope.keyboard.keyrelease = $timeout(function () {
      console.log($scope.keyboard.pressed);
      $scope.keyboard.pressed = null;
      $scope.keyboard.keyrelease = null;
    }, 100);

    if (key === 'C') {
      $scope.keyboard.curdot = false;
      $scope.keyboard.curpostdot = '';
      $scope.keyboard.curpredot = '';
      $scope.keyboard.curminus = false;
      $scope.keyboard.preview = '';
    } else if (key === '.') {
      $scope.keyboard.curdot = true;
      if ($scope.keyboard.curpredot === '') {
        $scope.keyboard.curpredot = '0';
      }
    } else if (key === '-') {
      $scope.keyboard.curminus = !$scope.keyboard.curminus;
      if ($scope.keyboard.curpredot === '') {
        $scope.keyboard.curpredot = '0';
      }
    } else {
      if ($scope.keyboard.curdot) {
        $scope.keyboard.curpostdot += key;
      } else {
        if ($scope.keyboard.curpredot == '0') {
          $scope.keyboard.curpredot = '';
        }
        $scope.keyboard.curpredot += key
      }
    }
    $scope.keyboard.preview = ($scope.keyboard.curminus ? '-' : '') + ($scope.keyboard.curpredot === '' ? '0' : $scope.keyboard.curpredot) + ($scope.keyboard.curdot ? '.' + ($scope.keyboard.curpostdot === '' ? '0' : $scope.keyboard.curpostdot) : '');
    var val = null;
    if ($scope.keyboard.curpredot === '' && $scope.keyboard.curpostdot === '') {
      $scope.keyboard.preview = '';
    } else {
      val = parseFloat($scope.keyboard.preview);
    }
    $scope.item.rawvalues[$scope.keyboard.modelkey] = val;
  };

  $scope.choiceList = function () {
    if (!_.isUndefined(measure.calculation.t17_list)) {
      return measure.calculation.t17_list;
    }
  };

});