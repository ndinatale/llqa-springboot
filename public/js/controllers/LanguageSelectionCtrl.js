cmod.controller('LanguageSelectionCtrl', function ($scope, $translate, UsersService, i18n) {
  $scope.languages = i18n.languages;

  $scope.selectedLanguage = function () {
    return i18n.selectedLanguage;
  };

  $scope.selectLanguage = function () {
    i18n.updateLanguage(this.language);
    UsersService.getUserInfo().default_lang = this.language.code;
    UsersService.updateUserInfo();
  };
});