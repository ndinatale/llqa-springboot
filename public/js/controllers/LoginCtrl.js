cmod.controller('LoginCtrl', function ($scope, $rootScope, $modal, AuthService) {
  $scope.isMobileApp = $rootScope.isMobileApp;

  $scope.logininfo = {
    username: '',
    password: ''
  };

  $scope.login = function () {
    AuthService.login($scope.logininfo.username, $scope.logininfo.password);
  };

  $scope.$on("$destroy", function () {
    $modal.closeall();
  });

  $scope.quickloginKeymap = {13: 'quicklogin($event)'};

  $scope.quicklogin = function (ev) {
    $scope.login();
    ev.preventDefault();
  };

  $('#username').focus();
});