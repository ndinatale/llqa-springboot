cmod.controller('MeasureStatisticsCtrl', function ($scope, $modalInstance, $window, proc, i18n, Restangular, ModalEditorService, AlertService) {
  $scope.proc = proc;

  $scope.selectedMeasures = [];
  $scope.selectedChecks = [];

  $scope.selectMeasures = function () {
    // Calling route PSM_12
    Restangular.one('procedures', proc.id).all('steps').getList({copyselmeasure: true}).then(function (slist) {
      var blocks = slist;
      _.each(blocks, function (step) {
        step.items = step.measures;
      });
      ModalEditorService.showMultiPicker('MSRSTAT.SELMSR.TTL', 'MSRSTAT.SELMSR.TEXT', $scope.selectedMeasures, blocks, {
        blocks: true,
        style: 1
      }).result.then(function (res) {
        $scope.selectedMeasures = res;
      })
    }, AlertService.showRESTError);
  };

  $scope.addChecksStep1 = function () {
    // Calling route MISC_8
    Restangular.all('devicetypes').getList({formodelsel: true}).then(function (dtlist) {
      var blocks = dtlist;
      _.each(blocks, function (dtype) {
        _.each(dtype.models, function (model) {
          model.id = model.realid;
        });
        dtype.items = dtype.models;
      });
      ModalEditorService.showSinglePicker('MSRSTAT.SELCHK.TTL', 'MSRSTAT.SELCHK.S1TEXT', null, blocks, {
        blocks: true,
        style: 1,
        uncheckable: true
      }).result.then(function (res) {
        if (_.isNull(res)) {
          return;
        }
        $scope.addChecksStep2(res);
      });
    }, AlertService.showRESTError);
  };

  $scope.addChecksStep2 = function (modelid) {
    ModalEditorService.showTextLineEditor('MSRSTAT.SELCHK.TTL', 'MSRSTAT.SELCHK.S2TEXT', null, null).result.then(function (res) {
      if (_.isNull(res) || res == "") {
        return;
      }
      // Calling route CHECK_9
      Restangular.one('procedures', $scope.proc.realid).one('models', modelid).all('checks').getList({search: res}).then(function (ulist) {
        var blocks = ulist;
        if (blocks.length < 1) {
          AlertService.createErrorMessage("MSRSTAT.SELCHK.ERROR.TTL", "MSRSTAT.SELCHK.ERROR.NORES", {}).show()
          return;
        }
        if (_.last(blocks).num == 25) {
          AlertService.createAlertMessage("MSRSTAT.SELCHK.ERROR.TTL", "MSRSTAT.SELCHK.ERROR.TOOMANY", {}).show().result.then(function () {
            $scope.addChecksStep3(modelid, blocks);
          })
        } else {
          $scope.addChecksStep3(modelid, blocks);
        }
      }, AlertService.showRESTError);
    })
  };

  $scope.addChecksStep3 = function (modelid, blocks) {
    _.each(blocks, function (unit) {
      _.each(unit.checks, function (check) {
        check.name = check.id + " - " + i18n.translate(check.checktype.title);
      });
      unit.items = unit.checks;
      unit.name = unit.code + " (" + unit.customer + ")";
    });
    ModalEditorService.showMultiPicker('MSRSTAT.SELCHK.TTL', 'MSRSTAT.SELCHK.S3TEXT', null, blocks, {
      blocks: true,
      style: 3
    }).result.then(function (res) {
      _.each(blocks, function (unit) {
        _.each(unit.checks, function (check) {
          if (_.includes(res, check.id)) {
            if ($scope.selectedChecks.length >= 30) {
              AlertService.createErrorMessage("MSRSTAT.SELCHK.ERROR.TTL", "MSRSTAT.SELCHK.ERROR.LIMIT", {}).show();
              return;
            }
            $scope.selectedChecks.push({
              id: check.id,
              mcode: check.model.code,
              ucode: check.unit.code,
              ctcode: check.checktype.code
            });
          }
        });
      });
    });
  };

  $scope.addChecks = function () {
    $scope.addChecksStep1();
  };

  $scope.reuseMeasures = function () {
    ModalEditorService.showTextLineEditor('MSRSTAT.SELMSR.TTL', 'MSRSTAT.SELMSR.RUTEXT', null, null).result.then(function (res) {
      // Calling route PSM_10
      Restangular.one('procedures', $scope.proc.id).all('checkmeasureids').post({ids: res}).then(function (idlist) {
        $scope.selectedMeasures = idlist.valid_ids;
      });
    });
  };

  $scope.reuseChecks = function () {
    ModalEditorService.showTextLineEditor('MSRSTAT.SELCHK.TTL', 'MSRSTAT.SELCHK.RUTEXT', null, null).result.then(function (res) {
      // Calling route PSM_11
      Restangular.one('procedures', $scope.proc.id).all('checkcheckids').post({ids: res}).then(function (idlist) {
        $scope.selectedChecks = idlist.valid_ids;
      });
    });
  };

  $scope.removeCheck = function (idx) {
    $scope.selectedChecks.splice(idx, 1);
  };

  $scope.okpdf = function () {
    var measures = $scope.selectedMeasures.join("; ");
    var checks = _.map($scope.selectedChecks, function (check) {
      return check.id;
    }).join("; ");
    var tzoff = new Date().getTimezoneOffset();
    var query = "?lang=" + i18n.selectedLanguage.code + "&measures=" + encodeURIComponent(measures) + "&checks=" + encodeURIComponent(checks) + "&tzoff=" + tzoff
    $window.open("/servlet/pdf/msrstatistics/" + $scope.proc.realid + query);
    var finScr = AlertService.createConfirmMessage("MSRSTAT.SELCHK.SUCCESS.TTL", "MSRSTAT.SELCHK.SUCCESS.TEXT", {})
    finScr.addReason("MSRSTAT.SELCHK.SUCCESS.MSTR", {cstr: measures});
    finScr.addReason("MSRSTAT.SELCHK.SUCCESS.CSTR", {cstr: checks});
    finScr.show().result.then(function () {
      $modalInstance.close();
    });
  };

  $scope.okfile = function () {
    var genoptions = [
      {id: 'csv', name: 'CHECK.MAKEFILE.CSV'},
      {id: 'json', name: 'CHECK.MAKEFILE.JSON'},
      {id: 'xml', name: 'CHECK.MAKEFILE.XML'}
    ];
    ModalEditorService.showSinglePicker("CHECK.MAKEFILE.TTL", "CHECK.MAKEFILE.TEXT", null, genoptions).result.then(function (res) {
      var measures = $scope.selectedMeasures.join("; ");
      var checks = _.map($scope.selectedChecks, function (check) {
        return check.id;
      }).join("; ");
      var query = "?lang=" + i18n.selectedLanguage.code + "&measures=" + encodeURIComponent(measures) + "&checks=" + encodeURIComponent(checks) + "&format=" + res
      $window.open("/servlet/export/msrstatistics/" + $scope.proc.realid + query);
      var finScr = AlertService.createConfirmMessage("MSRSTAT.SELCHK.SUCCESS.TTL", "MSRSTAT.SELCHK.SUCCESS.TEXT", {});
      finScr.addReason("MSRSTAT.SELCHK.SUCCESS.MSTR", {cstr: measures});
      finScr.addReason("MSRSTAT.SELCHK.SUCCESS.CSTR", {cstr: checks});
      finScr.show().result.then(function () {
        $modalInstance.close();
      });
    });
  };

  $scope.cancel = function () {
    $modalInstance.dismiss();
  };

});