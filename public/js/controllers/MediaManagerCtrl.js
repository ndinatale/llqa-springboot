cmod.controller('MediaManagerCtrl', function ($modalInstance, $rootScope, $location, $anchorScroll, $scope, $window, owner, subtitle, FileUploader, ModalEditorService, AlertService, Restangular, MediaService) {
  $scope.subtitle = subtitle;

  $scope.sprintf = sprintf;

  $scope.$on("$destroy", function () {
    $scope.killZoomImage();
  });

  $scope.sublistSortFinalize = function (sublist, type, callback) {
    var ret = sublist.map(function (item) {
      return item.cnt;
    });
    // Calling route MEDIA_3(type='images'),MEDIA_7('docs')
    Restangular.all('media').one(type, sublist[0].id).all('action_reorder').post({neworder: ret}).then(function success(response) {
      callback();
    }, AlertService.showRESTError);
  };

  $scope.close = function () {
    $scope.killZoomImage();
    $modalInstance.close(null);
  };

  // --------------
  // IMAGES
  // --------------

  $scope.imageInfo = 0;
  $scope.selectedImage = null;
  $scope.selectedZoomImage = null;
  $scope.refreshImageData = function () {
    var selImageId = _.isNull($scope.selectedImage) ? -1 : $scope.selectedImage.id;
    $scope.disableZoom();
    // Calling route MEDIA_1
    Restangular.all('media').one(owner.type, owner.id).all('images').getList().then(function (medialist) {
      var cnt = 0;
      _.each(medialist, function (val) {
        val.title = (_.isNull(val.caption) || val.caption === "") ? val.fullimage.original_filename : val.caption
        cnt += 1;
        val.cnt = cnt;
      });
      $scope.imagelist = medialist;
      var selImage = null;
      _.each($scope.imagelist, function (image) {
        if (image.id == selImageId) {
          selImage = image;
        }
      });
      if (!_.isNull(selImage)) $scope.selectImage(selImage, -1);
      $scope.mmImageLoaded = true;
    }, AlertService.showSevereRESTError);
  };

  $scope.zoomableImageOptions = {
    zoomType: "window",
    zoomWindowPosition: 2,
    zoomWindowOffetx: 5,
    borderColour: '#005499',
    scrollZoom: true,
    zoomContainerZLevel: 1055
  };

  $scope.selectImage = function (image, where) {
    $scope.selectedImage = image;
    $scope.enableZoom();
    if (where == 0) {
      $location.hash('IMGTN' + image.id);
      $anchorScroll();
    }
    if (where == 1) {
      $location.hash('IMGROW' + image.id);
      $anchorScroll();
    }
  };

  $scope.enableZoom = function () {
    if (!_.isNull($scope.selectedImage)) {
      $scope.selectedZoomImage = "/media/" + $scope.selectedImage.fullimage.filename;
    }
  };

  $scope.disableZoom = function () {
    if (!_.isNull($scope.selectedImage)) {
      $scope.selectedZoomImage = null;
    }
  };

  $scope.toggleImageInfo = function () {
    $scope.imageInfo += 1;
    if ($scope.imageInfo == 2) {
      $scope.imageInfo = 0;
    }
  };

  $scope.imageSortOptions = {
    axis: 'y',
    placeholder: 'll-draggeditem',
    handle: '.ll-dragaction',
    stop: function (event, ui) {
      $scope.sublistSortFinalize($scope.imagelist, 'images', $scope.refreshImageData);
    }
  };

  $scope.editCaption = function (image) {
    ModalEditorService.showTextLineEditor("MEDIAMGR.IMAGE.EDIT.CAPTION.TITLE", "MEDIAMGR.IMAGE.EDIT.CAPTION.TEXT", image.caption).result.then(function (val) {
      image.caption = val;
      // Calling route MEDIA_4
      Restangular.all('media').all("images").post(image).then(function () {
        $scope.refreshImageData();
      }, AlertService.showRESTError);
    })
  };

  $scope.removeImage = function (image) {
    var msg = AlertService.createConfirmMessage("MEDIAMGR.IMAGE.ALERT.CONFHEADER", "MEDIAMGR.IMAGE.ALERT.DELETE", {fname: image.fullimage.original_filename});
    msg.show().result.then(function () {
      // Calling route MEDIA_2
      Restangular.all('media').one('images', image.id).remove().then(function () {
        $scope.refreshImageData();
      }, AlertService.showRESTError);
    });
  };

  $scope.killZoomImage = function () {
    MediaService.killZoomImage('zoomimg');
  };

  $scope.downloadImage = function (img) {
    $window.open("/servlet/mediadownload/image/" + img.id);
  };

  // --------------
  // DOCUMENTS/VIDEOS
  // --------------

  $scope.docInfo = 0;
  $scope.refreshDocData = function () {
    // Calling route MEDIA_5
    Restangular.all('media').one(owner.type, owner.id).all('docs').getList().then(function (medialist) {
      var cnt = 0;
      _.each(medialist, function (val) {
        val.title = (_.isNull(val.caption) || val.caption === "") ? val.binaryfile.original_filename : val.caption;
        cnt += 1;
        val.cnt = cnt;
      });
      $scope.doclist = medialist;
      $scope.mmDocLoaded = true;
    }, AlertService.showSevereRESTError);
  };

  $scope.toggleDocInfo = function () {
    $scope.docInfo += 1;
    if ($scope.docInfo == 2) $scope.docInfo = 0;
  };

  $scope.docSortOptions = {
    axis: 'y',
    placeholder: 'll-draggeditem',
    handle: '.ll-dragaction',
    stop: function (event, ui) {
      $scope.sublistSortFinalize($scope.doclist, 'docs', $scope.refreshDocData);
    }
  };

  $scope.editDocCaption = function (doc) {
    ModalEditorService.showTextLineEditor("MEDIAMGR.DOC.EDIT.CAPTION.TITLE", "MEDIAMGR.DOC.EDIT.CAPTION.TEXT", doc.caption).result.then(function (val) {
      doc.caption = val;
      // Calling route MEDIA_8
      Restangular.all('media').all("docs").post(doc).then(function () {
        $scope.refreshDocData();
      }, AlertService.showRESTError);
    });
  };

  $scope.removeDoc = function (doc) {
    var msg = AlertService.createConfirmMessage("MEDIAMGR.DOC.ALERT.CONFHEADER", "MEDIAMGR.DOC.ALERT.DELETE", {fname: doc.binaryfile.original_filename})
    msg.show().result.then(function () {
      // Calling route MEDIA_6
      Restangular.all('media').one('docs', doc.id).remove().then(function () {
        $scope.refreshDocData();
      }, AlertService.showRESTError);
    });
  };

  $scope.viewDoc = function (doc) {
    if (doc.doctype == 2) {
      MediaService.openVideoPlayer(doc);
    }
    if (doc.doctype == 1) {
      MediaService.openPdfViewer(doc)
    }
  };

  $scope.downloadDoc = function (doc) {
    $window.open("/servlet/mediadownload/doc/" + doc.id);
  };


  // --------------
  // UPLOADER
  // --------------

  $scope.filetypes = {
    'image/jpeg': {maxmb: 5, icon: 'fa-image'},
    'image/png': {maxmb: 5, icon: 'fa-image'},
    'image/tiff': {maxmb: 5, icon: 'fa-image'},
    'application/pdf': {maxmb: 10, icon: 'fa-file-text-o'},
    'text/plain': {maxmb: 1, icon: 'fa-file-text-o'},
    'video/mp4': {maxmb: 40, icon: 'fa-video-camera'}
    //'video/quicktime': { maxmb: 40, icon: 'fa-video-camera' },
    //'video/avi'      : { maxmb: 40, icon: 'fa-video-camera' },
    //'video/mkv'      : { maxmb: 40, icon: 'fa-video-camera' }
  };

  var uploader = $scope.uploader = new FileUploader({
    scope: $scope,
    removeAfterUpload: false,
    autoUpload: false,
    filters: [{
      fn: function (item) {
        var maxsize = undefined;
        if ($scope.filetypes[item.type]) maxsize = $scope.filetypes[item.type].maxmb;
        if (_.isUndefined(maxsize)) {
          AlertService.createErrorMessage("MEDIAMGR.UPLOAD.FILTER.TITLE", "MEDIAMGR.UPLOAD.FILTER.MESSAGE", {filename: item.name}).addReason("MEDIAMGR.UPLOAD.FILTER.UNSUPPORTED", {type: item.type}).show();
          return false;
        }
        var filesize = Math.floor(item.size / (1024 * 1024));
        if (filesize > maxsize) {
          AlertService.createErrorMessage("MEDIAMGR.UPLOAD.FILTER.TITLE", "MEDIAMGR.UPLOAD.FILTER.MESSAGE", {filename: item.name}).addReason("MEDIAMGR.UPLOAD.FILTER.TOOBIG", {
            has: filesize,
            max: maxsize
          }).show();
          return false;
        }
        return true;
      }
    }]
  });

  uploader.onAfterAddingFile = function (item) {
    item.resterror = null;
    item.url = '/api/media/' + owner.type + '/' + owner.id + '/upload';
  };

  uploader.onBeforeUploadItem = function (item) {
  };

  uploader.onProgressAll = function (progress) {
  };

  uploader.onCompleteItem = function (item, response) {
    if (response.status === "success") {
    } else {
      item.resterror = response.type;
      item.resterrortext = response.messages.msg;
      item.resterrorvals = response.messages.vals;
    }
  };

  $scope.itemStatus = function (item) {
    if (item.isUploading) return 2;
    if (item.isReady) return 1;
    if (item.isSuccess && _.isNull(item.resterror)) return 4;
    if (item.isSuccess && !_.isNull(item.resterror)) return 5;
    if (item.isCancel) return 3;
    if (item.isError) return 6;
    return 0;
  };

  $scope.filetypeIcon = function (filetype) {
    return 'fa ' + $scope.filetypes[filetype].icon;
  };

  $scope.actionIcon = function (item) {
    return 'fa ' + ['fa-question', 'fa-clock-o', 'fa-spinner fa-spin', 'fa-ban', 'fa-check', 'fa-warning', 'fa-warning'][$scope.itemStatus(item)];
  };

  $scope.uploadsReady = function () {
    return $scope.uploader.getNotUploadedItems().length > 0;
  };

  $scope.uploadsFinished = function () {
    var ret = false;
    _.each($scope.uploader.queue, function (item) {
      if ($scope.itemStatus(item) > 2) {
        ret = true;
      }
    });
    return ret;
  };

  $scope.startAllUploads = function () {
    $scope.uploader.uploadAll();
  };

  $scope.removeAllFinishedUploads = function () {
    var remFromQueue = [];
    _.each($scope.uploader.queue, function (item) {
      if ($scope.itemStatus(item) > 2) {
        remFromQueue.push(item);
      }
    });
    _.each(remFromQueue, function (item) {
      item.remove();
    });
  };
});