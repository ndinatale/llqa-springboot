cmod.controller('ModalEditorCtrl', function ($scope, $modalInstance, $document, type, title, text, value, elems, params, KeyboardShortcutService, i18n, ModalEditorService) {
  $scope.type = type;
  $scope.title = title;
  $scope.text = text;
  $scope.value = value;
  $scope.elems = elems;
  $scope.params = params;
  $scope.languageinfo = {
    selected: i18n.defaultLanguage.code,
    languages: i18n.languages,
    select: function (lang) {
      $scope.languageinfo.selected = $scope.languageinfo.languages[lang].code;
    }
  };

  $scope.ok = function () {
    if (type == 1 || type == 2) {
      $modalInstance.close($scope.value);
      return;
    }
    if (type == 3) {
      if (!_.isUndefined($scope.value) && !_.isNull($scope.value)) {
        $scope.value.setHours(12);
      }
      $modalInstance.close($scope.value);
      return;
    }
    if (type == 5) {
      var result = null;
      _.each($scope.elems, function (block) {
        _.each(block.items, function (item) {
          if (item.use) {
            result = item.id;
          }
        })
      });
      $modalInstance.close(result);
      return
    }
    if (type == 6) {
      var result = [];
      _.each($scope.elems, function (block) {
        _.each(block.items, function (item) {
          if (item.use) {
            result.push(item.id);
          }
        })
      });
      $modalInstance.close(result);
      return;
    }
    if (type == 7) {
      $modalInstance.close({value: $scope.value, sub_values: $scope.elems});
      return;
    }
    $modalInstance.dismiss();
  };
  $scope.reset = function () {
    $modalInstance.close(null);
  };
  $scope.cancel = function () {
    $modalInstance.dismiss();
  };

  $scope.quickcloseKeymap = {13: 'quickclose($event)'};
  $scope.quickclose = function (ev) {
    ev.preventDefault();
    $scope.ok();
  };


  // Multipicker

  $scope.getUseIcon = function (elem) {
    if ($scope.params.single) {
      if (elem.isblock) {
        return 'fa fa-xxx';
      }
      if (!elem.use) {
        return 'fa fa-circle-o';
      }
      return 'fa fa-circle'
    }
    if (elem.isblock) {
      if (elem.usecnt == 0) {
        return 'fa fa-star-o';
      }
      if (elem.usecnt < elem.items.length) {
        return 'fa fa-star-half-o';
      }
      return 'fa fa-star';
    }
    if (!elem.use) {
      return 'fa fa-square-o';
    }
    return 'fa fa-square';
  };

  $scope.collapse = function (block) {
    block.collapsed = !block.collapsed && block.usecnt == 0 && $scope.params.blocks;
  };

  $scope.toggleUse = function (elem, event) {
    if (!_.isUndefined(event)) {
      event.preventDefault();
      event.stopPropagation();
    }
    var uncheck = function (item) {
      $scope.countpicks -= 1;
      item.block.usecnt -= 1;
      item.use = false;
    };
    var check = function (item) {
      $scope.countpicks += 1;
      item.block.usecnt += 1;
      item.block.collapsed = false;
      item.use = true;
    };
    if (elem.isblock) {
      if (elem.usecnt < elem.items.length) {
        _.each(elem.items, function (item) {
          if (!item.use) {
            $scope.toggleUse(item);
          }
        })
      } else {
        _.each(elem.items, function (item) {
          if (item.use) {
            $scope.toggleUse(item);
          }
        })
      }
      return;
    }
    if ($scope.params.single) {
      var waschecked = elem.use;
      _.each($scope.elems, function (block) {
        _.each(block.items, function (item) {
          if (item.use) {
            uncheck(item);
          }
        })
      });
      if (waschecked && $scope.params.uncheckable) {
        return;
      }
      check(elem);
      return;
    }
    if (elem.use) {
      uncheck(elem);
    } else {
      if (!$scope.params.maxsel || $scope.params.maxsel > $scope.countpicks) {
        check(elem);
      }
    }
  };

  $scope.procAll = function (dosel) {
    _.forEach($scope.elems, function (block) {
      _.forEach(block.items, function (item) {
        if (dosel && !item.use) {
          $scope.countpicks += 1;
          item.block.usecnt += 1;
          item.block.collapsed = false;
          item.use = true;
        }
        if (!dosel && item.use) {
          $scope.countpicks -= 1;
          item.block.usecnt -= 1;
          item.block.collapsed = $scope.params.blocks;
          item.use = false;
        }
      })
    })
  };

  $scope.countpicks = 0;
  var globalshortcuts = {
    13: $scope.ok
  };

  function keySelect(goingdown) {
    var last = undefined, next = undefined, current = undefined;
    _.each($scope.elems, function (block) {
      _.each(block.items, function (item) {
        if (item.use) {
          current = item;
        } else {
          if (_.isUndefined(current)) {
            last = item;
          } else {
            if (_.isUndefined(next)) {
              next = item;
            }
          }
        }
      })
    });
    if (goingdown && !_.isUndefined(current) && !_.isUndefined(next)) {
      $scope.toggleUse(next);
    }
    if (!goingdown && !_.isUndefined(current) && !_.isUndefined(last)) {
      $scope.toggleUse(last);
    }
  }

  if ($scope.type == 5 || $scope.type == 6) {
    if (!$scope.params.blocks) {
      $scope.elems = [{items: $scope.elems}];
    }
    var firstitem = null;
    _.each($scope.elems, function (block) {
      block.isblock = true;
      block.usecnt = 0;
      _.each(block.items, function (item) {
        if (!firstitem) firstitem = item;
        item.use = false;
        if (_.includes($scope.value, item.id)) {
          $scope.countpicks += 1;
          item.use = true;
          block.usecnt += 1;
        }
        item.block = block;
      });
      block.collapsed = block.usecnt == 0 && $scope.params.blocks;
    });
    // TODO preventing bug where firstitem is null (first measure in step, has a tool)
    // Why can this be zero? Are there no tool units for choosing?
    if ($scope.type == 5 && !$scope.params.uncheckable && $scope.countpicks == 0 && firstitem != null) {
      $scope.toggleUse(firstitem);
    }
    if (!$scope.params.blocks && $scope.params.single) {
      globalshortcuts[40] = function () {
        keySelect(true);
      };
      globalshortcuts[38] = function () {
        keySelect(false);
      };
      globalshortcuts[39] = function () {
        keySelect(true);
      };
      globalshortcuts[37] = function () {
        keySelect(false);
      }
    }
  }

  // settings editor
  $scope.removeTagValue = function (elem) {
    _.remove($scope.elems, function (e) {
      return elem === e;
    });
  };

  $scope.addTagValue = function () {
    ModalEditorService.showTextLineEditor('SETTINGS.PROC.NEWTAGVALUE.TITLE', '', null, null).result.then(function (result) {
      if (!$scope.elems) {
        $scope.elems = [];
      }
      $scope.elems.push(result);
    });
  };

  $scope.editTagValue = function (value, index) {
    ModalEditorService.showTextLineEditor('SETTINGS.PROC.EDITTAGVALUE.TITLE', '', value, null).result.then(function (result) {
      $scope.elems.splice(index, 1);
      $scope.elems.push(result);
    });
  };

  KeyboardShortcutService.installKeyboardShortcuts($scope, undefined, globalshortcuts);
});
