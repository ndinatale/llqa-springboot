cmod.controller('ModelEditCtrl', function ($window, $rootScope, $scope, $modal, $stateParams, $location, i18n, Restangular, UsersService, VersioningService, AlertService, EditorManagementService) {
  EditorManagementService.installEditorManagement($scope, 'models', '/models')

  $window.rangy.init();

  $scope.$on("$destroy", function () {
    $modal.closeall();
  });

  $scope.refreshData = function () {
    // Calling route MISC_8
    Restangular.all('devicetypes').getList().then(function dtypeData(response) {
      $scope.devicetypes = response;
      if ($stateParams.modid === "new") {
        $rootScope.selecteditem = null;
        var model = {code: "", devicetype_id: -1};
        $scope.fillupLang(model, 'title');
        $scope.fillupLang(model, 'description');
        $scope.editor.title = "MODEL.EDITOR.TITLE.NEW";
        $scope.item = model;
        $scope.initCodeCheck(null);
        $scope.modelEditLoaded = true;
      } else {
        // Calling route MU_3
        Restangular.one('models', $stateParams.modid).get({foredit: true}).then(function modelData(response) {
          var model = response;
          $rootScope.selecteditem = model;
          $scope.fillupLang(model, 'title');
          $scope.fillupLang(model, 'description');
          $scope.editor.title = "MODEL.EDITOR.TITLE.EDIT";
          $scope.editor.titlevals = $scope.makeTranslateVals(model.title, 'modelname');
          if (model.version != null) {
            $scope.editor.subtitle = VersioningService.versionName(model.version, model.versions);
          }
          $scope.item = model;
          $scope.initCodeCheck($scope.item.code);
          $scope.modelEditLoaded = true;
        }, AlertService.showSevereRESTError);
      }
    }, AlertService.showSevereRESTError);
  };

  UsersService.addGrantMethods($scope).then(function () {
    $scope.refreshData();
    $('#md_code').focus();
  });

  $scope.devicetypeCboxName = function (dtype) {
    return dtype.code + ': ' + i18n.translate(dtype.title);
  };

});