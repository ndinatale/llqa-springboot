cmod.controller('ModelListCtrl', function ($rootScope, $modal, $location, FileUploader, $scope, i18n, Restangular, UsersService, AlertService, ListManagementService, UploaderService, ViewerManagementService) {
  ListManagementService.installListManagement($scope, 'model_filters');
  ViewerManagementService.installViewerManagement($scope, null, 'model', '/models');

  $scope.$on("$destroy", function () {
    $modal.closeall();
  });

  $scope.refreshListData = function () {
    // Calling route MU_1
    Restangular.all('models').getList().then(function modelList(response) {
      var models = response;
      $scope.realidstatus = ListManagementService.calcRealidStatus(models);
      $scope.items = models;
      $scope.modelListLoaded = true;
    }, AlertService.showSevereRESTError);
  };

  $scope.goto = function (where) {
    $location.url("/app/models/view/" + where);
  };

  $scope.newModel = function () {
    $location.url("/app/models/edit/new");
  };

  UsersService.addGrantMethods($scope).then(function () {
    $scope.loadFilterSettings();
    $scope.refreshListData();
  });

  UploaderService.installImportUploader($scope, "models", "/models/view/", null);

  $scope.gotoUnits = function (modid, event) {
    if (!_.isNull(event)) event.stopPropagation();
    $location.url("/app/units/" + modid);
  };
});