cmod.controller('ModelViewCtrl', function ($rootScope, $modal, $scope, $location, $window, $filter, i18n, $stateParams,
                                           Restangular, ConstantService, MediaService, VersioningService, AlertService,
                                           UsersService, RestService, ModalEditorService, ViewerManagementService,
                                           ListManagementService, ChangeLogService, featureFlags) {

  var C = {},
    CUSTOM_TAGS_FEATURE = 'customTags';
  ConstantService.installConstants($scope, C, 'PSM_');

  ViewerManagementService.installViewerManagement($scope, null, 'model', '/models');
  ListManagementService.installReorderableSublistManagement($scope, 'activeprocedures');

  $scope.$on("$destroy", function () {
    $modal.closeall()
  });

  $scope.refreshData = function () {
    // Calling route MU_3
    Restangular.one('models', $stateParams.modid).get().then(function modData(response) {
      var model = response;
      model.versname = VersioningService.versionName(model.version, model.versions);
      ListManagementService.prepareSublist($scope, model.activeprocedures);
      ListManagementService.setSelectedItem(model);
      model.activeprocedures.forEach(function (val) {
        val.procedure.versname = VersioningService.versionName(val.procedure.version, null)
      });
      $scope.item = model;
      if ($scope.isVersion()) $scope.activateReordering(false);
      $scope.modelViewLoaded = true
    }, AlertService.showSevereRESTError)
  };

  $scope.gotoProcedure = function (aproc) {
    $location.url("/app/procedures/view/" + aproc.procedure.id)
  };

  $scope.gotoConfigtable = function (id) {
    $location.url("/app/configmgt/view/" + id);
  };

  UsersService.addGrantMethods($scope).then(function () {
    $scope.refreshData()
  });

  $scope.addProcedure = function () {
    $scope.wrapEditWarning(function () {
      var tagfilter = UsersService.getUserInfo().proc_filters.tagfilter;
      // Calling route PSM_1
      Restangular.all('procedures').getList({selector: true}).then(function dtypeData(response) {
        var haveprocs = _.map($scope.item.activeprocedures, function (val) {
          return val.procedure.realid
        });
        var procedures = [];
        _.each(response, function (val) {
          if (!_.includes(haveprocs, val.realid)) {
            procedures.push(val);
            val.use = false
          }
        });
        if (featureFlags.isOn(CUSTOM_TAGS_FEATURE) && tagfilter) {
          procedures = $filter('filter')(procedures, tagfilter, function (expected, actual) {
            return actual.indexOf(expected) > -1;
          });
        }
        ModalEditorService.showMultiPicker("MODEL.VIEW.ADDPROC.TITLE", "MODEL.VIEW.ADDPROC.TEXT", null, procedures, {style: 1}).result.then(function (res) {
          // Calling route MU_14
          Restangular.one('models', $scope.item.id).all('activeprocedures').post(res).then(function () {
            $scope.refreshListData();
            $scope.refreshData()
          }, AlertService.showRESTError)
        })
      })
    }, true)
  };
  $scope.removeProcedure = function (aproc) {
    $scope.wrapEditWarning(function () {
      // Calling route MU_12
      Restangular.one('activeprocedures', aproc.id).remove().then(function () {
        $scope.refreshListData();
        $scope.refreshData()
      }, AlertService.showRESTError)
    }, true)
  };
  $scope.updateProcedure = function (aproc) {
    $scope.wrapEditWarning(function () {
      aproc.procedure_id = aproc.procedure.vinfo.latest_id;
      // Calling route MU_15
      Restangular.all('activeprocedures').post(aproc).then(function (ret) {
        $scope.refreshListData();
        $scope.refreshData()
      }, AlertService.showRESTError)
    }, true)
  };

  $scope.addChecktype = function () {
    $scope.wrapEditWarning(function () {
      // Calling route MISC_11
      Restangular.all('checktypes').getList().then(function ctypeData(response) {
        var havectypes = _.map($scope.item.activechecktypes, function (val) {
          return val.checktype.id
        });
        var checktypes = [];
        _.each(response, function (val) {
          if (!_.includes(havectypes, val.id)) {
            checktypes.push(val);
            val.use = false
          }
        });
        ModalEditorService.showMultiPicker("MODEL.VIEW.ADDCTYPE.TITLE", "MODEL.VIEW.ADDCTYPE.TEXT", null, checktypes, {style: 1}).result.then(function (res) {
          // Calling route MU_16
          Restangular.one('models', $scope.item.id).all('activechecktypes').post(res).then(function () {
            $scope.refreshListData();
            $scope.refreshData()
          }, AlertService.showRESTError)
        })
      })
    }, true)
  };
  $scope.removeChecktype = function (actype) {
    $scope.wrapEditWarning(function () {
      // Calling route MU_13
      Restangular.one('activechecktypes', actype.id).remove().then(function () {
        $scope.refreshListData();
        $scope.refreshData()
      }, AlertService.showRESTError)
    }, true)
  };

  $scope.deleteItem = function () {
    $scope.wrapEditWarning(function () {
      var msg = AlertService.createConfirmMessage("MODEL.ALERT.CONFHEADER", "MODEL.ALERT.DELETE", {code: $scope.item.code});
      if ($scope.isVersion()) msg.addReason("MODEL.ALERT.DELETEVERSION", {});
      msg.show().result.then(function () {
        // Calling route MU_5
        Restangular.one('models', $scope.item.id).remove().then(function () {
          $scope.refreshListData();
          $location.url("/app/models")
        }, AlertService.showRESTError)
      })
    }, true);
  };

  $scope.finalizeItem = function () {
    var msg = AlertService.createConfirmMessage("MODEL.ALERT.CONFHEADER", "MODEL.ALERT.FINALIZE", {code: $scope.item.code});
    msg.addReason("MODEL.ALERT.FINALIZEDETAIL", {});
    msg.show().result.then(function () {
      // Calling route MU_7
      Restangular.one('models', $scope.item.id).one('action_finalize').get().then(function (result) {
        $scope.refreshListData();
        $location.url("/app/models/view/" + result.trunk_id)
      }, AlertService.showRESTError)
    })
  };

  $scope.resetChanges = function () {
    var msg = AlertService.createConfirmMessage("MODEL.ALERT.CONFHEADER", "MODEL.ALERT.RESET.TITLE", {code: $scope.item.code});
    msg.addReason("MODEL.ALERT.RESET.TEXT", {});
    msg.show().result.then(function () {
      // Calling route MU_8
      Restangular.one('models', $scope.item.id).one('action_reset').get().then(function (result) {
        $scope.refreshListData();
        $location.url("/app/models/view/" + result.trunk_id)
      }, AlertService.showRESTError)
    })
  };

  $scope.mediamanager = function () {
    $scope.wrapEditWarning(function () {
      MediaService.openMediaManager("Model", $scope.item.id, "STEP.MODEL.MEDIAMANAGER", {
        mcode: $scope.item.code, mtitle: i18n.translate($scope.item.title)
      }).result.then(function () {
        $scope.refreshListData();
        $scope.refreshData()
      })
    })
  };

  $scope.testVersion = function () {
    var vlist = [];
    _.each($scope.item.activechecktypes, function (val) {
      vlist.push({id: val.checktype.id, title: val.checktype.title})
    });
    ModalEditorService.showSinglePicker("MODEL.TESTCHECK.TITLE", "MODEL.TESTCHECK.TEXT", null, vlist).result.then(function (selv) {
      // Calling route CHECK_12
      Restangular.all("testrun").one("model", $scope.item.id).one("ctype", selv).get().then(function (tcheck) {
        $location.url("/app/workflow/" + tcheck.id)
      }, AlertService.showRESTError)
    })
  };

  $scope.showChangeLog = function () {
    ChangeLogService.showChangeLog($scope.item, 'models')
  };

  $scope.preallocateProcedure = function (aproc) {
    $scope.wrapEditWarning(function () {
      var checktypes = [];
      _.each($scope.item.activechecktypes, function (act) {
        var code = '';
        if (aproc.preallocation.status > 0) {
          var prealloc = aproc.preallocation.allocations[act.checktype.id];
          if (!_.isUndefined(prealloc) && !_.isNull(prealloc)) {
            if (prealloc.user_id >= 0) {
              code = aproc.preallocation.userlist[prealloc.user_id]
            } else if (prealloc.group_id >= 0) {
              code = $filter('translate')('MODEL.VIEW.PREALLOC.GROUP') + " " + aproc.preallocation.grouplist[prealloc.group_id]
            } else {
              code = $filter('translate')('MODEL.VIEW.PREALLOC.ANY')
            }
          }
        }
        checktypes.push({id: act.checktype.id, title: act.checktype.title, code: code})
      });

      ModalEditorService.showMultiPicker("MODEL.VIEW.PREALLOC.TITLE", "MODEL.VIEW.PREALLOC.TEXT1", null, checktypes, {style: 2}).result.then(function (ctypes) {
        if (ctypes.length == 0) return;
        UsersService.showUserPicker("MODEL.VIEW.PREALLOC.TITLE", "MODEL.VIEW.PREALLOC.TEXT2", -1, -1, true, true, true).result.then(function (seluser) {
          seluser.checktypes = ctypes;
          seluser.proc_rid = aproc.procedure.realid;
          // Calling route MU_10
          Restangular.one('models', $scope.item.id).all('preallocate').post(seluser).then(function () {
            $scope.refreshListData();
            $scope.refreshData()
          })
        })
      })
    }, true)
  };

  $scope.setEnforceLevel = function (aproc, newlevel) {
    $scope.wrapEditWarning(function () {
      // Calling route PSM_23
      Restangular.all('activeprocedures').post({id: aproc.id, enforce: newlevel}).then(function (ret) {
        $scope.refreshListData();
        $scope.refreshData();
      }, AlertService.showRESTError);
    }, true);
  };

  $scope.linkConfigtable = function () {
    $scope.wrapEditWarning(function () {
      Restangular.all('configtables').getList().then(function ctypeData(response) {
        var configtables = [];
        _.each(response, function (val) {
          if (!val.deleted && (val.parent === '' || val.parent == null)) {
            val.code = val.id;
            val.title = {de: val.title, en: val.title, cn: val.title, fr: val.title};
            // filter tables which already have a parent
            configtables.push(val);
          }
        });
        ModalEditorService.showSinglePicker("MODEL.VIEW.ADDCONFIGTABLE.TITLE", "MODEL.VIEW.ADDCONFIGTABLE.TEXT", null, configtables, {style: 2}).result.then(function (tableid) {
          // this will call /api/configtables/:tableid/linktomodel/:modelid
          Restangular.one('configtables', tableid).one('linktomodel', $scope.item.id).put().then(function () {
            $scope.refreshListData();
            $scope.refreshData()
          }, AlertService.showRESTError);
        })
      })
    }, true)
  };

  $scope.unlinkConfigtable = function (tableid) {
    $scope.wrapEditWarning(function () {
      // Maybe ask if user is sure? Although nothing really breaks, and other delete buttons don't have this either...
      Restangular.one('configtables', tableid).all('linktomodel').remove().then(function () {
        $scope.refreshListData();
        $scope.refreshData();
      }, AlertService.showRESTError);
    }, true)
  };

});

