cmod.controller('NoticeManageCtrl', function ($scope, $q, $modalInstance, $rootScope, notice, ModalEditorService, Restangular, AlertService) {

  $scope.notice = notice;
  $scope.status = notice.statusstrings[notice.statusstrings.length - 1].id;

  $scope.close = function () {
    $modalInstance.dismiss();
  };

  $scope.switch = function (from, to) {
    ModalEditorService.showTextAreaEditor("NOTICES.BUTTON.STT_" + from + "" + to, "NOTICES.TEXT.STTCHANGE", "", {}).result.then(function (comment) {
      $scope.notice.comment = comment;
      $scope.notice.status = to;
      Restangular.all('notices').post($scope.notice).then(function (response) {
        $modalInstance.close("status");
      }, AlertService.showRESTError);
    });
  };

  $scope.correctCategory = function () {
    Restangular.one('notices', 'editinfo').get().then(function procList(response) {
      var categories = _.map(response.categories, function (e) {
        e.name = e.text;
        return e;
      });
      ModalEditorService.showSinglePicker("NOTICES.CORRECT.TITLE", "NOTICES.CORRECT.CATEGORY", $scope.notice.category.id, categories, {style: 2}).result.then(function (res) {
        var updblock = {
          id: $scope.notice.id,
          category_id: res
        };
        Restangular.all('notices').post(updblock).then(function (response) {
          $modalInstance.close("correct");
        }, AlertService.showRESTError);
      });
    }, AlertService.showSevereRESTError);
  };

  $scope.correctTimeloss = function () {
    var options = [
      {id: null, name: 'NOTICES.VIEW.NOTEXT'},
      {id: 15, name: 'NOTICES.TIMELOSS.15'},
      {id: 30, name: 'NOTICES.TIMELOSS.30'},
      {id: 60, name: 'NOTICES.TIMELOSS.60'},
      {id: 90, name: 'NOTICES.TIMELOSS.90'},
      {id: 120, name: 'NOTICES.TIMELOSS.120'},
      {id: 180, name: 'NOTICES.TIMELOSS.180'},
      {id: 240, name: 'NOTICES.TIMELOSS.240'}
    ];
    ModalEditorService.showSinglePicker("NOTICES.CORRECT.TITLE", "NOTICES.CORRECT.TIMELOSS", $scope.notice.timeloss, options, {style: 2}).result.then(function (res) {
      var updblock = {
        id: $scope.notice.id,
        timeloss: res
      };
      Restangular.all('notices').post(updblock).then(function (response) {
        $modalInstance.close("correct");
      }, AlertService.showRESTError);
    });
  };

  $scope.correctDescription = function () {
    ModalEditorService.showTextAreaEditor("NOTICES.CORRECT.TITLE", "NOTICES.CORRECT.DESCRIPTION", $scope.notice.text, {}).result.then(function (res) {
      if (res.length < 3) {
        return;
      }
      var updblock = {
        id: $scope.notice.id,
        text: res
      };
      Restangular.all('notices').post(updblock).then(function (response) {
        $modalInstance.close("correct");
      }, AlertService.showRESTError);
    });
  };

  $scope.correctArticle = function () {
    ModalEditorService.showTextLineEditor("NOTICES.CORRECT.TITLE", "NOTICES.CORRECT.ARTICLE1", $scope.notice.artno, {}).result.then(function (res1) {
      ModalEditorService.showTextAreaEditor("NOTICES.CORRECT.TITLE", "NOTICES.CORRECT.ARTICLE2", $scope.notice.artdesc, {}).result.then(function (res2) {
        var updblock = {
          id: $scope.notice.id,
          artno: res1,
          artdesc: res2
        };
        Restangular.all('notices').post(updblock).then(function (response) {
          $modalInstance.close("correct");
        }, AlertService.showRESTError);
      });
    });
  };

});