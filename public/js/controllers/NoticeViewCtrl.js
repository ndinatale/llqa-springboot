cmod.controller('NoticeViewCtrl', function ($rootScope, $modal, $location, $stateParams, $filter, FileUploader, $scope, $window, i18n, VersioningService, Restangular, UsersService, AlertService, ModalEditorService, SnippetEditorService, ListManagementService) {
  ListManagementService.installListManagement($scope, 'usermgt_filters');

  $scope.$on("$destroy", function () {
    $modal.closeall();
  });

  $scope.allnotices = [];
  $scope.notices = [];
  $scope.filters = {
    category: {show: 2, filter: false, filterdata: {}},
    path: {show: 2, filter: false, filterdata: {}},
    text: {show: 2, filter: false, filterdata: {}},
    article: {show: 2, filter: false, filterdata: {}},
    timeloss: {show: 2, filter: false, filterdata: {}},
    status: {show: 1, filter: false, filterdata: {}},
    id: {show: -1, filter: false, filterdata: {}}
  };
  $scope.filterlist = ['id', 'category', 'path', 'text', 'article', 'timeloss', 'status'];
  $scope.filterres = {
    actCategories: []
  };

  $scope.refreshData = function (currNotice) {
    // Calling route MISC_23
    Restangular.all('notices').getList().then(function (response) {
      $scope.filterres.actCategories = [];
      var notices = response;
      _.each(notices, function (notice) {
        notice.articlestring = "";
        if (notice.artno && notice.artno.length > 0) {
          notice.articlestring += "<b>" + notice.artno + "</b>";
        }
        if (notice.artno && notice.artno.length > 0 && notice.artdesc && notice.artdesc.length > 0) {
          notice.articlestring += ": ";
        }
        if (notice.artdesc && notice.artdesc.length > 0) {
          notice.articlestring += notice.artdesc;
        }
        notice.timelossstring = "NOTICES.VIEW.NOTEXT";
        if (notice.timeloss) {
          notice.timelossstring = 'NOTICES.TIMELOSS.' + notice.timeloss;
        }
        notice.statusstrings = [];
        var laststatus = null;
        _.each(notice.timeline, function (statchange) { //new_status, timestamp, user
          var user = {username: '???', realname: '???'};
          if (notice.users && notice.users[statchange.user]) user = notice.users[statchange.user];
          var status = {
            text: "NOTICES.STATUS." + {
              1: "OPEN",
              2: "PROCESSED",
              5: "CLOSED",
              9: "ARCHIVED"
            }[statchange.new_status],
            id: statchange.new_status,
            user: user.username,
            realname: user.realname,
            time: $filter('date')(statchange.timestamp * 1000, i18n.selectedLanguage.tformat, 'utc'),
            comment: statchange.comment,
            show: true
          };
          notice.statusstrings.push(status);
          if (laststatus && laststatus.id != 1) {
            laststatus.show = false;
          }
          laststatus = status;
        });
        notice.pathstrings = [];
        notice.typestring = "NOTICES.PATHTYPE." + notice.path.type.toUpperCase();
        notice.targeturl = "/";
        if (notice.path.url.type === 'workflowstep') {
          notice.targeturl = "/app/workflow/" + notice.path.url.checkid + "/" + notice.path.url.stepnum;
        }
        if (notice.path.url.type === 'workflowintro') {
          notice.targeturl = "/app/workflow/" + notice.path.url.checkid;
        }
        if (notice.pathobj && _.isArray(notice.pathobj)) {
          _.each(notice.pathobj, function (segment) {
            var pstring = segment.obj;
            pstring.text = "NOTICES.SEGMENT." + segment.type.toUpperCase();
            notice.pathstrings.push(pstring);
          });
        }
        notice.hide = false;
        $scope.filterres.actCategories.push(notice.category);
      });
      $scope.allnotices = notices;
      $scope.filterres.actCategories = _.sortBy(_.uniqBy(_.map($scope.filterres.actCategories, function (cat) {
        return {id: cat.id, name: cat.text, seqnum: cat.seqnum};
      }), function (cat) {
        return cat.id;
      }), 'seqnum');
      $scope.recalculateFilters();
      if (currNotice) {
        _.each($scope.allnotices, function (notice) {
          if (notice.id === currNotice.id) {
            $scope.manageNotice(notice);
          }
        });
      }
    }, AlertService.showSevereRESTError);
  };

  UsersService.addGrantMethods($scope).then(function (sdata) {
    $scope.refreshData();
  });

  $scope.toggleShow = function (filtername) {
    if ($scope.filters[filtername].show == 2) {
      $scope.filters[filtername].show = 0;
      $scope.filters[filtername].filter = false;
    } else {
      if (filtername === 'status') {
        $scope.filters[filtername].show += 1;
      }
      else {
        $scope.filters[filtername].show = 2;
      }
    }
    if ($scope.filters.category.show == 0 && $scope.filters.path.show == 0 && $scope.filters.text.show == 0) {
      $scope.filters.category.show = 2;
    }
    if ($scope.filters.article.show == 0 && $scope.filters.timeloss.show == 0 && $scope.filters.status.show == 0) {
      $scope.filters.status.show = 2;
    }
    $scope.recalculateFilters();
  };

  $scope.toggleFilter = function (filtername) {
    if ($scope.filters[filtername].filter) {
      $scope.filters[filtername].filter = false;
      $scope.recalculateFilters();
    } else {
      $scope['editFilter' + _.capitalize(filtername)](function () {
          $scope.filters[filtername].filter = true;
          if ($scope.filters[filtername].show >= 0) {
            $scope.filters[filtername].show = 2;
          }
          $scope.recalculateFilters();
        }
      );
    }
  };

  $scope.recalculateFilters = function () {
    var notices = [];
    _.each($scope.allnotices, function (notice) {
      var hide = false;
      if (!hide && $scope.filters.id.filter && $scope.filters.id.show != 0) {
        var nId = parseInt(notice.id);
        if (nId < $scope.filters.id.filterdata.from || nId > $scope.filters.id.filterdata.to) {
          hide = true;
        }
      }
      if (!hide && $scope.filters.category.filter && $scope.filters.category.show != 0) {
        if (!_.includes($scope.filters.category.filterdata.selected, notice.category.id)) {
          hide = true;
        }
      }
      if (!hide && $scope.filters.path.filter && $scope.filters.path.show != 0) {
        var path = _.map(notice.pathstrings, function (ps) {
          return $filter('translate')(ps.text, ps)
        }).join(', ').replace(/\<[^\>]+\>/g, '');
        if (!_.includes(path.toLowerCase(), $scope.filters.path.filterdata.string.toLowerCase())) {
          hide = true;
        }
      }
      if (!hide && $scope.filters.text.filter && $scope.filters.text.show != 0) {
        if (!_.includes(notice.text.toLowerCase(), $scope.filters.text.filterdata.string.toLowerCase())) {
          hide = true;
        }
      }
      if (!hide && $scope.filters.article.filter && $scope.filters.article.show != 0) {
        if ($scope.filters.article.filterdata.desc && (_.isNull(notice.artdesc) || !_.includes(notice.artdesc.toLowerCase(), $scope.filters.article.filterdata.string.toLowerCase()))) {
          hide = true;
        }
        if (!$scope.filters.article.filterdata.desc && (_.isNull(notice.artno) || !_.includes(notice.artno.toLowerCase(), $scope.filters.article.filterdata.string.toLowerCase()))) {
          hide = true;
        }
      }
      if (!hide && $scope.filters.timeloss.filter && $scope.filters.timeloss.show != 0) {
        if (!_.includes($scope.filters.timeloss.filterdata.selected, notice.timeloss)) {
          hide = true;
        }
      }
      if (!hide && $scope.filters.status.filter && $scope.filters.status.show != 0) {
        if (!_.includes($scope.filters.status.filterdata.selected, notice.statusstrings[notice.statusstrings.length - 1].id)) {
          hide = true;
        }
      }
      if (!hide) {
        notices.push(notice);
      }
    });
    $scope.notices = notices;
  };

  $scope.editFilterId = function (finish) {
    ModalEditorService.showTextLineEditor("NOTICES.FILTEREDIT.TITLE", "NOTICES.FILTEREDIT.ID", $scope.filters.id.filterdata.raw).result.then(function (res) {
      var match = res.match(/^(\d*)\-(\d*)$/);
      if (_.isNull(match)) {
        return;
      }
      $scope.filters.id.filterdata.raw = res;
      $scope.filters.id.filterdata.from = match[1].length > 0 ? parseInt(match[1]) : 0;
      $scope.filters.id.filterdata.to = match[2].length > 0 ? parseInt(match[2]) : 100000000;
      finish();
    });
  };

  $scope.editFilterCategory = function (finish) {
    ModalEditorService.showMultiPicker("NOTICES.FILTEREDIT.TITLE", "NOTICES.FILTEREDIT.CATEGORY", $scope.filters.category.filterdata.selected, $scope.filterres.actCategories, {style: 2}).result.then(function (res) {
      $scope.filters.category.filterdata.selected = res;
      finish();
    });
  };

  $scope.editFilterPath = function (finish) {
    ModalEditorService.showTextLineEditor("NOTICES.FILTEREDIT.TITLE", "NOTICES.FILTEREDIT.PATH", $scope.filters.path.filterdata.string).result.then(function (res) {
      if (res.length < 1) {
        return;
      }
      $scope.filters.path.filterdata.string = res;
      finish();
    });
  };

  $scope.editFilterText = function (finish) {
    ModalEditorService.showTextLineEditor("NOTICES.FILTEREDIT.TITLE", "NOTICES.FILTEREDIT.TEXT", $scope.filters.text.filterdata.string).result.then(function (res) {
      if (res.length < 2) {
        return;
      }
      $scope.filters.text.filterdata.string = res;
      finish();
    });
  };

  $scope.editFilterArticle = function (finish) {
    ModalEditorService.showTextLineEditor("NOTICES.FILTEREDIT.TITLE", "NOTICES.FILTEREDIT.ARTICLE", $scope.filters.article.filterdata.raw).result.then(function (res) {
      if (res.length < 2) {
        return;
      }
      $scope.filters.article.filterdata.raw = $scope.filters.article.filterdata.string = res;
      $scope.filters.article.filterdata.desc = false;
      if (_.startsWith(res, ':')) {
        $scope.filters.article.filterdata.desc = true;
        $scope.filters.article.filterdata.string = res.substr(1);
      }
      finish();
    });
  };

  $scope.editFilterTimeloss = function (finish) {
    var choice = _.map([15, 30, 60, 90, 120, 180, 240], function (time) {
      return {id: time, name: "NOTICES.TIMELOSS." + time}
    });
    ModalEditorService.showMultiPicker("NOTICES.FILTEREDIT.TITLE", "NOTICES.FILTEREDIT.TIMELOSS", $scope.filters.timeloss.filterdata.selected, choice, {style: 2}).result.then(function (res) {
      $scope.filters.timeloss.filterdata.selected = res;
      finish();
    });
  };

  $scope.editFilterStatus = function (finish) {
    var choice = _.map([1, 2, 5], function (code) {
      return {id: code, name: "NOTICES.STATUS." + code}
    });
    ModalEditorService.showMultiPicker("NOTICES.FILTEREDIT.TITLE", "NOTICES.FILTEREDIT.STATUS", $scope.filters.status.filterdata.selected, choice, {style: 2}).result.then(function (res) {
      $scope.filters.status.filterdata.selected = res;
      finish();
    });
  };

  $scope.editCategories = function () {
    SnippetEditorService.showSnippetEditor('notice_category', 'NOTICES.MODAL.SNIPPETCAT.TITLE', 'NOTICES.MODAL.SNIPPETCAT.SNIPPET', false);
  };

  $scope.editTemplates = function () {
    SnippetEditorService.showSnippetEditor('notice_textprop', 'NOTICES.MODAL.SNIPPETDESC.TITLE', 'NOTICES.MODAL.SNIPPETDESC.SNIPPET', true);
  };

  $scope.manageNotice = function (notice) {
    $modal.open({
      templateUrl: 'templates/modal/notice.html',
      controller: 'NoticeManageCtrl',
      size: 'lg',
      resolve: {
        notice: function () {
          return notice
        }
      }
    }).result.then(function (action) {
      if (action === 'correct') {
        $scope.refreshData(notice);
      } else {
        $scope.refreshData();
      }
    });
  };

  $scope.export = function () {
    var genoptions = [
      {id: 'csv', name: 'NOTICES.EXPORT.CSV'},
      {id: 'json', name: 'NOTICES.EXPORT.JSON'},
      {id: 'xml', name: 'NOTICES.EXPORT.XML'}
    ];
    var filteroptions = [
      {id: 'unarchived', name: 'NOTICES.EXPORT.UNARCHIVED'},
      {id: 'all', name: 'NOTICES.EXPORT.ALL'},
      {id: 'filtered', name: 'NOTICES.EXPORT.FILTERED'}
    ];
    ModalEditorService.showSinglePicker("NOTICES.EXPORT.TTL", "NOTICES.EXPORT.TYPETEXT", null, genoptions).result.then(function (selgen) {
      ModalEditorService.showSinglePicker("NOTICES.EXPORT.TTL", "NOTICES.EXPORT.FILTERTEXT", null, filteroptions).result.then(function (selfilter) {
        var query = "?scope=" + selfilter + "&format=" + selgen;
        if (selfilter === 'filtered') {
          query += "&showids=" + _.map($scope.notices, function (notice) {
              return notice.id;
            }).join(',');
        }
        $window.open("/servlet/export/notices" + query);
      });
    });
  };

  /* 'id', 'category', 'path', 'text', 'article', 'timeloss', 'status' ] */
});