cmod.controller('PdfViewerCtrl', function ($modalInstance, $rootScope, $scope, medium) {

  $scope.setupConfig = setupConfig;
  $scope.close = close;

  $scope.setupConfig();


  //////////


  function setupConfig() {
    $scope.title = medium.caption;
    if ($scope.title === null || $scope.title === "") {
      $scope.title = medium.binaryfile.original_filename;
    }
    $scope.pdfurl = "/media/" + medium.binaryfile.filename;
  }

  function close() {
    $modalInstance.close();
  }

});