cmod.controller('ProcedureEditCtrl', function ($rootScope, $modal, $scope, $stateParams, $location, i18n, Restangular, UsersService, VersioningService, AlertService, EditorManagementService, ProcedureTagService) {
  EditorManagementService.installEditorManagement($scope, 'procedures', '/procedures');

  $scope.tagValues = [];
  $scope.$on("$destroy", function () {
    $modal.closeall()
  });

  $scope.selected = undefined;

  activate();

  $scope.refreshData = function () {
    if ($stateParams.procid === "new") {
      $rootScope.selecteditem = null;
      var procedure = {code: ""};
      $scope.fillupLang(procedure, 'title');
      $scope.fillupLang(procedure, 'description');
      $scope.editor.title = "PROCEDURE.EDITOR.TITLE.NEW";
      $scope.item = procedure;
      $scope.initCodeCheck(null);
      initializeTags();
      $scope.procEditLoaded = true;
    } else {
      // Calling route PSM_2
      Restangular.one('procedures', $stateParams.procid).get({foredit: true}).then(function procData(response) {
        var procedure = response;
        $rootScope.selecteditem = procedure;
        $scope.fillupLang(procedure, 'title');
        $scope.fillupLang(procedure, 'description');
        $scope.editor.title = "PROCEDURE.EDITOR.TITLE.EDIT";
        $scope.editor.titlevals = $scope.makeTranslateVals(procedure.title, 'procname');
        if (procedure.version != null) $scope.editor.subtitle = VersioningService.versionName(procedure.version, null);
        $scope.item = procedure;
        $scope.initCodeCheck($scope.item.code);
        initializeTags();
        $scope.procEditLoaded = true;
      }, AlertService.showSevereRESTError)
    }
  };

  $scope.removeTagValue = function (index) {
    $scope.tagValues[index] = null;
  };

  $scope.getSelectedLanguage = i18n.getSelectedLanguage;


  UsersService.addGrantMethods($scope).then(function () {
    $scope.refreshData();
    $('#pr_code').focus();
  });

  function initializeTags() {
    // Calling route MISC_27
    Restangular.all('settings').getList({all: true}).then(function (data) {
      $scope.tags = ProcedureTagService.initializeTags(data);
      var idx = 0;
      _.forEach($scope.tags, function () {
        idx++;
        $scope.tagValues.push({value: $scope.item['tag_' + idx]});
      });
    }, AlertService.showSevereRESTError);
  }

  function activate() {
    $scope.$watch('tagValues', function (newValue, oldValue) {
      if (newValue !== oldValue) {
        var idx = 0;
        _.forEach(newValue, function (tag) {
          idx++;
          if (tag) {
            $scope.item['tag_' + idx] = tag.value;
          } else {
            $scope.item['tag_' + idx] = null;
          }
        });
      }
    }, true);
  }

});