cmod.controller('ProcedureFilterCtrl', function ($modalInstance, Restangular, AlertService, i18n, selectedValues, ProcedureTagService) {
  var ctrl = this;

  ctrl.applyFilter = applyFilter;
  ctrl.cancel = cancel;
  ctrl.getSelectedLanguage = i18n.getSelectedLanguage;

  activate();

  function activate() {
    // Calling route MISC_27
    Restangular.all('settings').getList({all: true}).then(function (data) {
      ctrl.tags = ProcedureTagService.initializeTags(data);
      initializeSelectedValues();
    }, AlertService.showSevereRESTError);
  }

  function initializeSelectedValues() {
    var idx = 0;
    _.forEach(ctrl.tags, function (tag) {
      idx++;
      if (selectedValues && selectedValues['tag_' + idx]) {
        tag.selectedTagValues = selectedValues['tag_' + idx].split(',').map(function (item) {
          return {value: item};
        });
      } else {
        tag.selectedTagValues = [];
      }
    });
  }

  function applyFilter() {
    var idx = 0,
      tagFilter = {};
    _.forEach(ctrl.tags, function (tag) {
      idx++;
      if (tag.selectedTagValues && tag.selectedTagValues.length > 0) {
        var values = _.map(tag.selectedTagValues, 'value');
        tagFilter['tag_' + idx] = values.join();
      }
    });
    $modalInstance.close(tagFilter);
  }

  function cancel() {
    $modalInstance.close();
  }

});