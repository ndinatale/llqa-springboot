cmod.controller('ProcedureListCtrl', function ($rootScope, $modal, $filter, $location, FileUploader, $scope, i18n, Restangular, ViewerManagementService,
                                               UsersService, AlertService, ListManagementService, ModalEditorService, UploaderService, VersioningService) {

  ListManagementService.installListManagement($scope, 'proc_filters');
  ViewerManagementService.installViewerManagement($scope, null, 'procedure', '/procedures');

  $scope.lang = i18n.selectedLanguage.code;

  UsersService.addGrantMethods($scope).then(function () {
    $scope.loadFilterSettings();
    $scope.refreshListData();
  });

  UploaderService.installImportUploader($scope, "procedures", "/procedures/view/", null);


  /////////


  $scope.refreshListData = function () {
    // Calling route PSM_1
    Restangular.all('procedures').getList().then(function procList(response) {
      var procedures = response;
      $scope.realidstatus = ListManagementService.calcRealidStatus(procedures);
      $scope.items = procedures;
      $scope.procListLoaded = true;
    }, AlertService.showSevereRESTError);
  };

  $scope.goto = function (where) {
    $location.url("/app/procedures/view/" + where);
  };

  $scope.newProcedure = function () {
    $scope.wrapEditWarning(function () {
      $location.url("/app/procedures/edit/new");
    }, true);
  };

  $scope.cloneProcedure = function () {
    $scope.wrapEditWarning(function () {
      UploaderService.cloneProcedure($scope.refreshListData);
    }, true);
  };

  $scope.gotoModels = function (proc, event) {
    if (!_.isNull(event)) {
      event.stopPropagation();
    }

    // Calling route PSM_2
    Restangular.one('procedures', proc.id).get().then(function (response) {
      var blocks = [];

      _.each(response.versions, function (vers) {
        if (!_.isNull(vers[0])) {
          var verst = VersioningService.versionName(vers[0]);
          blocks[vers[0] - 1] = {name: $filter('translate')(verst.text, verst), code: '', items: []}
        }
      });

      _.each(response.usage, function (md) {
        blocks[md[2] - 1].items.push({id: md[1].id, code: md[1].code, title: md[1].title})
      });

      console.log(blocks);

      blocks = _.compact(_.map(blocks, function (blk) {
        if (blk && blk.items && blk.items.length < 1) return null;
        return blk
      }));

      ModalEditorService.showSinglePicker('PROCEDURE.LIST.GOTOPROC.TITLE', 'PROCEDURE.LIST.GOTOPROC.TEXT', null, blocks, {
        blocks: true,
        style: 2,
        uncheckable: true
      }).result.then(function (res) {
        if (_.isNull(res)) return;
        $location.url("/app/models/view/" + res)
      })
    }, AlertService.showRESTError)
  };

  $scope.openFilterDialog = function () {
    $modal.open({
      templateUrl: 'templates/modal/procedurefilter.html',
      controller: 'ProcedureFilterCtrl',
      controllerAs: 'ctrl',
      resolve: {
        selectedValues: function () {
          return $scope.filters.tagfilter;
        }
      }
    }).result.then(function (tagfilter) {
      if (_.isEmpty(tagfilter)) tagfilter = undefined;
      $scope.filters.tagfilter = tagfilter;
      UsersService.updateUserInfo();
    });
  };

  $scope.isTagfilterActive = function () {
    return !_.isEmpty($scope.filters.tagfilter);
  };

  $scope.containsComparator = function (actual, expected) {
    if (actual && actual.length > 0) {
      return expected.indexOf(actual) > -1;
    } else {
      return false;
    }
  };

  $scope.$on("$destroy", function () {
    $modal.closeall();
  });

});