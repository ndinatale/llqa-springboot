cmod.controller('ProcedureViewCtrl', function ($rootScope, $scope, $modal, $location, $window, i18n, $stateParams, ConstantService, Restangular, VersioningService, WorkflowModifyService, AlertService, UsersService, RestService, UploaderService, ModalEditorService, ViewerManagementService, ListManagementService, ChangeLogService) {

  var C = {};
  ConstantService.installConstants($scope, C, 'PSM_');

  ViewerManagementService.installViewerManagement($scope, null, 'procedure', '/procedures');
  ListManagementService.installReorderableSublistManagement($scope, 'steps');

  $scope.$on("$destroy", function () {
    $modal.closeall();
  });

  $scope.refreshData = function () {
    // Calling route PSM_2
    Restangular.one('procedures', $stateParams.procid).get().then(function procData(response) {
      var procedure = response;
      procedure.versname = VersioningService.versionName(procedure.version, procedure.versions);
      ListManagementService.prepareSublist($scope, procedure.steps);
      ListManagementService.setSelectedItem(procedure);
      $scope.item = procedure;
      $scope.wflowRulesProc = WorkflowModifyService.getWorkflowModifiersForDisplay($scope.item.flowcontrol);
      if ($scope.isVersion()) {
        $scope.activateReordering(false);
      }
      initializeTags();
      $scope.procViewLoaded = true;
    }, AlertService.showSevereRESTError);
  };

  $scope.gotoStep = function (step) {
    $location.url("/app/steps/" + $scope.item.id + "/view/" + step.id);
  };

  $scope.gotoSteps = function () {
    $location.url("/app/steps/" + $scope.item.id);
  };

  UsersService.addGrantMethods($scope).then(function () {
    $scope.refreshData();
  });

  $scope.newStep = function () {
    $scope.wrapEditWarning(function () {
      $location.url("/app/steps/" + $scope.item.id + "/edit/new");
    }, true);
  };

  $scope.deleteStep = function (step) {
    $scope.wrapEditWarning(function () {
      RestService.deleteStep($scope, step, $scope.item, null);
    }, true);
  };

  $scope.deleteItem = function () {
    var msg = AlertService.createConfirmMessage("PROCEDURE.ALERT.CONFHEADER", "PROCEDURE.ALERT.DELETE", {code: $scope.item.code});
    if ($scope.item.steps.length > 0) {
      msg.addReason("PROCEDURE.ALERT.DELETECASCADING", {scnt: $scope.item.steps.length});
    }
    if ($scope.isVersion()) {
      msg.addReason("PROCEDURE.ALERT.DELETEVERSION", {});
    }
    msg.show().result.then(function () {
      // Calling route PSM_7
      Restangular.one('procedures', $scope.item.id).remove().then(function () {
        $scope.refreshListData();
        $location.url("/app/procedures");
      }, AlertService.showRESTError);
    });
  };

  $scope.finalizeItem = function () {
    var msg = AlertService.createConfirmMessage("PROCEDURE.ALERT.CONFHEADER", "PROCEDURE.ALERT.FINALIZE.TITLE", {code: $scope.item.code})
    msg.addReason("PROCEDURE.ALERT.FINALIZE.TEXT", {});
    msg.show().result.then(function () {
      // Calling route PSM_8
      Restangular.one('procedures', $scope.item.id).one('action_finalize').get().then(function (result) {
        $scope.refreshListData();
        $location.url("/app/procedures/view/" + result.trunk_id);
      }, AlertService.showRESTError);
    });
  };

  $scope.resetChanges = function () {
    var msg = AlertService.createConfirmMessage("PROCEDURE.ALERT.CONFHEADER", "PROCEDURE.ALERT.RESET.TITLE", {code: $scope.item.code})
    msg.addReason("PROCEDURE.ALERT.RESET.TEXT", {});
    msg.show().result.then(function () {
      // Calling route PSM_9
      Restangular.one('procedures', $scope.item.id).one('action_reset').get().then(function (result) {
        $scope.refreshListData();
        $location.url("/app/procedures/view/" + result.trunk_id);
      }, AlertService.showRESTError);
    });
  };

  $scope.updateModels = function () {
    var msg = AlertService.createConfirmMessage("PROCEDURE.ALERT.CONFHEADER", "PROCEDURE.ALERT.FULLUPDATE.TITLE", {})
    msg.addReason("PROCEDURE.ALERT.FULLUPDATE.TEXT", {});
    msg.show().result.then(function () {
      // Calling route MU_9
      Restangular.all('models').one('action_auto_update', $scope.item.realid).get().then(function (result) {
        $scope.refreshListData();
        $scope.refreshData();
      }, AlertService.showRESTError);
    });
  };

  $scope.exportStep = function (step, event) {
    $window.open("/servlet/export/steps/" + step.id + ((event.shiftKey || event.metaKey) ? '?complete=true' : ''));
  };

  UploaderService.installImportUploader($scope, "steps", null, $stateParams.procid);

  $scope.wfRuleManager = function () {
    $scope.wrapEditWarning(function () {
      WorkflowModifyService.openWorkflowModifierEditor({
        msg: 'PROCEDURE.VIEW.FLOWEDITOR',
        vals: {pcode: $scope.item.code, ptitle: i18n.translate($scope.item.title)}
      }, null, $scope.item.flowcontrol).result.then(function () {
        // Calling route PSM_4
        Restangular.all('procedures').post($scope.item).then(function (item) {
          $scope.refreshListData();
          $scope.refreshData();
        }, AlertService.showRESTError);
      });
    }, true);
  };

  $scope.cloneStep = function () {
    $scope.wrapEditWarning(function () {
      UploaderService.cloneStep($stateParams.procid, function () {
        $scope.refreshListData();
        $scope.refreshData();
      });
    }, true);
  };

  $scope.measureStatistics = function () {
    var modalInstance = $modal.open({
      templateUrl: 'templates/modal/msrstatistics.html',
      controller: 'MeasureStatisticsCtrl',
      //size: 'lg',
      keyboard: false,
      backdrop: 'static',
      resolve: {
        proc: function () {
          return $scope.item
        }
      }
    });
  };

  $scope.testVersion = function () {
    // Calling route MISC_11
    Restangular.all('checktypes').getList().then(function ctypeData(response) {
      var vlist = [];
      _.each(response, function (val) {
        vlist.push({id: val.id, title: val.title})
      });
      ModalEditorService.showSinglePicker("PROCEDURE.TESTCHECK.TITLE", "PROCEDURE.TESTCHECK.TEXT", null, vlist, {style: 1}).result.then(function (selv) {
        // Calling route CHECK_11
        Restangular.all("testrun").one("procedure", $scope.item.id).one("ctype", selv).get().then(function (tcheck) {
          $location.url("/app/workflow/" + tcheck.id);
        }, AlertService.showRESTError);
      });
    });
  };

  $scope.showChangeLog = function () {
    ChangeLogService.showChangeLog($scope.item, 'procedures');
  };

  $scope.wfRuleManagerStep = function (step) {
    $scope.wrapEditWarning(function () {
      WorkflowModifyService.openWorkflowModifierEditor({
        msg: 'STEP.VIEW.FLOWEDITOR',
        vals: {scode: step.code, stitle: i18n.translate(step.title)}
      }, step.procedure_id, step.flowcontrol).result.then(function () {
        // Calling route PSM_18
        Restangular.all('steps').post(step).then(function (item) {
          $scope.refreshListData();
          $scope.refreshData();
        }, AlertService.showRESTError);
      });
    }, true);
  };

  $scope.setEnforceLevel = function (step, newlevel) {
    $scope.wrapEditWarning(function () {
      // Calling route PSM_18
      Restangular.all('steps').post({id: step.id, enforce: newlevel}).then(function (item) {
        $scope.refreshListData();
        $scope.refreshData();
      }, AlertService.showRESTError);
    }, true);
  };

  $scope.getTagValue = function (index) {
    return $scope.item['tag_' + index];
  };

  function initializeTags() {
    // Calling route MISC_27
    Restangular.all('settings').getList({all: true}).then(function (data) {
      $scope.tags = _.filter(data, function (item) {
        return item.key === 'procedure_tag';
      });
    }, AlertService.showSevereRESTError);
  }

  $scope.getSelectedLanguage = i18n.getSelectedLanguage;

});