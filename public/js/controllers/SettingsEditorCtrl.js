cmod.controller('SettingsEditorCtrl', function ($modalInstance, Restangular, AlertService, ModalEditorService) {

  var ctrl = this, SETTING_KEY = 'procedure_tag', MAX_NUM_TAGS_ALLOWED = 6;

  ctrl.toggleCollapse = toggleCollapse;
  ctrl.addTag = addTag;
  ctrl.close = close;
  ctrl.editTag = editTag;
  ctrl.removeTag = removeTag;
  ctrl.isAddButtonEnabled = isAddButtonEnabled;
  ctrl.sublistSortOptions = {
    axis: 'y',
    placeholder: 'll-draggeditem',
    handle: '.ll-dragaction',
    stop: function (event, ui) {

    }
  };

  activate();

  function activate() {
    loadData();
  }

  function toggleCollapse(item) {
      item.collapsed = !item.collapsed;
  }

  function addTag() {
    ModalEditorService.showSettingsDetailEditor("SETTINGS.PROC.NEWTAG.TITLE", "", {en: ''}, []).result.then(function (result) {
      var setting = {
        key: SETTING_KEY,
        value: result.value,
        sub_values: []
      };
      // Calling route MISC_25
      Restangular.all('settings').post(setting).then(function () {
        loadData();
      }, AlertService.showRESTError);
    });
  }

  function close() {
    $modalInstance.close();
  }

  function editTag(setting) {
    ModalEditorService.showSettingsDetailEditor("SETTINGS.PROC.EDITTAG.TITLE", "", setting.value, setting.sub_values, null).result.then(function (result) {
      // Calling route MISC_25
      setting.value = result.value;
      setting.sub_values = result.sub_values;
      Restangular.all('settings').post(setting).then(function () {
        loadData();
      }, AlertService.showRESTError);
    });
  }

  function removeTag(setting) {
    AlertService.createConfirmMessage("SETTINGS.ALERT.CONFDEL.TITLE", "SETTINGS.ALERT.CONFDEL.TEXT", null).show().result.then(function () {
      // Calling route MISC_24
      Restangular.one('settings', setting.id).remove().then(function () {
        loadData();
      }, AlertService.showRESTError);
    })
  }

  function isAddButtonEnabled() {
    return ctrl.items && ctrl.items.length < MAX_NUM_TAGS_ALLOWED;
  }

  function loadData() {
    // Calling route MISC_27
    Restangular.all('settings').getList({all: true}).then(function (data) {
      ctrl.items = _.filter(data, function (item) {
        return item.key === SETTING_KEY;
      });

      _.each(ctrl.items, function (item) {
          if (item.sub_values) {
            item.sub_values.sort();
          }
      });
    }, AlertService.showSevereRESTError);
  }

});