cmod.controller('SnippetEditorCtrl', function ($scope, $q, $modalInstance, $rootScope, category, catname, mutable, ModalEditorService, Restangular, i18n, $filter, UsersService, AlertService, $modal, ListManagementService) {
  $scope.category = category;
  $scope.catname = catname;
  $scope.mutable = mutable;

  $scope.snippets = [];

  ListManagementService.installReorderableSublistManagement($scope, 'snippets');

  $scope.refreshModalData = function (optfun) {
    // Calling route MISC_18
    Restangular.one('snippets', $scope.category).getList().then(function procList(response) {
      $scope.snippets = response;
      ListManagementService.prepareSublist($scope, response);
      $scope.stepSnippetEditLoaded = true
    }, AlertService.showSevereRESTError);
  };

  $scope.stepSnippetEditLoaded = false;

  UsersService.addGrantMethods($scope).then(function () {
    $scope.refreshModalData();
  });

  $scope.lockSnippet = function (snippet) {
    snippet.status = 2;
    // Calling route MISC_20
    Restangular.all('snippets').post(snippet).then(function () {
      $scope.refreshModalData();
    }, AlertService.showRESTError);
  };

  $scope.unlockSnippet = function (snippet) {
    snippet.status = 1;
    // Calling route MISC_20
    Restangular.all('snippets').post(snippet).then(function () {
      $scope.refreshModalData();
    }, AlertService.showRESTError);
  };

  $scope.deleteSnippet = function (snippet) {
    if (mutable) {
      AlertService.createConfirmMessage("NOTICES.ALERT.CONFDEL.TITLE", "NOTICES.ALERT.CONFDEL.TEXT1", null).show().result.then(function () {
        Restangular.one('snippets', snippet.id).remove().then(function () {
          $scope.refreshModalData();
        }, AlertService.showRESTError);
      })
    } else {
      AlertService.createConfirmMessage("NOTICES.ALERT.CONFDEL.TITLE", "NOTICES.ALERT.CONFDEL.TEXT2", null).show().result.then(function () {
        snippet.status = 9;
        // Calling route MISC_20
        Restangular.all('snippets').post(snippet).then(function () {
          $scope.refreshModalData();
        }, AlertService.showRESTError);
      });
    }
  };

  $scope.editSnippet = function (snippet) {
    ModalEditorService.showTextAreaEditor("NOTICES.MODAL.EDITTEXT.TITLE", "NOTICES.MODAL.EDITTEXT.TEXT", snippet.text, null).result.then(function (newtext) {
      if (newtext.length >= 3) {
        if (mutable) {
          snippet.text = newtext;
          // Calling route MISC_20
          Restangular.all('snippets').post(snippet).then(function () {
            $scope.refreshModalData();
          }, AlertService.showRESTError);
        } else {
          snippet.status = 9;
          // Calling route MISC_20
          Restangular.all('snippets').post(snippet).then(function () {
            var newsnippet = {
              text: newtext,
              status: 1,
              category: $scope.category,
              seqnum: snippet.seqnum
            };
            // Calling route MISC_20
            Restangular.all('snippets').post(newsnippet).then(function () {
              $scope.refreshModalData();
            }, AlertService.showRESTError);
          }, AlertService.showRESTError);
        }
      }
    });
  };

  $scope.addSnippet = function () {
    ModalEditorService.showTextAreaEditor("NOTICES.MODAL.NEWTEXT.TITLE", "NOTICES.MODAL.NEWTEXT.TEXT", "", null).result.then(function (newtext) {
      if (newtext.length >= 3) {
        var snippet = {
          text: newtext,
          status: 1,
          category: $scope.category
        };
        // Calling route MISC_20
        Restangular.all('snippets').post(snippet).then(function () {
          $scope.refreshModalData();
        }, AlertService.showRESTError);
      }
    });
  };

  $scope.close = function () {
    $modalInstance.close()
  };

});