cmod.controller('StepEditCtrl', function ($rootScope, $modal, $scope, $stateParams, $location, i18n, Restangular, UsersService, VersioningService, AlertService, EditorManagementService) {
  EditorManagementService.installEditorManagement($scope, 'steps', '/steps/' + $stateParams.procid);

  $scope.$on("$destroy", function () {
    $modal.closeall();
  });

  $scope.refreshData = function () {
    if ($stateParams.stepid === "new") {
      $rootScope.selecteditem = null;
      var step = {code: "", procedure_id: $stateParams.procid};
      $scope.fillupLang(step, 'title');
      $scope.fillupLang(step, 'description');
      $scope.editor.title = "STEP.EDITOR.TITLE.NEW";
      $scope.item = step;
      $scope.initCodeCheck(null);
      $scope.stepEditLoaded = true;
    } else {
      // Calling route PSM_15
      Restangular.one('steps', $stateParams.stepid).get({foredit: true}).then(function procData(response) {
        var step = response;
        $rootScope.selecteditem = step;
        $scope.fillupLang(step, 'title');
        $scope.fillupLang(step, 'description');
        $scope.editor.title = "STEP.EDITOR.TITLE.EDIT";
        $scope.editor.titlevals = $scope.makeTranslateVals(step.title, 'stepname');
        if (step.version != null) {
          $scope.editor.subtitle = VersioningService.versionName(step.procedure.version, null);
        }
        $scope.item = step;
        $scope.initCodeCheck($scope.item.code);
        $scope.stepEditLoaded = true;
      }, AlertService.showSevereRESTError);
    }
  };

  UsersService.addGrantMethods($scope).then(function () {
    $scope.refreshData();
    $('#st_code').focus();
  });

});