cmod.controller('StepEditMatrixCtrl', function ($scope, $modalInstance, calculation) {

  var rows = [];
  for (var i = 0; i < calculation.mx_ysize; i++) {
    rows.push(i + 1);
  }
  var cols = [];
  for (i = 0; i < calculation.mx_xsize; i++) {
    cols.push(i + 1);
  }
  $scope.calculation = calculation;
  $scope.rows = rows;
  $scope.cols = cols;
  $scope.yttlclass = "ll-ytitle" + calculation.mx_ysize;
  $scope.editing = null;

  $scope.continue = function () {
    $scope.selectTitle(null);
    $modalInstance.close();
  };

  $scope.editclass = function (what) {
    if ($scope.editing == what) {
      return 'll-mxetitleeditsel';
    }
    if (_.isUndefined($scope.calculation["mx_" + what]) || _.isNull($scope.calculation["mx_" + what])) {
      return "ll-mxetitleeditempty";
    }
    return 'll-mxetitleedit';
  };

  $scope.selectTitle = function (what) {
    if (!_.isNull($scope.editing)) {
      if ("" === $scope.calculation["mx_" + $scope.editing]) {
        $scope.calculation["mx_" + $scope.editing] = null;
      }
    }
    $scope.editing = what;
    $('#mx_edit').focus();
  };

  $scope.matrixTitle = function (what) {
    if (_.isUndefined($scope.calculation["mx_" + what]) || _.isNull($scope.calculation["mx_" + what]) || $scope.calculation["mx_" + what] == "") {
      return $scope.editing == what ? "-" : "XXX";
    }
    return $scope.calculation["mx_" + what];
  };

});