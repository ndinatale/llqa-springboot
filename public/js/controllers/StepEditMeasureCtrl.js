cmod.controller('StepEditMeasureCtrl', function ($scope, $q, $modalInstance, $rootScope, measure, mainscope, EditorManagementService, VersioningService, MeasureTypeService, ModalEditorService, Restangular, UsersService, i18n, $filter, AlertService, $modal) {
  EditorManagementService.installEditorManagement($scope, 'measures', null);

  // FIX variable assigned to itself..
  //$scope.mainscope = $scope.mainscope
  $scope.step = mainscope.item;
  $scope.measure = measure;
  $scope.mtinfo = {type: null, input: null};
  $scope.isVersion = mainscope.isVersion();

  $scope.floatformats = [
    {id: "default", name: "MEASURE.EDIT.FLOATFRM.STD"},
    {id: "%0.0f", name: "MEASURE.EDIT.FLOATFRM.INT"},
    {id: "%0.1f", name: "MEASURE.EDIT.FLOATFRM.1DIGIT"},
    {id: "%0.2f", name: "MEASURE.EDIT.FLOATFRM.2DIGIT"},
    {id: "%0.3f", name: "MEASURE.EDIT.FLOATFRM.3DIGIT"},
    {id: "%0.4f", name: "MEASURE.EDIT.FLOATFRM.4DIGIT"},
    {id: "%0.6f", name: "MEASURE.EDIT.FLOATFRM.6DIGIT"}
  ];

  UsersService.addGrantMethods($scope).then(function () {
    MeasureTypeService.getMeasuretypes($scope).then(function () {
      $scope.refreshModalData();
      $('#m_code').focus();
    }, AlertService.showSevereRESTError);
  });


  ////////


  $scope.refreshModalData = function (optfun) {
    // Calling route MISC_1
    Restangular.all('tooltypes').getList({selector: true}).then(function ttData(response) {
      $scope.tooltypes = response;
      $scope.tooltypes.unshift({id: -1, code: '---', title: null});
      if ($scope.measure < 0) {
        var measure = {code: "", tooltype_id: -1, step_id: $scope.step.id, measuretype: 0, calculation: {internal: 1}};
        $scope.fillupLang(measure, 'title');
        $scope.fillupLang(measure, 'description');
        $scope.editor.title = "MEASURE.EDITOR.TITLE.NEW";
        $scope.item = measure;
        $scope.initCodeCheck(null);
        $scope.stepMEditLoaded = true;
        if (optfun) {
          optfun();
        }
      } else {
        // Calling route PSM_24
        Restangular.one('measures', $scope.measure).get({foredit: true}).then(function mData(response) {
          var measure = response;
          $scope.fillupLang(measure, 'title');
          $scope.fillupLang(measure, 'description');
          $scope.editor.title = "MEASURE.EDITOR.TITLE.EDIT";
          $scope.editor.titlevals = $scope.makeTranslateVals(measure.title, 'mname');
          if ($scope.step.procedure.version != null) {
            $scope.editor.subtitle = VersioningService.versionName($scope.step.procedure.version, null);
          }
          if (_.isNull(measure.tooltype_id)) {
            measure.tooltype_id = -1;
          }
          $scope.item = measure;
          $scope.setMttype();
          $scope.initCodeCheck($scope.item.code);
          if (_.isUndefined($scope.item.metadata.floatformat)) {
            $scope.item.metadata.floatformat = "default";
          }
          $scope.stepMEditLoaded = true;
          if (optfun) {
            optfun();
          }
        }, AlertService.showSevereRESTError)
      }
    })
  };

  $scope.tooltypeCboxName = function (ttype) {
    if (_.isNull(ttype.title)) {
      return $filter('translate')('MEASURE.EDITOR.NOTOOL');
    }
    return ttype.code + ': ' + i18n.translate(ttype.title);
  };

  $scope.measuretypeCboxName = function (mtype) {
    //return mtype.id + ' - ' +  $filter('translate')(mtype.name)
    // FIX nothing returned here
    //mtype.name
  };

  $scope.setMttype = function () {
    $scope.mtinfo = MeasureTypeService.mtinfoForId($scope, $scope.item.measuretype);
  };

  $scope.prepareCalcArea = function () {
    $scope.setMttype();
    if ($scope.mtinfo.type == "frtext") {
      if (_.isUndefined($scope.item.calculation.t3_minlen) || _.isNaN($scope.item.calculation.t3_minlen)) {
        $scope.item.calculation.t3_minlen = 3;
      }
    }
    if ($scope.mtinfo.input == "matrix") {
      if (_.isUndefined($scope.item.calculation.mx_xsize) || _.isNaN($scope.item.calculation.mx_xsize)) {
        $scope.item.calculation.mx_xsize = 3;
      }
      if (_.isUndefined($scope.item.calculation.mx_ysize) || _.isNaN($scope.item.calculation.mx_ysize)) {
        $scope.item.calculation.mx_ysize = 3;
      }
    }
    //
    if ($scope.mtinfo.type == "choice") {
      if (_.isUndefined($scope.item.calculation.t17_len) || _.isNaN($scope.item.calculation.t17_len)) {
        $scope.item.calculation.t17_len = 3;
        $scope.recalculateChoiceList();
      }
    }
  };

  $scope.decrvalue = function (what, minv) {
    $scope.item.calculation[what] -= 1;
    if ($scope.item.calculation[what] < minv) {
      $scope.item.calculation[what] = minv;
    }
  };

  $scope.incrvalue = function (what, maxv) {
    $scope.item.calculation[what] += 1;
    if ($scope.item.calculation[what] > maxv) {
      $scope.item.calculation[what] = maxv;
    }
  };

  $scope.setupMatrix = function () {
    var modalInstance = $modal.open({
      templateUrl: 'templates/steps/editmatrix.html',
      controller: 'StepEditMatrixCtrl',
      resolve: {
        calculation: function () {
          return $scope.item.calculation;
        }
      }
    })
  };

  $scope.cancel = function () {
    $modalInstance.dismiss();
  };

  $scope.continue = function () {
    $scope.codeCheck($scope.item, function () {
      if ($scope.item.tooltype_id < 0) {
        $scope.item.tooltype_id = null;
      }
      // Calling route PSM_23
      Restangular.all("measures").post($scope.item).then(function (item) {
        $scope.measure = item.id;
        mainscope.refreshData(function () {
          $modalInstance.close();
        });
      }, AlertService.showRESTError);
    })
  };

  $scope.check = function () {
    $scope.codeCheck($scope.item, function () {
      if ($scope.item.tooltype_id < 0) {
        $scope.item.tooltype_id = null;
      }
      // Calling route PSM_23
      Restangular.all("measures").post($scope.item).then(function (item) {
        $scope.measure = item.id;
        $scope.refreshModalData(function () {
          mainscope.refreshData(function () {
            mainscope.testMeasure($scope.item, $scope.mtinfo);
          });
        });
      }, AlertService.showRESTError);
    })
  };

  $scope.getUnitTypeahead = function (plain) {
    if (!_.isUndefined($rootScope.session.userinfo) && !_.isUndefined($rootScope.session.userinfo.typeahead_unit)) {
      if (!plain) {
        var ret = [];
        _.forEach($rootScope.session.userinfo.typeahead_unit, function (x) {
          ret.unshift(x);
        });
        return ret;
      }
      return $rootScope.session.userinfo.typeahead_unit;
    } else {
      if (!_.isUndefined($rootScope.session.userinfo)) {
        return $rootScope.session.userinfo.typeahead_unit = [];
      }
      return [];
    }
  };

  $scope.addUnitTypeahead = function (value) {
    if (_.isUndefined(value) || _.isNull(value) || value === '' || value === ' ') {
      return;
    }
    var list = $scope.getUnitTypeahead(true);
    _.remove(list, function (el) {
      return el === value;
    });
    list.push(value);
    while (list.length > 10) {
      list.shift();
    }
    UsersService.updateUserInfo();
  };

  // this is for measure type 17, choice list
  $scope.recalculateChoiceList = function () {
    // if item undefined, return
    if (_.isUndefined($scope.item) || _.isUndefined($scope.item.calculation)) {
      return;
    }

    // if called for first time, initialize new array
    if (_.isUndefined($scope.item.calculation.t17_list)) {
      $scope.item.calculation.t17_list = new Array($scope.item.calculation.t17_len);
    } else {
      // copy values
      var oldLength = $scope.item.calculation.t17_list.length;
      var newLength = $scope.item.calculation.t17_len;

      if (oldLength > newLength) {
        $scope.item.calculation.t17_list.pop();
      } else if (oldLength < newLength) {
        $scope.item.calculation.t17_list.push(null);
      }
    }
  };

});