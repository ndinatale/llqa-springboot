cmod.controller('StepListCtrl', function ($rootScope, $modal, $location, $stateParams, FileUploader, $scope, i18n, Restangular, UsersService, AlertService, ViewerManagementService, ListManagementService, UploaderService) {
  ListManagementService.installListManagement($scope, 'step_filters');
  ViewerManagementService.installViewerManagement($scope, 'procedure', 'step', '/steps/' + $stateParams.procid);

  $scope.$on("$destroy", function () {
    $modal.closeall();
  });

  $scope.refreshListData = function () {
    // Calling route PSM_12
    Restangular.one('procedures', $stateParams.procid).all('steps').getList().then(function procList(response) {
      var procedures = response;
      $scope.realidstatus = {};
      $scope.items = procedures;
      // Calling route PSM_2
      return Restangular.one('procedures', $stateParams.procid).get({foredit: true});
    }, AlertService.showSevereRESTError).then(function procData(response) {
      $scope.procedure = response;
      $scope.stepListLoaded = true;
    }, AlertService.showSevereRESTError);
  };

  $scope.gotoProcedure = function () {
    $location.url("/app/procedures/view/" + $scope.procedure.id);
  };

  $scope.goto = function (where) {
    $location.url("/app/steps/" + $scope.procedure.id + "/view/" + where);
  };

  $scope.newStep = function () {
    $scope.wrapEditWarning(function () {
      $location.url("/app/steps/" + $scope.procedure.id + "/edit/new");
    }, true);
  };

  UsersService.addGrantMethods($scope).then(function () {
    $scope.loadFilterSettings();
    $scope.refreshListData();
  });

  UploaderService.installImportUploader($scope, "steps", "/steps/" + $stateParams.procid + "/view/", $stateParams.procid);

  $scope.cloneStep = function () {
    $scope.wrapEditWarning(function () {
      UploaderService.cloneStep($stateParams.procid, $scope.refreshListData);
    }, true);
  };

});