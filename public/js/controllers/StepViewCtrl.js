cmod.controller('StepViewCtrl', function ($rootScope, $scope, $location, $window, $modal, i18n, $stateParams, ConstantService, MediaService, Restangular, MeasureTypeService, VersioningService, WorkflowModifyService, AlertService, UsersService, RestService, ModalEditorService, ViewerManagementService, ListManagementService, UploaderService) {

  var C = {};
  ConstantService.installConstants($scope, C, 'PSM_');

  ViewerManagementService.installViewerManagement($scope, 'procedure', 'step', '/steps/' + $stateParams.procid);
  ListManagementService.installReorderableSublistManagement($scope, 'measures');

  $scope.$on("$destroy", function () {
    $modal.closeall();
  });

  $scope.refreshData = function (optfun) {
    // Calling route PSM_15
    Restangular.one('steps', $stateParams.stepid).get().then(function procData(response) {
      var step = response;
      step.versname = VersioningService.versionName(step.procedure.version, step.procedure.versions);
      ListManagementService.prepareSublist($scope, step.measures);
      ListManagementService.setSelectedItem(step);
      $scope.wflowRulesStep = WorkflowModifyService.getWorkflowModifiersForDisplay(step.flowcontrol);
      $scope.item = step;
      $scope.procedure = step.procedure;
      if ($scope.isVersion()) {
        $scope.activateReordering(false);
      }
      $scope.stepViewLoaded = true;
      if (optfun) {
        optfun();
      }
    }, AlertService.showSevereRESTError);
  };

  UsersService.addGrantMethods($scope).then(function () {
    MeasureTypeService.getMeasuretypes($scope).then(function () {
      $scope.refreshData();
    }, AlertService.showSevereRESTError);
  });

  $scope.deleteItem = function () {
    $scope.wrapEditWarning(function () {
      RestService.deleteStep($scope, $scope.item, $scope.item.procedure, "/steps/" + $scope.item.procedure.id);
    }, true);
  };

  $scope.deleteMeasure = function (measure) {
    $scope.wrapEditWarning(function () {
      RestService.deleteMeasure($scope, measure, $scope.item, $scope.item.procedure, null);
    }, true);
  };

  $scope.editMeasure = function (measure) {
    $scope.wrapEditWarning(function () {
      var modalInstance = $modal.open({
        templateUrl: 'templates/steps/editmeasure.html',
        controller: 'StepEditMeasureCtrl',
        size: 'lg',
        resolve: {
          measure: function () {
            return measure.id
          },
          mainscope: function () {
            return $scope
          }
        }
      });
    });
  };

  $scope.newMeasure = function () {
    $scope.wrapEditWarning(function () {
      var modalInstance = $modal.open({
        templateUrl: 'templates/steps/editmeasure.html',
        controller: 'StepEditMeasureCtrl',
        size: 'lg',
        resolve: {
          measure: function () {
            return -1
          },
          mainscope: function () {
            return $scope
          }
        }
      });
    }, true);
  };

  $scope.testMeasure = function (measure, mtinfo) {
    if (mtinfo == null) mtinfo = MeasureTypeService.mtinfoForId($scope, measure.measuretype)
    var modalInstance = $modal.open({
      templateUrl: 'templates/checks/input.html',
      controller: 'InputMeasurementCtrl',
      //size: 'lg',
      resolve: {
        measure: function () {
          return measure
        },
        mainscope: function () {
          return $scope
        },
        testmode: function () {
          return true
        },
        measurement: function () {
          return null
        },
        mtinfo: function () {
          return mtinfo
        }
      }
    })
  };

  $scope.exportMeasure = function (measure, event) {
    $window.open("/servlet/export/measures/" + measure.id + ((event.shiftKey || event.metaKey) ? '?complete=true' : ''))
  };

  $scope.mediamanager = function () {
    $scope.wrapEditWarning(function () {
      MediaService.openMediaManager("Step", $scope.item.id, "STEP.VIEW.MEDIAMANAGER", {
        pcode: $scope.item.procedure.code, ptitle: i18n.translate($scope.item.procedure.title),
        scode: $scope.item.code, stitle: i18n.translate($scope.item.title)
      }).result.then(function () {
        $scope.refreshData();
      });
    });
  };

  UploaderService.installImportUploader($scope, "measures", null, $stateParams.stepid);

  $scope.wfRuleManagerStep = function () {
    $scope.wrapEditWarning(function () {
      WorkflowModifyService.openWorkflowModifierEditor({
        msg: 'STEP.VIEW.FLOWEDITOR',
        vals: {scode: $scope.item.code, stitle: i18n.translate($scope.item.title)}
      }, $scope.item.procedure.id, $scope.item.flowcontrol).result.then(function () {
        // Calling route PSM_18
        Restangular.all('steps').post($scope.item).then(function (item) {
          $scope.refreshData();
        }, AlertService.showRESTError);
      });
    }, true);
  };

  $scope.wfRuleManagerMeasure = function (measure) {
    $scope.wrapEditWarning(function () {
      WorkflowModifyService.openWorkflowModifierEditor({
        msg: 'MEASURE.VIEW.FLOWEDITOR',
        vals: {mcode: measure.code, mtitle: i18n.translate(measure.title)}
      }, $scope.item.procedure.id, measure.flowcontrol).result.then(function () {
        // Calling route PSM_23
        Restangular.all('measures').post(measure).then(function (item) {
          $scope.refreshData();
        }, AlertService.showRESTError);
      });
    }, true);
  };

  $scope.cloneMeasure = function () {
    $scope.wrapEditWarning(function () {
      UploaderService.cloneMeasure($stateParams.stepid, $scope.refreshData);
    }, true);
  };

  $scope.setEnforceLevel = function (measure, newlevel) {
    $scope.wrapEditWarning(function () {
      // Calling route PSM_23
      Restangular.all('measures').post({id: measure.id, enforce: newlevel}).then(function (item) {
        $scope.refreshData();
      }, AlertService.showRESTError);
    }, true);
  };

});

