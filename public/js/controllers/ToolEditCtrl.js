cmod.controller('ToolEditCtrl', function ($rootScope, $modal, $scope, $stateParams, $location, i18n, Restangular, UsersService, VersioningService, AlertService, EditorManagementService) {
  EditorManagementService.installEditorManagement($scope, 'tools', '/tools');

  $scope.$on("$destroy", function () {
    $modal.closeall();
  });

  $scope.refreshData = function () {
    if ($stateParams.toolid === "new") {
      $rootScope.selecteditem = null;
      var tool = {code: ""};
      $scope.fillupLang(tool, 'title');
      $scope.fillupLang(tool, 'description');
      $scope.editor.title = "TOOL.EDITOR.TITLE.NEW";
      $scope.item = tool;
      $scope.initCodeCheck(null);
      $scope.toolEditLoaded = true
    } else {
      // Calling route MISC_3
      Restangular.one('tools', $stateParams.toolid).get({foredit: true}).then(function unitData(response) {
        var tool = response;
        $rootScope.selecteditem = tool;
        $scope.fillupLang(tool, 'description');
        $scope.editor.title = "TOOL.EDITOR.TITLE.EDIT";
        $scope.editor.titlevals = {unit: tool.code};
        $scope.item = tool;
        $scope.initCodeCheck($scope.item.code);
        $scope.toolEditLoaded = true;
      }, AlertService.showSevereRESTError);
    }
  };

  UsersService.addGrantMethods($scope).then(function () {
    $scope.refreshData();
    $('#tt_code').focus();
  });

});