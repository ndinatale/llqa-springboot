cmod.controller('ToolListCtrl', function ($rootScope, $modal, $location, FileUploader, $scope, i18n, VersioningService, Restangular, UsersService, AlertService, ListManagementService) {
  ListManagementService.installListManagement($scope, 'tool_filters');

  $scope.$on("$destroy", function () {
    $modal.closeall();
  });

  $scope.refreshListData = function () {
    // Calling route MISC_2
    Restangular.all('tools').getList().then(function ttypeList(response) {
      var tooltypes = response;
      $scope.realidstatus = {};
      $scope.items = tooltypes;
      $scope.toolListLoaded = true;
    }, AlertService.showSevereRESTError);
  };

  $scope.goto = function (where) {
    $location.url("/app/tools/view/" + where);
  };

  $scope.newTooltype = function () {
    $location.url("/app/tools/edit/new");
  };

  UsersService.addGrantMethods($scope).then(function () {
    $scope.loadFilterSettings();
    $scope.refreshListData();
  });
});