cmod.controller('ToolViewCtrl', function ($rootScope, $modal, $scope, $location, $window, i18n, $stateParams, Restangular, MediaService, AlertService, UsersService, RestService, ModalEditorService, ViewerManagementService, ListManagementService) {

  ViewerManagementService.installViewerManagement($scope, null, 'tool', '/tools');

  $scope.$on("$destroy", function () {
    $modal.closeall();
  });

  $scope.showDeleted = false;
  $scope.toggleShowDeleted = function () {
    $scope.showDeleted = !$scope.showDeleted;
  };

  $scope.refreshData = function () {
    // Calling route MISC_3
    Restangular.one('tools', $stateParams.toolid).get().then(function toolData(response) {
      var tooltype = response;
      ListManagementService.setSelectedItem(tooltype);
      $scope.item = tooltype;
      $scope.toolViewLoaded = true;
      _.each($scope.item.toolunits, function (unit) {
        unit.hasComment = !_.isNull(unit.comment) && unit.comment != '';
      });
    }, AlertService.showSevereRESTError);
  };

  UsersService.addGrantMethods($scope).then(function () {
    $scope.refreshData();
  });

  $scope.newToolunit = function () {
    ModalEditorService.showTextLineEditor("TOOL.NEWUNIT.TITLE", "TOOL.NEWUNIT.TEXT", null).result.then(function (selv) {
      // Calling route MISC_6
      Restangular.all("toolunits").post({tooltype_id: $scope.item.id, code: selv}).then(function () {
        $scope.refreshData();
        $scope.refreshListData();
      }, AlertService.showRESTError);
    });
  };

  $scope.editToolunit = function (unit) {
    ModalEditorService.showTextLineEditor("TOOL.EDITUNIT.TITLE", "TOOL.EDITUNIT.TEXT", unit.code).result.then(function (selv) {
      unit.code = selv;
      // Calling route MISC_6
      Restangular.all("toolunits").post(unit).then(function () {
        $scope.refreshData();
        $scope.refreshListData();
      }, AlertService.showRESTError);
    })
  };

  $scope.toggleToolunitDeleted = function (unit) {
    unit.deleted = !unit.deleted;
    // Calling route MISC_6
    Restangular.all("toolunits").post(unit).then(function () {
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  $scope.deleteToolunit = function (unit) {
    if ($scope.item.toolunits.length == 1 && $scope.item.measures.length > 0) {
      var msg = AlertService.createErrorMessage("TOOL.ALERT.DENY", "TOOL.ALERT.NODELETEUNIT", {code: unit.code});
      msg.addReason("TOOL.ALERT.DELETELASTUNITCASCADING", {ccnt: unit.measurements.length});
      msg.show();
      return;
    }
    if (unit.measurements.length > 0) {
      var msg = AlertService.createErrorMessage("TOOL.ALERT.DENY", "TOOL.ALERT.NODELETEUNIT", {code: unit.code});
      msg.addReason("TOOL.ALERT.DELETEUNITCASCADING", {ccnt: unit.measurements.length});
      msg.show();
      return;
    }
    var msg = AlertService.createConfirmMessage("TOOL.ALERT.CONFHEADER", "TOOL.ALERT.DELETEUNIT", {code: unit.code});
    msg.show().result.then(function () {
      // Calling route MISC_7
      Restangular.one('toolunits', unit.id).remove().then(function () {
        $scope.refreshData();
        $scope.refreshListData();
      }, AlertService.showRESTError);
    });
  };

  $scope.setToolunitComment = function (unit) {
    ModalEditorService.showTextLineEditor("TOOL.COMMENTUNIT.TITLE", "TOOL.COMMENTUNIT.TEXT", unit.comment).result.then(function (selv) {
      unit.comment = selv;
      // Calling route MISC_6
      Restangular.all("toolunits").post(unit).then(function () {
        $scope.refreshData();
        $scope.refreshListData();
      }, AlertService.showRESTError);
    });
  };

  $scope.mediamanager = function () {
    MediaService.openMediaManager("Tooltype", $scope.item.id, "TOOL.MEDIAMANAGER", {
      ttcode: $scope.item.code, tttitle: i18n.translate($scope.item.title)
    }).result.then(function () {
      $scope.refreshData();
    })
  };

  $scope.deleteItem = function () {
    if ($scope.item.measures.length > 0) {
      var msg = AlertService.createErrorMessage("TOOL.ALERT.DENY", "TOOL.ALERT.NODELETE", {code: $scope.item.code});
      msg.addReason("TOOL.ALERT.DELETECASCADING", {ccnt: $scope.item.measures.length});
      msg.show();
      return;
    }
    var msg = AlertService.createConfirmMessage("TOOL.ALERT.CONFHEADER", "TOOL.ALERT.DELETE", {code: $scope.item.code});
    msg.show().result.then(function () {
      // Calling route MISC_4
      Restangular.one('tools', $scope.item.id).remove().then(function () {
        $scope.refreshListData();
        $location.url("/app/tools");
      }, AlertService.showRESTError);
    });
  };

  $scope.generateReport = function (unit) {
    ModalEditorService.showDatePicker("TOOL.REPORT.TITLE", "TOOL.REPORT.DATE1", null).result.then(function (date1) {
      ModalEditorService.showDatePicker("TOOL.REPORT.TITLE", "TOOL.REPORT.DATE2", null).result.then(function (date2) {
        var sortoptions = [
          {id: 'check', name: 'TOOL.REPORT.SORT.MODEL'},
          {id: 'time', name: 'TOOL.REPORT.SORT.TIME'}
        ];
        ModalEditorService.showSinglePicker("TOOL.REPORT.TITLE", "TOOL.REPORT.SORT.TEXT", 'check', sortoptions).result.then(function (soption) {
          var tzoff = new Date().getTimezoneOffset();
          var query = "?lang=" + i18n.selectedLanguage.code + "&tzoff=" + tzoff + "&sort=" + soption;
          if (date1) {
            date1.setHours(0);
            query += "&dfrom=" + (date1.getTime() / 1000);
          }
          if (date2) {
            date2.setHours(23);
            date2.setMinutes(59);
            date2.setSeconds(59);
            query += "&dto=" + (date2.getTime() / 1000);
          }
          $window.open("/servlet/pdf/unitreport/" + unit.id + query);
        });
      });
    });
  };

});

