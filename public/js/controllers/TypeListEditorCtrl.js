cmod.controller('TypeListEditorCtrl', function ($rootScope, $scope, $modalInstance, list, param, i18n, Restangular, EditorManagementService, UsersService, MediaService, AlertService) {
  EditorManagementService.installEditorManagement($scope, list + "s", null);

  $scope.list = list;
  $scope.title = param.title;
  $scope.text = param.text;
  $scope.mmEnabled = param.media;
  $scope.editmode = false;
  $scope.edititem = null;

  $scope.refreshData = function () {
    // Calling route MISC_8(list='devicetype'),MISC_11('checktype')
    Restangular.all(list + "s").getList({all: true}).then(function (data) {
      $scope.items = data;
      $scope.typeListLoaded = true
    }, AlertService.showSevereRESTError);
  };

  $scope.mayDelete = function (item) {
    if ((item.modelcnt && item.modelcnt > 0) || (item.checkcnt && item.checkcnt > 0)) {
      return false;
    }
    return true;
  };

  $scope.actionDelete = function (item) {
    // Calling route MISC_9(list='devicetype'),MISC_12('checktype')
    Restangular.one(list + "s", item.id).remove().then(function () {
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  $scope.actionMediaManager = function (item) {
    MediaService.openMediaManager(list.charAt(0).toUpperCase() + list.substring(1), item.id, list.toUpperCase() + ".MEDIAMANAGER", {
      code: item.code, title: i18n.translate(item.title)
    }).result.then(function () {
      $scope.refreshData();
    });
  };

  $scope.actionEdit = function (item) {
    $scope.edititem = item;
    $scope.editmode = true;
    $scope.editnew = false;
    $scope.initCodeCheck(item.code);
  };

  $scope.actionDisable = function (item, val) {
    item.deleted = val;
    // Calling route MISC_10(list='devicetype'),MISC_13('checktype')
    Restangular.all(list + "s").post(item).then(function () {
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  $scope.actionNewItem = function () {
    var item = {code: "", deleted: false};
    $scope.fillupLang(item, 'title');
    $scope.fillupLang(item, 'description');
    $scope.edititem = item;
    $scope.editmode = true;
    $scope.editnew = true;
    $scope.initCodeCheck(null);
  };

  $scope.cancelEdit = function () {
    $scope.editmode = false;
    $scope.edititem = null;
  };

  $scope.saveEdit = function () {
    $scope.codeCheck($scope.edititem, function () {
      // Calling route MISC_10(list='devicetype'),MISC_13('checktype')
      Restangular.all(list + "s").post($scope.edititem).then(function () {
        $scope.editmode = false;
        $scope.refreshData();
      }, AlertService.showRESTError);
    });
  };

  UsersService.addGrantMethods($scope).then(function () {
    $scope.refreshData();
  });

  $scope.close = function () {
    $modalInstance.close();
  };
});