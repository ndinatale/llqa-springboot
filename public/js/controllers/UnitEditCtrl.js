cmod.controller('UnitEditCtrl', function ($rootScope, $modal, $scope, $timeout, $stateParams, $location, i18n, Restangular, UsersService, VersioningService, AlertService, EditorManagementService) {
  EditorManagementService.installEditorManagement($scope, 'units', '/units/' + $stateParams.modid)

  $scope.$on("$destroy", function () {
    $modal.closeall()
  });

  $scope.refreshData = function () {
    // Calling route MU_3
    Restangular.one('models', $stateParams.modid).get().then(function (response) {
      $scope.model = response;
      // Calling route MU_1
      Restangular.all('models').getList({selector: true}).then(function dtypeData(response) {
        $scope.models = response;
        if ($stateParams.unitid === "new") {
          $rootScope.selecteditem = null;
          var unit = {code: "", customer: "", status: 0};
          $scope.fillupLang(unit, 'description');
          _.each($scope.models, function (model) {
            if (model.realid == $scope.model.realid) {
              unit.model_id = model.id;
            }
          });
          $scope.editor.title = "UNIT.EDITOR.TITLE.NEW";
          $scope.item = unit;
          $scope.initCodeCheck(null);
          $scope.unitEditLoaded = true;
          console.log("X");
          focusCodeField();
        } else {
          // Calling route MU_18
          Restangular.one('units', $stateParams.unitid).get({foredit: true}).then(function unitData(response) {
            var unit = response;
            $rootScope.selecteditem = unit;
            $scope.fillupLang(unit, 'description');
            $scope.editor.title = "UNIT.EDITOR.TITLE.EDIT";
            $scope.editor.titlevals = {unit: unit.code};
            $scope.item = unit;
            $scope.initCodeCheck($scope.item.code);
            $scope.unitEditLoaded = true;
            focusCodeField();
          }, AlertService.showSevereRESTError);
        }
      }, AlertService.showSevereRESTError);
    }, AlertService.showSevereRESTError);
  };

  function focusCodeField() {
    $timeout(function () {
      $('#un_code').focus();
    });
  }

  UsersService.addGrantMethods($scope).then(function () {
    $scope.refreshData();
  });

  $scope.modelCboxName = function (model) {
    return model.code + ': ' + i18n.translate(model.title);
  }

});