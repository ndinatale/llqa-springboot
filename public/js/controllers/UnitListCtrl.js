cmod.controller('UnitListCtrl', function ($rootScope, $modal, $stateParams, $location, FileUploader, $scope, i18n, ModalEditorService, VersioningService, Restangular, UsersService, AlertService, ListManagementService, UploaderService) {
  ListManagementService.installListManagement($scope, 'unit_filters');

  $scope.$on("$destroy", function () {
    $modal.closeall();
  });

  $scope.archivemode = null;

  $scope.refreshListData = function () {
    // Calling route MU_3
    Restangular.one('models', $stateParams.modid).get({foredit: true}).then(function (response) {
      $scope.model = response;
      $scope.unitListLoaded = true;
      // Calling route MU_17
      return Restangular.one('models', $scope.model.realid).all('units').getList($scope.archivemode ? {archive: $scope.archivemode} : null)
    }, AlertService.showSevereRESTError).then(function (response) {
      var units = response;
      $scope.realidstatus = {};
      $scope.items = units;
      if ($scope.archivemode && $scope.items.length >= 100) {
        AlertService.createAlertMessage("UNIT.ARCHIVE.MANYRES.TTL", "UNIT.ARCHIVE.MANYRES.TXT", {}).show();
      }
    }, AlertService.showSevereRESTError);
  };

  $scope.goto = function (where) {
    $location.url("/app/units/" + $scope.model.id + "/view/" + where);
  };

  $scope.newUnit = function () {
    $location.url("/app/units/" + $scope.model.id + "/edit/new");
  };

  $scope.gotoModel = function () {
    $location.url("/app/models/view/" + $scope.model.id);
  };

  UsersService.addGrantMethods($scope).then(function () {
    $scope.loadFilterSettings();
    $scope.refreshListData();
  });

  $scope.toggleShowLocked = function () {
    if (_.isUndefined($scope.filters.showLocked)) {
      $scope.filters.showLocked = true;
    }
    else {
      $scope.filters.showLocked = !$scope.filters.showLocked;
    }
    $scope.saveFilterSettings();
  };

  $scope.toggleArchiveMode = function () {
    if (!_.isNull($scope.archivemode)) {
      $scope.archivemode = null;
      $scope.refreshListData();
      return;
    }
    ModalEditorService.showTextLineEditor("UNIT.ARCHIVE.TTL", "UNIT.ARCHIVE.TXT", null, null).result.then(function (res) {
      if (_.isNull(res) || res == "") {
        return;
      }
      $scope.archivemode = res;
      $scope.refreshListData();
    });
  };
});