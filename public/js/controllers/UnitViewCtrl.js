cmod.controller('UnitViewCtrl', function ($rootScope, $modal, $scope, $location, $filter, $window, i18n, $stateParams, Restangular, VersioningService, AlertService, UsersService, RestService, ModalEditorService, ViewerManagementService, ListManagementService, featureFlags) {
  $scope.skipsched = $rootScope.appProperties.skipcheckschedule;

  ViewerManagementService.installViewerManagement($scope, null, 'unit', '/units/' + $stateParams.modid);

  $scope.$on("$destroy", function () {
    $modal.closeall();
  });

  $scope.refreshData = function () {
    // Calling route MU_18
    Restangular.one('units', $stateParams.unitid).get().then(function procData(response) {
      var unit = response;
      ListManagementService.setSelectedItem(unit);
      unit.model.versname = VersioningService.versionName(unit.model.version, null);
      var statusChangeable = true;
      _.each(unit.checks, function (chk) {
        if (chk.status < 40) {
          statusChangeable = false;
        }
      });
      $scope.statusChangeable = statusChangeable;
      $scope.item = unit;
      $scope.unitViewLoaded = true;
    }, AlertService.showSevereRESTError);
  };

  UsersService.addGrantMethods($scope).then(function () {
    $scope.refreshData();
  });

  $scope.newCheck = function () {
    var vlist = [];
    _.each($scope.item.model.activechecktypes, function (val) {
      vlist.push({id: val.checktype.id, title: val.checktype.title})
    });

    // first, choose checktype
    ModalEditorService.showSinglePicker("UNIT.NEWCHECK.TITLE", "UNIT.NEWCHECK.TEXT", null, vlist).result.then(function (selv) {
      // if feature flag for configtable is not set, we're done, fire request for a new check
      if (!featureFlags.isOn("configtable")) {
        Restangular.all("checks").post({
          unit_id: $scope.item.id,
          model_id: $scope.item.model.id,
          checktype_id: selv,
          configtable: -1
        }).then(function () {
          $scope.refreshData();
          $scope.refreshListData();
        }, AlertService.showRESTError);
      }
      else {
        // else ask which configtable to use (if more than one available)
        Restangular.one('units', $scope.item.id).all("configtables").getList().then(function procData(response) {
          var unitTables = response;
          if (unitTables.length === 0) {
            // new config table
            Restangular.all("checks").post({
              unit_id: $scope.item.id,
              model_id: $scope.item.model.id,
              checktype_id: selv,
              configtable: -1
            }).then(function () {
              $scope.refreshData();
              $scope.refreshListData();
            }, AlertService.showRESTError);
          }
          else {
            // prepare choices
            var choices = [];
            // TODO do translations the proper way...
            var txt = $filter('translate')('UNIT.NEWCHECK.CONFIGTABLE.NEW');
            choices.push({id: -1, title: {de: txt, en: txt, cn: txt, fr: txt}});
            _.each(unitTables, function (val) {
              var title = val.title + ' (' + $filter('translate')('UNIT.NEWCHECK.CONFIGTABLE.CHECK') + ' #' + val.parentid + ')';
              val.title = {de: title, en: title, cn: title, fr: title};
              choices.push(val);
            });
            ModalEditorService.showSinglePicker("UNIT.NEWCHECK.CONFIGTABLE.TITLE", "UNIT.NEWCHECK.CONFIGTABLE.TEXT", null, choices).result.then(function (choice) {
              Restangular.all("checks").post({
                unit_id: $scope.item.id,
                model_id: $scope.item.model.id,
                checktype_id: selv,
                configtable: choice
              }).then(function () {
                $scope.refreshData();
                $scope.refreshListData();
              }, AlertService.showRESTError);
            });
          }
        }, AlertService.showRESTError);
      }
    })
  };

  $scope.deleteCheck = function (check) {
    var msg = AlertService.createConfirmMessage("UNIT.ALERT.CONFHEADER", "UNIT.ALERT.DELETECHECK.TITLE", {});
    msg.addReason("UNIT.ALERT.DELETECHECK.TEXT", {});
    msg.show().result.then(function () {
      // Calling route CHECK_6
      Restangular.one('workflow', check.id).all("action").post({action: 'delete'}).then(function () {
        $scope.refreshData();
        $scope.refreshListData();
      }, AlertService.showRESTError);
    })
  };

  $scope.editDateCommissioned = function () {
    ModalEditorService.showDatePicker("UNIT.MODIFY.DATE.TITLE", "UNIT.MODIFY.DATE.COMMISSIONED", $scope.item.commissioned).result.then(function (val) {
      $scope.item.commissioned = val;
      // Calling route MU_20
      Restangular.all("units").post($scope.item).then(function () {
        $scope.refreshData();
      }, AlertService.showRESTError);
    })
  };

  $scope.editDateFinished = function () {
    ModalEditorService.showDatePicker("UNIT.MODIFY.DATE.TITLE", "UNIT.MODIFY.DATE.FINISHED", $scope.item.finished).result.then(function (val) {
      $scope.item.finished = val;
      // Calling route MU_20
      Restangular.all("units").post($scope.item).then(function () {
        $scope.refreshData();
      }, AlertService.showRESTError);
    })
  };

  $scope.editDateDelivered = function () {
    ModalEditorService.showDatePicker("UNIT.MODIFY.DATE.TITLE", "UNIT.MODIFY.DATE.DELIVERED", $scope.item.delivered).result.then(function (val) {
      $scope.item.delivered = val;
      // Calling route MU_20
      Restangular.all("units").post($scope.item).then(function () {
        $scope.refreshData();
      }, AlertService.showRESTError);
    })
  };

  $scope.editDateApproved = function () {
    ModalEditorService.showDatePicker("UNIT.MODIFY.DATE.TITLE", "UNIT.MODIFY.DATE.APPROVED", $scope.item.approved).result.then(function (val) {
      $scope.item.approved = val;
      // Calling route MU_20
      Restangular.all("units").post($scope.item).then(function () {
        $scope.refreshData();
      }, AlertService.showRESTError);
    })
  };

  $scope.editComment = function () {
    ModalEditorService.showTextAreaEditor("UNIT.MODIFY.COMMENT.TITLE", "UNIT.MODIFY.COMMENT.TEXT", $scope.item.comment).result.then(function (val) {
      $scope.item.comment = val;
      // Calling route MU_20
      Restangular.all("units").post($scope.item).then(function () {
        $scope.refreshData();
      }, AlertService.showRESTError);
    })
  };

  $scope.deleteItem = function () {
    var msg = AlertService.createConfirmMessage("UNIT.ALERT.CONFHEADER", "UNIT.ALERT.DELETE", {code: $scope.item.code});
    if ($scope.item.checks.length > 0) {
      msg.addReason("UNIT.ALERT.DELETECASCADING", {ccnt: $scope.item.checks.length});
    }
    msg.show().result.then(function () {
      // Calling route MU_19
      Restangular.one('units', $scope.item.id).remove().then(function () {
        $scope.refreshListData();
        $location.url("/app/units/" + $stateParams.modid);
      }, AlertService.showRESTError);
    });
  };

  $scope.gotoModel = function (model) {
    $location.url("/app/models/view/" + model.id);
  };

  $scope.gotoCheck = function (check) {
    $location.url("/app/workflow/" + check.id);
  };

  $scope.gotoConfigtable = function (configtable) {
    $location.url("/app/configmgt/view/" + configtable.id);
  };

  $scope.getUnitStatus = function () {
    if ($scope.item && $scope.item.status == 10) {
      return "UNIT.VIEW.STATUS.CLOSED";
    }
    if ($scope.item && $scope.item.status == 20) {
      return "UNIT.VIEW.STATUS.DISCARDED";
    }
    if ($scope.item && $scope.item.status == 110) {
      return "UNIT.VIEW.STATUS.CLOSEDARCH";
    }
    if ($scope.item && $scope.item.status == 120) {
      return "UNIT.VIEW.STATUS.DISCARCH";
    }
    if ($scope.item && $scope.item.status >= 100) {
      return "UNIT.VIEW.STATUS.ARCHIVED";
    }
    return "UNIT.VIEW.STATUS.OPEN";
  };

  $scope.setUnitStatus = function () {
    var vlist = [
      {id: 0, name: 'UNIT.VIEW.STATUS.OPEN'},
      {id: 10, name: 'UNIT.VIEW.STATUS.CLOSED'},
      {id: 20, name: 'UNIT.VIEW.STATUS.DISCARDED'}
    ];
    ModalEditorService.showSinglePicker("UNIT.ALERT.STATUSCHG.TTL", "UNIT.ALERT.STATUSCHG.TXT", $scope.item.status, vlist).result.then(function (selv) {
      // Calling route MU_20
      Restangular.all("units").post({id: $scope.item.id, status: selv}).then(function () {
        $scope.refreshData();
        $scope.refreshListData();
      }, AlertService.showRESTError);
    })
  };

  $scope.unarchive = function () {
    AlertService.createConfirmMessage("UNIT.ALERT.STATUSCHG.TTL", "UNIT.ALERT.UNARCHIVE.TXT", {}).show().result.then(function (res) {
      // Calling route MU_20
      Restangular.all("units").post({id: $scope.item.id, status: $scope.item.status - 100}).then(function () {
        $scope.refreshData();
        $scope.refreshListData();
      }, AlertService.showRESTError);
    });
  };
});

