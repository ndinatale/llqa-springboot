cmod.controller('UserPickerCtrl', function ($rootScope, $scope, $filter, $modalInstance, i18n, title, text, presel, param, Restangular, AlertService) {
  $scope.title = title;
  $scope.text = text;
  $scope.selusr = presel.user;
  $scope.selgrp = presel.group;
  $scope.lastselgrp = null;

  $scope.refreshPickerData = function () {
    // Calling route USER_1
    Restangular.setBaseUrl('http://localhost:8080/api');
    Restangular.all('users').one('shortlist').get().then(function (res) {
      Restangular.setBaseUrl('/api');
      $scope.ressource = res;
      $scope.setupPicker(false);
      $scope.userPickLoaded = true;
    }, AlertService.showSevereRESTError);
  };

  $scope.setupPicker = function (resetuser) {
    if (_.isUndefined($scope.ressource)) {
      return;
    }
    if ($scope.lastselgrp == $scope.selgrp) {
      return;
    }
    $scope.lastselgrp = $scope.selgrp;
    if (_.isUndefined($scope.groups)) {
      var groups = [];
      if (param.delassign) {
        groups.push({id: -2, name: $filter('translate')("USERS.DELASSIGN")});
      }
      if (param.anygroup) {
        groups.push({id: -1, name: $filter('translate')("USERS.ANYGROUP")});
      }
      _.each($scope.ressource.groups, function (grp) {
        var grpname = "";
        if (!_.isNull(grp.level)) {
          grpname += grp.level + ": ";
        }
        grpname += grp.name;
        groups.push({id: grp.id, name: grpname});
        if (!param.anygroup && $scope.selgrp < 0) {
          $scope.selgrp = grp.id
        }
      });
      $scope.groups = groups;
    }
    if (resetuser) {
      $scope.selusr = $scope.selgrp == -2 ? -2 : -1;
    }
    var users = [];
    if (param.delassign) {
      users.push({id: -2, name: $filter('translate')("USERS.DELASSIGN")});
    }
    if (param.anyuser && $scope.selgrp >= -1) {
      users.push({id: -1, name: $filter('translate')("USERS.ANYUSER")});
    }
    _.each($scope.ressource.users, function (usr) {
      if ($scope.selgrp == -1 || _.includes(usr.group_ids, $scope.selgrp)) {
        var usrname = usr.username + " (" + usr.realname + ")";
        users.push({id: usr.id, name: usrname});
        if (!param.anyuser && $scope.seluser < 0) {
          $scope.seluser = usr.id;
        }
      }
    });
    $scope.users = users;
  };

  $scope.refreshPickerData();

  $scope.ok = function () {
    $modalInstance.close({user: $scope.selusr, group: $scope.selgrp});
  };

  $scope.cancel = function () {
    $modalInstance.dismiss();
  }

});