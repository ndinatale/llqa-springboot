cmod.controller('UserManagerCtrl', function ($rootScope, $modal, $location, $stateParams, FileUploader, $scope, i18n, VersioningService, Restangular, UsersService, AlertService, ListManagementService, ModalEditorService) {
  ListManagementService.installListManagement($scope, 'usermgt_filters');

  $scope.$on("$destroy", function () {
    $modal.closeall();
  });

  $scope.refreshData = function () {
    // Calling route USER_2
     Restangular.setBaseUrl('http://localhost:8080/api');
    Restangular.one('users').one('full').get().then(function (response) {
      Restangular.setBaseUrl('/api');
      var groups = response.groups;
      var users = response.users;

      var privileges = {};
      _.forEach(response.privileges, function (priv) {
        privileges[priv.id] = priv;
        priv.multi = _.endsWith(priv.code, 'ALL');
      });

      var groupMap = {};
      _.forEach(response.groups, function (group) {
        groupMap[group.id] = group;
        group.users = [];
        var privs = [];
        _.forEach(group.grants, function (grant) {
          privs.push(privileges[grant.privilege_id]);
        });
        group.privileges = _.sortBy(privs, function (gr) {
          return gr.code;
        });
        if (group.id === $scope.selgroupid) {
          $scope.selgroup = group;
        }
      });

      _.forEach(response.users, function (user) {
        user.groups = [];
        var allprivs = {};
        _.forEach(user.affiliations, function (aff) {
          var group = groupMap[aff.usergroup_id];
          user.groups.push(group);
          group.users.push(user);
          _.forEach(group.grants, function (grant) {
            allprivs[grant.privilege_id] = 1;
          });
        });

        _.forEach(user.grants, function (grant) {
          allprivs[grant.privilege_id] = allprivs[grant.privilege_id] ? 3 : 2;
        });

        var privs = {group: [], user: [], redundant: {}};
        _.forEach(allprivs, function (val, priv_id) {
          if (val == 1) {
            privs.group.push(privileges[priv_id]);
          }
          if (val >= 2) {
            privs.user.push(privileges[priv_id]);
            if (val == 3) {
              privs.redundant[priv_id] = true;
            }
          }
        });

        privs.group = _.sortBy(privs.group, function (gr) {
          return gr.code;
        });

        privs.user = _.sortBy(privs.user, function (gr) {
          return gr.code;
        });

        user.privileges = privs;

        if (user.id === $scope.seluserid) {
          $scope.seluser = user;
        }
      });

      $scope.groups = groups;
      $scope.users = users;
      $scope.privileges = privileges;
      $scope.userMgrLoaded = true;
    }, AlertService.showSevereRESTError);
  };

  UsersService.addGrantMethods($scope).then(function (sdata) {
    $scope.seluser = $scope.selgroup = null;
    $scope.selMode = 'user';
    $scope.showGroups = false;

    $scope.userid = sdata.userid;
    $scope.ownlevel = sdata.level;
    $scope.seluserid = null;
    $scope.selgroupid = null;

    if ($stateParams.what == 'me' || $stateParams.what == 'users') {
      $scope.seluserid = $scope.userid;
    } else {
      if ($stateParams.what == 'user') {
        $scope.seluserid = parseInt($stateParams.id);
      }
      if ($stateParams.what == 'group') {
        $scope.selgroupid = parseInt($stateParams.id);
        $scope.selMode = 'group';
        $scope.showGroups = true;
      }
    }

    $scope.loadFilterSettings();
    $scope.refreshData();
  });

  $scope.selectUser = function (user) {
    $scope.seluser = user;
    $scope.seluserid = user.id;
    $scope.selMode = 'user';
  };

  $scope.selectGroup = function (group) {
    $scope.selgroup = group;
    $scope.selgroupid = group.id;
    $scope.selMode = 'group';
    $scope.showGroups = true;
  };

  $scope.toggleGroups = function () {
    $scope.showGroups = !$scope.showGroups;
  };

  $scope.toggleShowDeletedGroups = function () {
    $scope.showDeletedGroups = !$scope.showDeletedGroups;
  };

  $scope.quickFilter = function (value, index) {
    if (_.isNull($scope.filters.textfilter) || $scope.filters.textfilter === '') {
      return true;
    }
    var filter = $scope.filters.textfilter.toLowerCase();
    if (!_.isUndefined(value.name) && !_.isNull(value.name) && value.name.toLowerCase().indexOf(filter) >= 0) {
      return true;
    }
    if (!_.isUndefined(value.username) && !_.isNull(value.username) && value.username.toLowerCase().indexOf(filter) >= 0) {
      return true;
    }
    if (!_.isUndefined(value.realname) && !_.isNull(value.realname) && value.realname.toLowerCase().indexOf(filter) >= 0) {
      return true;
    }
    return false;
  };

  $scope.updateUser = function (data) {
    data.id = $scope.seluser.id;
    // Calling route USER_3
    Restangular.setBaseUrl('http://localhost:8080/api');
    Restangular.all('users').customPUT(data).then(function () {
      Restangular.setBaseUrl('/api');
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  $scope.updateGroup = function (data) {
    data.id = $scope.selgroup.id;
    // Calling route USER_4
    Restangular.setBaseUrl('http://localhost:8080/api');
      Restangular.all('groups').customPUT(data).then(function () {
      Restangular.setBaseUrl('/api');
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  $scope.newUser = function () {
    ModalEditorService.showTextLineEditor("USERMGR.ACTION.ADDUSER.TITLE", "USERMGR.ACTION.USER.USERNAME", null).result.then(function (username) {
      if (_.isNull(username)) {
        return;
      }
      ModalEditorService.showTextLineEditor("USERMGR.ACTION.ADDUSER.TITLE", "USERMGR.ACTION.USER.REALNAME", null).result.then(function (realname) {
        if (_.isNull(realname)) {
          return;
        }
        // Calling route USER_3
        Restangular.setBaseUrl('http://localhost:8080/api');
        Restangular.all('users').post({username: username, realname: realname, passhash: "*"}).then(function (newuser) {
          Restangular.setBaseUrl('/api');
          $scope.seluserid = newuser.id;
          $scope.selgroupid = null;
          $scope.selMode = 'user';
          $scope.refreshData();
        }, AlertService.showRESTError);
      });
    });
  };

  $scope.newGroup = function () {
    ModalEditorService.showTextLineEditor("USERMGR.ACTION.ADDGROUP.TITLE", "USERMGR.ACTION.GROUP.NAME", null).result.then(function (name) {
      if (_.isNull(name)) {
        return;
      }
      // Calling route USER_4
      Restangular.setBaseUrl('http://localhost:8080/api');
      Restangular.all('groups').post({name: name, level: 1}).then(function (newgroup) {
        Restangular.setBaseUrl('/api');
        $scope.seluserid = null;
        $scope.selgroupid = newgroup.id;
        $scope.showGroups = true;
        $scope.selMode = 'group';
        $scope.refreshData();
      }, AlertService.showRESTError);
    });
  };

  $scope.toggleUserActive = function () {
    $scope.updateUser({status: ($scope.seluser.status == 1 ? 0 : 1)});
  };

  $scope.editUserUsername = function () {
    ModalEditorService.showTextLineEditor("USERMGR.ACTION.EDITUSER.TITLE", "USERMGR.ACTION.USER.USERNAME", $scope.seluser.username).result.then(function (input) {
      if (_.isNull(input)) {
        return;
      }
      $scope.updateUser({username: input});
    });
  };

  $scope.editUserRealname = function () {
    ModalEditorService.showTextLineEditor("USERMGR.ACTION.EDITUSER.TITLE", "USERMGR.ACTION.USER.REALNAME", $scope.seluser.realname).result.then(function (input) {
      if (_.isNull(input)) {
        return;
      }
      $scope.updateUser({realname: input});
    });
  };

  $scope.editUserComment = function () {
    ModalEditorService.showTextLineEditor("USERMGR.ACTION.EDITUSER.TITLE", "USERMGR.ACTION.USER.COMMENT", $scope.seluser.comment).result.then(function (input) {
      if (_.isNull(input)) {
        return;
      }
      $scope.updateUser({comment: input});
    });
  };

  $scope.editGroupName = function () {
    ModalEditorService.showTextLineEditor("USERMGR.ACTION.EDITGROUP.TITLE", "USERMGR.ACTION.GROUP.NAME", $scope.selgroup.name).result.then(function (input) {
      if (_.isNull(input)) {
        return;
      }
      $scope.updateGroup({name: input});
    });
  };

  $scope.editGroupLevel = function () {
    ModalEditorService.showTextLineEditor("USERMGR.ACTION.EDITGROUP.TITLE", "USERMGR.ACTION.GROUP.LEVEL", $scope.selgroup.level).result.then(function (input) {
      if (_.isNull(input)) {
        return;
      }
      var val = parseInt(input);
      if (val < 1) {
        return;
      }
      if (val >= $rootScope.session.level && !$scope.hasGrant('USRMGA')) {
        val = $rootScope.session.level - 1;
      }
      $scope.updateGroup({level: val});
    });
  };

  $scope.editGroupDescription = function () {
    ModalEditorService.showTextLineEditor("USERMGR.ACTION.EDITGROUP.TITLE", "USERMGR.ACTION.GROUP.DESCRIPTION", $scope.selgroup.description).result.then(function (input) {
      if (_.isNull(input)) {
        return;
      }
      $scope.updateGroup({description: input});
    });
  };

  $scope.changePassword = function () {
    ModalEditorService.showTextLineEditor("USERMGR.ACTION.EDITUSER.TITLE", "USERMGR.ACTION.USER.PASSWORD", null).result.then(function (input) {
      if (_.isNull(input)) {
        input = "";
      }
      $scope.updateUser({passhash: input == "" ? "*" : CryptoJS.SHA1(input) + ""});
    });
  };

  $scope.deleteGroup = function () {
    var msg = AlertService.createConfirmMessage("USERMGR.ACTION.DELETEGROUP.TITLE", "USERMGR.ACTION.DELETEGROUP.TEXT", null);
    msg.show().result.then(function () {
      // Calling route USER_4
      Restangular.setBaseUrl('http://localhost:8080/api');
      Restangular.one('groups').customPUT({id: $scope.selgroup.id, deleted: true}).then(function () {
        Restangular.setBaseUrl('/api');
        $scope.seluserid = $scope.userid;
        $scope.selMode = 'user';
        $scope.showGroups = true;
        $scope.refreshData();
      }, AlertService.showRESTError);
    });
  };

  $scope.reactivateGroup = function () {
    var msg = AlertService.createConfirmMessage("USERMGR.ACTION.REACTIVATEGROUP.TITLE", "USERMGR.ACTION.REACTIVATEGROUP.TEXT", null);
    msg.show().result.then(function () {
      $scope.updateGroup({deleted: false});
    });
  };

  $scope.addUserToGroup = function () {
    var items = [];
    _.each($scope.users, function (user) {
      if (!_.includes($scope.selgroup.users, user) && (user.rank < $scope.ownlevel || $scope.hasGrant('USRMGA'))) {
        items.push({id: {uid: user.id, gid: $scope.selgroup.id}, name: user.realname, code: user.username});
      }
    });
    items = _.sortBy(items, 'username');
    ModalEditorService.showMultiPicker("USERMGR.ACTION.ADDUSERTOGRP.TITLE", "USERMGR.ACTION.ADDUSERTOGRP.TEXT", null, items, {style: 1}).result.then(function (res) {
      // Calling route USER_7
      Restangular.setBaseUrl('http://localhost:8080/api');
      Restangular.all('affiliations').post(res).then(function () {
        Restangular.setBaseUrl('/api');
        $scope.refreshData();
      }, AlertService.showRESTError);
    });
  };

  $scope.addGroupToUser = function () {
    var items = [];
    _.each($scope.groups, function (group) {
      if (!_.includes($scope.seluser.groups, group) && (group.level < $scope.ownlevel || $scope.hasGrant('USRMGA'))) {
        items.push({id: {uid: $scope.seluser.id, gid: group.id}, name: group.name, code: group.level});
      }
    });

    items = _.sortBy(items, 'name');
    ModalEditorService.showMultiPicker("USERMGR.ACTION.ADDGRPTOUSER.TITLE", "USERMGR.ACTION.ADDGRPTOUSER.TEXT", null, items, {style: 1}).result.then(function (res) {
      // Calling route USER_7
      Restangular.setBaseUrl('http://localhost:8080/api');
      Restangular.all('affiliations').post(res).then(function () {
        Restangular.setBaseUrl('/api');
        $scope.refreshData();
      }, AlertService.showRESTError);
    });
  };

  $scope.removeUserFromGroup = function (user) {
    // Calling route USER_6
    Restangular.setBaseUrl('http://localhost:8080/api');
    Restangular.one('affiliations', user.id).one('group', $scope.selgroup.id).remove().then(function () {
      Restangular.setBaseUrl('/api');
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  $scope.removeGroupFromUser = function (group) {
    // Calling route USER_6
    Restangular.setBaseUrl('http://localhost:8080/api');
    Restangular.one('affiliations', $scope.seluser.id).one('group', group.id).remove().then(function () {
      Restangular.setBaseUrl('/api');
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  $scope.addGroupPrivilege = function () {
    var items = [];
    _.each($scope.privileges, function (pr) {
      if (!_.includes($scope.selgroup.privileges, pr) && ($scope.hasGrant(pr.code) || $scope.hasGrant('GRTPRA'))) items.push({
        id: pr.id,
        name: pr.name,
        code: pr.code,
        ttip: pr.description,
        emph: _.endsWith(pr.code, 'ALL')
      })
    });
    items = _.sortBy(items, 'code');
    ModalEditorService.showMultiPicker("USERMGR.ACTION.ADDGRANT.TITLE", "USERMGR.ACTION.ADDGRANT.TEXT.GROUP", null, items, {style: 1}).result.then(function (res) {
      // Calling route USER_9
      Restangular.setBaseUrl('http://localhost:8080/api');
      Restangular.one('groups', $scope.selgroup.id).all("grants").post(res).then(function () {
        Restangular.setBaseUrl('/api');
        $scope.refreshData();
      }, AlertService.showRESTError);
    });
  };

  $scope.addUserPrivilege = function () {
    var items = [];
    _.each($scope.privileges, function (pr) {
      if (!_.includes($scope.seluser.privileges.user, pr) && ($scope.hasGrant(pr.code) || $scope.hasGrant('GRTPRA'))) {
        items.push({id: pr.id, name: pr.name, code: pr.code, ttip: pr.description, emph: _.endsWith(pr.code, 'ALL')});
      }
    });
    items = _.sortBy(items, 'code');
    ModalEditorService.showMultiPicker("USERMGR.ACTION.ADDGRANT.TITLE", "USERMGR.ACTION.ADDGRANT.TEXT.USER", null, items, {style: 1}).result.then(function (res) {
      // Calling route USER_8
      Restangular.setBaseUrl('http://localhost:8080/api');
        Restangular.one('users', $scope.seluser.id).all("grants").post(res).then(function () {
        Restangular.setBaseUrl('/api');
        $scope.refreshData();
      }, AlertService.showRESTError);
    });
  };

  $scope.removeGroupPrivilege = function (grant) {
    // Calling route USER_9
    Restangular.setBaseUrl('http://localhost:8080/api');
    Restangular.one('groups', $scope.selgroup.id).one("grants", grant.id).remove().then(function () {
      Restangular.setBaseUrl('/api');
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  $scope.removeUserPrivilege = function (grant) {
    // Calling route USER_8
    Restangular.setBaseUrl('http://localhost:8080/api');
      Restangular.one('users', $scope.seluser.id).one("grants", grant.id).customDELETE().then(function () {
      Restangular.setBaseUrl('/api');
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  $scope.mayCreateNewUser = function () {
    return $scope.hasGrant('USRMGO');
  };

  $scope.mayBeEdited = function (obj) {
    if (_.isUndefined(obj) || _.isNull(obj)) {
      return false;
    }
    if (!$scope.hasGrant('USRMGO')) {
      return false;
    }
    if ($scope.hasGrant('USRMGA')) {
      return true;
    }
    if (_.isUndefined(obj.level)) { // User
      return $scope.hasEffectiveLevel(obj.rank + 1);
    } else { // Group
      return $scope.hasEffectiveLevel(obj.level + 1);
    }
  };

  $scope.mayAddGrants = function (obj) {
    if (!$scope.mayBeEdited(obj)) {
      return false;
    }
    if (_.isUndefined(obj.level)) { // User
      return $scope.hasGrant('GRTPRO') || $scope.hasGrant('GRTPRA');
    } else { // Group
      return $scope.hasGrant('GRTTOG') && ($scope.hasGrant('GRTPRO') || $scope.hasGrant('GRTPRA'));
    }
  };

  $scope.mayDelGrant = function (obj, grant) {
    if (!$scope.mayBeEdited(obj)) {
      return false;
    }
    if (_.isUndefined(obj.level)) { // User
      return ($scope.hasGrant('GRTPRO') && $scope.hasGrant(grant.code)) || $scope.hasGrant('GRTPRA');
    } else { // Group
      return $scope.hasGrant('GRTTOG') && (($scope.hasGrant('GRTPRO') && $scope.hasGrant(grant.code)) || $scope.hasGrant('GRTPRA'));
    }
  };

});