cmod.controller('VideoModalCtrl', function ($modalInstance, $rootScope, $scope, medium) {
  $scope.setupConfig = function () {
    var width = medium.binaryfile.metadata.width;
    var height = medium.binaryfile.metadata.height;
    var stretch = 'none';
    if (width > 0 && height > 0) {
      var scale = Math.min(800.0 / width, 500.0 / height);
      height = Math.floor(height * scale);
      width = Math.floor(width * scale);
      stretch = 'fill';
    } else {
      width = 800;
      height = 500;
    }

    $scope.title = medium.caption;
    if ($scope.title === null || $scope.title === "") {
      $scope.title = medium.binaryfile.original_filename;
    }

    $scope.medium = {url: "/media/" + medium.binaryfile.filename, type: medium.binaryfile.mimetype};
    $scope.config = {
      width: width,
      height: height,
      theme: "/css/videogular.css",
      autoplay: true,
      stretch: stretch,
      responsive: false,
      autoHide: true,
      autoHideTime: 4000
    }
  };

  $scope.setupConfig();

  $scope.close = function () {
    $modalInstance.close();
  };

});