cmod.controller('WorkflowIntroCtrl', function ($rootScope, $modal, $scope, $window, $stateParams, $location, $timeout, $filter, i18n, Restangular, ConstantService, VersioningService, MediaService, UsersService, ModalEditorService, AlertService) {
  $scope.isMobileApp = $rootScope.isMobileApp;
  $scope.highlightedAssignee = null;
  $scope.skipsched = $rootScope.appProperties.skipcheckschedule;

  var C = {};
  ConstantService.installConstants($scope, C, ['CHK_', 'MEDIA_', 'UNIT_']);

  var stat_statistic_map = [];
  stat_statistic_map[C.CHK_CDSTATUS_TODO] = 'todo';
  stat_statistic_map[C.CHK_CDSTATUS_PASSED] = 'pass';
  stat_statistic_map[C.CHK_CDSTATUS_WARNINGS] = 'fail';
  stat_statistic_map[C.CHK_CDSTATUS_FAILED] = 'fail';
  stat_statistic_map[C.CHK_CDSTATUS_OMITTED_HARD] = 'skip';
  stat_statistic_map[C.CHK_CDSTATUS_OMITTED_SOFT] = 'skip';
  stat_statistic_map[C.CHK_CDSTATUS_SKIPPED] = 'skip';

  $scope.sprintf = sprintf;

  UsersService.addGrantMethods($scope).then(function (info) {
    $scope.userinfo = info;
    $scope.refreshData();
  });

  $scope.selImage = null;
  $scope.selectedZoomImage = null;
  $scope.selImageData = null;

  $scope.zoomableImageOptions = {
    zoomType: "window",
    zoomWindowPosition: 6,
    zoomWindowOffety: 15,
    borderColour: '#005499',
    scrollZoom: true
  };


  /////////


  $scope.$on("$destroy", function () {
    $rootScope.deactivateNoticeButton();
    MediaService.killZoomImage('zoomimg');
    $modal.closeall();
  });

  $scope.refreshData = function () {
    // Calling route CHECK_3
    Restangular.one('workflow', $stateParams.checkid).get({userid: $scope.userinfo.userid}).then(function checkData(response) {
      var assignees = [];
      var statistics = {
        proc: {todo: 0, pass: 0, fail: 0, skip: 0},
        step: {todo: 0, pass: 0, fail: 0, skip: 0},
        instruction: 0,
        meas: {todo: 0, pass: 0, fail: 0, skip: 0}
      };
      var unassigned = {steps: 0, proclist: {}, procs: 0};
      var cnt = 0;
      $scope.mayreg = [];
      $scope.mayunreg = [];
      $scope.maycommit = false;
      $scope.testrun = (!_.isUndefined(response.metadata) && !_.isUndefined(response.metadata.testrun)) ? response.metadata.testrun.type : undefined;

      // process assignees
      _.each(response.checkdata.assignees, function (ass) {
        var assinfo;
        if (!_.isUndefined(ass.user_id)) {
          var uinfo = response.users[ass.user_id];
          var owned = ass.user_id == $scope.userinfo.userid;
          assinfo = {
            num: cnt, owned: owned, mayreg: false, mayunreg: owned && !_.isUndefined(ass.selfassign) && ass.selfassign,
            maytakeover: !owned && ass.selfassign && (!ass.group_id || _.includes(_.map($scope.userinfo.groups, 'groupid'), ass.group_id)) && $scope.hasGrant("TKOVER"),
            icon: "fa-user", procs: {}, numstep: 0, nummeas: 0,
            name: "#" + (cnt + 1) + ": " + uinfo.realname + " (" + ass.user_id + ", " + uinfo.username + ")"
          }
        } else if (!_.isUndefined(ass.group_id)) {
          var ginfo = response.groups[ass.group_id];
          assinfo = {
            num: cnt,
            owned: false,
            mayreg: _.includes(_.map($scope.userinfo.groups, 'groupid'), ass.group_id),
            mayunreg: false,
            maytakeover: false,
            icon: "fa-group",
            procs: {},
            numstep: 0,
            nummeas: 0,
            name: "#" + (cnt + 1) + ": " + ginfo.name + (ginfo.level ? " [" + ginfo.level + "]" : "")
          }
        } else {
          assinfo = {
            num: cnt, onwed: false, mayreg: true, mayunreg: false,
            maytakeover: false,
            icon: "fa-question-circle", procs: {}, numstep: 0, nummeas: 0,
            name: "#" + (cnt + 1) + ": ???"
          }
        }
        assinfo.regmode = ass.regmode;
        assinfo.subof = ass.subof;
        assignees.push(assinfo);
        cnt += 1;
      });

      // process procedures, steps, measures
      _.each(response.checkdata.procedures, function (proc) {
        proc.owned = false;
        _.each(proc.steps, function (step) {
          _.each(step.measures, function (meas) {
            meas.info = response.measures[meas.id];
            meas.selected = false;
            statistics.meas[stat_statistic_map[meas.status]] += 1;
            if ((C.matchConst(meas.status, C.CHK_CDSTATUS_ALL_FINISHED)) && assignees[step.assignee] && assignees[step.assignee].regmode == C.CHK_REGMODE_COMPLETE && assignees[step.assignee].mayunreg) {
              assignees[step.assignee].mayunreg = false;
            }
            if (step.assignee >= 0) {
              assignees[step.assignee].nummeas += 1;
            }
          });

          step.info = response.steps[step.id];
          step.asssel = false;
          step.collapsed = true;
          step.selected = false;
          step.owned = assignees[step.assignee] && assignees[step.assignee].owned;

          if (step.owned) {
            proc.owned = true;
            if (step.status > 0 && !step.committed) $scope.maycommit = true;
          }

          if (step.steptype != 1) {
            statistics.step[stat_statistic_map[step.status]] += 1;
          } else {
            statistics.instruction += 1;
          }

          if (step.assignee >= 0) {
            assignees[step.assignee].numstep += 1;
            assignees[step.assignee].procs[proc.code] = 1;
          } else {
            unassigned.steps += 1;
            unassigned.proclist[proc.id] = 1;
          }
        });
        proc.info = response.procedures[proc.id];
        proc.asssel = false;
        proc.collapsed = true;
        proc.selected = false;
        statistics.proc[stat_statistic_map[proc.status]] += 1;
      });

      _.forEach(assignees, function (assinfo) {
        if (assinfo.mayunreg) $scope.mayunreg.push(assinfo.num);
        if (assinfo.mayreg || assinfo.maytakeover) $scope.mayreg.push(assinfo.num);
        assinfo.numproc = _.keys(assinfo.procs).length;
      });

      _.forEach(['proc', 'step', 'meas'], function (coll) {
        var dc = statistics[coll];
        dc.total = dc.todo + dc.pass + dc.fail + dc.skip;
        if (dc.total > 0) {
          dc.todoprc = dc.todo * (100.0 / dc.total);
          dc.passprc = dc.pass * (100.0 / dc.total);
          dc.failprc = dc.fail * (100.0 / dc.total);
          dc.skipprc = dc.skip * (100.0 / dc.total);
        } else {
          dc.todoprc = dc.passprc = dc.failprc = dc.skipprc = 100.0
        }
      });

      // process media
      unassigned.procs = _.keys(unassigned.proclist).length;
      var documentlist = [];
      var imagelist = [];

      _.each(response.model.images, function (i) {
        imagelist.push(i);
      });

      _.each(response.model.devicetype.images, function (i) {
        imagelist.push(i);
      });

      _.each(response.model.documents, function (i) {
        documentlist.push(i);
      });

      _.each(response.model.devicetype.documents, function (i) {
        documentlist.push(i);
      });

      $scope.assignees = assignees;
      $scope.statistics = statistics;
      $scope.unassigned = unassigned;
      $scope.check = response;
      $scope.highlightedAssignee = null;
      $scope.imagelist = imagelist;
      $scope.documentlist = documentlist;
      if ($scope.imagelist.length > 0) {
        $scope.toggleImage(0);
      }
      var noticePath = {
        type: 'CheckGeneral',
        url: {type: 'workflowintro', checkid: $scope.check.id},
        path: [
          {type: 'Model', id: $scope.check.model.id},
          {type: 'Unit', id: $scope.check.unit.id},
          {type: 'Check', id: $scope.check.id}
        ]
      };
      $rootScope.activateNoticeButton(noticePath);
      $scope.wfIntroLoaded = true;

      // LLQA-207 - Für die Verbesserung der Performance wird der Filter für die Prozeduren/Schritte/Messungen
      // zurückgesetzt, damit diese alle eingeklappt sind (Es werden nur Prozeduren angezeigt)
      resetFilter();
      loadListFilter();
    }, AlertService.showSevereRESTError);
  };

  $scope.checkStatusName = function () {
    if (_.isUndefined($scope.check)) {
      return "???";
    }
    switch ($scope.check.status) {
      case null :
        return "WFLOW.INTRO.STATUS.INIT";
      case C.CHK_STATUS_INIT:
        return "WFLOW.INTRO.STATUS.INIT";
      case C.CHK_STATUS_SCHEDULED:
        return "WFLOW.INTRO.STATUS.SCHED";
      case C.CHK_STATUS_STARTED:
        return "WFLOW.INTRO.STATUS.START";
      case C.CHK_STATUS_FIN_FAILED:
        return "WFLOW.INTRO.STATUS.FAIL";
      case C.CHK_STATUS_FIN_WARNINGS:
        return "WFLOW.INTRO.STATUS.WARN";
      case C.CHK_STATUS_FIN_SUCCESS:
        return "WFLOW.INTRO.STATUS.PASS";
      case C.CHK_STATUS_CLOSED_FAILED:
        return "WFLOW.INTRO.STATUS.FAILC";
      case C.CHK_STATUS_CLOSED_WARNINGS:
        return "WFLOW.INTRO.STATUS.WARNC";
      case C.CHK_STATUS_CLOSED_SUCCESS:
        return "WFLOW.INTRO.STATUS.PASSC";
      case C.CHK_STATUS_CANCELLED:
        return "WFLOW.INTRO.STATUS.CANCEL";
    }
    return "???" + $scope.check.status
  };

  $scope.imageCount = function () {
    if (_.isUndefined($scope.check)) {
      return 0;
    }
    return $scope.imagelist.length;
  };

  $scope.getAssignSelectData = function (assignee) {
    var procs = [];
    var steps = [];
    var data = {assign: {assignee: assignee, target: {}}};

    _.each($scope.check.checkdata.procedures, function (proc) {
      if (proc.asssel) {
        procs.push(proc.id);
        proc.asssel = false;

        _.each(proc.steps, function (step) {
          step.asssel = false
        })
      } else {
        _.each(proc.steps, function (step) {
          if (step.asssel) {
            steps.push(step.id);
            step.asssel = false
          }
        })
      }
    });

    if (procs.length > 0) {
      data.assign.target.procedures = procs;
    }
    if (steps.length > 0) {
      data.assign.target.steps = steps;
    }
    return data;
  };

  $scope.assignToAction = function (assignee) {
    var data = $scope.getAssignSelectData(assignee.num);
    if (_.isUndefined(data.assign.target.procedures) && _.isUndefined(data.assign.target.steps)) {
      return;
    }

    // Calling route CHECK_5
    Restangular.one("workflow", $scope.check.id).all("assign").post(data).then(function () {
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  $scope.addAssigneeAction = function () {
    var data = $scope.getAssignSelectData(-1);
    if (_.isUndefined(data.assign.target.procedures) && _.isUndefined(data.assign.target.steps)) {
      data = {};
    }
    UsersService.showUserPicker("CHECK.OVERVIEW.ACTION.SCHEDULE.TITLE", "CHECK.OVERVIEW.ACTION.SCHEDULE.TEXTADDASSIGN", -1, -1, true, true, false).result.then(function (seluser) {
      data.add = {};
      if (seluser.user >= 0) {
        data.add.user_id = seluser.user;
        // Calling route CHECK_5
        Restangular.one("workflow", $scope.check.id).all("assign").post(data).then(function () {
          $scope.refreshData()
        }, AlertService.showRESTError);
      } else {
        if (seluser.group >= 0 && seluser.user < 0) data.add.group_id = seluser.group;
        selectRegistrationMethod().then(function (regmode) {
          data.add.regmode = regmode;

          // Calling route CHECK_5
          Restangular.one("workflow", $scope.check.id).all("assign").post(data).then(function () {
            $scope.refreshData();
          }, AlertService.showRESTError);
        })
      }
    });
  };

  $scope.changeAssignmentAction = function (assignee) {
    UsersService.showUserPicker("CHECK.OVERVIEW.ACTION.CHANGEASS.TITLE", "CHECK.OVERVIEW.ACTION.CHANGEASS.TEXT", -1, -1, true, true, false).result.then(function (seluser) {
      var data = {action: "changeassign", num: assignee.num};
      if (seluser.user >= 0) {
        data.user_id = seluser.user;

        // Calling route CHECK_6
        Restangular.one("workflow", $scope.check.id).all("action").post(data).then(function () {
          $scope.refreshData();
        }, AlertService.showRESTError);
      } else {
        if (seluser.group >= 0 && seluser.user < 0) {
          data.group_id = seluser.group;
        }
        selectRegistrationMethod().then(function (regmode) {
          data.regmode = regmode;
          // Calling route CHECK_6
          Restangular.one("workflow", $scope.check.id).all("action").post(data).then(function () {
            $scope.refreshData();
          }, AlertService.showRESTError);
        })
      }
    })
  };

  $scope.removeAssigneeAction = function (assignee) {
    var msg = AlertService.createConfirmMessage("CHECK.ALERT.CONFHEADER", "CHECK.ALERT.DELETEASS.TITLE", {name: assignee.name});
    msg.addReason("CHECK.ALERT.DELETEASS.TEXT", {});
    msg.show().result.then(function () {
      var data = {remove: assignee.num};
      // Calling route CHECK_5
      Restangular.one('workflow', $scope.check.id).all("assign").post(data).then(function () {
        $scope.refreshData();
      }, AlertService.showRESTError);
    })
  };

  $scope.gotoModel = function () {
    $location.url("/app/models/view/" + $scope.check.model.id);
  };

  $scope.gotoUnit = function () {
    $location.url("/app/units/" + $scope.check.model.id + "/view/" + $scope.check.unit.id);
  };

  $scope.editComment = function () {
    ModalEditorService.showTextAreaEditor("CHECK.MODIFY.COMMENT.TITLE", "CHECK.MODIFY.COMMENT.TEXT", $scope.check.comment).result.then(function (val) {
      // Calling route CHECK_6
      Restangular.one('workflow', $scope.check.id).all("action").post({
        action: "comment",
        comment: val
      }).then(function () {
        $scope.refreshData();
      }, AlertService.showRESTError);
    })
  };

  $scope.mediamanager = function () {
    MediaService.openMediaManager("Check", $scope.check.id, "WFLOW.INTRO.MEDIAMANAGER", {}).result.then(function () {
      $scope.refreshData();
    })
  };

  $scope.showFileInfoModal = function (file, isImage) {
    var fileInfo = {};
    console.log(file);

    if (isImage) {
        fileInfo.filename = file.fullimage.original_filename;
    } else {
        fileInfo.filename = file.binaryfile.original_filename;
    }

    fileInfo.date = file.created_at.substring(0, file.created_at.indexOf(' '));

    fileInfo.time = file.created_at.substring(file.created_at.indexOf(' ')+1, file.created_at.indexOf(' ', file.created_at.indexOf(' ')+1));
    fileInfo.time = file.created_at.substring(file.created_at.indexOf(' ')+1, file.created_at.indexOf(' ', file.created_at.indexOf(' ')+1));
    fileInfo.time = fileInfo.time.substring(0, fileInfo.time.indexOf(':', fileInfo.time.indexOf(':')+1));

    var timeZone = file.created_at.substring(file.created_at.length-4);
    if (timeZone.indexOf(" ") < 0) timeZone = "";
    fileInfo.time += timeZone;

    fileInfo.user = "";
    // Route USER_10
    Restangular.one('users', file.created_by).get().then(function(response) {
      fileInfo.user = response.user[0].realname;
    });

    AlertService.showFileInfo(fileInfo)
  };

  $scope.toggleImage = function (dir) {
    var newSelImageNum = $scope.selImage == null ? 0 : $scope.selImage.num + dir;
    if (newSelImageNum < 0) {
      newSelImageNum = $scope.imagelist.length - 1;
    }
    if (newSelImageNum >= $scope.imagelist.length) {
      newSelImageNum = 0;
    }
    var newSelImage = MediaService.fitIntoBox($scope.imagelist[newSelImageNum].preview, 320, 250, 2);
    newSelImage.num = newSelImageNum;
    newSelImage.path = "/media/" + $scope.imagelist[newSelImageNum].preview.filename;
    $scope.selImage = newSelImage;
    $scope.selectedZoomImage = "/media/" + $scope.imagelist[newSelImageNum].fullimage.filename;
    $scope.selImageData = $scope.imagelist[newSelImageNum];
  };

  $scope.openPlainImageViewer = function () {
    return MediaService.openPlainImgViewer($scope.selImageData)
  };

  $scope.viewDoc = function (doc) {
    if (doc.doctype == C.MEDIA_DOCTYPE_VIDEO) {
      MediaService.openVideoPlayer(doc);
    }
    if (doc.doctype == C.MEDIA_DOCTYPE_PDF) {
      MediaService.openPdfViewer(doc);
    }
  };

  $scope.viewImage = function (img) {
    MediaService.openImgViewer(img)
  };

  // List

  $scope.listStatusIcon = function (item) {
    // sets a icon in the list for each procedure / step / measure item
    switch (item.status) {
      case C.CHK_CDSTATUS_TODO:
        return "fa-circle-o";
      case C.CHK_CDSTATUS_PASSED:
        return "fa-thumbs-up text-success";
      case C.CHK_CDSTATUS_WARNINGS:
        return "fa-thumbs-down text-warning";
      case C.CHK_CDSTATUS_FAILED:
        return "fa-thumbs-down text-danger";
      case C.CHK_CDSTATUS_OMITTED_HARD:
        return "fa-times-circle-o";
      case C.CHK_CDSTATUS_OMITTED_SOFT:
        return "fa-times-circle-o";
      case C.CHK_CDSTATUS_SKIPPED:
        return "fa-times";
    }
    return "fa-question";
  };


  $scope.listViewEditIcon = function (procedure) {
    // returns matching icon. only used for procedures for now, as we have special rules here!
    // special case: show fa-book if procedure only has instruction steps (LLQA-183)
    if (procedure.steps && procedure.steps.length > 0) {
      var p_instruction = true;
      _.some(procedure.steps, function (step) {
        if (step.steptype != 1) {
          p_instruction = false;
          return false;
        }
      });
      if (p_instruction) {
        return "fa-book";
      }
    }

    // normal cases
    // from code: ((procedure.owned && check.status == CHK_STATUS_STARTED) ? (procedure.locked == 1 ? 'fa-lock' : 'fa-edit') : 'fa-eye')
    if (procedure.owned && $scope.check.status == C.CHK_STATUS_STARTED) {
      if (procedure.locked == 1) {
        return 'fa-lock';
      } else {
        return 'fa-edit';
      }
    } else {
      return 'fa-eye';
    }
  };

  $scope.listUserAssignIcon = function (item) {
    var ret = "fa";
    if (item.asssel) {
      ret += " ll-asssel";
    } else if (item.assignee == C.CHK_ASSIGN_UNASSIGNED) {
      ret += " ll-assno";
    }
    if (item.assignee == C.CHK_ASSIGN_DETAILED) {
      ret += " fa-users";
    } else {
      ret += " fa-user";
    }
    return ret;
  };

  $scope.listToggleCollapse = function (item) {
    item.collapsed = !item.collapsed;
    $scope.setFilter('collapse', 0);
  };

  $scope.listUserAssignAction = function (procedure, step) {
    if (!$scope.hasGrant('WFLMNG')) {
      return;
    }
    if (_.isNull(step)) {
      procedure.asssel = !procedure.asssel;
      _.each(procedure.steps, function (step) {
        step.asssel = procedure.asssel;
      })
    } else {
      step.asssel = !step.asssel;
      procedure.asssel = false;
    }
  };

  $scope.gotoStep = function (item) {
    var gotonum = -1;
    var cnt = 0;
    _.each($scope.check.checkdata.procedures, function (proc) {
      _.each(proc.steps, function (step) {
        cnt += 1;
        if ((step === item || proc === item) && gotonum < 0) {
          gotonum = cnt;
        }
      })
    });
    if (gotonum < 0) {
      return;
    }
    $location.url("/app/workflow/" + $scope.check.id + "/" + gotonum)
  };

  // Buttons

  $scope.showButtonCancel = function () {
    return $scope.hasGrant('WFLMNG') && $scope.check && ($scope.check.status == C.CHK_STATUS_SCHEDULED || C.matchConst($scope.check.status, C.CHK_STATUS_ALL_FINISHED));
  };

  $scope.showButtonDelete = function () {
    return $scope.hasGrant('WFLMNG') && $scope.hasGrant('DELCHK') && $scope.check && (!$scope.check.status || C.matchConst($scope.check.status, C.CHK_STATUS_ALL_PRESTART));
  };

  $scope.showButtonResched = function () {
    return !$scope.skipsched && $scope.hasGrant('WFLMNG') && $scope.check && $scope.check.status && $scope.check.status == C.CHK_STATUS_SCHEDULED;
  };

  $scope.showButtonSchedule = function () {
    return $scope.hasGrant('WFLMNG') && $scope.check && (!$scope.check.status || $scope.check.status == C.CHK_STATUS_INIT);
  };

  $scope.showButtonStart = function () {
    return $scope.hasGrant('WFLMNG') && $scope.check && $scope.check.status == C.CHK_STATUS_SCHEDULED;
  };

  $scope.showButtonReassign = function () {
    return $scope.hasGrant('WFLMNG') && $scope.check && ($scope.check.status == C.CHK_STATUS_STARTED || C.matchConst($scope.check.status, C.CHK_STATUS_ALL_FINISHED));
  };

  $scope.showButtonUnreg = function () {
    return $scope.hasGrant('WFLREG') && $scope.check && C.matchConst($scope.check.status, C.CHK_STATUS_ALL_POSTINITUNFIN) && $scope.mayunreg && $scope.mayunreg.length > 0
  };

  $scope.showButtonRegister = function () {
    return $scope.hasGrant('WFLREG') && $scope.check && C.matchConst($scope.check.status, C.CHK_STATUS_ALL_POSTINITUNFIN) && $scope.mayreg && $scope.mayreg.length > 0
  };

  $scope.showButtonContinue = function () {
    return $scope.check && C.matchConst($scope.check.status, C.CHK_STATUS_ALL_STARTED) && !_.isNull($scope.check.nextstep) && !$scope.check.timing.tooearly;
  };

  $scope.showButtonCommit = function () {
    return $scope.check && $scope.check.status == C.CHK_STATUS_STARTED && $scope.maycommit && !$scope.check.timing.tooearly
  };

  $scope.showButtonClose = function () {
    return $scope.hasGrant('WFLMNG') && $scope.check && C.matchConst($scope.check.status, C.CHK_STATUS_ALL_FINISHED);
  };

  $scope.showButtonCopy = function () {
    return $scope.hasGrant('WFLMNG') && $scope.check && $scope.check.status == C.CHK_STATUS_CANCELLED;
  };

  $scope.showButtonReopen = function () {
    return $scope.hasGrant('WFLMNG') && $scope.check && C.matchConst($scope.check.status, C.CHK_STATUS_ALL_DONE) && C.matchConst($scope.check.unit.status, C.UNIT_STATUS_ALL_ACTIVE);
  };

  $scope.showGotoModel = function () {
    return $scope.hasGrant('MNGMUC') && (!$scope.testrun || $scope.testrun === 'model') && !$scope.isMobileApp
  };

  $scope.showGotoUnit = function () {
    return $scope.hasGrant('MNGMUC') && !$scope.testrun && !$scope.isMobileApp
  };

  $scope.actionCancel = function () {
    // Calling route CHECK_6
    Restangular.one('workflow', $scope.check.id).all("action").post({action: 'cancel'}).then(function () {
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  $scope.actionDelete = function () {
    var msg = AlertService.createConfirmMessage("CHECK.ALERT.CONFHEADER", "CHECK.ALERT.DELETE.TITLE", {});
    msg.addReason("CHECK.ALERT.DELETE.TEXT", {});
    msg.show().result.then(function () {
      // Calling route CHECK_6
      Restangular.one('workflow', $scope.check.id).all("action").post({action: 'delete'}).then(function () {
        $location.url("/app/dashboard");
      }, AlertService.showRESTError);
    })
  };

  function selectRegistrationMethod() {
    var registrationoptions = [
      {id: C.CHK_REGMODE_COMPLETE, name: 'CHECK.ALERT.REGMODE.COMPLETE', ttip: 'CHECK.ALERT.REGMODETT.COMPLETE'},
      {id: C.CHK_REGMODE_MAYPART, name: 'CHECK.ALERT.REGMODE.MAYPART', ttip: 'CHECK.ALERT.REGMODETT.MAYPART'},
      {id: C.CHK_REGMODE_MUSTPART, name: 'CHECK.ALERT.REGMODE.MUSTPART', ttip: 'CHECK.ALERT.REGMODETT.MUSTPART'}
    ];
    return ModalEditorService.showSinglePicker("CHECK.ALERT.REGMODE.TITLE", "CHECK.ALERT.REGMODE.TEXT", C.CHK_REGMODE_MAYPART, registrationoptions).result;
  }

  $scope.actionSchedule = function () {
    if ($scope.skipsched) {
      $scope.actionScheduleStep2(null, null);
      return;
    }
    ModalEditorService.showDatePicker("CHECK.OVERVIEW.ACTION.SCHEDULE.TITLE", "CHECK.OVERVIEW.ACTION.SCHEDULE.TEXTSCHED", null).result.then(function (date1) {
      if (_.isNull(date1)) {
        return;
      }
      ModalEditorService.showDatePicker("CHECK.OVERVIEW.ACTION.SCHEDULE.TITLE", "CHECK.OVERVIEW.ACTION.SCHEDULE.TEXTDUE", null).result.then(function (date2) {
        if (_.isNull(date2)) {
          return;
        }
        $scope.actionScheduleStep2(date1, date2)
      })
    })
  };

  $scope.actionScheduleStep2 = function (date1, date2) {
    var assignmentoptions = [
      {
        id: C.CHK_ASSMODE_FREEPREALLOC,
        name: 'CHECK.ALERT.SCHED.ASSPREALLOCFREE',
        ttip: 'CHECK.ALERT.SCHEDTT.ASSPREALLOCFREE'
      },
      {
        id: C.CHK_ASSMODE_DETAILEDPREALLOC,
        name: 'CHECK.ALERT.SCHED.ASSPREALLOCDETAILED',
        ttip: 'CHECK.ALERT.SCHEDTT.ASSPREALLOCDETAILED'
      },
      {id: C.CHK_ASSMODE_FREE, name: 'CHECK.ALERT.SCHED.ASSFREE', ttip: 'CHECK.ALERT.SCHEDTT.ASSFREE'},
      {id: C.CHK_ASSMODE_FULL, name: 'CHECK.ALERT.SCHED.ASSFULL', ttip: 'CHECK.ALERT.SCHEDTT.ASSFULL'},
      {id: C.CHK_ASSMODE_DETAILED, name: 'CHECK.ALERT.SCHED.ASSDETAILED', ttip: 'CHECK.ALERT.SCHEDTT.ASSDETAILED'}
    ];

    ModalEditorService.showSinglePicker("CHECK.OVERVIEW.ACTION.SCHEDULE.TITLE", "CHECK.OVERVIEW.ACTION.SCHEDULE.TEXTASSIGN", C.CHK_ASSMODE_FREEPREALLOC, assignmentoptions).result.then(function (ass) {
      if (ass == 2) {
        UsersService.showUserPicker("CHECK.OVERVIEW.ACTION.SCHEDULE.TITLE", "CHECK.OVERVIEW.ACTION.SCHEDULE.TEXTASSIGNTO", -1, -1, true, true, false).result.then(function (seluser) {
          var assdata = {};
          if (seluser.user >= 0) {
            assdata.user_id = seluser.user;
            $scope.actionScheduleFinal('schedule', date1, date2, ass, assdata, 0);
          } else {
            if (seluser.group >= 0 && seluser.user < 0) {
              assdata.group_id = seluser.group;
            }
            selectRegistrationMethod().then(function (regmode) {
              $scope.actionScheduleFinal('schedule', date1, date2, ass, assdata, regmode);
            })
          }
        })
      } else if (C.matchConst(ass, C.CHK_ASSMODE_ALL_NEEDREGMODE)) {
        selectRegistrationMethod().then(function (regmode) {
          $scope.actionScheduleFinal('schedule', date1, date2, ass, null, regmode);
        })
      } else {
        $scope.actionScheduleFinal('schedule', date1, date2, ass, null, 0);
      }
    })
  };

  $scope.actionResched = function () {
    ModalEditorService.showDatePicker("CHECK.OVERVIEW.ACTION.SCHEDULE.TITLE", "CHECK.OVERVIEW.ACTION.SCHEDULE.TEXTSCHED", null).result.then(function (date1) {
      if (_.isNull(date1)) {
        return;
      }
      ModalEditorService.showDatePicker("CHECK.OVERVIEW.ACTION.SCHEDULE.TITLE", "CHECK.OVERVIEW.ACTION.SCHEDULE.TEXTDUE", null).result.then(function (date2) {
        if (_.isNull(date2)) {
          return;
        }
        $scope.actionScheduleFinal('reschedule', date1, date2, null, null, null);
      })
    })
  };

  $scope.actionScheduleFinal = function (action, scheddate, duedate, assignment, assignto, regmode) {
    var data = {
      action: action,
      scheduled: scheddate,
      dueby: duedate,
      assignment: assignment,
      assignee: assignto,
      regmode: regmode
    };
    // Calling route CHECK_6
    Restangular.one('workflow', $scope.check.id).all("action").post(data).then(function () {
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  $scope.actionQuickSchedule = function () {
    $scope.actionScheduleFinal("schedule", "2014-08-20T10:00:00.000Z", "2014-08-22T10:00:00.000Z", 1, null);
  };

  $scope.actionReassign = function () {
    var msg = AlertService.createConfirmMessage("CHECK.ALERT.CONFHEADER", "CHECK.ALERT.REASSIGN.TITLE", {});
    msg.addReason("CHECK.ALERT.REASSIGN.TEXT", {});
    msg.show().result.then(function () {
      // Calling route CHECK_6
      Restangular.one('workflow', $scope.check.id).all("action").post({action: 'reassign'}).then(function () {
        $scope.refreshData();
      }, AlertService.showRESTError);
    })
  };

  $scope.actionStart = function () {
    // Calling route CHECK_6
    Restangular.one('workflow', $scope.check.id).all("action").post({action: 'start'}).then(function () {
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  function actionRegistryHelper3(item) {
    var msg = AlertService.createConfirmMessage("CHECK.ALERT.CONFHEADER", "CHECK.ALERT.TAKEOVER.CONFIRM", {});
    msg.show().result.then(function () {
      actionRegistryHelper2("takeover", item, []);
    });
  }

  function actionRegistryHelper2(action, item, procs) {
    // Calling route CHECK_6
    Restangular.one('workflow', $scope.check.id).all("action").post({
      action: action,
      assignment: item,
      userid: $scope.userinfo.userid,
      proclist: procs
    }).then(function () {
      $scope.refreshData();
    }, AlertService.showRESTError);
  }

  function actionRegistryHelper1(item) {
    var regmode = 0;
    var procs = [];

    _.each($scope.assignees, function (ass) {
      if (item == ass.num) {
        procs = _.keys(ass.procs);
        regmode = ass.regmode;
      }
    });

    if (!regmode) {
      regmode = C.CHK_REGMODE_COMPLETE;
    }

    if (regmode == C.CHK_REGMODE_COMPLETE || regmode == C.CHK_REGMODE_APPENDED || procs.length == 1) {
      actionRegistryHelper2("register", item, procs);
      return;
    }

    var items = [];
    _.each($scope.check.checkdata.procedures, function (proc) {
      if (_.indexOf(procs, proc.info.code) > -1) {
        items.push({id: proc.info.code, title: proc.info.title, code: proc.info.code});
      }
    });

    if (regmode == C.CHK_REGMODE_MAYPART) {
      ModalEditorService.showMultiPicker("CHECK.ALERT.SELPROC.TITLE", "CHECK.ALERT.SELPROC.TEXTMULT", procs, items, {style: 1}).result.then(function (selprocs) {
        actionRegistryHelper2("register", item, selprocs);
      })
    }

    if (regmode == C.CHK_REGMODE_MUSTPART) {
      var maxproc = $rootScope.appProperties.regmaxprocedures;
      if (maxproc < 2) {
        ModalEditorService.showSinglePicker("CHECK.ALERT.SELPROC.TITLE", "CHECK.ALERT.SELPROC.TEXTSING", procs, items, {style: 1}).result.then(function (selproc) {
          actionRegistryHelper2("register", item, [selproc]);
        })
      } else {
        ModalEditorService.showMultiPicker("CHECK.ALERT.SELPROC.TITLE", "CHECK.ALERT.SELPROC.TEXTMULT", [], items, {
          style: 1,
          maxsel: maxproc
        }).result.then(function (selproc) {
          actionRegistryHelper2("register", item, selproc);
        });
      }
    }
  }

  $scope.actionRegister = function () {
    if ($scope.mayreg.length > 1 || $scope.assignees[$scope.mayreg[0]].maytakeover) {
      var items = [];

      _.each($scope.assignees, function (ass) {
        if (ass.mayreg) {
          items.push({
            id: [ass.num, false],
            name: ass.name,
            code: {text: 'CHECK.ALERT.STEPINPROC', nums: ass.numstep, nump: ass.numproc}
          })
        }
        if (ass.maytakeover) {
          items.push({id: [ass.num, true], name: ass.name, code: {text: 'CHECK.ALERT.TAKEOVER.INFO'}})
        }
      });

      ModalEditorService.showSinglePicker("CHECK.ALERT.REGISTER.TITLE", "CHECK.ALERT.REGISTER.TEXT", null, items, {style: 3}).result.then(function (res) {
        if (!res[1]) {
          actionRegistryHelper1(res[0]);
        } else {
          actionRegistryHelper3(res[0]);
        }
      })
    } else {
      actionRegistryHelper1($scope.mayreg[0]);
    }
  };

  $scope.actionUnreg = function () {
    if ($scope.mayunreg.length > 1) {
      var items = [];
      _.each($scope.assignees, function (ass) {
        if (ass.mayunreg) {
          items.push({
            id: ass.num,
            name: ass.name,
            code: {text: 'CHECK.ALERT.STEPINPROC', nums: ass.numstep, nump: ass.numproc}
          })
        }
      });

      ModalEditorService.showSinglePicker("CHECK.ALERT.UNREGISTER.TITLE", "CHECK.ALERT.UNREGISTER.TITLE", null, items, {style: 3}).result.then(function (res) {
        actionRegistryHelper2("unregister", res, null);
      })
    } else {
      actionRegistryHelper2("unregister", $scope.mayunreg[0], null)
    }
  };

  $scope.actionCommit = function () {
    // Calling route CHECK_6
    Restangular.one('workflow', $scope.check.id).all("action").post({
      action: 'commit',
      userid: $scope.userinfo.userid
    }).then(function () {
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  $scope.actionClose = function () {
    // Calling route CHECK_6
    Restangular.one('workflow', $scope.check.id).all("action").post({
      action: 'close',
      userid: $scope.userinfo.userid
    }).then(function () {
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  function actionCopyHelper(procs, upgrade) {
    // Calling route CHECK_2
    Restangular.one('checks', $scope.check.id).all("copy").post({
      procs: procs,
      upgrade: upgrade
    }).then(function (newcheck) {
      $location.url("/app/workflow/" + newcheck.id);
    }, AlertService.showRESTError);
  }

  $scope.actionCopy = function () {
    var items = [];

    _.each($scope.check.checkdata.procedures, function (proc) {
      items.push({id: proc.info.code, title: proc.info.title, code: proc.info.code})
    });

    ModalEditorService.showMultiPicker("CHECK.ALERT.COPY.TITLE", "CHECK.ALERT.COPY.TEXT", null, items, {style: 1}).result.then(function (res) {
      if ($scope.check.newerversions.length > 0) {
        items = [{id: -1, name: "CHECK.ALERT.COPY.NEWVERS.NOCHANGE", code: ''}];
        $scope.check.newerversions.forEach(function (val) {
          if (val[3]) {
            var vname = VersioningService.versionName(val[0], null);

            var data = _.isNull(val[0]) ? {text: ''} : {
                text: 'VERSIONING.LASTCHG',
                date: $filter('date')(val[2].date, i18n.selectedLanguage.tformat, 'utc'),
                realname: val[2].realname,
                username: val[2].username
              };
            items.push({id: val[1], name: $filter('translate')(vname.text, vname), code: data});
          }
        });
        if (items.length > 1) {
          ModalEditorService.showSinglePicker("CHECK.ALERT.COPY.NEWVERS.TITLE", "CHECK.ALERT.COPY.NEWVERS.TEXT", -1, items, {style: 3}).result.then(function (selv) {
            actionCopyHelper(res, selv);
          })
        } else {
          actionCopyHelper(res, -1);
        }
      } else {
        actionCopyHelper(res, -1);
      }
    });
  };

  $scope.actionReopen = function () {
    var items = [];
    _.each($scope.assignees, function (ass) {
      items.push({
        id: ass.num,
        name: ass.name,
        code: {text: 'CHECK.ALERT.STEPINPROC', nums: ass.numstep, nump: ass.numproc}
      })
    });
    ModalEditorService.showMultiPicker("CHECK.ALERT.REOPEN.TITLE", "CHECK.ALERT.REOPEN.TEXT", null, items, {style: 3}).result.then(function (res) {
      // Calling route CHECK_6
      Restangular.one('workflow', $scope.check.id).all("action").post({
        action: 'reopen',
        userid: $scope.userinfo.userid,
        assignees: res
      }).then(function () {
        $scope.refreshData();
      }, AlertService.showRESTError);
    })
  };

  $scope.actionContinue = function () {
    if (_.isNull($scope.check.nextstep)) {
      return;
    }
    $location.url("/app/workflow/" + $scope.check.id + "/" + $scope.check.nextstep);
  };

  $scope.exportPdf = function () {
    var genoptions = [
      {id: 'showv', name: 'CHECK.MAKEPDF.OPTV'},
      {id: 'showadm', name: 'CHECK.MAKEPDF.OPTADM'},
      {id: 'showcomm', name: 'CHECK.MAKEPDF.OPTCOMM'},
      {id: 'showraw', name: 'CHECK.MAKEPDF.OPTRAW'},
      {id: 'showtool', name: 'CHECK.MAKEPDF.OPTTOOL'},
      {id: 'showrule', name: 'CHECK.MAKEPDF.OPTRULE'}
    ];
    ModalEditorService.showMultiPicker("CHECK.MAKEPDF.TTL", "CHECK.MAKEPDF.TEXT", UsersService.getUserInfo()['export_pdf_presel'], genoptions).result.then(function (options) {
      $scope.exportPdfStep2(options);
    })
  };

  $scope.exportPdfStep2 = function (options) {
    var genoptions = [
      {id: 0, name: 'CHECK.MAKEPDF.INTERNAL.TEXT', ttip: 'CHECK.MAKEPDF.INTERNAL.TTIP'},
      {id: 1, name: 'CHECK.MAKEPDF.CUSTOMER.TEXT', ttip: 'CHECK.MAKEPDF.CUSTOMER.TTIP'}
    ];
    ModalEditorService.showSinglePicker("CHECK.MAKEPDF.TTL", "CHECK.MAKEPDF.TEXT2", 0, genoptions).result.then(function (type) {
      UsersService.getUserInfo()['export_pdf_presel'] = options;
      UsersService.updateUserInfo();
      var tzoff = new Date().getTimezoneOffset();
      var query = "?lang=" + i18n.selectedLanguage.code + "&tzoff=" + tzoff;
      _.each(options, function (el) {
        query += "&" + el + "=1";
      });
      if (type === 1) query += "&customer=1";
      $window.open("/servlet/pdf/checkreport/" + $scope.check.id + query);
    })
  };

  $scope.exportFile = function () {
    var genoptions = [
      {id: 'csv', name: 'CHECK.MAKEFILE.CSV'},
      {id: 'json', name: 'CHECK.MAKEFILE.JSON'},
      {id: 'xml', name: 'CHECK.MAKEFILE.XML'}
    ];
    ModalEditorService.showSinglePicker("CHECK.MAKEFILE.TTL", "CHECK.MAKEFILE.TEXT", null, genoptions).result.then(function (res) {
      var query = "?lang=" + i18n.selectedLanguage.code + "&format=" + res;
      $window.open("/servlet/export/checkreport/" + $scope.check.id + query);
    })
  };

  // - Filters

  function resetFilter() {
    $scope.listFilter = {
      showFilterLine: false,
      collapse: 3,   // 0 = custom, 1 = everything (P/S/M), 2 = standard (P/S), 3 = procedures only, 4 = filter, 5 = filter extended
      filterText: null,
      applyMeasures: false,
      applySteps: true,
      applyProcedures: true,
      useOnCodes: true,
      useOnTitle: false,
      focusUser: null,
      filterStatus: null  // 1 = only passed, 2 = only failed, 3 = only unfinished
    };
    applyFilter();
    applyCollapse();
    saveListFilter();
  }

  $scope.resetFilter = function () {
    resetFilter();
  };

  function saveListFilter() {
    $rootScope.session.userinfo.checkintro_filters = $scope.listFilter;
    UsersService.updateUserInfo();
  }

  function loadListFilter() {
    if (!_.isUndefined($rootScope.session.userinfo) && !_.isUndefined($rootScope.session.userinfo.checkintro_filters) && !$scope.isMobileApp) {
      $scope.listFilter = $rootScope.session.userinfo.checkintro_filters;
      applyFilter();
      applyCollapse();
    } else {
      resetFilter();
    }
  }

  $scope.toggleFilter = function (key) {
    $scope.listFilter[key] = !$scope.listFilter[key];
    if (!($scope.listFilter.applyMeasures || $scope.listFilter.applySteps || $scope.listFilter.applyProcedures)) {
      $scope.listFilter.applyProcedures = true;
    }
    if (!($scope.listFilter.useOnCodes || $scope.listFilter.useOnTitle)) {
      $scope.listFilter.useOnCodes = true;
    }
    if ($scope.listFilter.collapse && $scope.listFilter.collapse >= 4 && !$scope.listFilter.showFilterLine) {
      $scope.listFilter.collapse = 0;
      applyCollapse();
    }
    applyFilter();
    saveListFilter();
  };

  $scope.setFilter = function (key, value, resetIfSet) {
    if (key === 'focusUser') {
      // if setting a new or different focusUser, remove filters (LLQA-167)
      if ($scope.listFilter.focusUser === null || $scope.listFilter.focusUser != value) {
        $scope.listFilter.filterText = '';
        resetFilter();
      }

      $scope.listFilter.showFilterLine = true;
    }
    if (key === 'collapse' && value && value >= 4) {
      $scope.listFilter.showFilterLine = true;
    }
    if (resetIfSet && $scope.listFilter[key] === value) {
      value = null;
    }
    $scope.listFilter[key] = value;
    if (key === 'collapse') {
      applyCollapse();
      saveListFilter();
      return;
    }
    applyFilter();
    saveListFilter();
  };

  $scope.highlightAssigneeAction = function (assignee) {
    $scope.highlightedAssignee = $scope.highlightedAssignee != assignee.num ? assignee.num : null
  };


  $scope.textFilterChanged = function () {
    $timeout(function () {
      applyFilter()
    })
  };

  function checkFilterOnItem(item, applytext, parent) {
    var selected = false;
    if (applytext && $scope.listFilter.filterText != undefined && $scope.listFilter.filterText != null && $scope.listFilter.filterText != '') {
      selected |= $scope.listFilter.useOnCodes && item.info.code.toLowerCase().indexOf($scope.listFilter.filterText) >= 0;
      selected |= $scope.listFilter.useOnTitle && item.info.title[i18n.selectedLanguage.code].toLowerCase().indexOf($scope.listFilter.filterText) >= 0
    } else if ((!_.isNull($scope.listFilter.filterStatus) && C.matchConst($scope.check.status, C.CHK_STATUS_ALL_STARTED)) || !_.isNull($scope.listFilter.focusUser)) {
      selected = true
    }
    selected &= $scope.listFilter.filterStatus !== 3 || C.matchConst($scope.check.status, C.CHK_STATUS_ALL_PRESTART) || item.status === C.CHK_CDSTATUS_TODO;
    selected &= $scope.listFilter.filterStatus !== 1 || C.matchConst($scope.check.status, C.CHK_STATUS_ALL_PRESTART) || item.status === C.CHK_CDSTATUS_PASSED;
    selected &= $scope.listFilter.filterStatus !== 2 || C.matchConst($scope.check.status, C.CHK_STATUS_ALL_PRESTART) || C.matchConst(item.status, C.CHK_CDSTATUS_ALL_FAILURE);
    selected &= _.isNull($scope.listFilter.focusUser) || $scope.listFilter.focusUser < 0 || (!_.isUndefined(item.assignee) && item.assignee === $scope.listFilter.focusUser) || (!_.isUndefined(parent) && !_.isUndefined(parent.assignee) && parent.assignee === $scope.listFilter.focusUser);
    selected &= _.isNull($scope.listFilter.focusUser) || $scope.listFilter.focusUser !== -1 || (!_.isUndefined(item.assignee) && $scope.assignees[item.assignee].owned) || (!_.isUndefined(parent) && !_.isUndefined(parent.assignee) && $scope.assignees[parent.assignee].owned);
    return selected
  }

  function applyFilter() {
    // step.assignee == highlightedAssignee
    if ($scope.listFilter.filterText != null) {
      $scope.listFilter.filterText = $scope.listFilter.filterText.toLowerCase();
    }
    _.forEach($scope.check.checkdata.procedures, function (proc) {
      proc.selected = $scope.listFilter.showFilterLine && checkFilterOnItem(proc, $scope.listFilter.applyProcedures, undefined);
      _.forEach(proc.steps, function (step) {
        step.selected = $scope.listFilter.showFilterLine && checkFilterOnItem(step, $scope.listFilter.applySteps, proc);
        _.forEach(step.measures, function (meas) {
          meas.selected = $scope.listFilter.showFilterLine && checkFilterOnItem(meas, $scope.listFilter.applyMeasures, step);
        })
      })
    });
    if ($scope.listFilter.collapse > 3) {
      applyCollapse();
    }
  }

  function applyCollapse() {
    _.forEach($scope.check.checkdata.procedures, function (proc) {
      var hasSelectedSteps = false;
      _.forEach(proc.steps, function (step) {
        if (step.selected) hasSelectedSteps = true;
        var hasSelectedMeasures = false;
        _.forEach(step.measures, function (meas) {
          if (meas.selected) hasSelectedMeasures = true;
        });
        if (hasSelectedMeasures) hasSelectedSteps = true;
        switch ($scope.listFilter.collapse) {
          case 1:
            step.collapsed = false;
            break;
          case 2:
          case 3:
            step.collapsed = true;
            break;
          case 4:
            step.collapsed = !hasSelectedMeasures;
            break;
          case 5:
            step.collapsed = !step.selected && !hasSelectedMeasures;
            break;
        }
      });
      switch ($scope.listFilter.collapse) {
        case 1 :
        case 2 :
          proc.collapsed = false;
          break;
        case 3 :
          proc.collapsed = true;
          break;
        case 4 :
          proc.collapsed = !hasSelectedSteps;
          break;
        case 5 :
          proc.collapsed = !proc.selected && !hasSelectedSteps;
          break;
      }
    })
  }

  $scope.openConfigtable = function () {
    $modal.open({
      templateUrl: 'templates/modal/configtable_check.html',
      controller: 'ConfigViewModalCtrl',
      size: 'lg',
      resolve: {
        configtable_id: function () {
          return $scope.check.configtable_id;
        },
        check_id: function () {
          return $scope.check.id;
        }
      }
    }).result.then(function () {
      $scope.refreshData();
    });
  };

});
