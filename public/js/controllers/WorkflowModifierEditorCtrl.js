cmod.controller('WorkflowModifierEditorCtrl', function ($rootScope, $scope, $modalInstance, $translate, i18n, subtitle, procid, modifiers, Restangular, WorkflowModifyService, AlertService, featureFlags) {
  $scope.modifiers = modifiers;
  $scope.subtitle = subtitle;
  $scope.procid = procid;

  $scope.types = [];
  $scope.codes = [];
  $scope.triggers = [];
  $scope.resolutions = [];

  $scope.newitem = {type: 'CT', code: '', trigger: '', res: ''};

  $scope.refreshData = function () {
    // Calling route CHECK_8
    Restangular.one('wflowmod').get({proc: procid}).then(function (data) {
      // types of rules ("workflow modifiers")
      if (data.checktypes && data.checktypes.length > 0) $scope.types.push({
        id: 'CT',
        title: $translate.instant('WFLOWEDIT.OPT1.CT')
      });
      if (data.models && data.models.length > 0) $scope.types.push({
        id: 'MD',
        title: $translate.instant('WFLOWEDIT.OPT1.MD')
      });
      if (data.devicetypes && data.devicetypes.length > 0) $scope.types.push({
        id: 'DT',
        title: $translate.instant('WFLOWEDIT.OPT1.DT')
      });
      if (featureFlags.isOn("configtable") && data.configtables && data.configtables.length > 0) $scope.types.push({
        id: 'CO',
        title: $translate.instant('WFLOWEDIT.OPT1.CO')
      });

      if (data.procedures && data.procedures.length > 0) $scope.types.push({
        id: 'P',
        title: $translate.instant('WFLOWEDIT.OPT1.P')
      });
      if (data.steps && data.steps.length > 0) $scope.types.push({
        id: 'S',
        title: $translate.instant('WFLOWEDIT.OPT1.S')
      });
      if (data.measures && data.measures.length > 0) $scope.types.push({
        id: 'M',
        title: $translate.instant('WFLOWEDIT.OPT1.M')
      });
      if ($scope.types.length < 1) $modalInstance.dismiss();
      $scope.newitem.type = $scope.types[0].id;
      $scope.resdata = data;
      $scope.updateComboboxes(0);
      $scope.wfmods = WorkflowModifyService.getWorkflowModifiersForDisplay($scope.modifiers);
      $scope.wfModEditorLoaded = true;
    }, AlertService.showSevereRESTError)
  };

  $scope.oldType = null;
  $scope.oldTrigger = null;

  $scope.updateComboboxes = function (what) {
    if (!$scope.resdata) return;
    if (what == 0) {
      if ($scope.oldType == $scope.newitem.type) return;
      $scope.oldType = $scope.newitem.type;
      $scope.codes = _.map(
        $scope.resdata[{
          CT: 'checktypes',
          MD: 'models',
          DT: 'devicetypes',
          P: 'procedures',
          S: 'steps',
          M: 'measures',
          CO: 'configtables'
        }[$scope.newitem.type]],
        function (entry) {
          var lang = i18n.getSelectedLanguage()['code'];
          var listitem = {};
          if (typeof entry === "string") {
            listitem.id = entry;
            listitem.title = entry;
          }
          else {
            // title will be translated when available, else just set the whole (e.g. configtables)
            listitem.id = entry[0];
            listitem.title = entry[0] + " - " + ( entry[1][lang] ? i18n.translate(entry[1]) : entry[1] );
          }
          return listitem;
        }
      );
      $scope.newitem.code = $scope.codes[0].id;
      var triggers = [];
      if (_.includes(['CT', 'MD', 'DT'], $scope.newitem.type)) {
        triggers.push({id: 'Select', title: $translate.instant('WFLOWEDIT.OPT2.SEL')});
        triggers.push({id: '!Select', title: $translate.instant('WFLOWEDIT.OPT2.NSEL')});
      }
      if (_.includes(['CO'], $scope.newitem.type)) {
        triggers.push({id: '!Select', title: $translate.instant('WFLOWEDIT.OPT2.NACTIVE')});
        triggers.push({id: 'Select', title: $translate.instant('WFLOWEDIT.OPT2.ACTIVE')});
      }
      if (_.includes(['P'], $scope.newitem.type)) {
        triggers.push({id: 'InQueue', title: $translate.instant('WFLOWEDIT.OPT2.QUEUED')});
        triggers.push({id: '!InQueue', title: $translate.instant('WFLOWEDIT.OPT2.NQUEUED')});
      }
      if (_.includes(['P', 'M', 'S'], $scope.newitem.type)) {
        triggers.push({id: 'Pass', title: $translate.instant('WFLOWEDIT.OPT2.FINPASS')});
        triggers.push({id: 'Fail', title: $translate.instant('WFLOWEDIT.OPT2.FINFAIL')});
        triggers.push({id: 'Skip', title: $translate.instant('WFLOWEDIT.OPT2.SKIP')});
        triggers.push({id: '!Skip', title: $translate.instant('WFLOWEDIT.OPT2.NSKIP')});
        triggers.push({id: 'Omit', title: $translate.instant('WFLOWEDIT.OPT2.OMIT')});
        triggers.push({id: '!Omit', title: $translate.instant('WFLOWEDIT.OPT2.NOMIT')});
      }
      if (_.includes(['M'], $scope.newitem.type)) {
        triggers.push({id: 'Yes', title: $translate.instant('WFLOWEDIT.OPT2.YES')});
        triggers.push({id: 'No', title: $translate.instant('WFLOWEDIT.OPT2.NO')});
      }
      $scope.triggers = triggers;
      $scope.newitem.trigger = $scope.triggers[0].id;
    }
    if ($scope.oldTrigger == $scope.newitem.trigger) return;
    $scope.oldTrigger = $scope.newitem.trigger;
    var resolutions = [{id: 'omit', title: $translate.instant('WFLOWEDIT.OPT3.OMIT')}];
    if (!_.includes(['Select', '!Select', 'InQueue', '!InQueue'], $scope.newitem.trigger)) resolutions.push({
      id: 'skip',
      title: $translate.instant('WFLOWEDIT.OPT3.SKIP')
    });
    $scope.resolutions = resolutions;
    $scope.newitem.res = $scope.resolutions[0].id;
  };

  $scope.addWorkflowModifier = function () {
    var newitem = {};
    newitem.type = $scope.newitem.type;
    newitem.code = $scope.newitem.code;
    newitem.res = $scope.newitem.res;
    if ($scope.newitem.trigger.substr(0, 1) == "!") {
      newitem.trigger = $scope.newitem.trigger.substr(1);
      newitem.inverse = true;
    } else {
      newitem.trigger = $scope.newitem.trigger;
    }
    $scope.modifiers.push(newitem);
    $scope.wfmods = WorkflowModifyService.getWorkflowModifiersForDisplay($scope.modifiers);
  };

  $scope.removeWorkflowModifier = function (idx) {
    var el = $scope.modifiers[idx - 1];
    if (el.type === 'CO') {
      var type = $scope.subtitle.msg.charAt(0).toLowerCase();
      var id = $rootScope.selecteditem.id;
      Restangular.one('configentry', el.code).one('blocker', type).one('id', id).remove().then(function (data) {
        // pass
      }, AlertService.showSevereRESTError);
    }

    $scope.modifiers.splice(idx - 1, 1);
    $scope.wfmods = WorkflowModifyService.getWorkflowModifiersForDisplay($scope.modifiers);
  };

  $scope.refreshData();

  $scope.close = function () {
    $modalInstance.close()
  };
});