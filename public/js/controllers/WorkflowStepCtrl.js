cmod.controller('WorkflowStepCtrl', function ($rootScope, $modal, $window, $scope, $stateParams, $location, i18n, Restangular, ConstantService, MeasureTypeService, MediaService, UsersService, ModalEditorService, AlertService, KeyboardShortcutService) {
  $scope.sprintf = sprintf;
  $scope.firstProcItem = undefined;
  $scope.autoProcess = false;

  $scope.isMobileApp = $rootScope.isMobileApp;
  $scope.viewport = 0;

  var C = {};
  ConstantService.installConstants($scope, C, ['CHK_', 'MEDIA_', 'UNIT_']);

  $scope.$on("$destroy", function () {
    $rootScope.deactivateNoticeButton();
    MediaService.killZoomImage('zoomimg');
    $modal.closeall();

  });

  $scope.refreshData = function () {
    $scope.firstProcItem = undefined;
    // Calling route CHECK_4
    Restangular.one('workflow', $stateParams.checkid).one('step', $stateParams.stepnum).get({userid: $scope.userinfo.userid}).then(function checkData(response) {
      response.toolmap = {};
      $scope.testrun = (!_.isUndefined(response.check.metadata) && !_.isUndefined(response.check.metadata.testrun)) ? response.check.metadata.testrun.type : undefined;
      var toolcnt = 0;
      _.each(response.tools, function (tool) {
        toolcnt += 1;
        response.toolmap[tool[0].id] = tool;
        tool[0].num = toolcnt
      });
      var helper = {};
      $scope.stepnum = $stateParams.stepnum;
      _.each(response.cdentry.measures, function (measure) {
        helper[measure.id] = measure
      });
      _.each(response.measures, function (measure) {
        measure.cdentry = helper[measure.id];
        measure.measurement = response.measurements[measure.id];
        measure.needinput = measure.cdentry.status == C.CHK_CDSTATUS_TODO && !response.readonly;
        if (measure.needinput && !measure.cdentry.locked && _.isUndefined($scope.firstProcItem)) {
          $scope.firstProcItem = {type: 'measure', obj: measure};
        }
        if (measure.cdentry.needstool && !measure.cdentry.locked && _.isUndefined($scope.firstProcItem)) {
          $scope.firstProcItem = {type: 'tool', obj: response.toolmap[measure.tooltype_id]};
        }
      });
      $scope.step = response;
      if ($scope.step.images.length > 0 && _.isNull($scope.selImage)) {
        $scope.selectImage(0);
      }
      var noticePath = {
        type: 'CheckStep',
        url: {type: 'workflowstep', checkid: $scope.step.check.id, stepnum: $stateParams.stepnum},
        path: [
          {type: 'Model', id: $scope.step.check.model.id},
          {type: 'Unit', id: $scope.step.check.unit.id},
          {type: 'Check', id: $scope.step.check.id},
          {type: 'Procedure', id: $scope.step.procedure.id},
          {type: 'Step', id: $scope.step.id}
        ]
      };
      $rootScope.activateNoticeButton(noticePath);
      $scope.wfStepLoaded = true;
      if ($scope.autoProcess) {
        if (_.isUndefined($scope.firstProcItem)) {
          $scope.autoProcess = false;
        } else {
          if ($scope.firstProcItem.type === 'measure') {
            $scope.enterMeasurement($scope.firstProcItem.obj);
          }
          if ($scope.firstProcItem.type === 'tool') {
            $scope.chooseTool($scope.firstProcItem.obj, undefined);
          }
        }
      }
    }, AlertService.showSevereRESTError)
  };

  $scope.showGotoModel = function () {
    return $scope.hasGrant('MNGMUC') && (!$scope.testrun || $scope.testrun === 'model') && !$scope.isMobileApp
  };
  $scope.showGotoUnit = function () {
    return $scope.hasGrant('MNGMUC') && !$scope.testrun && !$scope.isMobileApp
  };

  UsersService.addGrantMethods($scope).then(function (info) {
    $scope.userinfo = info;
    MeasureTypeService.getMeasuretypes($scope).then(function () {
      $scope.refreshData();
    });
  });

  // Info Display

  $scope.checkStatusName = function () {
    if (_.isUndefined($scope.step)) {
      return "???";
    }
    if ($scope.step.cdentry.completem && $scope.step.cdentry.completet) {
      switch ($scope.step.cdentry.status) {
        case null :
          return "WFLOW.STEP.STATUS.TODO";
        case C.CHK_CDSTATUS_TODO :
          return "WFLOW.STEP.STATUS.TODO";
        case C.CHK_CDSTATUS_PASSED :
          return "WFLOW.STEP.STATUS.PASS";
        case C.CHK_CDSTATUS_WARNINGS :
          return "WFLOW.STEP.STATUS.WARN";
        case C.CHK_CDSTATUS_FAILED :
          return "WFLOW.STEP.STATUS.FAIL";
        case C.CHK_CDSTATUS_OMITTED_HARD :
          return "WFLOW.STEP.STATUS.OMIT";
        case C.CHK_CDSTATUS_OMITTED_SOFT :
          return "WFLOW.STEP.STATUS.OMIT";
        case C.CHK_CDSTATUS_SKIPPED :
          return "WFLOW.STEP.STATUS.SKIP";
      }
    } else {
      switch ($scope.step.cdentry.status) {
        case null :
          return "WFLOW.STEP.STATUS.TODO";
        case C.CHK_CDSTATUS_TODO :
          return "WFLOW.STEP.STATUS.TODO";
        case C.CHK_CDSTATUS_PASSED :
          return "WFLOW.STEP.STATUS.PASSNF";
        case C.CHK_CDSTATUS_WARNINGS :
          return "WFLOW.STEP.STATUS.WARNNF";
        case C.CHK_CDSTATUS_FAILED :
          return "WFLOW.STEP.STATUS.FAILNF";
        case C.CHK_CDSTATUS_OMITTED_HARD :
          return "WFLOW.STEP.STATUS.OMIT";
        case C.CHK_CDSTATUS_OMITTED_SOFT :
          return "WFLOW.STEP.STATUS.OMIT";
        case C.CHK_CDSTATUS_SKIPPED :
          return "WFLOW.STEP.STATUS.SKIP";
      }
    }
    return "???" + $scope.step.cdentry.status;
  };

  $scope.measureStatusIcon = function (item) {
    switch (item.cdentry.status) {
      case C.CHK_CDSTATUS_TODO :
        return "fa-circle-o";
      case C.CHK_CDSTATUS_PASSED :
        return "fa-thumbs-up text-success";
      case C.CHK_CDSTATUS_WARNINGS :
        return "fa-thumbs-down text-warning";
      case C.CHK_CDSTATUS_FAILED :
        return "fa-thumbs-down text-danger";
      case C.CHK_CDSTATUS_OMITTED_HARD :
        return "fa-times-circle-o";
      case C.CHK_CDSTATUS_OMITTED_SOFT :
        return "fa-times-circle-o";
      case C.CHK_CDSTATUS_SKIPPED :
        return "fa-times";
    }
    return "fa-question";
  };

  // Imagegallery

  $scope.selImage = null;
  $scope.selectedZoomImage = null;

  $scope.zoomableImageOptions = {
    zoomType: "window",
    zoomWindowPosition: 11,
    zoomWindowOffetx: -10,
    borderColour: '#005499',
    scrollZoom: true,
    zoomWindowWidth: 450,
    zoomWindowHeight: 450
  };

  $scope.gotoModel = function () {
    $location.url("/app/models/view/" + $scope.step.check.model.id);
  };
  $scope.gotoUnit = function () {
    $location.url("/app/units/" + $scope.step.check.model.id + "/view/" + $scope.step.check.unit.id);
  };
  $scope.gotoProcedure = function () {
    $location.url("/app/procedures/view/" + $scope.step.procedure.id);
  };

  $scope.selectImage = function (num) {
    var newSelImageNum = num;
    if (newSelImageNum < 0) {
      newSelImageNum = $scope.step.images.length - 1;
    }
    if (newSelImageNum >= $scope.step.images.length) {
      newSelImageNum = 0;
    }
    var newSelImage = MediaService.fitIntoBox($scope.step.images[newSelImageNum].preview, 480, 380, 2);
    newSelImage.num = newSelImageNum;
    newSelImage.path = "/media/" + $scope.step.images[newSelImageNum].preview.filename;
    newSelImage.caption = $scope.step.images[newSelImageNum].caption;
    if (!newSelImage.caption) {
      newSelImage.caption = $scope.step.images[newSelImageNum].preview.original_filename;
    }
    $scope.selImage = newSelImage;
    $scope.selectedZoomImage = "/media/" + $scope.step.images[newSelImageNum].fullimage.filename;
    $scope.selImageData = $scope.step.images[newSelImageNum];
  };

  $scope.showImage = function (num) {
    $scope.selectImage(num);
    $scope.selImageData.num = num;
    $scope.selImageData.imageList = $scope.step.images;
    return MediaService.openPlainImgViewer($scope.selImageData);
  };

  $scope.viewDoc = function (doc) {
    if (doc.doctype == C.MEDIA_DOCTYPE_VIDEO) {
      MediaService.openVideoPlayer(doc);
    }
    if (doc.doctype == C.MEDIA_DOCTYPE_PDF) {
      if ($scope.isMobileApp) {
        $window.open("/servlet/mediadownload/doc/" + doc.id);
      } else {
        MediaService.openPdfViewer(doc);
      }
    }
  };

  $scope.viewCurrentImage = function () {
    if ($scope.selImage) {
      $scope.viewImage($scope.step.images[$scope.selImage.num]);
    }
  };

  $scope.viewImage = function (image) {
    if ($scope.selImage) {
      MediaService.killZoomImage('zoomimg');
      image.num = $scope.selImage.num;
      image.imageList = $scope.step.images;
      var selimage = $scope.selImage;
      $scope.selImage = null;
      MediaService.openImgViewer(image).result.then(function () {
          $scope.selImage = selimage;
      }).catch(function () {
          $scope.selImage = selimage;
          $rootScope.$emit("close");
      });
    } else {
      MediaService.openImgViewer(image);
    }
  };

  // Collapsibles

  $scope.collapseDocs = false;
  $scope.toggleShowDocs = function () {
    $scope.collapseDocs = !$scope.collapseDocs;
  };
  $scope.collapseMeasures = false;
  $scope.toggleShowMeasures = function () {
    $scope.collapseMeasures = !$scope.collapseMeasures;
  };
  $scope.collapseTools = false;
  $scope.toggleShowTools = function () {
    $scope.collapseTools = !$scope.collapseTools;
  };

  // Actions

  $scope.enterMeasurement = function (measure) {
    if ($scope.step.readonly) {
      return;
    }
    if (measure.cdentry.locked) {
      return;
    }
    var wasSkipped = (measure.cdentry.status === C.CHK_CDSTATUS_SKIPPED && measure.calculation.optional === 1 && !_.isUndefined(measure.measurement) && !_.isUndefined(measure.measurement.rawvalues) && measure.measurement.rawvalues.skip);
    if (C.matchConst(measure.cdentry.status, C.CHK_CDSTATUS_ALL_LEFTOUT) && !wasSkipped) {
      return;
    }

    // 15.02.2016, ast: bugfix https://suisse.jira.com/browse/LLQA-75 do not copy values if not matrix measurement
    if (MeasureTypeService.isMatrixMeasurement(measure)) {
      // copy matrix values from earlier matrix if identical
      if (!_.isUndefined(measure.calculation.mx_usedfields) && !measure.measurement.value) {
        var last = null;
        var fieldid = measure.calculation.mx_usedfields.sort().join();
        _.each($scope.step.measures, function (curr) {
          if (curr == measure && last != null && !_.isUndefined(last.calculation.mx_usedfields) && !_.isUndefined(last.measurement.rawvalues) && last.calculation.mx_usedfields.sort().join() == fieldid) {
            measure.measurement.rawvalues = last.measurement.rawvalues;
          }
          last = curr;
        })
      }
    }
    var modalInstance = $modal.open({
      templateUrl: 'templates/checks/input.html',
      controller: 'InputMeasurementCtrl',
      size: $scope.isMobileApp ? 'lg' : undefined,
      resolve: {
        measure: function () {
          return measure
        },
        mainscope: function () {
          return $scope
        },
        testmode: function () {
          return false
        },
        measurement: function () {
          return measure.measurement
        },
        mtinfo: function () {
          return MeasureTypeService.mtinfoForId($scope, measure.measuretype)
        }
      }
    });
    modalInstance.result.then(function () {
      if (measure.cdentry.needstool) {
        $scope.chooseTool($scope.step.toolmap[measure.tooltype_id]);
      } else {
        $scope.refreshData();
      }
    }, function () {
      $scope.autoProcess = false;
      $scope.refreshData();
    });
  };

  $scope.goBack = function () {
    $location.url("/app/workflow/" + $stateParams.checkid);
  };

  $scope.goForward = function () {
    if (!$scope.step.readonly) {
      $location.url("/app/workflow/" + $stateParams.checkid + "/" + $scope.step.nextstepid);
    }
  };

  $scope.rewind = function () {
    if (!$('#modalimageviewer').length) {
      var previousStep = parseInt($stateParams.stepnum) - 1;
      if (previousStep > 0) {
        $location.url("/app/workflow/" + $stateParams.checkid + "/" + previousStep);
      }
    } else {
      $rootScope.$emit("toggleImage", -1);
    }
  };

  $scope.forward = function () {
    if (!$('#modalimageviewer').length) {
      var nextStep = parseInt($stateParams.stepnum) + 1;
      if (nextStep <= $scope.step.numsteps) {
          $location.url("/app/workflow/" + $stateParams.checkid + "/" + nextStep);
      }
    } else {
      $rootScope.$emit("toggleImage", 1);
    }
  };

  $scope.showRewindButton = function () {
    var previousStep = parseInt($stateParams.stepnum) - 1;
    return previousStep > 0;
  };

  $scope.showForwardButton = function () {
    if ($scope.step) {
      var nextStep = parseInt($stateParams.stepnum) + 1;
      return nextStep <= $scope.step.numsteps;
    }
  };

  $scope.installKeyboard = function () {
    KeyboardShortcutService.installKeyboardShortcuts($scope, undefined, {
      37: $scope.rewind,
      39: $scope.forward
    });
  };

  $scope.installKeyboard();

  $scope.chooseTool = function (tool, event) {
    if (!_.isUndefined(event)) {
      event.preventDefault();
      event.stopPropagation();
    }
    if ($scope.step.readonly) {
      return;
    }
    var curval = null;

    if (_.isUndefined($rootScope.usedTools)) {
      $rootScope.usedTools = {};
    }

    var vlist = [];
    _.each(tool[0].toolunits, function (tu) {
      if (!tu.deleted) {
        vlist.push({id: tu.id, name: tu.code});
        if (curval == null) {
          curval = tu.id;
        }
      }
    });
    if (!_.isUndefined($rootScope.usedTools[tool[0].id])) {
      curval = $rootScope.usedTools[tool[0].id];
    }
    if (tool[1]) {
      curval = tool[1].id;
    }

    ModalEditorService.showSinglePicker("CHECK.SELTOOL.TITLE", "CHECK.SELTOOL.MESSAGE", curval, vlist).result.then(function (selv) {
      // Calling route CHECK_7
      Restangular.one('workflow', $scope.step.check.id).one('step', $scope.stepnum).all("action").post({
        action: 'savetool',
        tooltype_id: tool[0].id,
        toolunit_id: selv,
        userid: $scope.userinfo.userid
      }).then(function () {
        $rootScope.usedTools[tool[0].id] = selv;
        // LLQA-192 bugfix, if tool on last measure, install keys for step again
        $scope.installKeyboard();
        $scope.refreshData();
      }, AlertService.showRESTError);
    }, function () {
      $scope.autoProcess = false;
      $scope.installKeyboard();
      $scope.refreshData();
    });
  };

  $scope.processAll = function (event) {
    console.log("ProcessAll started!");
    event.stopPropagation();
    event.preventDefault();
    $scope.autoProcess = true;
    $scope.refreshData();
  };

  $scope.quickEnterMeasurement = function (measure, event) {
    if ($scope.step.readonly) {
      return;
    }
    event.stopPropagation();
    // Calling route CHECK_7
    Restangular.one('workflow', $scope.step.check.id).one('step', $scope.stepnum).all("action").post({
      action: 'savemeasure',
      measure_id: measure.id,
      rawvalues: {val: 0.0},
      comment: "QuickInput",
      userid: $scope.userinfo.userid
    }).then(function () {
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  $scope.quickChooseTool = function (tool) {
    if ($scope.step.readonly) {
      return;
    }
    event.stopPropagation();
    // Calling route CHECK_7
    Restangular.one('workflow', $scope.step.check.id).one('step', $scope.stepnum).all("action").post({
      action: 'savetool',
      tooltype_id: tool[0].id,
      toolunit_id: tool[0].toolunits[0].id,
      userid: $scope.userinfo.userid
    }).then(function () {
      $scope.refreshData();
    }, AlertService.showRESTError);
  };

  $scope.setToolunitComment = function (unit, event) {
    event.stopPropagation();
    event.preventDefault();
    ModalEditorService.showTextLineEditor("TOOL.COMMENTUNIT.TITLE", "TOOL.COMMENTUNIT.TEXT", unit.comment).result.then(function (selv) {
      console.log(selv);
      unit.comment = selv;
      // Calling route MISC_6
      Restangular.all("toolunits").post(unit).then(function () {
        $scope.refreshData();
      }, AlertService.showRESTError);
    });
  };

  $scope.changeViewport = function (newvp, doit) {
    if (doit) {
      $scope.viewport = newvp;
    }
  };
});


