dmod.directive('autofocus', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (attr.autofocus !== 'false') $timeout(function() { element[0].focus() }, 100, true)
        }
    };
});
