dmod.directive('commReport', function ($window, i18n) {
    var directiveDefinitionObject = {
        templateUrl: 'templates/plugins/commreport.html',
        restrict: 'E',
        replace: true,
        scope: {
            type: '@type',
            objId: '@objId'
        },
        link: function ($scope, elm, attrs) {
            $scope.openReport = function() {
                var tzoff = new Date().getTimezoneOffset();
                var query = "?type="+$scope.type+"&id="+$scope.objId+"&lang="+i18n.selectedLanguage.code+"&tzoff="+tzoff
                $window.open("/servlet/pdf/comments" + query)
            }
        }
    }
    return directiveDefinitionObject
})
