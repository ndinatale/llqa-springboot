dmod.directive('draggabilly', function() {
    return {
        restrict: 'A',
        scope: {
            options: '=draggabilly'
        },
        link: function(scope, element, attrs) {
            if (!_.isUndefined(scope.options) && !_.isUndefined(scope.options.parent)) {
                for (var i=0; i<scope.options.parent; i++) {
                    element = element.parent()
                }
            }
            element.draggabilly(scope.options)

            if (!_.isUndefined(scope.options) && !_.isUndefined(scope.options.initX)) {
                element.data('draggabilly').position.x = scope.options.initX
                element.draggabilly('setLeftTop')
            }
            if (!_.isUndefined(scope.options) && !_.isUndefined(scope.options.initY)) {
                element.data('draggabilly').position.y = scope.options.initY
                element.draggabilly('setLeftTop')
            }

            /*
            var $draggable =
             $('.draggable').draggabilly({
             // options...
             })
             */
        }
    };
});