dmod.directive('dropbox', function () {
    var directiveDefinitionObject = {
        templateUrl: 'templates/plugins/dropbox.html',
        restrict: 'E',
        replace: true,
        scope: {
            data: '=data'
        }
    }
    return directiveDefinitionObject
})
