dmod.directive('langCompletionInfo', function (i18n, $timeout, textAngularManager) {
    var directiveDefinitionObject = {
        templateUrl: 'templates/plugins/langcompletion.html',
        restrict: 'E',
        replace: true,
        scope: {
            langinfo: '=langinfo',
            model: '=model'
        },
        link: function postLink($scope, elm, attrs) {
            $scope.select = function(idx) {
                $scope.langinfo.select(idx)
                if (!_.isUndefined(attrs['focusAfterChange'])) {
                    $timeout(function() { $(attrs['focusAfterChange'])[0].focus() })
                }
                if (!_.isUndefined(attrs['focusTaAfterChange'])) {
                    $timeout(function() {
                        var editorScope = textAngularManager.retrieveEditor(attrs['focusTaAfterChange']).scope;
                        $timeout(function(){
                            editorScope.displayElements.text.trigger('focus');
                        });
                    })
                }
            }
        }
    }
    return directiveDefinitionObject
})