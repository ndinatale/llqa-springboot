dmod.directive('zoomableImage', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            attrs.$observe('zoomImageSrc',function(){
                linkElevateZoom();
            })
            scope.$watch(attrs['zoomImageSrc'],function(){
                linkElevateZoom();
            })
            function linkElevateZoom(){
                if (!attrs.zoomImageSrc) return;
                var firstinit = _.isUndefined(element.attr('data-zoom-image'))
                var zoomImage = scope[attrs.zoomImageSrc]
                if (_.isNull(zoomImage)) {
                    if (!firstinit) {
                        $(element).data('elevateZoom').changeState('disable')
                    }
                } else {
                    if (firstinit) {
                        element.attr('data-zoom-image',zoomImage);
                        $(element).elevateZoom(scope[attrs.zoomableImage]);
                    } else {
                        element.attr('data-zoom-image',zoomImage);
                        $(element).data('elevateZoom').swaptheimage(attrs.ngSrc,zoomImage);
                        $(element).data('elevateZoom').changeState('enable')
                    }
                }
            }

            linkElevateZoom();
        }
    };
});