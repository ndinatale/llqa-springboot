/*
 Custom attribute filter for filtering an array of objects.
 See attributefilter.spec.js for a documentation of features.

 Expected arguments:
 - elements: array of objects to be filtered
 - attributes: array of strings to filter, for example ['code', 'title'] (for procedure list)
 - searchtext: text to search for
 - language (optional): language to search for in multilingual fields

 Usage: procedure in items | attributefilter : ['code', 'title'] : 'searchtext' : 'de'
 */

fmod.filter('attributefilter', function () {
  return function (elements, attributes, searchtext, language) {
    if (!elements || searchtext == null) {
      return elements;
    }
    searchtext = searchtext.toString().toLowerCase();
    var out = [];
    angular.forEach(elements, function (element) {
      angular.forEach(attributes, function (attr) {
        var value = element[attr];

        // if multilingual attribute, get language
        if (typeof value == 'object' && language) {
          value = value[language];
        }

        // set default if null, else get lowercase string
        if (!value) {
          value = '';
        } else {
          value = value.toString().toLowerCase();
        }
        if (value.indexOf(searchtext) !== -1 && out.indexOf(element) === -1) {
          out.push(element);
        }
      });
    });
    return out;
  };
});