fmod.filter('dashboarditemfilter', function() {
    return function (items) {
        var filtered = [];

        if (items != null) {
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                if (item.code != null) {
                    var code = item.code;
                    if (!(code.slice(-2) == "()" || code.slice(-4) == "(TR)")) {
                        filtered.push(item);
                    }
                } else {
                    filtered.push(item);
                }
            }
        }
        return filtered;
    };
});