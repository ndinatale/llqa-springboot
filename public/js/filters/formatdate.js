fmod.filter('formatdate', function ($filter, i18n) {
    var frmtspan = /\[SPAN:(\d+)]/g
    var frmtdate = /\[DATE:(\d+)]/g
    var frmttime = /\[TIME:(\d+)]/g
    var frmttims = /\[TIMS:(\d+)]/g

    function repldate(format, seccnt) {
        var date = new Date(_.parseInt(seccnt)*1000)
        var format =  i18n.selectedLanguage[format]
        return $filter('date')(date, format, 'utc')
    }

    function replspan(span) {
        var span = _.parseInt(span)
        return sprintf("%dh %02dmin", span/3600, (span/60)%60)
    }

    return function(text) {
        if (_.isUndefined(text) || _.isNull(text)) return text
        text = text.replace(frmtspan, function(_,span) { return replspan(span) })
        text = text.replace(frmtdate, function(_,seccnt) { return repldate('dformat', seccnt) })
        text = text.replace(frmttime, function(_,seccnt) { return repldate('tformat', seccnt) })
        text = text.replace(frmttims, function(_,seccnt) { return repldate('tformat', seccnt).replace(/.*? /,'') })
        return text
    }
})
