fmod.filter('sprintf', function () {
    return function(number,format) {
        if (_.isUndefined(number) || _.isNull(number) || !_.isNumber(number)) number = 0
        return sprintf(format,number)
    }
})
