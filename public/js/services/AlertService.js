smod.factory('AlertService', function ($modal,$location,$rootScope) {
    var service = {}
    service.showMessage = function(msg) {
        var modalInstance = $modal.open({
            templateUrl: 'templates/modal/message.html',
            controller: 'AlertModalCtrl',
            resolve: {
                type: function () { return msg.type },
                intro: function () { return msg.intro },
                reasons: function () { return msg.reasons },
                head: function () { return msg.head }
            }
        });

        return modalInstance
    }
    service.showFileInfo = function(fileInfo) {
        var modalInstance = $modal.open({
            templateUrl: 'templates/modal/fileinfo.html',
            controller: 'FileInfoModalCtrl',
            resolve: {
                fileInfo: function () { return fileInfo }
            }
        });

        return modalInstance
    }
    service.createMessage = function(head,intro,introvals,type) {
        var ret = { head: head, intro: { msg: intro, vals: introvals}, type: type, reasons: [] }
        ret.show = function() { return service.showMessage(ret) }
        ret.addReason = function(reason,reasonvals) { ret.reasons.push({ msg: reason, vals: reasonvals}); return ret }
        return ret
    }
    service.createConfirmMessage = function(head,intro,introvals) {
        return service.createMessage(head,intro,introvals,1)
    }
    service.createErrorMessage = function(head,intro,introvals) {
        return service.createMessage(head,intro,introvals,2)
    }
    service.createAlertMessage = function(head,intro,introvals) {
        return service.createMessage(head,intro,introvals,3)
    }
    service.createSuccessMessage = function(head,intro,introvals) {
        return service.createMessage(head,intro,introvals,4)
    }

    service.showRESTError = function(fail) {
        if (_.isNull(fail)) {
            return
        }
        var alert = service.createErrorMessage("SERVER.ERROR.TITLE",fail.type,{})
        fail.messages.forEach(function (message) {
            alert.addReason(message.msg,message.vals)
        })
        return alert.show()
    }

    service.showSevereRESTError = function(fail) {
        if (_.isNull(fail)) return
        $rootScope.showError("SEVREST",JSON.stringify(fail, null, 4))
    }

    return service
})