smod.factory('AuthService', function (Restangular, $location, $rootScope, $q, i18n, AlertService) {
        var auth = {}
        auth.session = { level: 0 }

        auth.login = function(username, password) {
            // Calling route SESSION_1
            Restangular.all("auth").customGET("seed").then(function success(response) {
                var seed = response.seed
                var passhash = CryptoJS.SHA1(password)
                var passcode = CryptoJS.SHA1(passhash+"."+seed)
                // Calling route SESSION_2
                return Restangular.all("auth").customPOST({ username: username, passcode: ""+passcode }, "login" )
            }).then(function success(response) {
                auth.session = response
                auth.session.level = response.level
                i18n.updateLanguageBySession(auth.session)
                $rootScope.refreshTitleData()
                $location.url('/app/dashboard')
            },function error(response) {
                auth.session = { level: 0 }
                AlertService.showRESTError(response)
            })
        }

        auth.getsession = function() {
            var deferred = $q.defer();

            if (!auth.session.level || auth.session.level < 1) {
                var params = {}
                var header = {}
                // Calling route SESSION_5
                Restangular.all("auth").customGET("check", params, header).then(
                    function success(response) {
                        auth.session = response;
                        auth.session.level = response.level
                        $rootScope.session = auth.session
                        i18n.updateLanguageBySession(auth.session)
                        deferred.resolve(auth.session)
                    },
                    function error() {
                        auth.session = { level: 0 }
                        $rootScope.session = auth.session
                        $location.url('/app/login');
                        deferred.resolve(auth.session)
                    })
            } else {
                $rootScope.session = auth.session
                deferred.resolve(auth.session)
            }

            return deferred.promise;
        }

        auth.updateUserInfo = function() {
            if (_.isUndefined($rootScope.session) || _.isNull($rootScope.session) || $rootScope.session.level < 1) return;
            // Calling route SESSION_4
            Restangular.all("auth").customPOST({ userinfo: $rootScope.session.userinfo }, "userinfo" )
        }
        auth.updateDashboardInfo = function() {
            if (_.isUndefined($rootScope.session) || _.isNull($rootScope.session) || $rootScope.session.level < 1) return;
            // Calling route SESSION_4
            // LLQA-77: perform asynchronous post request + return promise, resolve promise when finished
            return $q(function(resolve, reject) {
                Restangular.all("auth").customPOST({ dashboardinfo: $rootScope.session.dashboardinfo }, "userinfo" )
                    .then(resolve("success"))
            })
        }

        auth.logout = function() {
            var params = {}
            var header = {}
            // Calling route SESSION_3
            Restangular.all("auth").customGET("logout", params, header)
                .then(function success(){
                    auth.session = { level: 0 };
                    $rootScope.session = auth.session
                    $location.url('/app/login');
                }, function() {
                    auth.session = { level: 0 };
                    $rootScope.session = auth.session
                    $location.url('/app/login');
                })
        }

        return auth
    })