smod.factory('BrowserService', function () {
        var browser = {
            mobile: {
                isAndroid: function() {
                    return navigator.userAgent.match(/Android/i) ? true : false
                },
                isBlackBerry: function() {
                    return navigator.userAgent.match(/BlackBerry/i) ? true : false
                },
                isIOS: function() {
                    return navigator.userAgent.match(/iPhone|iPad|iPod/i) ? true : false
                },
                isOpera: function() {
                    return navigator.userAgent.match(/Opera Mini/i) ? true : false
                },
                isWindows: function() {
                    return navigator.userAgent.match(/IEMobile/i) ? true : false
                }
            },
            desktop: {
                isWindows: function() {
                    return (navigator.appVersion.indexOf("Win")!=-1 && browser.isDesktop()) ? true : false
                },
                isMacOSX: function() {
                    return (navigator.appVersion.indexOf("Mac")!=-1 && browser.isDesktop()) ? true : false
                },
                isUnix: function() {
                    return (navigator.appVersion.indexOf("X11")!=-1 && browser.isDesktop()) ? true : false
                },
                isLinux: function() {
                    return (navigator.appVersion.indexOf("Mac")!=-1 && browser.isDesktop()) ? true : false
                }
            },
            isMobile: function() {
                return (browser.mobile.isAndroid() || browser.mobile.isBlackBerry() || browser.mobile.isIOS() || browser.mobile.isOpera() || browser.mobile.isWindows())
            },
            isDesktop: function() {
                return (!browser.isMobile())
            }
        }
        return browser
    })
