smod.factory('ChangeLogService', function ($modal) {
  var service = {};

  service.showChangeLog = function (forObject, objectType) {
    var modalInstance = $modal.open({
      templateUrl: 'templates/modal/changelog.html',
      controller: 'ChangeLogViewCtrl',
      size: 'lg',
      resolve: {
        item: function () {
          return forObject;
        },
        type: function () {
          return objectType;
        }
      }
    });
    return modalInstance;
  };

  return service;
});