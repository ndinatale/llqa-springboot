smod.factory('ConstantService', function (ConstantBundle) {
    var service = {}

    service.installConstants = function(scope, cvar, prefix) {
        _.each(ConstantBundle, function(value,key) {
            var use = false
            if (_.isNull(prefix))
                use = true
            else if (_.isArray(prefix))
                _.each(prefix, function(pf) { if (_.startsWith(key, pf)) use = true })
            else
                if (_.startsWith(key, prefix)) use = true
            if (use) {
                scope[key] = value
                if (!_.isNull(cvar)) cvar[key] = value
            }
        })
        scope.matchConst = function(value, constant) {
            if (_.isNull(constant) || _.isNull(value) || _.isUndefined(constant) || _.isUndefined(value)) return false
            if (_.isArray(constant)) return _.includes(constant,value);
            return constant === value
        }
        if (!_.isNull(cvar)) cvar.matchConst = scope.matchConst
    }

    return service
})