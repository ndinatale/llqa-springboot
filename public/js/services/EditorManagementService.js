smod.factory('EditorManagementService', function(i18n,$rootScope,$location,AlertService,Restangular) {
    var service = {}

     service.installEditorManagement = function(scope,type,mainurl) {
        scope.editor = {
            title: "EDITOR.LOADING",
            titlevals: {},
            subtitle: ""
        }
        scope.languageinfo = {
            selected: i18n.defaultLanguage.code,
            languages: i18n.languages,
            select: function(lang) {
                scope.languageinfo.selected = scope.languageinfo.languages[lang].code
            }
        }

        scope.fillupLang = function(obj,field) {
            if (_.isUndefined(obj[field]) || !_.isObject(obj[field]) || _.isArray(obj[field]) || _.isFunction(obj[field])) obj[field] = {}
            i18n.languages.forEach(function (lang) { if (_.isUndefined(obj[field][lang.code])) obj[field][lang.code] = "" })
        }
        scope.makeTranslateVals = function(model,prefix) {
            var ret = {}
            i18n.languages.forEach(function (lang) { ret[prefix+"_"+lang.code] = model[lang.code] } )
            return ret
        }

        scope.langToggleKeymap = { 33: 'toggleLang(-1,$event)', 34: 'toggleLang(1,$event)' }
        scope.toggleLang = function(dir,event) {
            event.preventDefault()
            event.stopPropagation()
            var curpos = -1
            var cnt = 0
            scope.languageinfo.languages.forEach(function (val) { if (val.code === scope.languageinfo.selected) curpos = cnt; cnt += 1 })
            if (curpos < 0) return true
            curpos += dir
            if (curpos == scope.languageinfo.languages.length) curpos = 0
            if (curpos < 0) curpos = scope.languageinfo.languages.length - 1
            scope.languageinfo.selected = scope.languageinfo.languages[curpos].code
        }

        scope.initCodeCheck = function(code) {
            scope.initialCode = code
        }

        scope.mayChangeCode = function(item) {
            return _.isUndefined(item) || _.isUndefined(item.id) || _.isNull(item.id) || _.isUndefined(scope.initialCode) || _.isNull(scope.initialCode) || scope.initialCode.indexOf('(') >= 0
        }

        scope.codeCheck = function(item,fun) {
            if (!_.isNull(scope.initialCode) && item.code != scope.initialCode && !scope.mayChangeCode(item)) {
                var msg = AlertService.createConfirmMessage("EDITOR.CODECHNG.TITLE", "EDITOR.CODECHNG.TEXT", { })
                msg.show().result.then(function () {
                    fun()
                })
            } else fun()
        }

        scope.saveAndReturn = function() {
            scope.codeCheck(scope.item, function() {
                // Calling route PSM_23(type='measures'),PSM_18('steps'),PSM_4('procedures'),MU_20('units'),MU_6('models'),MISC_5('tools')
                Restangular.all(type).post(scope.item).then(function (item) {
                    scope.refreshListData()
                    $location.url("/app" + mainurl + "/view/" + item.id)
                }, AlertService.showRESTError)
            })
        }

        scope.cancelEdit = function() {
            if ($rootScope.selecteditem == null) {
                $location.url("/app" + mainurl)
            } else {
                $location.url("/app" + mainurl + "/view/" + scope.item.id)
            }
        }

    }

    return service
})