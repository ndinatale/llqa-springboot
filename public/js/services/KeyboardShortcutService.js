smod.factory('KeyboardShortcutService', function ($rootScope, $document, $timeout) {
  var service = {};

  function handleKey(handlerlist, evt, type, keycode, shiftKey, ctrlKey, altKey, setno) {
    var handler = (_.isUndefined(handlerlist) || _.isUndefined(handlerlist[keycode])) ? undefined : handlerlist[keycode];
    if (!_.isUndefined(handler)) {
      evt.preventDefault();
      evt.stopPropagation();
      $timeout(function () {
        handler(type, keycode, shiftKey, ctrlKey, altKey)
      })
    }
  }

  var counter = 1;
  var currentSet = 0;

  service.installKeyboardShortcuts = function (scope, pressShortCuts, upShortCuts, downShortCuts) {
    var thisSet = counter;
    var lastSet = currentSet;
    currentSet = thisSet;
    counter += 1;
    if (_.isObject(pressShortCuts)) {
      $document.bind('keypress.shortcut_' + thisSet, function (evt) {
        if (currentSet == thisSet) {
          handleKey(pressShortCuts, evt, "press", evt.keyCode, evt.shiftKey, evt.ctrlKey, evt.altKey, thisSet);
        }
      });
    }
    if (_.isObject(downShortCuts)) {
      $document.bind('keydown.shortcut_' + thisSet, function (evt) {
        if (currentSet == thisSet) {
          handleKey(downShortCuts, evt, "down", evt.keyCode, evt.shiftKey, evt.ctrlKey, evt.altKey, thisSet);
        }
      });
    }
    if (_.isObject(upShortCuts)) {
      $document.bind('keyup.shortcut_' + thisSet, function (evt) {
        if (currentSet == thisSet) {
          handleKey(upShortCuts, evt, "up", evt.keyCode, evt.shiftKey, evt.ctrlKey, evt.altKey, thisSet);
        }
      });
    }

    scope.$on("$destroy", function () {
      $document.unbind('keypress.shortcut_' + thisSet);
      $document.unbind('keyup.shortcut_' + thisSet);
      $document.unbind('keydown.shortcut_' + thisSet);
      if (thisSet == currentSet) {
        currentSet = lastSet;
      }
    });
  };

  return service;
});
