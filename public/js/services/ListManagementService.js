smod.factory('ListManagementService', function ($rootScope, UsersService, VersioningService, Restangular, AlertService) {
  var service = {};

  service.setSelectedItem = function (item) {
    $rootScope.selecteditem = item;
  };

  service.calcRealidStatus = function (list) {
    var realidstatus = {};
    list.forEach(function (item) {
      item.versname = VersioningService.versionName(item.version, item.versions);
      if (item.version == null) {
        realidstatus[item.realid] = true;
      }
    });
    return realidstatus
  };

  service.installListManagement = function (scope, userinfoblock) {
    $rootScope.selecteditem = -1;
    scope.showDeleted = false;

    scope.filters = {
      textfilter: '',
      sorted: null
    };

    scope.itemSelected = function (item) {
      if (_.isUndefined($rootScope.selecteditem) || _.isNull($rootScope.selecteditem)) {
        return {};
      }
      if ($rootScope.selecteditem.id == item.id) {
        return {'ll-selected': true}
      }
      if (!_.isUndefined($rootScope.selecteditem.realid) && $rootScope.selecteditem.realid == item.realid) {
        return {'ll-subselected': true};
      }
      return {}
    };

    scope.itemDeleted = function (item) {
      return _.isUndefined(scope.realidstatus[item.realid]);
    };

    scope.toggleShowDeleted = function () {
      scope.showDeleted = !scope.showDeleted
    };

    scope.clearTextfilter = function () {
      scope.filters.textfilter = '';
      scope.saveFilterSettings();
    };

    scope.saveFilterSettings = function () {
      UsersService.updateUserInfo();
    };

    scope.loadFilterSettings = function () {
      if (_.isUndefined(UsersService.getUserInfo()[userinfoblock])) {
        UsersService.getUserInfo()[userinfoblock] = scope.filters;
        scope.saveFilterSettings();
      } else {
        scope.filters = UsersService.getUserInfo()[userinfoblock];
        if (_.isNull(scope.filters.textfilter)) {
          scope.filters.textfilter = '';
        }
      }
    };
  };

  service.prepareSublist = function (scope, sublist) {
    var cnt = 0;
    sublist.forEach(function (item) {
      cnt += 1;
      item.cnt = cnt;
    });
    scope.sublist = sublist;
  };

  service.installReorderableSublistManagement = function (scope, sublist) {
    scope.reorderingActivated = false;
    scope.reorderingNeedsActivation = false;

    scope.activateReordering = function (status) {
      if (!status) {
        scope.reorderingNeedsActivation = true;
      }
      else {
        scope.wrapEditWarning(function () {
          scope.reorderingActivated = true;
        }, true);
      }
    };

    scope.showActivateReorderIcon = function (necGrant) {
      if (!scope.showEditButtons(necGrant)) {
        return false;
      }
      return scope.reorderingNeedsActivation && !scope.reorderingActivated;
    };

    scope.showReorderIcon = function (necGrant) {
      if (!scope.showEditButtons(necGrant)) {
        return false;
      }
      return !scope.reorderingNeedsActivation || scope.reorderingActivated;
    };

    scope.sublistSortFinalize = function () {
      console.log("SORT FINALIZE");
      scope.sublistSortOptions.disabled = true;
      var ret = scope.sublist.map(function (item) {
        return item.cnt;
      });
      // Calling route PSM_19(sublist='measures'),PSM_16('steps'),MU_11('activeprocedures'),MISC_24('snippets')
      Restangular.one(sublist, scope.sublist[0].id).all('action_reorder').post({neworder: ret}).then(function success(response) {
        if (_.isFunction(scope.refreshData)) {
          scope.refreshData();
        }
        if (_.isFunction(scope.refreshModalData)) {
          scope.refreshModalData();
        }
        if (_.isFunction(scope.refreshListData)) {
          scope.refreshListData();
        }
        scope.sublistSortOptions.disabled = false;
      }, AlertService.showRESTError);
    };

    scope.sublistSortOptions = {
      axis: 'y',
      placeholder: 'll-draggeditem',
      handle: '.ll-dragaction',
      stop: function (event, ui) {
        scope.sublistSortFinalize();
      }
    }
  };

  return service
});
