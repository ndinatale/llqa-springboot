smod.factory('MeasureTypeService', function($q, Restangular) {

    function getMeasuretypes (scope) {
        var deferred = $q.defer()
        // Calling route PSM_25
        Restangular.all('measuretypes').getList().then(function mtData(response) {
            scope.measuretypes = response
            deferred.resolve(response)
        }, function(error) { deferred.reject(error) })
        return deferred.promise
    }

    function mtinfoForId (scope, id) {
        var newmtinfo = {id: 0, title: "", type: null, input: null}
        _.each(scope.measuretypes,function(mt) { if (mt.id == id) newmtinfo = {id: mt.id, title: mt.title, type: mt.type, input: mt.input, link: mt.link } })
        return newmtinfo
    }

    // 15.02.2016, ast: bugfix https://suisse.jira.com/browse/LLQA-75 do not copy values if not matrix measurement
    // consider measures.measuretype in 6, 7, 9, 12 as matrix measurements
    function isMatrixMeasurement (measurement) {
        var isMatrixMeasurement = false;
        _.forEach([6, 7, 9, 12], function (value) {
            if (measurement.measuretype === value) {
                isMatrixMeasurement = true;
            }
        });
        return isMatrixMeasurement;
    }

    return {
        getMeasuretypes: getMeasuretypes,
        mtinfoForId: mtinfoForId,
        isMatrixMeasurement: isMatrixMeasurement
    };
})
