smod.factory('MediaService', function ($modal) {
    var service = {}

    service.openMediaManager = function (ownertype,ownerid,subtitle,subtvals) {
        var modalInstance = $modal.open({
            templateUrl: 'templates/modal/mediamgr.html',
            controller: 'MediaManagerCtrl',
            size: 'lg',
            keyboard: false,
            backdrop: 'static',
            resolve: {
                owner: function() { return { type: ownertype, id: ownerid } },
                subtitle: function() { return { msg: subtitle, vals: subtvals } }
            }
        })
        return modalInstance
    }

    service.openVideoPlayer = function (video) {
        var modalInstance = $modal.open({
            templateUrl: 'templates/modal/video.html',
            controller: 'VideoModalCtrl',
            size: 'lg',
            keyboard: false,
            backdrop: 'static',
            resolve: {
                medium: function() { return video }
            }
        })
        return modalInstance
    }

    service.openPdfViewer = function (pdf) {
        var modalInstance = $modal.open({
            templateUrl: 'templates/modal/pdfview.html',
            controller: 'PdfViewerCtrl',
            size: 'lg',
            keyboard: false,
            backdrop: 'static',
            resolve: {
                medium: function() { return pdf }
            }
        })
        return modalInstance
    }

    service.openImgViewer = function (image) {
        var modalInstance = $modal.open({
            templateUrl: 'templates/modal/image.html',
            controller: 'ImageModalCtrl',
            size: 'lg',
            resolve: {
                medium: function() { return image },
                disptype: function() { return 'zoom' }
            }
        })
        return modalInstance
    }

    service.openPlainImgViewer = function (imagedata) {
        if (_.isNull(imagedata)) {
            return
        }
        var modalInstance = $modal.open({
            templateUrl: 'templates/modal/imageplain.html',
            controller: 'ImageModalCtrl',
            size: 'lg',
            keyboard: false,
            backdrop: 'static',
            resolve: {
                medium: function() { return imagedata },
                disptype: function() { return 'plain' }
            }
        })
        return modalInstance
    }

    service.fitIntoBox = function (img, boxw, boxh, border) {
        var w = img.metadata.width
        var h = img.metadata.height
        var scale = Math.min((0.0+boxw-border*2)/w,(0.0+boxh-border*2)/h)
        w = Math.floor(w*scale)
        h = Math.floor(h*scale)
        return { width: w, height: h, scale: scale, padleft: Math.floor((boxw-border*2-w)/2.0), padtop: Math.floor((boxh-border*2-h)/2.0) }
    }

    service.placeIntoBox = function (img, boxw, boxh, border, actualscale) {
        var w = img.metadata.width
        var h = img.metadata.height
        var maxscale = Math.min((0.0+boxw-border*2)/w,(0.0+boxh-border*2)/h)
        var fitscale = Math.max((0.0+boxw-border*2)/w,(0.0+boxh-border*2)/h)
        if (_.isNull(actualscale)) actualscale = (fitscale < 1.0 && fitscale < maxscale*2) ? fitscale : maxscale
        w = Math.floor(w*actualscale)
        h = Math.floor(h*actualscale)
        return { width: w, height: h, maxscale: maxscale, fitscale: fitscale, scale: actualscale }
    }

    service.killZoomImage = function(imgid) {
        $('#'+imgid).removeAttr('data-zoom-image')
        $('#'+imgid).removeData('elevateZoom');
        $('.zoomWrapper img.zoomed').unwrap();
        $('.zoomContainer').remove();
    }

    return service
})