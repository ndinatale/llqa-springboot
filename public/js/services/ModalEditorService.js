smod.factory('ModalEditorService', function($modal, i18n) {
    var service = {};
    service.showTextLineEditor = function(title,text,value,params) {
        var modalInstance = $modal.open ({
            templateUrl: 'templates/modal/textlineeditor.html',
            controller: 'ModalEditorCtrl',
            resolve: {
                type: function () { return 1 },
                title: function () { return title },
                text: function () { return text },
                value: function () { return value },
                elems: function () { return null },
                params: function () { return (_.isUndefined(params) || _.isNull(params)) ? {} : params }
            }
        });
        return modalInstance
    };
    service.showTextAreaEditor = function(title,text,value,params) {
        var modalInstance = $modal.open ({
            templateUrl: 'templates/modal/textareaeditor.html',
            controller: 'ModalEditorCtrl',
            resolve: {
                type: function () { return 2 },
                title: function () { return title },
                text: function () { return text },
                value: function () { return value },
                elems: function () { return null },
                params: function () { return (_.isUndefined(params) || _.isNull(params)) ? {} : params }
            }
        });
        return modalInstance
    };
    service.showDatePicker = function(title,text,value,params) {
        var modalInstance = $modal.open ({
            templateUrl: 'templates/modal/datepicker.html',
            controller: 'ModalEditorCtrl',
            size: 'sm',
            resolve: {
                type: function () { return 3 },
                title: function () { return title },
                text: function () { return text },
                value: function () { return value },
                elems: function () { return null },
                params: function () { return (_.isUndefined(params) || _.isNull(params)) ? {} : params }
            }
        });
        return modalInstance
    };
    service.showMultiPicker = function(title,text,value,items,params) {
        var modalInstance = $modal.open ({
            templateUrl: 'templates/modal/multiselect.html',
            controller: 'ModalEditorCtrl',
            resolve: {
                type: function () { return 6 },
                title: function () { return title },
                text: function () { return text },
                value: function () { return value },
                elems: function () { return items },
                params: function () { return (_.isUndefined(params) || _.isNull(params)) ? {} : params }
            }
        });
        return modalInstance
    };
    service.showSinglePicker = function(title,text,value,items,params) {
        var modalInstance = $modal.open ({
            templateUrl: 'templates/modal/multiselect.html',
            controller: 'ModalEditorCtrl',
            resolve: {
                type: function () { return 5 },
                title: function () { return title },
                text: function () { return text },
                value: function () { return value ? [value] : null },
                elems: function () { return items },
                params: function () { params = (_.isUndefined(params) || _.isNull(params)) ? {} : params; params.single=true; return params }
            }
        });
        return modalInstance
    };
    service.showSettingsDetailEditor = function(title, text, value, subitems, params) {
        return $modal.open({
            templateUrl: 'templates/modal/settingsdetaileditor.html',
            controller: 'ModalEditorCtrl',
            controllerAs: 'ctrl',
            resolve: {
                type: function () { return 7 },
                title: function () { return title },
                text: function () { return text },
                value: function () { return value },
                elems: function () { return _.sortBy(subitems) },
                params: function () { return (_.isUndefined(params) || _.isNull(params)) ? {} : params }
            }
        });
    };
    return service
});