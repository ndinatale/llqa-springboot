smod.factory('ProcedureTagService', function() {

    function initializeTags(data) {
        return _.chain(data)
            .filter(function (item) {
                return item.key === 'procedure_tag';
            })
            .map(function (item) {
                var availableTagValues = _.chain(item.sub_values)
                    .map(function (subValue) {
                        return {
                            value: subValue
                        }
                    })
                    .sortBy('value')
                    .value();
                return {
                    description: item.value,
                    availableTagValues: availableTagValues
                }
            })
            .value();
    }

    return {
        initializeTags: initializeTags
    }

});
