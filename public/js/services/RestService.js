smod.factory('RestService', function($location, Restangular, AlertService) {
    var service = {}

    service.deleteStep = function(scope,step,procedure,url) {
        var msg = AlertService.createConfirmMessage("STEP.ALERT.CONFHEADER", "STEP.ALERT.DELETE", { scode: step.code, pcode: procedure.code })
        if (step.mcnt > 0) msg.addReason("STEP.ALERT.DELETECASCADING", { mcnt: step.mcnt })
        if (scope.isVersion()) msg.addReason("STEP.ALERT.DELETEVERSION", {})
        msg.show().result.then(function () {
            // Calling route PSM_17
            Restangular.one('steps',step.id).remove().then(function () {
                scope.refreshListData()
                if (_.isNull(url)) scope.refreshData(); else $location.url("/app" + url)
            }, AlertService.showRESTError)
        })
    }

    service.deleteMeasure = function(scope,measure,step,procedure,url) {
        var msg = AlertService.createConfirmMessage("MEASURE.ALERT.CONFHEADER", "MEASURE.ALERT.DELETE", { mcode: measure.code, scode: step.code, pcode: procedure.code })
        if (scope.isVersion()) msg.addReason("MEASURE.ALERT.DELETEVERSION", {})
        msg.show().result.then(function () {
            // Calling route PSM_20
            Restangular.one('measures',measure.id).remove().then(function () {
                scope.refreshListData()
                if (_.isNull(url)) scope.refreshData(); else $location.url("/app" + url)
            }, AlertService.showRESTError)
        })
    }

    return service
})
