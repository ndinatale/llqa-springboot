smod.factory('SnippetEditorService', function($modal) {
    var service = {}

    service.showSnippetEditor = function(category, catnamefullp, catnamesing, mutable) {
        $modal.open ({
            templateUrl: 'templates/modal/editsnippet.html',
            controller: 'SnippetEditorCtrl',
            size: 'lg',
            resolve: {
                category: function () { return category },
                catname: function () { return { full: catnamefullp, singular: catnamesing } },
                mutable: function () { return mutable }
            }
        })
    }

    return service
})
