smod.factory('UploaderService', function($location, $filter, i18n, FileUploader, Restangular, AlertService, ModalEditorService, VersioningService) {
    var service = {}

    service.installImportUploader = function (scope, type, importUrl, refid) {
        var uploader = scope.uploader = new FileUploader({
            scope: scope,
            removeAfterUpload: true,
            autoUpload: true,
            filters: []
        })

        scope.uploaderinfo = {
            collapsed: true,
            uploading: null,
            numremaining: null,
            numuploaded: 0,
            numsuccess: 0,
            numerror: 0,
            progress: 0
        }

        scope.toggleUploader = function () {
          scope.wrapEditWarning(function () {
            if (scope.uploaderinfo.numremaining > 0 || scope.uploaderinfo.uploading != null) return
            scope.uploaderinfo.uploading = null
            scope.uploaderinfo.numremaining = 0
            scope.uploaderinfo.numuploaded = 0
            scope.uploaderinfo.numsuccess = 0
            scope.uploaderinfo.numerror = 0
            scope.uploaderinfo.collapsed = !scope.uploaderinfo.collapsed
          }, true);
        }

        uploader.onAfterAddingFile = function (item) {
            scope.uploaderinfo.numremaining += 1
        }

        uploader.onBeforeUploadItem = function (item) {
            item.url = '/api/'+type+'/import'
            if (refid) item.url += "/"+refid
            scope.uploaderinfo.uploading = item.file.name
            scope.uploaderinfo.numremaining -= 1
        }

        uploader.onProgressAll = function (progress) {
            scope.uploaderinfo.progress = progress
        }

        uploader.onCompleteItem = function (item, response, status, headers) {
            scope.uploaderinfo.uploading = null
            if (response.status === "success") {
                scope.uploaderinfo.numsuccess += 1
                if (_.isNull(importUrl)) {
                    scope.refreshData()
                } else {
                    scope.refreshListData()
                    $location.url("/app" + importUrl + response.data.new_id)
                }
            } else {
                scope.uploaderinfo.numerror += 1
                AlertService.showRESTError(response)
            }
        }
    }

    var getProcList = function(cont) {
        // Calling route PSM_1
        Restangular.all('procedures').getList().then(function(plist) {
            var blocks = []
            var curitems = null
            _.each(plist, function (proc) {
                curitems = []
                blocks.push({ code: proc.code, title: proc.title, items: curitems })
                _.sortBy(proc.versions, 0).forEach(function (val) {
                    var vname = VersioningService.versionName(val[0],proc.versions)
                    var data = _.isNull(val[0]) ? { text: '' } : { text: 'VERSIONING.LASTCHG', date: $filter('date')(val[2].date, i18n.selectedLanguage.tformat, 'utc'), realname: val[2].realname, username: val[2].username }
                    curitems.push({id: val[1], name: $filter('translate')(vname.text,vname), code: data})
                })
                _(curitems).reverse().value()
            })
            blocks = _.sortBy(blocks,'code')
            cont(blocks)
        }, AlertService.showRESTError)
    }

    service.cloneProcedure = function(finalize) {
        getProcList(function(blocks) {
            ModalEditorService.showMultiPicker('PROCEDURE.CLONE.TITLE', 'PROCEDURE.CLONE.TEXT', null, blocks, {blocks: true, style: 3}).result.then(function (res) {
                // Calling route PSM_6
                Restangular.all('procedures').all('clone').post(res).then(function () {
                    finalize()
                }, AlertService.showRESTError)
            })
        })
    }

    service.cloneStep = function(proc_id,finalize) {
        getProcList(function(blocks) {
            ModalEditorService.showSinglePicker('STEP.CLONE.TITLE','STEP.CLONE.PSELTEXT',null,blocks,{blocks:true, style:3, uncheckable:true}).result.then(function (pid) {
                if (_.isNull(pid)) return
                // Calling route PSM_12
                Restangular.one('procedures',pid).all('steps').getList({copysel:true}).then(function(slist) {
                    ModalEditorService.showMultiPicker('STEP.CLONE.TITLE','STEP.CLONE.TEXT',null,slist,{style:1}).result.then(function(res) {
                        // Calling route PSM_14
                        Restangular.all('steps').all('clone').all(proc_id).post(res).then(function () {
                            finalize()
                        }, AlertService.showRESTError)
                    })
                })
            })
        })
    }

    service.cloneMeasure = function(step_id,finalize) {
        getProcList(function(blocks) {
            ModalEditorService.showSinglePicker('MEASURE.CLONE.TITLE','MEASURE.CLONE.PSELTEXT',null,blocks,{blocks:true, style:3, uncheckable:true}).result.then(function (pid) {
                if (_.isNull(pid)) return
                // Calling route PSM_12
                Restangular.one('procedures',pid).all('steps').getList({copyselmeasure:true}).then(function(slist) {
                    var blocks = slist
                    _.each(blocks,function(step) { step.items = step.measures })
                    ModalEditorService.showMultiPicker('MEASURE.CLONE.TITLE','MEASURE.CLONE.TEXT',null,blocks,{blocks:true, style:1}).result.then(function(res) {
                        // Calling route PSM_22
                        Restangular.all('measures').all('clone').all(step_id).post(res).then(function () {
                            finalize()
                        }, AlertService.showRESTError)
                    })
                })
            })
        })
    }

    return service
})