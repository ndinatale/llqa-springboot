smod.factory('UsersService', function ($rootScope, $modal, $filter, AuthService) {
  var service = {};

  service.userHasGrant = function (grantcodes) {
    if (_.isUndefined($rootScope.session) || _.isNull($rootScope.session) || $rootScope.session.level < 1) {
      return false;
    }
    var res = false;
    _.each(grantcodes.split(','), function (grantcode) {
      if (_.includes($rootScope.session.grants, grantcode.substr(0, 3) + "ALL")) {
        res = true;
      }
      if (_.includes($rootScope.session.grants, grantcode)) {
        res = true;
      }
    });
    return res;
  };

  service.userHasThisGrant = function (grantcodes) {
    if (_.isUndefined($rootScope.session) || _.isNull($rootScope.session) || $rootScope.session.level < 1) {
      return false;
    }
    var res = false;
    _.each(grantcodes.split(','), function (grantcode) {
      if (_.includes($rootScope.session.grants, grantcode)) {
        res = true;
      }
    });
    return res;
  };

  service.userHasAnyGrant = function (grantcodes) {
    if (_.isUndefined($rootScope.session) || _.isNull($rootScope.session) || $rootScope.session.level < 1) {
      return false;
    }
    var res = false;
    _.each(grantcodes.split(','), function (grantcode) {
      _.each($rootScope.session.grants, function (grnt) {
        if (grnt.substr(0, 3) == grantcode) {
          res = true;
        }
      });
    });
    return res;
  };

  service.addGrantMethods = function (scope) {
    scope.currUserName = function () {
      if (_.isUndefined($rootScope.session)) {
        return "???";
      }
      return $rootScope.session.username;
    };

    scope.currUserRealName = function () {
      if (_.isUndefined($rootScope.session)) {
        return "???";
      }
      return $rootScope.session.realname;
    };

    scope.isLoggedIn = function () {
      if (_.isUndefined($rootScope.session)) {
        return false;
      }
      return $rootScope.session.level > 0;
    };

    scope.hasGrant = function (grant) {
      if (_.isUndefined($rootScope.session)) {
        return false;
      }
      return service.userHasGrant(grant);
    };

    scope.hasAnyGrant = function (grant) {
      if (_.isUndefined($rootScope.session)) {
        return false;
      }
      return service.userHasAnyGrant(grant);
    };

    scope.hasThisGrant = function (grant) {
      if (_.isUndefined($rootScope.session)) {
        return false;
      }
      return service.userHasThisGrant(grant);
    };

    scope.removeGrant = function (grantcode) {
      if (_.isUndefined($rootScope.session)) {
        return false;
      }
      var idx = _.indexOf($rootScope.session.grants, grantcode);
      if (idx < 0) {
        return false;
      }
      $rootScope.session.grants.splice(idx, 1);
      return true;
    };

    scope.addGrant = function (grantcode) {
      if (_.isUndefined($rootScope.session)) {
        return false;
      }
      var idx = _.indexOf($rootScope.session.grants, grantcode);
      if (idx >= 0) {
        return false;
      }
      $rootScope.session.grants.splice(idx, -1, grantcode);
      return true;
    };

    scope.changeEffectiveLevel = function (level) {
      if (_.isUndefined($rootScope.session)) {
        return false;
      }
      $rootScope.session.level = level;
      return true;
    };

    scope.hasEffectiveLevel = function (level) {
      if (_.isUndefined($rootScope.session)) {
        return false;
      }
      return $rootScope.session.level >= level;
    };

    scope.toggleGrant = function (grantcode) {
      if (_.isUndefined($rootScope.session)) {
        return false;
      }
      if (!scope.addGrant(grantcode)) {
        scope.removeGrant(grantcode);
      }
    };

    scope.listOfGrants = function () {
      if (_.isUndefined($rootScope.session)) {
        return [];
      }
      return $rootScope.session.allgrants;
    };

    return AuthService.getsession();
  };

  service.updateUserInfo = function () {
    AuthService.updateUserInfo();
  };

  service.getUserInfo = function () {
    if (_.isUndefined($rootScope.session) || _.isNull($rootScope.session) || $rootScope.session.level < 1) {
      return {};
    }
    return $rootScope.session.userinfo;
  };

  service.updateDashboardInfo = function () {
    // LLQA-77: returning promise
    return AuthService.updateDashboardInfo();
  };

  service.getDashboardInfo = function () {
    if (_.isUndefined($rootScope.session) || _.isNull($rootScope.session) || $rootScope.session.level < 1) return [];
    var dashboardInfo = $rootScope.session.dashboardinfo;
    if (!_.isArray(dashboardInfo) || dashboardInfo.length < 1) {
      console.log("CREATE FIRST DASHBOARD");
      dashboardInfo = [
        {
          name: $filter('translate')('FRAME.DASHBOARD'),
          blocks: []
        }
      ];
      $rootScope.session.dashboardinfo = dashboardInfo;
      service.updateDashboardInfo();
    }
    return dashboardInfo;
  };

  service.showUserPicker = function (title, body, seluser, selgroup, anyuser, anygroup, delassign) {
    var modalInstance = $modal.open({
      templateUrl: 'templates/modal/userpicker.html',
      controller: 'UserPickerCtrl',
      resolve: {
        title: function () {
          return title
        },
        text: function () {
          return body
        },
        presel: function () {
          return {user: seluser, group: selgroup}
        },
        param: function () {
          return {anygroup: anygroup, anyuser: anyuser, delassign: delassign}
        }
      }
    });
    return modalInstance;;

  };

  return service;
});