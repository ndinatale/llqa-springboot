smod.factory('VersioningService', function () {
    var instance = {}

    instance.versionName = function(versid, versionlist) {
        var latestversion = null
        if (versionlist) {
            _.each(versionlist, function(v) { if (v[0] && (!latestversion || v[0]>latestversion)) latestversion = v[0] })
        }
        return _.isNull(versid) ? (latestversion ? { text: "VERSIONING.EDIT.VERSIONED", vers: latestversion } : { text: "VERSIONING.EDIT.NEW", vers: 0 }) : { text: "VERSIONING.VERSION", vers: versid }
    }

    return instance
})

