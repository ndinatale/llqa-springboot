smod.factory('ViewerManagementService', function($window,$filter,$location,i18n,AlertService,VersioningService,ModalEditorService) {
    var service = {}

    service.installViewerManagement = function(scope,versioncarrier,model,mainurl) {
        scope.showEditButtons = function(necGrant) {
            return scope.hasGrant(necGrant) && (scope.hasGrant('MODVRS') || !scope.isVersion())
        }

        scope.isVersion = function () {
            if (_.isNull(versioncarrier))
                return !_.isUndefined(scope.item) && !_.isUndefined(scope.item.version) && !_.isNull(scope.item.version)
            return !_.isUndefined(scope[versioncarrier]) && !_.isNull(scope[versioncarrier].version)
        }

        scope.wrapEditWarning = function (fun, denyModificationIfFinalized) {
            if (denyModificationIfFinalized === undefined) {
              denyModificationIfFinalized = false;
            }
            if (scope.isVersion() && denyModificationIfFinalized) {
                var msg = AlertService.createErrorMessage(model.toUpperCase()+".ALERT.DENYHEADER", model.toUpperCase()+".ALERT.DENYMODIFICATION")
                msg.show()
            }
            else if (scope.isVersion()) {
                var msg = AlertService.createConfirmMessage(model.toUpperCase()+".ALERT.CONFHEADER", model.toUpperCase()+".ALERT.EDITVERSION", { code: scope.item.code })
                msg.addReason(model.toUpperCase()+".ALERT.EDITVERSIONDETAIL", {})
                msg.show().result.then(function () {
                    fun()
                })
            } else {
                fun()
            }
        }

        scope.editItem = function () {
            scope.wrapEditWarning(function() { $location.url("/app" + mainurl+"/edit/" + scope.item.id) })
        }

        scope.exportItem = function (event) {
            $window.open("/servlet/export/"+model+"s/" + scope.item.id + ((event.shiftKey || event.metaKey) ? '?complete=true' : ''))
        }

        scope.changeVersion = function () {
            var curval = scope.item.id
            if (curval == null) curval = -1
            var vlist = []
            _.sortBy(scope.item.versions, 0).forEach(function (val) {
                var vname = VersioningService.versionName(val[0],scope.item.versions)
                var data = _.isNull(val[0]) ? { text: '' } : { text: 'VERSIONING.LASTCHG', date: $filter('date')(val[2].date, i18n.selectedLanguage.tformat, 'utc'), realname: val[2].realname, username: val[2].username }
                vlist.push({id: val[1], name: $filter('translate')(vname.text,vname), code: data})
            })
            _(vlist).reverse().value()
            ModalEditorService.showSinglePicker(model.toUpperCase()+".SWITCHV.TITLE", model.toUpperCase()+".SWITCHV.MESSAGE", curval, vlist, {style: 3}).result.then(function (selv) {
                $location.url("/app" + mainurl+"/view/"+selv)
            })
        }

    }

    return service
})
