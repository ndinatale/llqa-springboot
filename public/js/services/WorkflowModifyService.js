smod.factory('WorkflowModifyService', function($modal) {
    var service = {}

    service.openWorkflowModifierEditor = function (subtitle, procid, modlist) {
        var modalInstance = $modal.open({
            templateUrl: 'templates/modal/wflowmodify.html',
            controller: 'WorkflowModifierEditorCtrl',
            keyboard: false,
            backdrop: 'static',
            resolve: {
                subtitle: function() { return subtitle },
                procid: function() { return procid },
                modifiers: function() { return modlist }
            }
        })
        return modalInstance
    }

    service.getWorkflowModifiersForDisplay = function(mod) {
        if (mod.length == 0) return [ ['WFMODIFIER.NOMODIFIERS', {}] ]
        var res = [ ["WFMODIFIER.HASMODIFIERS", { num: mod.length }] ]
        _.each(mod,function(me) {
            var i18nstr = "WFMODIFIER."+me.type+"."+me.trigger.toUpperCase()
            if (_.includes(['Pass','Fail','Skip','Omit','Yes','No'],me.trigger)) i18nstr += "."+me.res.toUpperCase()
            if (!_.includes(['Pass','Fail','Yes','No'],me.trigger)) i18nstr += "."+(me.inverse ? "INV" : "STD")
            res.push( [i18nstr, { code: me.code, key: me.key }] )
        })
        return res
    }

    return service
})

/*
# type trigger   parameters
#  CT   Select    code, inverse
#  U    Set       key, inverse
#  AP   Set       key, inverse
#  M    Select    code, inverse
#  DT   Select    code, inverse
#  P    InQueue   code, inverse
#  P    P/F/S/O   code, res, inverse (S/O only)
#  S    P/F/S/O   code, res, inverse (S/O only)
#  M    P/F/S/O   code, res, inverse (S/O only)
#  M    Yes/No    code, res
*/