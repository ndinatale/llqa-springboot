smod.factory('i18n', function (Restangular, $translate, $timeout) {
    var i18n = {}

    i18n.languages =
        [/* INSERT_LANGUAGEBLOCK */]
    i18n.defaultLanguage = i18n.languages[0]

    i18n.updateLanguage = function (language) {
        i18n.selectedLanguage = language
        $translate.use(language.code)
        Restangular.setDefaultHeaders({'Accept-Language': i18n.selectedLanguage.code})
    }
    i18n.updateLanguageByCode = function (langcode) {
        var newlang = undefined
        _.forEach(i18n.languages, function(lang) { if (lang.code == langcode) newlang = lang })
        if (!_.isUndefined(newlang)) i18n.updateLanguage(newlang)
    }
    i18n.updateLanguageBySession = function (session) {
        $timeout(function() {
            if (!_.isUndefined(session.userinfo) && !_.isUndefined(session.userinfo.default_lang)) i18n.updateLanguageByCode(session.userinfo.default_lang)
        })
    }
    i18n.getSelectedLanguage = function () {
        return i18n.selectedLanguage;
    }

    i18n.updateLanguage(i18n.languages[0], true)

    i18n.translate = function(element) {
        return element ? element[i18n.selectedLanguage.code] : null
    }

    return i18n
})