var _ = require('../../components/lodash/lodash.js'),
    fs = require('fs'),
    german = require('../i18n/lang-de.json'),
    chinese = require('../i18n/lang-cn.json'),
    english = require('../i18n/lang-en.json'),
    keys = [],
    lines = [],
    SEPARATOR = '\t';


function iterate(obj, stack) {
    for (var property in obj) {
        if (obj.hasOwnProperty(property)) {
            if (typeof obj[property] == "object") {
                var stack;
                if (stack) {
                    stack + '.' + property;
                    iterate(obj[property], stack + '.' + property);
                } else {
                    iterate(obj[property], property);
                }
            } else {
                keys.push(stack + '.' + property);
            }
        }
    }
}

iterate(german, null);

for (var i = 0; i < keys.length; i++) {

    var key = keys[i],
        line = key + SEPARATOR;
    console.log('line', line);
    if (_.get(english, key)) {
        line += _.get(english, key) + SEPARATOR;
    } else {
        line += 'NOT_FOUND!,';
    }
    if (_.get(chinese, key)) {
        line += _.get(chinese, key) + SEPARATOR
    } else {
        line += 'NOT_FOUND!,';
    }
    if (_.get(german, key)) {
        line += _.get(german, key);
    } else {
        line += 'NOT_FOUND!';
    }
    console.log('line:', line);
    lines += line + '\n';
}

fs.writeFileSync('./translations_all.csv', lines, 'UTF-8');