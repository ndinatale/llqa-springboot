var _ = require('../../components/lodash/lodash.js'),
    fs = require('fs'),
    chinese = require('../i18n/lang-cn.json'),
    english = require('../i18n/lang-en.json'),
    lineReader,
    inputFile,
    outputDir;

console.log(process.argv.length);

if (process.argv.length >= 4) {
    inputFile = process.argv[2];
    outputDir = process.argv[3];
    console.log ('inputFile:', inputFile, 'outputDir:', outputDir);
} else {
    console.log('usage node i18nImport.js filename.csv outputdir');
    process.exit(-1);
}

lineReader = require('readline').createInterface({
    input: require('fs').createReadStream(inputFile)
});

lineReader.on('line', function (line) {
    var parts = line.split(',');
    if (!_.has(chinese, parts[0])) {
        if (_.isNumber(parts[0])) {
            _.set(chinese, '\'' + parts[0] + '\'', parts[2]);
        } else {
            _.set(chinese, parts[0], parts[2]);
        }
    }
    if (!_.has(english, parts[0])) {
        if (_.isNumber(parts[0])) {
            _.set(english, '\'' + parts[0] + '\'', parts[1]);
        } else {
            _.set(english, parts[0], parts[1]);
        }
    }

});

lineReader.on('close', function () {
    fs.writeFile(outputDir + '/lang-cn.json', JSON.stringify(chinese, null, 4));
    fs.writeFile(outputDir + '/lang-en.json', JSON.stringify(english, null, 4));
    console.log('done');
});