$system_parameters = {
    unitarchive: 14*24*60*60,
    noticearchive: 90*24*60*60
}

$application_parameters = {
    defaultfloatformat: '%0.3f',
    regmaxprocedures: 3,
    skipcheckschedule: false
}

$version_parameters = {
    appversion: "2.16.0"
}

$localization_override = Hash.new { |h,k| h[k] = {} }

def system_parameter(key, value)
  if $system_parameters[key.to_sym] == nil
    puts "No system parameter '#{key}'"
    exit
  end
  $system_parameters[key.to_sym] = value
end

def application_parameter(key, value)
  if $application_parameters[key.to_sym] == nil
    puts "No application parameter '#{key}'"
    exit
  end
  $application_parameters[key.to_sym] = value
end

def override_localization(lang, key, value)
  $localization_override[lang][key] = value
end