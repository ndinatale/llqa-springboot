set :environment, :development
set :port, 4567

$system_languages = [
    {code: 'en', name: 'English', dformat: 'MM/dd/yyyy', tformat: 'MM/dd/yyyy hh:mm:ss a', dformatr: '%m/%d/%Y', tformatr: '%m/%d/%Y %I:%M:%S %p' },
    {code: 'de', name: 'Deutsch', dformat: 'dd.MM.yyyy', tformat: 'dd.MM.yyyy HH:mm:ss',   dformatr: '%d.%m.%Y', tformatr: '%d.%m.%Y %H:%M:%S'    },
    {code: 'cn', name: '中文',     dformat: 'MM/dd/yyyy', tformat: 'MM/dd/yyyy hh:mm:ss a', dformatr: '%m/%d/%Y', tformatr: '%m/%d/%Y %I:%M:%S %p' },
    {code: 'fr', name: 'Français', dformat: 'dd.MM.yyyy', tformat: 'dd.MM.yyyy HH:mm:ss',   dformatr: '%d.%m.%Y', tformatr: '%d.%m.%Y %H:%M:%S'    },
]

$debugging_api = true

# expiration time in seconds
$expiry_time = 3600

# available customer features
$features = {
    autocode: true
}