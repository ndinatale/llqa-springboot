class CreateUsermanagement < ActiveRecord::Migration
  def up
    Privilege.create_table self,:privileges
    Usergroup.create_table self,:usergroups
    User.create_table self,:users
    Grant.create_table self,:grants
  end

  def down
    drop_table :users
    drop_table :usergroups
    drop_table :grants
    drop_table :privileges
  end
end
