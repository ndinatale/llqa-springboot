class SeedUsermanagement < ActiveRecord::Migration
  def up
    priv = {}
    priv[:login] = Privilege.create(name: 'Login', description: 'may log into the system', code: 'LOGIN')
    priv[:musers] = Privilege.create(name: 'Manage Users', description: 'may manage users below her own level', code: 'MUSERS')
    priv[:musersx] = Privilege.create(name: 'Manage All Users', description: 'may manage all users', code: 'MUSERSX')
    priv[:editvers] = Privilege.create(name: 'Edit Version', description: 'may edit versioned objects', code: 'EDITVERS')
    priv[:showvers] = Privilege.create(name: 'View Version', description: 'may see versioned objects', code: 'SHOWVERS')
    priv[:finalize] = Privilege.create(name: 'Finalize Version', description: 'may finalize current editing version', code: 'FINALIZE')
    priv[:edit] = Privilege.create(name: 'Edit', description: 'may edit trunk objects', code: 'EDIT')
    priv[:modify] = Privilege.create(name: 'Modify', description: 'may modify trunk objects', code: 'MODIFY')
    group_adm = Usergroup.create(name: 'Administrators', level: 5, description: 'Site administrators', grants: %w(login musers editvers showvers finalize edit modify).collect{|c| Grant.create(privilege: priv[c.to_sym])})
    group_mgt = Usergroup.create(name: 'Management', level: 4, description: 'Management', grants: %w(login musers showvers finalize edit modify).collect{|c| Grant.create(privilege: priv[c.to_sym])})
    group_editor = Usergroup.create(name: 'Secretary', level: 2, description: 'Secretary', grants: %w(login showvers edit modify).collect{|c| Grant.create(privilege: priv[c.to_sym])})
    group_user = Usergroup.create(name: 'Users', level: 2, description: 'Standard working user', grants: %w(login modify).collect{|c| Grant.create(privilege: priv[c.to_sym])})
    group_guest = Usergroup.create(name: 'Guests', level: 1, description: 'Standard login-only user', grants: %w(login).collect{|c| Grant.create(privilege: priv[c.to_sym])})
    User.create(username: 'root', passhash: '*', realname: 'Site Administrator', usergroup_id: group_adm.id, grants: [Grant.create(privilege: priv[:musersx])])
    User.create(username: 'admin', passhash: '*', realname: 'Administrator', usergroup_id: group_adm.id)
    User.create(username: 'guest', passhash: '*', realname: 'Guest', usergroup_id: group_guest.id)
    User.create(username: 'mgr', passhash: '*', realname: 'Manager', usergroup_id: group_mgt.id)
    User.create(username: 'edit', passhash: '*', realname: 'Editor', usergroup_id: group_editor.id)
    User.create(username: 'user', passhash: '*', realname: 'John Doe', usergroup_id: group_user.id)
  end

  def down
    Grant.delete_all
    User.delete_all
    Usergroup.delete_all
    Privilege.delete_all
  end
end
