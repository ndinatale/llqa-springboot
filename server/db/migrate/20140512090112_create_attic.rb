class CreateAttic < ActiveRecord::Migration
  def up
    Image.create_table self,:images
    Document.create_table self,:documents
    Binaryfile.create_table self,:binaryfiles
  end

  def down
    drop_table :images
    drop_table :documents
    drop_table :binaryfiles
  end
end
