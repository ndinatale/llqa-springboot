class CreateChecktemplate < ActiveRecord::Migration
  def up
    Tooltype.create_table self,:tooltypes
    Toolunit.create_table self,:toolunits
    Procedure.create_table self,:procedures
    Step.create_table self,:steps
    Measure.create_table self,:measures
    Checktype.create_table self,:checktypes
  end

  def down
    drop_table :checktypes
    drop_table :measures
    drop_table :steps
    drop_table :procedures
    drop_table :toolunits
    drop_table :tooltypes
  end
end
