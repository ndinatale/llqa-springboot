class CreateUnitdata < ActiveRecord::Migration
  def up
    Devicetype.create_table self, :devicetypes
    Model.create_table self, :models
    Unit.create_table self, :units
    Activeprocedure.create_table self, :activeprocedures
    Activechecktype.create_table self, :activechecktypes
  end

  def down
    drop_table self, :activechecktypes
    drop_table self, :activeprocedures
    drop_table self, :units
    drop_table self, :models
    drop_table self, :devicetypes
  end
end
