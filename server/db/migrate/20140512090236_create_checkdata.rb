class CreateCheckdata < ActiveRecord::Migration
  def up
    Check.create_table self, :checks
    Measurement.create_table self, :measurements
  end

  def down
    drop_table :measurements
    drop_table :checks
  end
end
