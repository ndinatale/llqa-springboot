class UpdatePrivileges < ActiveRecord::Migration
  def up
    Grant.delete_all
    User.delete_all
    Usergroup.delete_all
    Privilege.delete_all
    Privilege.setup_privileges
    admingrp = Usergroup.create(name: 'Administrators', level: 5, description: 'Site administrators')
    User.create(username: 'admin', passhash: '*', realname: 'Administrator', usergroup_id: admingrp.id)
    superadm = User.create(username: 'root', passhash: '*', realname: 'Super Administrator', usergroup_id: admingrp.id)
    Grant.add_grants_by_name(admingrp,%w(MNGALL EDTALL CHGCOD FINALZ CRTALL DELALL USRMGO GRTPRO GRPCRT GRTTOG WFLMNG WFLREG MODVRS))
    Grant.add_grants_by_name(superadm,%w(USRMGA GRTPRA))
  end

  def down
    Grant.delete_all
    User.delete_all
    Usergroup.delete_all
    Privilege.delete_all
  end
end
