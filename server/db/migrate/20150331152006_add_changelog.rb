class AddChangelog < ActiveRecord::Migration
  def up
    Datachange.create_table self, :datachanges
  end

  def down
    drop_table :datachanges
  end
end
