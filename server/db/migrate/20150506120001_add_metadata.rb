class AddMetadata < ActiveRecord::Migration
  def up
    User.update_table_01 self, :users
    Measurement.update_table_01 self, :measurements
  end

  def down
  end
end
