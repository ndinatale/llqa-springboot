class UpdateUsermanagement < ActiveRecord::Migration
  def up
    Affiliation.create_table self, :affiliations
    User.update_table_02 self, :users
  end

  def down
    drop_table :affiliations
  end
end
