class AddNotices < ActiveRecord::Migration
  def up
    Notice.create_table self, :notices
    Snippet.create_table self, :snippets
  end

  def down
    drop_table :notices
    drop_table :snippets
  end
end
