class AddEnforce < ActiveRecord::Migration
  def up
    Activeprocedure.update_table_01 self, :activeprocedures
    Step.update_table_01 self, :steps
    Measure.update_table_01 self, :measures
  end
end
