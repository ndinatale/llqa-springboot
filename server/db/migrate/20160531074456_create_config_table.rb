class CreateConfigTable < ActiveRecord::Migration
  def up
    Configtable.create_table self, :configtables
    Configentry.create_table self, :configentries
  end

  def down
    drop_table :configtables
    drop_table :configentries
  end
end
