class CleanupTableBinaryfiles < ActiveRecord::Migration
  def change

    #
    # Verarbeite tables IMAGES und DOCUMENTS
    #
    binaryfiles = Binaryfile.where.not(id: Image.pluck(:fullimage_id, :preview_id, :thumbnail_id).flatten).merge(Binaryfile.where.not(id: Document.pluck(:binaryfile_id).flatten))

    if binaryfiles
      puts "-----------------------------------------"
      puts "Following binaryfiles records will be deleted:"
      puts binaryfiles.inspect
      puts "\n"
    end

    # Alle Einträge in der Binaryfiles Tabelle löschen und die Bild/Documentdateien
    # aus dem /uploads Verzeichnis löschen
    binaryfiles.each do |bf|
      if File.exist?("../uploads/#{bf.filename}")
        File.delete("../uploads/#{bf.filename}")
        puts "File '{bf.filename}' has been deleted"
      end
    end
    binaryfiles.destroy_all
    puts "-----------------------------------------"
  end
end
