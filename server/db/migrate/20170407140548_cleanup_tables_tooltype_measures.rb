class CleanupTablesTooltypeMeasures < ActiveRecord::Migration
  def change

    # Messungen suchen, welche ein Tooltype benötigen und welche besagter Tooltype nicht mehr
    # vorhanden ist
    measures = Measure.where.not(tooltype_id: nil)
    measures = measures.where.not(tooltype_id: Tooltype.pluck(:id).flatten)

    if !measures.empty?

      # Daten für den Dummy-Tooltype aufsetzen
      data = {
          "code" => "DMMY",
          "title" => {
              "en" => "DUMMY",
              "de" => "DUMMY",
              "cn" => "DUMMY",
              "fr" => "DUMMY"
          },
          "description" => {
              "en" => "",
              "de" => "",
              "cn" => "",
              "fr" => ""
          },
          "metadata" => {
          },
          "deleted" => true,
          "created_at" => Time.now.to_s,
          "updated_at" => Time.now.to_s,
          "created_by" => "??",
          "updated_by" => "??"
      }

      # Dummy-Tooltype erstellen
      tooltype_dummy = Tooltype.new data
      tooltype_dummy.save!
      puts "---------------------------------------------"
      puts "Dummy-Tooltype created with id #{tooltype_dummy.id}"

      # Alle Messungen ohne vorhandenen Tooltype dem Dummy-Tooltype anhängen
      measure_count = 0
      measures.each do |m|
        m.tooltype_id = tooltype_dummy.id
        m.save!
        puts "Measure with id #{m.id} has been reattached to the Dummy-Tooltype!"
        measure_count += 1
      end
      puts "#{measure_count} measures have been attached to the Dummy-Tooltype!"
      puts "---------------------------------------------"

    end
  end
end
