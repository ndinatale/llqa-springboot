class CleanupTablesMeasuresMeasurements < ActiveRecord::Migration
  def change

    # Measurements suchen, welche keine Measures mehr zugewiesen sind
    measurements = Measurement.where.not(measure_id: Measure.pluck(:id).flatten)

    if !measurements.empty?
      # Alle Measurements ohne vorhandener Measure löschen
      puts "--------------------------------------------"
      puts "Measurements with following id will be deleted:"
      measurements.each do |m|
        puts m.id
      end
      measurements.destroy_all
      puts "--------------------------------------------"
    end
  end
end
