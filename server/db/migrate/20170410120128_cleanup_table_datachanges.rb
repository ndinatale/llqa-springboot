class CleanupTableDatachanges < ActiveRecord::Migration
  def change
    # Datachanges suchen, welche keine User mehr zugewiesen sind
    datachanges = Datachange.where.not(user: User.pluck(:id).flatten)


    if !datachanges.empty?
      # Alle Datachanges ohne vorhandenem User löschen
      datachanges.destroy_all
    end
  end
end

