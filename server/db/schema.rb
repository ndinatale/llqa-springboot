# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170410120128) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "adminpack"

  create_table "activechecktypes", force: true do |t|
    t.integer  "model_id",     limit: 8
    t.integer  "checktype_id", limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",             default: "system", null: false
    t.string   "updated_by",             default: "system", null: false
  end

  add_index "activechecktypes", ["checktype_id"], name: "fki_fk_activechecktype_checktype_id", using: :btree
  add_index "activechecktypes", ["model_id"], name: "fki_fk_activechecktype_model_id", using: :btree

  create_table "activeprocedures", force: true do |t|
    t.integer  "model_id",     limit: 8
    t.integer  "procedure_id", limit: 8
    t.text     "flowcontrol"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",             default: "system", null: false
    t.string   "updated_by",             default: "system", null: false
    t.integer  "seqnum",       limit: 8,                    null: false
    t.integer  "enforce",      limit: 8,                    null: false
  end

  add_index "activeprocedures", ["model_id"], name: "fki_fk_activeprocedures_model_id", using: :btree
  add_index "activeprocedures", ["procedure_id"], name: "fki_fk_activeprocedures_procedure_id", using: :btree

  create_table "affiliations", force: true do |t|
    t.integer  "user_id",      limit: 8
    t.integer  "usergroup_id", limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",             default: "system", null: false
    t.string   "updated_by",             default: "system", null: false
  end

  add_index "affiliations", ["user_id"], name: "fki_fk_affiliations_user_id", using: :btree
  add_index "affiliations", ["usergroup_id"], name: "fki_fk_affiliations_usergroup_id", using: :btree

  create_table "binaryfiles", force: true do |t|
    t.string   "filename"
    t.string   "original_filename"
    t.string   "hexhash"
    t.string   "mimetype"
    t.integer  "size",              limit: 8
    t.text     "metadata"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",                  default: "system", null: false
    t.string   "updated_by",                  default: "system", null: false
  end

  create_table "checks", force: true do |t|
    t.integer  "unit_id",           limit: 8,                    null: false
    t.integer  "model_id",          limit: 8,                    null: false
    t.integer  "checktype_id",      limit: 8,                    null: false
    t.text     "comment"
    t.date     "scheduled"
    t.date     "dueby"
    t.date     "started"
    t.date     "finished"
    t.integer  "status",            limit: 8
    t.text     "metadata"
    t.text     "checkdata"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",                  default: "system", null: false
    t.string   "updated_by",                  default: "system", null: false
    t.float    "progress_finished",           default: 0.0,      null: false
    t.float    "progress_error",              default: 0.0,      null: false
    t.text     "assign_data"
  end

  add_index "checks", ["checktype_id"], name: "fki_fk_checks_checktype_id", using: :btree
  add_index "checks", ["model_id"], name: "fki_fk_checks_model_id", using: :btree
  add_index "checks", ["unit_id"], name: "fki_fk_checks_unit_id", using: :btree

  create_table "checktypes", force: true do |t|
    t.string   "code",                           null: false
    t.text     "title",                          null: false
    t.text     "description"
    t.text     "metadata"
    t.boolean  "deleted",     default: false,    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",  default: "system", null: false
    t.string   "updated_by",  default: "system", null: false
  end

  create_table "configentries", force: true do |t|
    t.integer  "code_id",        limit: 8
    t.integer  "configtable_id", limit: 8
    t.boolean  "active",                   default: false
    t.boolean  "deleted",                  default: false
    t.string   "col1"
    t.string   "col2"
    t.string   "col3"
    t.string   "col4"
    t.string   "col5"
    t.string   "col6"
    t.text     "blocked_by"
    t.boolean  "blocked",                  default: false
    t.text     "timeline"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",               default: "system", null: false
    t.string   "updated_by",               default: "system", null: false
  end

  add_index "configentries", ["configtable_id"], name: "fki_fk_configentries_configtable_id", using: :btree

  create_table "configtables", force: true do |t|
    t.string   "title"
    t.string   "parent"
    t.integer  "parentid",   limit: 8
    t.string   "parentcode"
    t.string   "colheader1"
    t.string   "colheader2"
    t.string   "colheader3"
    t.string   "colheader4"
    t.string   "colheader5"
    t.string   "colheader6"
    t.text     "timeline"
    t.boolean  "editable",             default: true,     null: false
    t.boolean  "deleted",              default: false,    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",           default: "system", null: false
    t.string   "updated_by",           default: "system", null: false
  end

  create_table "datachanges", force: true do |t|
    t.integer "source_id"
    t.string  "source_type"
    t.string  "subobject",     null: false
    t.string  "subobjtype",    null: false
    t.string  "changetype",    null: false
    t.text    "changedetails"
    t.integer "user"
    t.integer "timestamp"
    t.boolean "post_finalize"
    t.boolean "dirty"
  end

  create_table "devicetypes", force: true do |t|
    t.string   "code",                           null: false
    t.text     "title",                          null: false
    t.text     "description"
    t.text     "metadata"
    t.boolean  "deleted",     default: false,    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",  default: "system", null: false
    t.string   "updated_by",  default: "system", null: false
  end

  create_table "documents", force: true do |t|
    t.integer  "owner_id",      limit: 8
    t.string   "owner_type"
    t.integer  "binaryfile_id", limit: 8
    t.string   "caption"
    t.integer  "doctype",       limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",              default: "system", null: false
    t.string   "updated_by",              default: "system", null: false
    t.integer  "seqnum",        limit: 8,                    null: false
  end

  add_index "documents", ["binaryfile_id"], name: "fki_fk_documents_binaryfile_id", using: :btree

  create_table "grants", force: true do |t|
    t.integer  "grantee_id",   limit: 8
    t.string   "grantee_type"
    t.integer  "privilege_id", limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",             default: "system", null: false
    t.string   "updated_by",             default: "system", null: false
  end

  add_index "grants", ["privilege_id"], name: "fki_fk_grants_privilege_id", using: :btree

  create_table "images", force: true do |t|
    t.integer  "owner_id",     limit: 8
    t.string   "owner_type"
    t.integer  "fullimage_id", limit: 8
    t.integer  "preview_id",   limit: 8
    t.integer  "thumbnail_id", limit: 8
    t.string   "caption"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",             default: "system", null: false
    t.string   "updated_by",             default: "system", null: false
    t.integer  "seqnum",       limit: 8,                    null: false
  end

  add_index "images", ["fullimage_id"], name: "fki_fk_images_binaryfiles_id", using: :btree
  add_index "images", ["preview_id"], name: "fki_fk_images_preview_binaryfiles_id", using: :btree
  add_index "images", ["thumbnail_id"], name: "fki_fk_images_thumb_binaryfiles_id", using: :btree

  create_table "measurements", force: true do |t|
    t.integer  "check_id",    limit: 8,                    null: false
    t.integer  "measure_id",  limit: 8,                    null: false
    t.integer  "toolunit_id", limit: 8
    t.text     "rawvalues"
    t.string   "value"
    t.string   "comment"
    t.integer  "status",      limit: 8,                    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",            default: "system", null: false
    t.string   "updated_by",            default: "system", null: false
    t.text     "metadata"
    t.integer  "savedon",     limit: 8
    t.integer  "savedby",     limit: 8
  end

  add_index "measurements", ["check_id"], name: "fki_fk_measurements_check_id", using: :btree
  add_index "measurements", ["toolunit_id"], name: "fki_fk_measurements_toolunit_id", using: :btree

  create_table "measures", force: true do |t|
    t.integer  "step_id",     limit: 8,                    null: false
    t.integer  "tooltype_id", limit: 8
    t.string   "code",                                     null: false
    t.text     "title",                                    null: false
    t.text     "description"
    t.integer  "measuretype", limit: 8,                    null: false
    t.text     "calculation",                              null: false
    t.text     "metadata"
    t.text     "flowcontrol"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",            default: "system", null: false
    t.string   "updated_by",            default: "system", null: false
    t.integer  "seqnum",      limit: 8,                    null: false
    t.integer  "enforce",     limit: 8,                    null: false
  end

  add_index "measures", ["step_id"], name: "fki_fk_measures_step_id", using: :btree

  create_table "models", force: true do |t|
    t.integer  "devicetype_id", limit: 8,                    null: false
    t.string   "code",                                       null: false
    t.text     "title",                                      null: false
    t.text     "description"
    t.text     "metadata"
    t.integer  "realid",        limit: 8,                    null: false
    t.integer  "version",       limit: 8
    t.boolean  "disabled",                default: false,    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",              default: "system", null: false
    t.string   "updated_by",              default: "system", null: false
  end

  add_index "models", ["devicetype_id"], name: "fki_fk_models_devicetype_id", using: :btree

  create_table "notices", force: true do |t|
    t.string   "text"
    t.integer  "category_id", limit: 8,                    null: false
    t.text     "path"
    t.string   "artno"
    t.string   "artdesc"
    t.integer  "timeloss",    limit: 8
    t.integer  "status",      limit: 8
    t.text     "timeline"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",            default: "system", null: false
    t.string   "updated_by",            default: "system", null: false
  end

  create_table "privileges", force: true do |t|
    t.string   "name",                           null: false
    t.text     "description"
    t.string   "code",                           null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",  default: "system", null: false
    t.string   "updated_by",  default: "system", null: false
  end

  create_table "procedures", force: true do |t|
    t.string   "code",                                     null: false
    t.text     "title",                                    null: false
    t.text     "description"
    t.text     "metadata"
    t.text     "flowcontrol"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",            default: "system", null: false
    t.string   "updated_by",            default: "system", null: false
    t.integer  "realid",      limit: 8,                    null: false
    t.integer  "version",     limit: 8
    t.boolean  "disabled",              default: false,    null: false
    t.text     "tag_1"
    t.text     "tag_2"
    t.text     "tag_3"
    t.text     "tag_4"
    t.text     "tag_5"
    t.text     "tag_6"
  end

  create_table "settings", force: true do |t|
    t.string   "key"
    t.string   "value"
    t.integer  "seqnum",     limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",           default: "system", null: false
    t.string   "updated_by",           default: "system", null: false
    t.string   "sub_values"
  end

  create_table "snippets", force: true do |t|
    t.string   "category"
    t.string   "text"
    t.integer  "status",     limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",           default: "system", null: false
    t.string   "updated_by",           default: "system", null: false
    t.integer  "seqnum",     limit: 8,                    null: false
  end

  create_table "steps", force: true do |t|
    t.integer  "procedure_id", limit: 8,                    null: false
    t.string   "code",                                      null: false
    t.text     "title",                                     null: false
    t.text     "description"
    t.text     "metadata"
    t.text     "flowcontrol"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",             default: "system", null: false
    t.string   "updated_by",             default: "system", null: false
    t.integer  "seqnum",       limit: 8,                    null: false
    t.integer  "enforce",      limit: 8,                    null: false
    t.integer  "steptype",     limit: 8
  end

  add_index "steps", ["procedure_id"], name: "fki_fk_steps_procedure_id", using: :btree

  create_table "tooltypes", force: true do |t|
    t.string   "code",                           null: false
    t.text     "title",                          null: false
    t.text     "description"
    t.text     "metadata"
    t.boolean  "deleted",     default: false,    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",  default: "system", null: false
    t.string   "updated_by",  default: "system", null: false
  end

  create_table "toolunits", force: true do |t|
    t.integer  "tooltype_id", limit: 8,                    null: false
    t.string   "code",                                     null: false
    t.text     "comment"
    t.text     "metadata"
    t.boolean  "deleted",               default: false,    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",            default: "system", null: false
    t.string   "updated_by",            default: "system", null: false
  end

  add_index "toolunits", ["tooltype_id"], name: "fki_fk_toolunits_tooltype_id", using: :btree

  create_table "units", force: true do |t|
    t.integer  "model_id",     limit: 8,                    null: false
    t.string   "code",                                      null: false
    t.string   "customer"
    t.text     "comment"
    t.date     "commissioned"
    t.date     "finished"
    t.date     "delivered"
    t.date     "approved"
    t.integer  "status",       limit: 8
    t.text     "metadata"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",             default: "system", null: false
    t.string   "updated_by",             default: "system", null: false
  end

  add_index "units", ["model_id"], name: "fki_fk_units_model_id", using: :btree

  create_table "usergroups", force: true do |t|
    t.string   "name",                                     null: false
    t.integer  "level",       limit: 8
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",            default: "system", null: false
    t.string   "updated_by",            default: "system", null: false
    t.boolean  "deleted",               default: false,    null: false
  end

  create_table "users", force: true do |t|
    t.string   "username",                                   null: false
    t.string   "passhash",                                   null: false
    t.text     "realname",                                   null: false
    t.text     "comment"
    t.text     "userinfo"
    t.integer  "status",        limit: 8,                    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "created_by",              default: "system", null: false
    t.string   "updated_by",              default: "system", null: false
    t.text     "metadata"
    t.text     "dashboardinfo"
  end

end
