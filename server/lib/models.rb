require_relative 'models_validators'
require_relative 'models_base'
require_relative 'models_versioning'
require_relative 'models_ordering'
require_relative 'models_searching'

require_relative '../lib/utils'

Dir.glob('models/*.rb').sort.each do |model|
  next if model =~ /.+_.+_.+/
  mname = "#{$1.capitalize}" if model =~ /_(.*)\.rb/
  $logger.info "Loading Modeldefinition #{mname}"
  require_relative "../#{model}"
end

