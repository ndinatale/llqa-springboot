require_relative '../lib/utils'

module ModelBase
  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods

    def augment_table_usertag(t)
      t.timestamps
      t.string :created_by, null: false, default: 'system'
      t.string :updated_by, null: false, default: 'system'
    end

    def stamp_realid(hash)
    end

    def stamp_seqnum(hash)
    end

    def augment_class_usertag
      before_create :stamp_user_on_create
      before_update :stamp_user_on_update
    end

    def import_from_hash(hash,changecode = true,complete = false)
      obj = self.new hash
      obj.has_been_created(hash) if obj.respond_to?(:has_been_created)
      obj.save!
    end

    def merge_data(data)
      obj, isnew, changes = {}, true, nil
      if data['id']
        obj, isnew, changes = find_by(id:data['id']), false, {}
        if !obj
          # object not found
          return assemble_error('NOTFOUND', 'TEXT', {class: self.name, id: data['id']}, []).rest_fail, false, nil
        end
      end
      data.each do |k,v|
        k = k.to_s
        next if %w(id realid version seqnum disabled created_at updated_at created_by updated_by).index(k)
        next if !columns_hash[k]

        # LLQA-191 bugfix, preallocations were lost if other field was updated because metadata would be reset
        next if obj.instance_of?(Model) && k == "metadata"

        # Careful with changes in this part, every object will pass here on merge_data()
        # Please leave the console output below for debugging.
        #puts "Changing attribute k: #{k.inspect}"
        #puts "- was before (obj[k]): #{obj[k].inspect}"
        #puts "- to new (v): #{v.inspect}"

        changes[k] = [obj[k], v] if changes && obj[k].inspect != v.inspect
        obj[k] = v
      end
      if isnew
        stamp_realid(obj)
        stamp_seqnum(obj)
        changes = obj.to_json
        obj = new(obj)
      end
      return assemble_validation_error(obj.errors.messages).rest_fail,false,nil if !obj.valid?
      yield(obj,isnew) if block_given?

      # uncommenting this again, remove the mysterious "(TR)" from code (for example from units)
      if obj['code'] && !obj['code'].index('(TR)')
        obj['code'] = obj['code'].gsub('(',')')
      end
      return assemble_validation_error(obj.errors.messages).rest_fail,false,nil if !obj.valid?
      obj.save!

      if isnew
        # save code after the id has been generated
        if obj['code'] && $features['autocode']
          obj.code = obj.id.to_s if obj.instance_of?(Procedure) || obj.instance_of?(Measure) || obj.instance_of?(Step)
        end

        if obj.instance_of?(Configentry)
          obj.code_id = Configentry.get_new_entry_code(data['configtable_id'])
        end

        # for a new check: copy configtable if feature is on
        if obj.instance_of?(Check) && $features['configtable']
          table_id = data['configtable']

          # take configtable from model if id <= 0 or not set
          if table_id == nil || table_id <= 0
            table_id = Model.get_table_id(obj.model.code)
          end

          # now we should have a valid configtable id of either the check or model, copy if it is valid
          if table_id != nil && table_id > 0
            # now that table_id is set, clone from table specified
            hash = Configtable.export_to_hash(table_id, false)[3]
            newtable = Configtable.import_from_hash(hash)

            # set some additional attributes
            newtable.parent = 'check'
            newtable.parentid = obj.id
            newtable.parentcode = obj.unit.code
            newtable.save
          end
        end
        obj.save
        obj.has_been_created(changes) if obj.respond_to?(:has_been_created)
      else
        obj.has_been_updated(changes) if obj.respond_to?(:has_been_updated)
      end

      # check if config-entry blockers need to be set
      if $features['configtable'] && obj.instance_of?(Procedure) || obj.instance_of?(Measure) || obj.instance_of?(Step)
        type = 'p' if obj.instance_of?(Procedure)
        type = 'm' if obj.instance_of?(Measure)
        type = 's' if obj.instance_of?(Step)
        obj.flowcontrol.each do |fc|
          if fc['type'] == 'CO'
            table_id = fc['code'].split('.').first.to_i
            entry_id = fc['code'].split('.').last.to_i
            # split table_id, entry_id
            entry = Configentry.find_by_table(table_id, entry_id)
            entry.add_blocker(obj.id, type)
          end
        end
      end

      return obj.serializable_hash.rest_success,true,obj
    end

    def get_list(limit = nil, page = nil)
      coll = all
      coll = (block_given?) ? (yield coll) : coll
      rowcnt = coll.count
      pages = (limit && limit > 0) ? (rowcnt.to_f/limit).ceil : 1
      coll = coll.limit(limit) if limit && limit > 0
      coll = coll.offset((page-1)*limit) if limit && limit > 0 && page && page > 1
      ret = { rows: coll.all.to_a, rowcnt: rowcnt, page: page || 1, pages: pages, limit: limit || 0 }
      def ret.serialize(include = nil)
        self[:rows].collect! do |row|
          hash = include ? (row.serializable_hash(include: include.incllist)) : row.serializable_hash
          (hash['code'] && hash['code'].end_with?('(TR)')) ? nil : ((block_given?) ? yield(hash,row) : hash)

        end
        self[:rows].compact!
        return self.rest_success,true,self
      end
      return ret
    end

    # new list method without COUNT(), so it can be called with multiple attributes
    def get_list2()
      coll = all
      coll = (block_given?) ? (yield coll) : coll
      ret = { rows: coll.all.to_a }
      def ret.serialize(include = nil)
        self[:rows].collect! do |row|
          hash = include ? (row.serializable_hash(include: include.incllist)) : row.serializable_hash
          (hash['code'] && hash['code'].end_with?('(TR)')) ? nil : ((block_given?) ? yield(hash,row) : hash)
        end
        self[:rows].compact!
        return self.rest_success,true,self
      end
      ret
    end

    def get_single(id = nil, forexport = false)
      coll = all
      coll = (block_given?) ? (yield coll) : coll
      if coll == nil || coll.size < 1
        return 404 if forexport
        return assemble_error('NOTFOUND', 'TEXT', {class: self.name, id: id || '?'}, []).rest_fail,false,nil
      end
      item = nil
      if id
        begin
          item = coll.find(id)
        rescue ActiveRecord::RecordNotFound
          return 404 if forexport
          return assemble_error('NOTFOUND', 'TEXT', {class: self.name, id: id}, []).rest_fail,false,nil
        end
      else
        item = coll.take
      end
      item = [item, forexport, self.name]
      def item.serialize(include = nil)
        hash = include ? (self[0].serializable_hash(include: include.incllist)) : self[0].serializable_hash
        hash = (block_given?) ? yield(hash,self[0]) : hash
        if self[1]
          if self[0].instance_of?(Configtable)
            filename = ('%s_%s%s.json' % [self[2].downcase, self[0].id, '_data'])
          else
            filename = ('%s_%s%s%s.json' % [self[2].downcase, self[0].code, (self[0].version ? ('_%03d' % [self[0].version]) : ''), hash['blobs'] ? '_full' : '_data'])
          end

          shash = JSON.pretty_generate(hash)
          data = build_import_hash(shash,self[2])
          return [200, { 'Content-Disposition' => "attachment; filename=#{filename}", 'Content-Type' => 'application/octet-stream', 'Content-Transfer-Encoding' => 'binary' }, data ]
        end
        return hash.rest_success,true,self[0],hash
      end
      return item
    end

  end

  protected

  def stamp_user_on_create
    self.updated_by = self.created_by = get_current_user
  end

  def stamp_user_on_update
    self.updated_by = get_current_user
  end

  public

  def complete_languageblock(copydefault, *fields)
    alllang = $system_languages.map { |a| a[:code] }
    fields.each do |fsym|
      field = self[fsym]
      field = field.delete_if { |k,_| !alllang.include?(k) }
      alllang.each do |lcd|
        field[lcd] = (copydefault && field.include?(alllang[0]) ? field[alllang[0]] : '') if !field.include? lcd
      end
    end
  end

  # Recurse finalization (started in versioned object!)

  def recurse_finalize
    nil
  end

  def finalize_trunk(father,setid)
    copy = serializable_hash
    %w(id version created_by created_at updated_by updated_at).each { |k| copy.delete k }
    copy[setid] = father.id
    trunk = self.class.create!(copy)
    self.recurse_finalize.each { |e| self.send(e[0]).each { |o| o.finalize_trunk(trunk,e[1]) } } if recurse_finalize
    trunk
  end

  def destroy_logged
    self.destroy
    self.has_been_deleted if self.respond_to?(:has_been_deleted)
  end

end