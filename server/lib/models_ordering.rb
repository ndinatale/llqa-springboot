require_relative '../lib/utils'

module Ordering
  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods

    def augment_table_ordering(t)
      t.integer :seqnum, null:false, default:-1
    end

    def stamp_seqnum(hash)
      hash['seqnum'] = (maximum(:seqnum) || 0)+1
    end

  end

  def reorder(neworder)
    set = reorder_parentset
    return if !set || set.size < 2 || neworder.size != set.size
    seqnums = set.map { |i| i.seqnum }
    set[0].has_been_updated({'seqnum' => neworder.to_json}) if set[0].respond_to?(:has_been_updated)
    [(1..set.size).to_a,neworder].transpose.each do |data|
      ordero,ordern = data
      next if ordero == ordern
      set[ordern-1].seqnum = seqnums[ordero-1]
      set[ordern-1].save!
    end
  end

  def reorder_parentset
    nil
  end
end
