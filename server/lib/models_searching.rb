require_relative '../lib/utils'

module Searching
  def self.included(base)
    base.extend(ClassMethods)

    @searchable ||= []
    @searchable.push(base)
  end

  def self.searchable_classes
    ret = []
    @searchable.each do |sc|
      ret << {
          model: sc.name,
          fields: sc.searchable_fields.keys
      }
    end
    ret.delete_if { |a| ['Configentry', 'Configtable'].include? a[:model] } if !$features['configtable']
    return ret
  end

  module ClassMethods

    def get_entity_array(fields, limitation = :none)
      self.order(updated_at: :desc).select(fields).all
    end

    # limitation: :none, :finalized, :latest_version, :edit_version
    # include: :all, :most_current
    def search_model(regexcoll, fields, languages, limit, limitation = :none, include = :all)
      is_versioned = (include == :most_current && (self.instance_methods.index(:effective_version)))
      return [] if(!fields || fields.size == 0 || !languages || languages.size == 0 || limit < 1)
      found = []
      enc_realids = {}
      nec_fields = ['id'] + fields
      self.get_entity_array(nec_fields, limitation).each do |entry|
        next if is_versioned && enc_realids[entry.effective_realid]
        fields.each do |field|
          res = regexcoll.collect { |regex| entry.search_field(regex, field, languages) }.compact
          if res.size == regexcoll.size && res.size > 0
            dbobj = self.find(entry.id)
            object = self.search_result_extend ? dbobj.serializable_hash(include: self.search_result_extend.incllist) : dbobj.serializable_hash

            # LLQA-87, global search: don't show deleted elements
            if !entry.has_attribute?(:deleted) or !entry.deleted
              found << { type: self.name, field: field, matches: res, object: object, updated_at: dbobj.updated_at }
            end
            enc_realids[entry.effective_realid] = 1 if is_versioned
            break
          end
        end
        break if found.size >= limit
      end
      found.sort { |a,b| b[:updated_at] <=> a[:updated_at] }
    end

    def searchable_fields
      { }
    end
    def search_result_extend
      nil
    end

  end

  def search_field_result(res, match, field)
      return nil if !res
      prematch = match.pre_match
      prematch = "..." + prematch[-47..-1] if prematch.size > 50
      postmatch = match.post_match
      postmatch = postmatch[1..47] + "..." if postmatch.size > 50
      match = match.to_s
      return { prematch: prematch, postmatch: postmatch, match: match }
  end

  def search_field(regex, field, languages)
    data = self[field]
    type = self.class.searchable_fields[field.to_sym]
    return nil if !type || !data || data == ''
    return search_field_result(data =~ regex, $~, field) if type == :text
    return search_field_result(data.gsub(/\<[^\>]+\>/,'') =~ regex, $~, field) if type == :html
    # LLQA-174: if choicelist, take list (if exists), convert to_s and check if text is in there
    (return data['t17_list'] ? search_field_result(data['t17_list'].to_s =~ regex, $~, field) : nil) if type == :choicelist
    return nil if !data.is_a?(Hash)
    languages.each do |lang|
      next if !data[lang]
      return search_field_result(true, $~, field) if(type == :localized_text && data[lang] =~ regex)
      return search_field_result(true, $~, field) if(type == :localized_html && data[lang].gsub(/\<[^\>]+\>/,'') =~ regex)
    end
    nil
  end

  def self.perform_search(data)
    max = data['max_results'].to_i
    searchstrings = data['string'].split('+')
    regexcoll = []
    searchstrings.each do |searchstring|
      next if searchstring.size < 3
      casesens = false
      searchstring,casesens = searchstring[1..-1],true if searchstring.start_with?('!')
      searchstring = searchstring.gsub(/([\\\<\>\$\.\-\[\]\{\}\(\)])/,'\\\\\1')
      # Bugfix LLQA-180 escaping Fragezeichen, sodass danach gesucht werden kann
      searchstring = searchstring.gsub(/\?/,'\\?')
      searchstring = searchstring.gsub(/\A\|/, '\A').gsub(/\|\z/, '\z').gsub(/(\*\z|\A\*)/,'')
      searchstring = searchstring.gsub('*','.{0,10}')
      regexcoll << Regexp.new(searchstring, (casesens ? 0 : Regexp::IGNORECASE) | Regexp::MULTILINE)
    end
    res = []
    # limitation: :none, :latest_version, :edit_version
    # include: :all, :most_current
    limitation, include = :none, :all
    case data['scope']
      when 'ALLALL'; nil
      when 'ALLRECENT'; include = :most_current
      when 'FINALL'; limitation = :finalized
      when 'FINRECENT'; limitation,include = :finalized,:most_current
      when 'LATESTV'; limitation = :latest_version
      when 'EDITV'; limitation = :edit_version
    end
    data['search_in'].each do |typedata|
      break if res.size >= max
      type,fields,langs = typedata['type'],typedata['fields'],data['languages']
      model = Object.const_get(type)
      model.search_model(regexcoll, fields, langs, max-res.size, limitation, include).each do |found|
        res << found
      end
    end
    res
  end
end


