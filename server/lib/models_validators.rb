require_relative '../lib/utils'

class CodevalidValidator < ActiveModel::EachValidator
  def initialize(options)
    super(options)
    @klass = options[:class]
  end

  def validate_each(record, attribute, value)
    if (value == nil || !(value.is_a? String) || value.size < 2)
      record.errors[attribute] << ['SHORT', @klass.name, { }]
      return
    end
    if (value !~ /\A[a-zA-Z\d_\-\.,]+(|\((TR|\d+)\))\z/)
      record.errors[attribute] << ['INVALID', @klass.name, { }]
      return
    end
    res = @klass.all.where(attribute => value).where.not(id: record.id)
    options[:scope].each { |sc| res = res.where(sc => record[sc]) } if options[:scope].is_a? Array
    res = res.where.not(realid: record.realid) if options[:scope] == 'realid'
    record.errors[attribute] << ['UNIQUE', @klass.name, { }] if res.count > 0
  end
end

class LanguageValidator < ActiveModel::EachValidator
  def initialize(options)
    super(options)
    @klass = options[:class]
  end

  def validate_each(record, attribute, value)
    alllang = $system_languages.map { |a| a[:code] }
    if (!(value.is_a? Hash) || !((value.keys & alllang).sort.eql? alllang.sort))
      record.errors[attribute] << ['INVALID', @klass.name, { }]
      return
    end
    return if options[:complete] == :none
    alllang.each do |lcode|
      if !value.include?(lcode) || value[lcode] == nil || value[lcode].size < 1
        record.errors[attribute] << ['INCOMPLETE', @klass.name, { lcode: lcode }]
      else
        record.errors[attribute] << ['SHORT', @klass.name, { lcode: lcode, minlength: options[:minlength] }] if options[:minlength] && value[lcode].size < options[:minlength]
      end
      break if options[:complete] == :first
    end
  end
end

class ReferenceValidator < ActiveModel::EachValidator
  def initialize(options)
    super(options)
    @klass = options[:class]
  end

  def validate_each(record, attribute, value)
    if !value && options[:notnull]
      record.errors[attribute] << ['NULL', @klass.name, { }]
      return
    end
    record.errors[attribute] << ['NOREF', @klass.name, { refid: value, refclass: options[:refclass] }] if Object.const_get(options[:refclass]).where(id: value).count != 1
  end
end