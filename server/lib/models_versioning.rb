require_relative '../lib/utils'

module Versioning
  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods

    def augment_table_version(t)
      t.integer :realid, null: false
      t.integer :version
      t.boolean :disabled, null: false, default: false
    end

    def all_versions(realid)
      where(realid: realid, disabled: false).select(:version,:id,:updated_by,:updated_at).collect do |o|
        user = User.find_by(id: o.updated_by)
        meta = {
            date: o.updated_at.to_i*1000,
            username: user ? user.username : 'Anonymous',
            realname: user ? user.realname : '',
            userid: user ? user.id : -1
        }
        [o.version, o.id, meta ]
      end
    end

    def latest_version(realid)
      where(realid: realid, disabled: false).maximum(:version)
    end

    def is_disabled?(realid)
      where(realid: realid, disabled: true).count > 0
    end

    def next_realid
      (maximum(:realid) || 0)+1
    end

    def trunk_id(realid)
      res = where(realid: realid, version: nil).select(:id)
      res.size != 1 ? -1 : res[0].id
    end

    def version_id(realid, version)
      res = where(realid: realid, version: version).select(:id)
      res.size != 1 ? -1 : res[0].id
    end

    def trunk_object(realid)
      where(realid: realid, version: nil)[0]
    end

    def version_object(realid, version)
      where(realid: realid, version: version)[0]
    end

    def latest_version_object(realid)
      version_object(realid, latest_version(realid))
    end

    #

    def find_current(realid)
      where(realid: realid).where.not(version: nil).order(version: :desc).limit(1)
    end

    def find_trunk(realid)
      where(realid: realid, version: nil)
    end

    def find_version(realid, version)
      where(realid: realid, version: version)
    end

    #

    def stamp_realid(hash)
      hash[:realid] = next_realid
    end

    #

    def all_version_objects(realid = nil)
      (realid ? where(realid: realid).where.not(version: nil) : where.not(version: nil)).order(version: :asc)
    end

    def trunk_ids
      where(version: nil, disabled: false).select(:realid, :id).collect { |o| o.id}.compact
    end

    def trunk_objects
      where(version: nil, disabled: false)
    end

    def latest_version_ids
      where.not(version: nil).select(:realid).distinct.collect { |o| version_object(o.realid, latest_version(o.realid)) }.compact.collect { |o| is_disabled?(o.realid) ? nil : o.id }.compact
    end

    def latest_version_objects
      where(id: latest_version_ids)
    end

    def trunk_and_disabled_object_ids
      ids = trunk_ids
      ids += where.not(version: nil).select(:realid).distinct.collect { |o| version_object(o.realid, latest_version(o.realid)) }.compact.collect { |o| is_disabled?(o.realid) ? o.id : nil }.compact
      ids
    end

    def trunk_and_disabled_objects
      where(id: trunk_and_disabled_object_ids)
    end

  end

  # Mark element and depending elements (recursively) with version, then create copies of them in the trunk
  def finalize_trunk(father = nil,setid = nil)
    copy = serializable_hash
    %w(id version created_by created_at updated_by updated_at).each { |k| copy.delete k }
    if !self.version
      version = (self.class.latest_version(self.realid) || 0)+1
      self.version = version
      has_been_updated({'version' => [nil, version]}) if !self.disabled
    end
    if father && setid
      copy[setid] = father.id
      self.disabled = father.disabled
    end
    save!
    return self if self.disabled
    trunk = self.class.create!(copy)
    self.recurse_finalize.each { |e| self.send(e[0]).each { |o| o.finalize_trunk(trunk,e[1]) } } if recurse_finalize
    self.update_after_finalize
    trunk
  end

  def reset_trunk
    latest = self.class.version_object(self.realid, self.class.latest_version(self.realid))
    self.destroy
    latest.finalize_trunk
  end

  def update_after_finalize
  end

  def clone_version
    copy = serializable_hash
    %w(id version created_by created_at updated_by updated_at).each { |k| copy.delete k }
    copy['version'] = (self.class.latest_version(self.realid) || 0)+1
    clone = self.class.create!(copy)
    self.recurse_finalize.each { |e| self.send(e[0]).each { |o| o.finalize_trunk(clone,e[1]) } } if recurse_finalize
    clone.update_after_finalize
    clone
  end

  def destroy_vsafe
    if self.class.latest_version(self.realid)
      self.disabled = true
      has_been_updated({'disabled' => [ false, true ]})
      finalize_trunk
    else
      destroy_logged
    end
  end

end
