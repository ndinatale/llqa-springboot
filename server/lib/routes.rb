require_relative 'routes_dashboard'

Dir.glob('routes/*.rb').sort.each do |route|
  rname = "#{$2.capitalize} (#{$1})" if route =~ /(\d\d)_(.*)\.rb/
  $logger.info "Loading Routedefinition #{rname}"
  require_relative "../#{route}"
end