require_relative '../lib/constants'

# Die Methode, mit der ein Dashboard definiert wird, muss exakt so spezifiziert sein:
#   def dashboard_type_XXXX(filter, get_info = false)
# wobei XXXX eine eindeutige Kombination aus genau vier Grossbuchstaben/Zahlen sein muss.
# Die i18n-Einträge müssen in den Sprachdateien unter "DASHBOARD"."TYPE"."XXXX" angelegt werden.

def get_all_dashboard_types
  list = []
  private_methods.each do |m|
    next if m.to_s !~ /\Adashboard_type_([A-Z1-9]{4})\z/
    type = $1
    dtype = method(m).call(nil,true)
    dtype['type'] = type
    list << dtype
  end
  list
end

def apply_modelsel_filter(filterblock, q)
  return q if !filterblock || !filterblock.is_a?(Array) || filterblock.size < 1
  filterblock.each do |filter|
    if filter['type'] == 'MODELSEL'
      q = q.where('models.realid' => filter['val']) if filter['val']
    end
  end
  q
end

# 'Unfinished checks assigned to me'
def dashboard_type_UCAM(filter, get_info = false, isWithCheckdata = false)
  return { needgrant: [], filters: %w(MODELSEL) } if get_info

  # 'checkdata'-Feld mitlesen? (Soll im Hintergrund geschehen) (Momentan nie der Fall)
  if isWithCheckdata

    ret = Check.joins(:model).get_list do |q|
      q = q.where(status: CHK_STATUS_STARTED)
      q = $application_parameters[:skipcheckschedule] ? q.order(:id) : q.order(:dueby)
      apply_modelsel_filter(filter, q)
    end.serialize(%w(unit model model.devicetype checktype)) do |h,o|
      h['showlines'] = $application_parameters[:skipcheckschedule] ? %w(T BNS) : %w(T B)
      h['url'] = "/workflow/#{h['id']}"
      h['checkdata'] = nil
      h = nil unless o.processed_by?(get_current_user_id)
      h = nil if o.unit.code =~ /\(TR\)/

      # Progress-Daten in der DB speichern
      save_progress_data_to_db(o, h)

      h
    end.last
    ret[:rows].sort_by! { |o| [o['unit']['code'], $application_parameters[:skipcheckschedule] ? o['id'] : o['dueby']] }
    ret.rest_success

  # ohne 'checkdata'-Feld lesen. Progress-Daten werden von der DB geholt, die zuvor gespeichert wurden
  # daher Daten nicht ganz up-to-date
  else

    cols = (Check.attribute_names - ['checkdata']).map { |attrib| "checks.#{attrib}" }
    ret = Check.select(*cols).joins(:model).get_list2 do |q|
      q = q.where(status: CHK_STATUS_STARTED)
      q = $application_parameters[:skipcheckschedule] ? q.order(:id) : q.order(:dueby)
      apply_modelsel_filter(filter, q)
    end.serialize(%w(unit model model.devicetype checktype)) do |h,o|
      h['showlines'] = $application_parameters[:skipcheckschedule] ? %w(T BNS) : %w(T B)
      h['url'] = "/workflow/#{h['id']}"
      h['checkdata'] = nil
      h = nil unless o.processed_by?(get_current_user_id)
      h = nil if o.unit.code =~ /\(TR\)/

      # Progress-Daten von der DB holen
      get_progress_data_from_db(o, h)

      h
    end.last
    ret[:rows].sort_by! { |o| [o['unit']['code'], $application_parameters[:skipcheckschedule] ? o['id'] : o['dueby']] }
    ret.rest_success

  end
end

# 'Unfinished checks that may be processed by me'
def dashboard_type_UCMP(filter, get_info = false, isWithCheckdata = false)
  return { needgrant: %w(WFLREG), filters: %w(MODELSEL) } if get_info

  # 'checkdata'-Feld mitlesen? (Soll im Hintergrund geschehen) (Momentan nie der Fall)
  if isWithCheckdata

    ret = Check.joins(:model).get_list do |q|
      q = q.where(status: CHK_STATUS_STARTED)
      q = $application_parameters[:skipcheckschedule] ? q.order(:id) : q.order(:dueby)
      apply_modelsel_filter(filter, q)
    end.serialize(%w(unit model model.devicetype checktype)) do |h,o|
      h['showlines'] = $application_parameters[:skipcheckschedule] ? %w(T BNS) : %w(T B)
      h['url'] = "/workflow/#{h['id']}"
      h['checkdata'] = nil
      h = nil unless o.processable_by?(get_current_user_group_ids)
      h = nil if o.unit.code =~ /\(TR\)/

      # Progress-Daten in der DB speichern
      save_progress_data_to_db(o, h)

      h
    end.last
    ret[:rows].sort_by! { |o| [o['unit']['code'], $application_parameters[:skipcheckschedule] ? o['id'] : o['dueby']] }
    ret.rest_success

  # ohne 'checkdata'-Feld lesen. Progress-Daten werden von der DB geholt, die zuvor gespeichert wurden
  # daher Daten nicht ganz up-to-date
  else

    cols = (Check.attribute_names - ['checkdata']).map { |attrib| "checks.#{attrib}" }
    ret = Check.select(*cols).joins(:model).get_list2 do |q|
      q = q.where(status: CHK_STATUS_STARTED)
      q = $application_parameters[:skipcheckschedule] ? q.order(:id) : q.order(:dueby)
      apply_modelsel_filter(filter, q)
    end.serialize(%w(unit model model.devicetype checktype)) do |h,o|
      h['showlines'] = $application_parameters[:skipcheckschedule] ? %w(T BNS) : %w(T B)
      h['url'] = "/workflow/#{h['id']}"
      h['checkdata'] = nil
      h = nil unless o.processable_by?(get_current_user_group_ids)
      h = nil if o.unit.code =~ /\(TR\)/

      # Progress-Daten von der DB holen
      get_progress_data_from_db(o, h)

      h
    end.last
    ret[:rows].sort_by! { |o| [o['unit']['code'], $application_parameters[:skipcheckschedule] ? o['id'] : o['dueby']] }
    ret.rest_success

  end
end

# 'Finished checks for review'
def dashboard_type_FCFR(filter, get_info = false, isWithCheckdata = false)
  return { needgrant: %w(MNGMUC WFLMNG), filters: %w(MODELSEL) } if get_info

  # 'checkdata'-Feld mitlesen? (Soll im Hintergrund geschehen) (Momentan nie der Fall)
  if isWithCheckdata

    ret = Check.joins(:model).get_list do |q|
      q = q.where(status: CHK_STATUS_ALL_FINISHED)
      q = $application_parameters[:skipcheckschedule] ? q.order(:id) : q.order(:dueby)
      apply_modelsel_filter(filter, q)
    end.serialize(%w(unit model model.devicetype checktype)) do |h,o|
      h['showlines'] = $application_parameters[:skipcheckschedule] ? %w(T BNS) : %w(T B)
      h['url'] = "/workflow/#{h['id']}"
      h['checkdata'] = nil
      h = nil if o.unit.code =~ /\(TR\)/

      # Progress-Daten in der DB speichern
      save_progress_data_to_db(o, h)

      h
    end.last
    ret[:rows].sort_by! { |o| [o['unit']['code'], $application_parameters[:skipcheckschedule] ? o['id'] : o['dueby']] }
    ret.rest_success

  # ohne 'checkdata'-Feld lesen. Progress-Daten werden von der DB geholt, die zuvor gespeichert wurden
  # daher Daten nicht ganz up-to-date
  else

    cols = (Check.attribute_names - ['checkdata']).map { |attrib| "checks.#{attrib}" }
    ret = Check.select(*cols).joins(:model).get_list2 do |q|
      q = q.where(status: CHK_STATUS_ALL_FINISHED)
      q = $application_parameters[:skipcheckschedule] ? q.order(:id) : q.order(:dueby)
      apply_modelsel_filter(filter, q)
    end.serialize(%w(unit model model.devicetype checktype)) do |h,o|
      h['showlines'] = $application_parameters[:skipcheckschedule] ? %w(T BNS) : %w(T B)
      h['url'] = "/workflow/#{h['id']}"
      h['checkdata'] = nil
      h = nil if o.unit.code =~ /\(TR\)/

      # Progress-Daten von der DB holen
      get_progress_data_from_db(o, h)

      h
    end.last
    ret[:rows].sort_by! { |o| [o['unit']['code'], $application_parameters[:skipcheckschedule] ? o['id'] : o['dueby']] }
    ret.rest_success

  end
end

# 'Units without any checks'
def dashboard_type_UWAC(filter, get_info = false, isWithCheckdata = false)
  return { needgrant: %w(MNGMUC CRTCHK), filters: %w(MODELSEL) } if get_info
  Unit.joins(:model).get_list do |q|
    q = q.where(status: 1).order(:code)
    apply_modelsel_filter(filter, q)
  end.serialize(%w(model model.devicetype)) do |h,o|
    p o.code
    if o.checks.size > 0
      h = nil
    else
      h['showlines'] = %w(T B)
      h['url'] = "/units/#{h['model']['id']}/view/#{h['id']}"
    end
    h
  end.first
end

# 'Currently processed checks'
def dashboard_type_CPCH(filter, get_info = false, isWithCheckdata = false)
  return { needgrant: %w(WFLREG), filters: %w(MODELSEL) } if get_info

  # 'checkdata'-Feld mitlesen? (Soll im Hintergrund geschehen) (Momentan nie der Fall)
  if isWithCheckdata

    ret = Check.joins(:model).get_list do |q|
      q = q.where(status:CHK_STATUS_STARTED)
      q = $application_parameters[:skipcheckschedule] ? q.order(:id) : q.order(:dueby)
      apply_modelsel_filter(filter, q)
    end.serialize(%w(unit model model.devicetype checktype)) do |h,o|
      h['showlines'] = $application_parameters[:skipcheckschedule] ? %w(T BNS) : %w(T B)
      h['url'] = "/workflow/#{h['id']}"
      h['checkdata'] = nil
      h = nil if o.unit.code =~ /\(TR\)/

      # Progress-Daten in der DB speichern
      save_progress_data_to_db(o, h)

      h
    end.last
    ret[:rows].sort_by! { |o| [o['unit']['code'], $application_parameters[:skipcheckschedule] ? o['id'] : o['dueby']] }
    ret.rest_success

  # ohne 'checkdata'-Feld lesen. Progress-Daten werden von der DB geholt, die zuvor gespeichert wurden
  # daher Daten nicht ganz up-to-date
  else

    cols = (Check.attribute_names - ['checkdata']).map { |attrib| "checks.#{attrib}" }
    ret = Check.select(*cols).joins(:model).get_list2 do |q|
      q = q.where(status:CHK_STATUS_STARTED)
      q = $application_parameters[:skipcheckschedule] ? q.order(:id) : q.order(:dueby)
      apply_modelsel_filter(filter, q)
    end.serialize(%w(unit model model.devicetype checktype)) do |h,o|
      h['showlines'] = $application_parameters[:skipcheckschedule] ? %w(T BNS) : %w(T B)
      h['url'] = "/workflow/#{h['id']}"
      h['checkdata'] = nil
      h = nil if o.unit.code =~ /\(TR\)/

      # Progress-Daten von der DB holen
      get_progress_data_from_db(o, h)

      h
    end.last
    ret[:rows].sort_by! { |o| [o['unit']['code'], $application_parameters[:skipcheckschedule] ? o['id'] : o['dueby']] }
    ret.rest_success

  end

end

# 'Procedures and models with pending changes'
def dashboard_type_PMPC(filter, get_info = false, isWithCheckdata = false)
  return { needgrant: %w(FINALZ), filters: [] } if get_info
  ret = { rows: [] }
  Procedure.trunk_objects.each do |item|
    next if !item.is_dirty?
    ret[:rows] << {
        'url' => "/procedures/view/#{item.id}",
        'code' => item.code,
        'title' => item.title,
        'showlines' => %w(PROC)
    }
  end
  Model.trunk_objects.each do |item|
    next if !item.is_dirty?
    ret[:rows] << {
        'url' => "/models/view/#{item.id}",
        'code' => item.code,
        'title' => item.title,
        'showlines' => %w(MOD)
    }
  end
  ret[:rows] = ret[:rows].sort { |v1,v2| v1['code'] <=> v2['code'] }
  ret.rest_success
end

# Progress-Daten in der DB speichern
def save_progress_data_to_db(o, h)
  if h
    h['progress'] = o.calc_progress(true)
    o.progress_finished = h['progress'][:passed]
    o.progress_error = h['progress'][:failed]
    o.save!
  end
end

# Progress-Daten von der DB holen
def get_progress_data_from_db(o, h)
  if h
    h['progress'] = { unfin: 100.0 - (o.progress_finished + o.progress_error), passed: o.progress_finished, failed: o.progress_error }
  end
end
