def find_font(font,weight,style,condensed)
  ffile = "srvlet/fonts/#{font}#{weight}#{style}#{condensed}.ttf"
  return ffile if test(?f,ffile)
  ffile = "srvlet/fonts/#{font}#{weight}#{style}.ttf"
  return ffile if test(?f,ffile)
  ffile = "srvlet/fonts/#{font}#{weight}r.ttf"
  return ffile if test(?f,ffile)
  p "WARNING: No font found for #{font} #{weight} #{style} #{condensed}!"
  return nil
end

def pdf_setup_fonts(pdf,font)
  pdf.font_families.update('FontStd' => {
                               normal:       find_font(font,1,'r',''),
                               bold:         find_font(font,4,'r',''),
                               italic:       find_font(font,1,'i',''),
                               bold_italic:  find_font(font,4,'i','')
                           })
  pdf.font_families.update('FontStdC' => {
                               normal:       find_font(font,1,'r','c'),
                               bold:         find_font(font,4,'r','c'),
                               italic:       find_font(font,1,'i','c'),
                               bold_italic:  find_font(font,4,'i','c')
                           })
  pdf.font_families.update('FontHdr' => {
                               normal:       find_font(font,2,'r',''),
                               bold:         find_font(font,5,'r',''),
                               italic:       find_font(font,2,'i',''),
                               bold_italic:  find_font(font,5,'i','')
                           })
  pdf.font_families.update('GKai' => {
                               normal:       'srvlet/fonts/gkai.ttf',
                               bold:         'srvlet/fonts/gkai.ttf',
                               italic:       'srvlet/fonts/gkai.ttf',
                               bold_italic:  'srvlet/fonts/gkai.ttf'
                           })

  pdf.fallback_fonts ['GKai']
end

def pdf_read_language(lang)
  lmap = load_localization(lang)
  lmap['__lcode'] = lang
  $system_languages.each { |data| next if lang != data[:code]; lmap['__fmtt'],lmap['__fmtd'] = data[:tformatr],data[:dformatr] }
  def lmap.resolve(code)
    pos = self
    code.upcase.split('_').each do |part|
      return '?!?' if !pos.is_a?(Hash) || !pos.key?(part)
      pos = pos[part]
    end
    pos = self.resolve($1.gsub('.','_')) if (pos.is_a?(String) && pos =~ /\A@:(.*)\z/)
    pos
  end
  def lmap.method_missing(method, *argsin)
    return super(method,argsin) if !method.to_s.start_with?('pdf_')
    resolve(method.to_s)
  end
  def lmap.code
    self['__lcode']
  end
  def lmap.formatt(tobj)
    tobj ? tobj.strftime(self['__fmtt'] || '???') : '---'
  end
  def lmap.formatd(tobj)
    tobj ? tobj.strftime(self['__fmtd'] || '???') : '---'
  end
  def lmap.replacedatetag(tobj,tzoff)
    tobj.gsub(/\[SPAN:(\d+)\]/) { |_| span = $1.to_i; sprintf('%dh %02dmin', span/3600, (span/60)%60) }
        .gsub(/\[TIME:(\d+)\]/) { |_| self.formatt(Time.at_with_tzoff($1.to_i, tzoff)) }
        .gsub(/\[DATE:(\d+)\]/) { |_| self.formatt(Time.at_with_tzoff($1.to_i, tzoff)) }
        .gsub(/\[TIMS:(\d+)\]/) { |_| self.formatt(Time.at_with_tzoff($1.to_i, tzoff)).gsub(/.*? /,'') }
  end
  lmap
end

class String
  def html_to_pdf
    self.gsub(/\<[^\>]+\>/,' ').gsub(/&[^;]+;/,'')
  end
end

def pdf_text_helper(pdf,text,options,ifo)
  if ifo
    ifo = [] unless p.is_a?(Array)
    array = pdf.text_formatter.format(text, *ifo)
    Prawn::Text::Formatted::Box.new(array, options)
  else
    Prawn::Text::Box.new(text, options)
  end
end

def pdf_text(pdf,x,y,w,h,text,header,params)
  return [0,0,nil] if !text || text == ''
  options = { document: pdf, at: [x.mm, (y < 0 ? (0-y) : 297-y).mm], width: w.mm, height: h.mm }
  options[:size] = params[:fontsize] || 12
  options[:valign] = params[:valign] || :top
  options[:align] = params[:halign] || :left
  options[:kerning] = params[:kerning] || true
  options[:style] = params[:style] || :normal
  options[:character_spacing] = params[:spacing] || 0
  options[:overflow] = params[:overflow] || :truncate
  options[:rotate] = 90 if params[:direction] == :up
  options[:rotate_around] = :upper_left if params[:direction] == :up

  pdf.font header ? 'FontHdr' : 'FontStd'
  oldcol = pdf.fill_color
  pdf.fill_color params[:color] if params[:color]

  box = nil
  if (params[:autofit])
    again = true
    spacing = options[:character_spacing]
    condensed = header
    fsize = options[:size]
    begin
      box = pdf_text_helper(pdf,text,options,params[:inline_format])
      again = box.render(dry_run: true)
      again = nil if again == '' || again == []
      if again
        if spacing < -0.3 && !condensed
          condensed,spacing = true,params[:spacing] || 0
          options[:character_spacing] = spacing
          pdf.font 'FontStdC'
        elsif spacing < -0.3
          fsize *= 0.9
          spacing = 0
          options[:size] = fsize
          options[:character_spacing] = spacing
        else
          spacing -= 0.1
          options[:character_spacing] = spacing
        end
      end
    end while again
  else
    box = pdf_text_helper(pdf,text,options,params[:inline_format])
  end

  res = box.render(dry_run: params[:dry] == true)

  pdf.fill_color oldcol

  [box.height == 0 ? 0 : (box.height/1.mm+(params[:linespc] || 1)),res,box]
end

def pdf_line(pdf,x1,y1,x2,y2,scol,swdth)
  oscol,oswdth = pdf.stroke_color,pdf.line_width
  pdf.stroke_color,pdf.line_width = scol,swdth
  pdf.stroke { pdf.line [x1.mm,(297-y1).mm], [x2.mm,(297-y2).mm] } if scol
  pdf.stroke_color,pdf.line_width = oscol,oswdth
end

def pdf_rectangle(pdf,x,y,w,h,fcol,scol,swdth)
  ofcol,oscol,oswdth = pdf.fill_color,pdf.stroke_color,pdf.line_width
  pdf.fill_color = fcol if fcol
  pdf.stroke_color,pdf.line_width = scol,swdth if scol
  pdf.fill { pdf.rectangle [x.mm,(297-y).mm], w.mm, h.mm } if fcol
  pdf.stroke { pdf.rectangle [x.mm,(297-y).mm], w.mm, h.mm } if scol
  pdf.fill_color,pdf.stroke_color,pdf.line_width = ofcol,oscol,oswdth
end

def pdf_ellipse(pdf,cx,cy,rx,ry,fcol,scol,swdth)
  ofcol,oscol,oswdth = pdf.fill_color,pdf.stroke_color,pdf.line_width
  pdf.fill_color = fcol if fcol
  pdf.stroke_color,pdf.line_width = scol,swdth if scol
  pdf.fill { pdf.ellipse [cx.mm,(297-cy).mm], rx.mm, ry.mm } if fcol
  pdf.stroke { pdf.ellipse [cx.mm,(297-cy).mm], rx.mm, ry.mm } if scol
  pdf.fill_color,pdf.stroke_color,pdf.line_width = ofcol,oscol,oswdth
end

def csv_helper(csv,hash,keys)
  csv << keys.map { |key| hash.has_key?(key.to_sym) ? (hash[key.to_sym] || '-') : '-/-' }
  hash.each do |_,v|
    csv_helper(csv,v,keys) if v.is_a?(Hash)
    v.each { |l| csv_helper(csv,l,keys) } if v.is_a?(Array)
  end
end

def csv_helper_stringkeys(csv,hash,keys)
  csv << keys.map { |key| hash.has_key?(key) ? (hash[key] || '-') : '-/-' }
  hash.each do |_,v|
    csv_helper_stringkeys(csv,v,keys) if v.is_a?(Hash)
    v.each { |l| csv_helper_stringkeys(csv,l,keys) } if v.is_a?(Array)
  end
end

$loremipsum = %w(Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.)
def loremipsum(min, max)
  li, rndsize, skip = '', rand(max-min)+min, rand(20)
  loop do
    $loremipsum.each do |word|
      skip -= 1
      next if skip > 0
      li += "#{word} "
      return li[0..-2] if li.size > rndsize
    end
  end
end

class Hash
  def format_as_json
    return JSON.pretty_generate(self)
  end

  def format_as_xml
    return self.to_xml
  end

  def format_as_csv(keys)
    return CSV.generate do |csv|
      csv << keys
      csv_helper(csv,self,keys)
    end
  end

  def format_as_csv_stringkeys(keys)
    return CSV.generate do |csv|
      csv << keys
      csv_helper_stringkeys(csv,self,keys)
    end
  end
end