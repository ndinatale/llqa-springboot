class Time
  self.singleton_class.send(:alias_method, :orig_now, :now)
  def self.now
    orig_now.getutc
  end
  def self.at_with_tzoff(mseccnt, offpar)
    return self.at(mseccnt) if !offpar
    offpar = -(offpar.to_i)*60
    return self.at(mseccnt).localtime(offpar)
  end
end

$logger = $logger || [ STDOUT ]

def $logger.log(lvl,msg)
  msgstr = sprintf('%s %s %s',lvl,Time.now.strftime('%Y/%m/%d %H:%M:%S.%L'),msg)
  self.each do |out|
    out.puts msgstr
  end
end

def $logger.debug(msg)
  $logger.log('DBG',msg)
end

def $logger.info(msg)
  $logger.log('INF',msg)
end

def $logger.warn(msg)
  $logger.log('WRM',msg)
end

def $logger.error(msg)
  $logger.log('ERR',msg)
end

def $logger.fatal(msg)
  $logger.log('FAT',msg)
end

def get_current_user
  return Thread.current[:session][:logininfo][:userid] if Thread.current[:session] && Thread.current[:session][:logininfo] && Thread.current[:session][:logininfo][:userid]
  '??'
end

def get_current_user_id
  return Thread.current[:session][:logininfo][:userid] if Thread.current[:session] && Thread.current[:session][:logininfo] && Thread.current[:session][:logininfo][:userid]
  -1
end

def get_current_user_group_ids
  uid = get_current_user
  return [] if uid == '??'
  uobj = User.find_by(id: uid)
  return uobj.affiliations.collect { |aff| aff.usergroup.id }
  []
end

def get_current_username
  uid = get_current_user
  return '??' if uid == '??'
  uobj = User.find_by(id: uid)
  return uobj.username if uobj
  '??'
end

def get_current_userobject
  uid = get_current_user
  return nil if uid == '??'
  uobj = User.find_by(id: uid)
  return uobj if uobj
  nil
end

class Array
  def incllist
    ret = {}
    self.each do |incl|
      sret = ret
      incl.split('.').each do |sinc|
        sincs = sinc.to_sym
        if sret != ret
          sret[:include]={} if !sret[:include]
          sret = sret[:include]
        end
        sret[sincs]={} if !sret[sincs]
        sret = sret[sincs]
      end
    end
    return ret
  end

  def serialize(include = nil)
    self
  end
end

class Hash
  def rest_success
    [200, JSON.pretty_generate({ status: 'success', code: 200, data: self })]
  end

  def rest_fail(code = 200)
    [code, JSON.pretty_generate({ status: 'error', code: self[:code] || 0, stack: self[:stack], messages: self[:messages], type: self[:type]})]
  end

  def clean_export_hash
    %w(id realid version created_at updated_at created_by updated_by).each { |f| self.delete f }
    self.each do |_,v|
      v.each { |subhash| subhash.clean_export_hash } if v.is_a? Array
    end
    self
  end

  def serialize(include = nil)
    self
  end
end

class Fixnum
  def serialize(include = nil)
    self
  end
end

def assemble_error(cat,msgtext,values,trace)
  return {
      type: "SERVER.ERROR.#{cat}.TYPE",
      messages: [
          {
            msg: "SERVER.ERROR.#{cat}.#{msgtext}",
            vals: values
          }
      ],
      stack: trace
  }
end

def assemble_validation_error(errors)
  ret = {
      type: 'SERVER.ERROR.VALIDATION.TYPE',
      messages: [],
      stack: nil
  }
  errors.each do |field,emsgs|
    emsgs.each do |emsg|
      ret[:messages] << { msg: "SERVER.ERROR.VALIDATION.#{emsg[1].upcase}.#{field.to_s.upcase}.#{emsg[0]}", vals: emsg[2] }
    end
  end
  return ret
end

def build_import_hash(data,type)
  "type:#{type},crc:#{Digest::SHA1.hexdigest(data)},data:\n#{data}"
end

def check_import_hash(hash,reqtype,fname)
  begin
    if hash =~ /\Atype:([A-Za-z]+),crc:([\da-f]+),data:\n(.*)\z/m
      type,crc,data = $1,$2,$3
      return assemble_error('IMPORT','WRONGTYPE',{ req: reqtype, is: type, fname: fname },[]).rest_fail if type != reqtype
      return assemble_error('IMPORT','BADCRC',{ fname: fname },[]).rest_fail if crc != Digest::SHA1.hexdigest(data)
      return JSON.parse(data)
    end
  rescue
  end
  return assemble_error('IMPORT','INVALIDFILE',{ fname: fname },[]).rest_fail
end

$internal_cache = {}

def serve_cached(key)
  puts "Serving #{key}"
  return $internal_cache[key] if $internal_cache.has_key? key
  puts "Building #{key}"
  value = yield
  $internal_cache[key] = value if !$debugging_api
  return value
end

#

def has_privilege(*privs)
  user = get_current_userobject
  return false if !user
  privcoll = user.get_all_privileges
  privs.each do |priv|
    return true if privcoll[priv]
    return true if privcoll[priv[0..2]+'ALL']
  end
  false
end

def not_logged_in
  assemble_error('LOGIN', 'NOTLOGGEDIN', {}, []).rest_fail(403)
end

def illegal_access
  assemble_error('ACCDENIED', 'TEXT', {}, []).rest_fail(403)
end

$localization_loaded = {}
def load_localization(lang)
  return $localization_loaded[lang] if $localization_loaded[lang] && !$debugging_api
  lmap = JSON.parse(File.read("../public/i18n/lang-#{lang}.json"))
  $localization_override[lang].each do |k,v|
    val = lmap
    path = k.split('.')
    path[0..-2].each do |e|
      val = val[e] if val
    end
    val[path[-1]] = v if val && val.is_a?(Hash) && val[path[-1]].is_a?(String)
  end
  $localization_loaded[lang] = lmap
  lmap
end


