require_relative '../lib/utils'
require_relative 'checks_check_prepare'
require_relative 'checks_check_copy'
require_relative 'checks_check_calculations'
require_relative 'checks_check_checkaction'
require_relative 'checks_check_stepaction'
require_relative 'checks_check_register'

class Check < ActiveRecord::Base
  include ModelBase
  include Searching

  # -----
  # Table setup
  # -----

  serialize :metadata, Hash
  serialize :checkdata, Hash
  serialize :assign_data, Hash

  belongs_to :checktype
  belongs_to :unit
  belongs_to :model
  has_many :measurements, dependent: :destroy
  has_many :images, -> { order('seqnum ASC') }, as: :owner, dependent: :destroy
  has_many :documents, -> { order('seqnum ASC') }, as: :owner, dependent: :destroy

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.references :unit, null: false
      t.references :model, null: false
      t.references :checktype, null: false
      t.text :comment
      t.date :scheduled
      t.date :dueby
      t.date :started
      t.date :finished
      t.integer :status
      t.text :metadata
      t.text :checkdata
      augment_table_usertag t
    end
  end
  def self.update_table_01(m,tn)
    # no change to the table; only to data
    Check.all.each do |check|
      check.checkdata['procedures'].each do |p|
        p['status'] = [0,10,15,20,21,30][p['status']]
        p['steps'].each do |s|
          s['status'] = [0,10,15,20,21,30][s['status']]
          s['measures'].each do |m|
            m['status'] = [0,10,15,20,21,30][m['status']]
            m['cstatus'] = [0,10,15,20,21,30][m['cstatus']]
          end
        end
      end
      check.save!
    end
  end

  # -----
  # Global search
  # -----

  def self.searchable_fields
    {
        comment: :text
    }
  end
  def self.search_result_extend
    %w(unit unit.model checktype)
  end

  # -----
  # DB hooks
  # -----

  def has_been_created(_)
    metadata['_commentby'] = get_current_user
  end

  def has_been_updated(changes)
    changes.each do |k,_|
      if k == 'comment'
        metadata['_commentby'] = get_current_user
      end
    end
  end

  # -----
  # Aggregation and helper methods
  # -----

  def newer_model_versions
    cur_version = self.model.version
    newer_versions = []
    Model.where(realid: self.model.realid).where("version > ?", cur_version).to_a.each do |model|
      user = User.find_by(id: model.updated_by)
      meta = {
          date: model.updated_at.to_i*1000,
          username: user ? user.username : 'Anonymous',
          realname: user ? user.realname : '',
          userid: user ? user.id : -1
      }
      newer_versions << [model.version, model.id, meta, Activechecktype.find_by(model: model.id, checktype: self.checktype.id) ? true : false]
    end
    newer_versions.sort_by { |el| el[0] }
  end

  def uses_procedure?(prid)
    model.activeprocedures.each { |ap| return true if ap.procedure.realid == prid }
    false
  end

  # noinspection RubyScope
  def get_current_and_next_step(curstepid,userid)
    cdentry,cdnextentry,cdpreventry,cnt,numsteps = nil,nil,nil,0,0
    checkdata['procedures'].each do |p|
      numsteps += p['steps'].length
      p['steps'].each do |s|
        cnt += 1
        if curstepid
          cdnextentry = cnt if !cdnextentry && cdentry && status == CHK_STATUS_STARTED && (CHK_CDSTATUS_ALL_FINISHED.index(cdentry['status']) || CHK_CDSTATUS_ALL_OMITTED.index(cdentry['status'])) && s['assignee'] >= 0 && checkdata['assignees'][s['assignee']]['user_id'] == userid && !CHK_CDSTATUS_ALL_LEFTOUT.index(s['status']) && !(s['completem'] && s['completet']) && (!p['locked'] || p['locked'] == 0) && (!s['locked'] || s['locked'] == 0)
          cdentry = s if cnt == curstepid
          cdpreventry = cnt if !cdentry && status == CHK_STATUS_STARTED && s['assignee'] >= 0 && checkdata['assignees'][s['assignee']]['user_id'] == userid && !CHK_CDSTATUS_ALL_LEFTOUT.index(s['status']) && (!p['locked'] || p['locked'] == 0) && (!s['locked'] || s['locked'] == 0)
        else
          cdnextentry = cnt if !cdnextentry && status == CHK_STATUS_STARTED && s['assignee'] >= 0 && checkdata['assignees'][s['assignee']]['user_id'] == userid && !CHK_CDSTATUS_ALL_LEFTOUT.index(s['status']) && !(s['completem'] && s['completet']) && (!p['locked'] || p['locked'] == 0) && (!s['locked'] || s['locked'] == 0)
        end
      end
    end
    cdnextentry = get_current_and_next_step(nil,userid)[1] if (curstepid && !cdnextentry)
    return cdentry,cdnextentry,cdpreventry, numsteps
  end

  def get_current_and_next_step_by_measurement_id(msmntid,userid)
    cnt = 0
    puts "#{msmntid}"
    checkdata['procedures'].each do |p|
      p['steps'].each do |s|
        cnt += 1
        s['measures'].each do |m|
          puts "#{m['mid']}"
          return get_current_and_next_step(cnt,userid) if m['mid'] == msmntid
        end
      end
    end
    return get_current_and_next_step(1,userid)
  end

  def dump_checkdata
    self.checkdata['procedures'].each do |p|
      print "#{p['status']} : "
      p['steps'].each do |s|
        print "(#{s['status']}) "
        s['measures'].each do |m|
          print "#{m['status']}/#{m['cstatus']}"
        end
      end
    end
  end

  def processed_by?(user_id)

    # User ist dieser Prozedur assigned?
    if assign_data['user_ids'].include? user_id
      return true
    end
    false
  end

  def processable_by?(group_ids)

    # Ist User in einer Gruppe welche auch dieser Prozedur assigned ist? (untenstehen wird ein neues Array erstellt
    # mit all den id's, welche in beiden Arrays vorkommen)
    if (assign_data['group_ids'] & group_ids).empty?
      return false
    end
    true
  end

  def calc_progress(deduct_skipped = true)
    stats,total = Hash.new(0),0
    self.checkdata['procedures'].each do |proc|
      proc['steps'].each do |step|
        step['measures'].each do |measure|
          stats[measure['status']] += 1
          total += 1
        end
      end
    end
    cnt_unfin = stats[CHK_CDSTATUS_TODO]
    cnt_passed = stats[CHK_CDSTATUS_PASSED]
    cnt_failed = stats[CHK_CDSTATUS_WARNINGS]+stats[CHK_CDSTATUS_FAILED]
    cnt_skipped = stats[CHK_CDSTATUS_OMITTED_HARD]+stats[CHK_CDSTATUS_OMITTED_SOFT]+stats[CHK_CDSTATUS_SKIPPED]
    total -= cnt_skipped if deduct_skipped
    perc = total == 0 ? 0.0 : (100.0/total)
    { unfin: perc*cnt_unfin, passed: perc*cnt_passed, failed: perc*cnt_failed }
  end

  # -----
  # Comments
  # -----

  def self.all_comments(id, group)
    comments = []
    element = Check.find(id)
    ret = { comments: comments, element: element }
    if group
      toolunits = Hash.new { |h,v| h[v] = { tooltype: nil, units: {} } }
      element.checkdata['procedures'].each do |proc|
        comments << { type: 'proc', obj: Procedure.find(proc['id']), text: nil, children: pcomments = [] }
        proc['steps'].each do |step|
          pcomments << { type: 'step', obj: Step.find(step['id']), text: nil, children: scomments = [] }
          step['measures'].each do |measure|
            msmnt = Measurement.find(measure['mid']) if measure['mid'] >= 0
            if msmnt
              scomments << { type: 'msmnts', obj: msmnt, text: msmnt.comment, owner: msmnt.metadata['_commentby'] || msmnt.savedby, children: [] }
              if msmnt.toolunit && msmnt.toolunit.comment && msmnt.toolunit.comment.size > 0
                toolunits[msmnt.toolunit.tooltype][:tooltype] = msmnt.toolunit.tooltype
                toolunits[msmnt.toolunit.tooltype][:units][msmnt.toolunit.code] = msmnt.toolunit
              end
            end
          end
        end
      end
      toolunits.sort.each do |type|
        comments << { type: 'tooltype', obj: type[1][:tooltype], text: nil, children: tcomments = [] }
        type[1][:units].sort.each do |unit|
          tcomments << { type: 'tool', obj: unit[1], text: unit[1].comment, owner: unit[1].metadata['_commentby'], children:[] }
        end
      end
    else
      element.checkdata['procedures'].each do |proc|
        proc['steps'].each do |step|
          step['measures'].each do |measure|
            msmnt = Measurement.find(measure['mid']) if measure['mid'] >= 0
            comments << { type: 'msmnt', obj: msmnt, text: msmnt.comment, owner: msmnt.metadata['_commentby'] || msmnt.savedby, children: [] } if msmnt
          end
        end
      end
    end
    ret
  end

  # -----
  # Configtable
  # -----

  def self.get_table_id(id)
    # get the configuration table associated with this model
    sql = "SELECT id FROM configtables WHERE parent = 'check' AND parentid = " + id.to_s
    results = connection.select_all(sql)
    return nil if results.empty?
    return results[0][0]
  end

  def revalidate
    # revalidates the current check. used for configuration tables, first resets all status values
    self.checkdata['procedures'].each do |p|
      p['status'] = CHK_CDSTATUS_TODO
      p['steps'].each do |s|
        s['status'] = CHK_CDSTATUS_TODO
        s['measures'].each do |m|
          m['status'] = CHK_CDSTATUS_TODO
        end
      end
    end

    recalc_workflow(nil)
    save!
    print "Workflow recalculated!\n"
  end

  def self.update_table_02(m,tn)
    m.change_table tn do |t|
      t.float :progress_finished, null: false, default: 0.0
      t.float :progress_error, null: false, default: 0.0
      t.text :assign_data
    end

    self.all.each do |check|
      stats,total = Hash.new(0),0
      if check.checkdata['procedures']
        check.checkdata['procedures'].each do |proc|
          proc['steps'].each do |step|
            step['measures'].each do |measure|
              stats[measure['status']] += 1
              total += 1
            end
          end
        end
      end

      cnt_unfin = stats[CHK_CDSTATUS_TODO]
      cnt_passed = stats[CHK_CDSTATUS_PASSED]
      cnt_failed = stats[CHK_CDSTATUS_WARNINGS]+stats[CHK_CDSTATUS_FAILED]
      cnt_skipped = stats[CHK_CDSTATUS_OMITTED_HARD]+stats[CHK_CDSTATUS_OMITTED_SOFT]+stats[CHK_CDSTATUS_SKIPPED]
      total -= cnt_skipped
      perc = total == 0 ? 0.0 : (100.0/total)
      check.progress_finished = perc*cnt_passed
      check.progress_error = perc*cnt_failed

      check.assign_data = {
            'user_ids' => [], 'group_ids' => []
      }

      # assignees durchgehen und die user_ids und group_ids im assign_data speichern
      if check.checkdata['assignees']
        check.checkdata['assignees'].each do |ass|
          if ass['user_id']                                 # Wenn user_id schon vorhanden ist, nicht einfügen (Redundanzen)
            check.assign_data['user_ids'] << ass['user_id'] if !check.assign_data['user_ids'].include? ass['user_id']
          end
          if ass['group_id']                                  # Wenn group_id schon vorhanden ist, nicht einfügen (Redundanzen)
            check.assign_data['group_ids'] << ass['group_id'] if !check.assign_data['group_ids'].include? ass['group_id']
          end
        end
      end

      check.save!
    end




  end

end

# Checkdata:
# Assignees: user_id OR group_id (if none: anyone); selfassign (true or false/missing); regmode: 1 = complete only, 2 = May partition, 3 = Must partition
# Procedure: assignee, ap_id, id, code, status
#  -> Step: assignee, id, status, code
#    -> Measure: id, status, code, mid (-> measurement, -1 if not set yet)

# assignee: -1 = unassigned, -2 = assigned in detail >= 0 = assigned to user # in assignment hash
# homit: true = omitted on hard (unchangeable) parameter (won't change during workflow)
# pstatus: 0 = not finished, 10 = passed, 13 = passed with warnings, 15 = failed, 20 = omitted on hard parameter, 21 = omitted on soft parameter, 30 = skipped on soft parameter
# sstatus: 0 = not finished, 10 = passed, 13 = passed with warnings, 15 = failed, 20 = omitted on hard parameter, 21 = omitted on soft parameter, 30 = skipped on soft parameter
# mstatus: 0 = not finished, 10 = passed, 13 = passed with warnings, 15 = failed, 20 = omitted on hard parameter, 21 = omitted on soft parameter, 30 = skipped on soft parameter
# (skip = appear on report as skipped, omit = does not appear on report)

# type trigger   parameters
#  CT   Select    code, inverse
#  U    Set       key, inverse
#  AP   Set       key, inverse
#  MD   Select    code, inverse
#  DT   Select    code, inverse
#  P    InQueue   code, inverse
#  P    P/F/S/O   code, res, inverse (S/O only)
#  S    P/F/S/O   code, res, inverse (S/O only)
#  M    P/F/S/O   code, res, inverse (S/O only)
#  M    Yes/No    code, res

# Status
#  0 - Initialized
# 10 - Scheduled (not started)
# 20 - Started
# 30 - Finished with failure, awaiting review (one or more failed measures, at least one not marked 'internal')
# 33 - Finished with warnings, awaiting review (one or more failed measures all marked as 'internal')
# 35 - Finished with success, awaiting review (no failed measures)
# 40 - Closed, finished with failure (one or more failed measures, at least one not marked 'internal')
# 43 - Closed, finished with warnings (one or more failed measures all marked as 'internal')
# 45 - Closed, finished with success (no failed measures)
# 49 - Closed, unfinished (cancelled)