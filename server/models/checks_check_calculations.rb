require_relative '../lib/constants'

class Check < ActiveRecord::Base

  # -----
  # (Re)calculate general workflow (flow control / status / enforcement)
  # -----

  def recalc_workflow_helper(klass,preval,calc,type,data,id,hasic,changed)
    return nil if !calc && !preval
    return CHK_CDSTATUS_OMITTED_HARD if data['status'] == CHK_CDSTATUS_OMITTED_HARD
    newstatus = 0
    newstatus = Check.calculate_flowcontrol(klass.find(data[id]),self,hasic,!hasic) if (calc && (!preval || !CHK_CDSTATUS_ALL_OMITTED.index(preval)))
    if preval
      newstatus = CHK_CDSTATUS_OMITTED_HARD if preval == CHK_CDSTATUS_OMITTED_HARD
      newstatus = CHK_CDSTATUS_OMITTED_SOFT if preval == CHK_CDSTATUS_OMITTED_SOFT
      newstatus = CHK_CDSTATUS_SKIPPED if preval == CHK_CDSTATUS_SKIPPED && !CHK_CDSTATUS_ALL_LEFTOUT.index(newstatus)
    end
    if newstatus == CHK_CDSTATUS_TODO && CHK_CDSTATUS_ALL_LEFTOUT.index(data['status']) && (!data['cstatus'] || data['cstatus'] != CHK_CDSTATUS_SKIPPED)
      data['status'] = CHK_CDSTATUS_TODO
      changed[type] << data['code']
    elsif CHK_CDSTATUS_ALL_LEFTOUT.index(newstatus) && data['status'] != newstatus
      data['status'] = newstatus
      changed[type] << data['code']
    end
    newstatus
  end

  def recalc_workflow_recurse(itemcodes,checkforce = {})
    changed = Hash.new { |h,k| h[k] = [] }
    hasic = itemcodes != nil

    if hasic
      return if itemcodes.size == 0
      depitemcodes = Hash.new { |h,k| h[k] = [] }
      itemcodes.each do |cat,list|
        next if !checkdata['dependencies'].has_key?(cat)
        dependencies = checkdata['dependencies'][cat]
        list.each do |item|
          next if !dependencies.has_key?(item)
          dependencies[item].each { |ditem| depitemcodes[ditem[0]] << ditem[1] }
        end
      end
      itemcodes = depitemcodes
      checkforce.each { |type,codes| codes.each { |code| itemcodes[type] << code } }
    end

    self.checkdata['procedures'].each do |p|
      calc = !hasic
      ppv = recalc_workflow_helper(Activeprocedure,nil,calc,'AP',p,'ap_id',false,changed)
      calc = !hasic || (itemcodes['P'] && itemcodes['P'].index(p['code']))
      ppv = recalc_workflow_helper(Procedure,ppv,calc,'P',p,'id',hasic,changed)

      p['steps'].each do |s|
        calc = !hasic || (itemcodes['S'] && itemcodes['S'].index(s['code']))
        spv = recalc_workflow_helper(Step,ppv,calc,'S',s,'id',hasic,changed)

        s['measures'].each do |m|
          calc = !hasic || (itemcodes['M'] && itemcodes['M'].index(m['code']))
          recalc_workflow_helper(Measure,spv,calc,'M',m,'id',hasic,changed)
        end
      end
    end if !hasic || itemcodes.size > 0

    recalc_status(changed)
    recalc_workflow_recurse(changed) if hasic

    # if everything is done, set status bottom up (steps with all measures omitted are also omitted, same for procedures/steps)
    self.checkdata['procedures'].each do |p|
      all_steps_omitted = true
      p['steps'].each do |s|
        all_measures_omitted = true
        s['measures'].each do |m|
          all_measures_omitted = false if !CHK_CDSTATUS_ALL_OMITTED.index(m['status'])
        end
        s['status'] = CHK_CDSTATUS_OMITTED_HARD if all_measures_omitted
        all_steps_omitted = false if !all_measures_omitted
      end
      p['status'] = CHK_CDSTATUS_OMITTED_HARD if all_steps_omitted
      end
  end

  def recalc_workflow(itemcodes, checkforce={})
    recalc_workflow_recurse(itemcodes, checkforce)
    recalc_enforce
  end

  # -----
  # (Re)calculate status
  # -----

  def recalc_status(changed)

    self.checkdata['procedures'].each do |p|
      next if CHK_CDSTATUS_ALL_LEFTOUT.index(p['status'])
      newpstatus = CHK_CDSTATUS_PASSED

      p['steps'].each do |s|
        next if CHK_CDSTATUS_ALL_LEFTOUT.index(s['status'])

        newsstatus = CHK_CDSTATUS_PASSED
        scompltool,scomplmeas = true,true

        s['measures'].each do |m|
          origmstatus = m['status']
          m['cstatus'] = CHK_CDSTATUS_TODO unless m['cstatus']
          m['status'] = m['cstatus'] if m['status'] == CHK_CDSTATUS_TODO && m['status'] != m['cstatus']
          newsstatus = CHK_CDSTATUS_TODO if newsstatus == CHK_CDSTATUS_PASSED && m['status'] == CHK_CDSTATUS_TODO
          newsstatus = CHK_CDSTATUS_WARNINGS if newsstatus != CHK_CDSTATUS_FAILED && m['status'] == CHK_CDSTATUS_WARNINGS
          newsstatus = CHK_CDSTATUS_FAILED if m['status'] == CHK_CDSTATUS_FAILED
          changed['M'] << m['code'] if origmstatus != m['status']
          scomplmeas = false if m['status'] == CHK_CDSTATUS_TODO
          scompltool = false if m['needstool'] && !CHK_CDSTATUS_ALL_LEFTOUT.index(m['status'])
        end
        changed['S'] << s['code'] if newsstatus != s['status']
        s['status'] = newsstatus
        s['completem'] = scomplmeas
        s['completet'] = scompltool
        newpstatus = CHK_CDSTATUS_TODO if newpstatus == CHK_CDSTATUS_PASSED && s['status'] == CHK_CDSTATUS_TODO
        newpstatus = CHK_CDSTATUS_WARNINGS if newpstatus != CHK_CDSTATUS_FAILED && s['status'] == CHK_CDSTATUS_WARNINGS
        newpstatus = CHK_CDSTATUS_FAILED if s['status'] == CHK_CDSTATUS_FAILED
      end
      changed['P'] << p['code'] if newpstatus != p['status']
      p['status'] = newpstatus
    end

  end

  # -----
  # (Re)calculate flow control
  # -----

  def self.calculate_flowcontrol(item,check,soft,hard)
    if hard
      item.flowcontrol.each do |fc|
        fc['inverse'] = false if fc['inverse'] == nil
        if fc['type'] == 'CT'
          if fc['trigger'] == 'Select'
            return CHK_CDSTATUS_OMITTED_HARD if (check.checktype.code == fc['code']) != fc['inverse']
          end
        end
        if fc['type'] == 'CO'
          if fc['trigger'] == 'Select'

            # get ids of both table and entry
            table_id = fc['code'].split('.').first.to_i
            entry_id = fc['code'].split('.').last.to_i

            # first, get model from check
            check_table = Configtable.where("parent = 'check' AND parentid = ?", check.id).first

            # now, get model table that was specified in rule # .select('id, parentcode').
            model_table_from_rule = Configtable.where("parent = 'model' AND id = ?", table_id).first

            # get model code from model this check belongs to
            check_model_code = Model.where('id = ?', check.model_id).first.code

            if model_table_from_rule && check_table && check_model_code == model_table_from_rule.parentcode
              entry = Configentry.select('active').where('configtable_id = ? AND code_id = ?', check_table.id, entry_id).first
              if entry != nil
                return CHK_CDSTATUS_OMITTED_HARD if entry.active != fc['inverse']
              end
            end
          end
        end
        if fc['type'] == 'U'
          if fc['trigger'] == 'Set'
            return CHK_CDSTATUS_OMITTED_HARD if (check.unit.metadata[fc['key']] != null) != fc['inverse']
          end
        end
        if fc['type'] == 'AP'
          if fc['trigger'] == 'Set'
            check.model.activeprocedures.each do |ap|
              next if ap.procedure.code != procedure.code
              return CHK_CDSTATUS_OMITTED_HARD if (ap.metadata[fc['key']] != null) != fc['inverse']
            end
          end
        end
        if fc['type'] == 'MD'
          if fc['trigger'] == 'Select'
            return CHK_CDSTATUS_OMITTED_HARD if (check.model.code == fc['code']) != fc['inverse']
          end
        end
        if fc['type'] == 'DT'
          if fc['trigger'] == 'Select'
            return CHK_CDSTATUS_OMITTED_HARD if (check.model.devicetype.code == fc['code']) != fc['inverse']
          end
        end
        if fc['type'] == 'P'
          if fc['trigger'] == 'InQueue'
            check.model.activeprocedures.each do |ap|
              return CHK_CDSTATUS_OMITTED_HARD if (ap.procedure.code == fc['code']) != fc['inverse']
            end
          end
        end
      end
    end
    ret = 0
    if soft
      item.flowcontrol.each do |fc|
        fc['inverse'] = false if fc['inverse'] == nil
        if fc['type'] == 'P'
          chkstatus = {'Pass' => [[CHK_CDSTATUS_PASSED],true], 'Fail' => [[CHK_CDSTATUS_WARNINGS,CHK_CDSTATUS_FAILED],true], 'Skip' => [[CHK_CDSTATUS_SKIPPED],!fc['inverse']], 'Omit' => [[CHK_CDSTATUS_OMITTED_HARD,CHK_CDSTATUS_OMITTED_SOFT],!fc['inverse']] }[fc['trigger']]
          if chkstatus
            check.checkdata['procedures'].each do |p|
              next if p['code'] != fc['code']
              ret = (fc['res'] == 'skip' ? CHK_CDSTATUS_SKIPPED : CHK_CDSTATUS_OMITTED_SOFT) if (chkstatus[0].index(p['status']) != nil) == chkstatus[1]
            end
          end
        end
        if fc['type'] == 'S'
          chkstatus = {'Pass' => [[CHK_CDSTATUS_PASSED],true], 'Fail' => [[CHK_CDSTATUS_WARNINGS,CHK_CDSTATUS_FAILED],true], 'Skip' => [[CHK_CDSTATUS_SKIPPED],!fc['inverse']], 'Omit' => [[CHK_CDSTATUS_OMITTED_HARD,CHK_CDSTATUS_OMITTED_SOFT],!fc['inverse']] }[fc['trigger']]
          if chkstatus
            check.checkdata['procedures'].each do |p|
              next if (item.is_a?(Step) && item.procedure.id != p['id'])
              next if (item.is_a?(Measure) && item.step.procedure.id != p['id'])
              p['steps'].each do |s|
                next if s['code'] != fc['code']
                ret = (fc['res'] == 'skip' ? CHK_CDSTATUS_SKIPPED : CHK_CDSTATUS_OMITTED_SOFT) if (chkstatus[0].index(s['status']) != nil) == chkstatus[1]
              end
            end
          end
        end
        if fc['type'] == 'M'
          chkstatus = {'Pass' => [[CHK_CDSTATUS_PASSED],true], 'Fail' => [[CHK_CDSTATUS_WARNINGS,CHK_CDSTATUS_FAILED],true], 'Skip' => [[CHK_CDSTATUS_SKIPPED],!fc['inverse']], 'Omit' => [[CHK_CDSTATUS_OMITTED_HARD,CHK_CDSTATUS_OMITTED_SOFT],!fc['inverse']] }[fc['trigger']]
          if chkstatus
            check.checkdata['procedures'].each do |p|
              next if (item.is_a?(Step) && item.procedure.id != p['id'])
              next if (item.is_a?(Measure) && item.step.procedure.id != p['id'])
              p['steps'].each do |s|
                s['measures'].each do |m|
                  next if m['code'] != fc['code']
                  ret = (fc['res'] == 'skip' ? CHK_CDSTATUS_SKIPPED : CHK_CDSTATUS_OMITTED_SOFT) if (chkstatus[0].index(m['status']) != nil) == chkstatus[1]
                end
              end
            end
          end
          if %w(Yes No).index(fc['trigger'])
            check.checkdata['procedures'].each do |p|
              next if (item.is_a?(Step) && item.procedure.id != p['id'])
              next if (item.is_a?(Measure) && item.step.procedure.id != p['id'])
              p['steps'].each do |s|
                s['measures'].each do |m|
                  next if m['mid'] < 0 || Measure.get_measuretype_by_id(m['mtype'])[:type] != 'flag' || m['code'] != fc['code']
                  mobj = Measurement.find(m['mid'])
                  next if !mobj.rawvalues || mobj.rawvalues == nil
                  ret = (fc['res'] == 'skip' ? CHK_CDSTATUS_SKIPPED : CHK_CDSTATUS_OMITTED_SOFT) if (mobj.value == fc['trigger'].upcase)
                end
              end
            end
          end
        end
        return CHK_CDSTATUS_OMITTED_SOFT if ret == CHK_CDSTATUS_OMITTED_SOFT
      end
    end
    ret
  end

  def self.flowcontrol_dependencies(item,type,dephash)
    code = type == 'M' ? item.step.code+'.'+item.code : item.code
    item.flowcontrol.each do |fc|
      dephash['P'][fc['code']] << [type,code] if fc['type'] == 'P' && %w(Fail Pass Skip Omit).index(fc['trigger'])
      dephash['S'][fc['code']] << [type,code] if fc['type'] == 'S' && %w(Fail Pass Skip Omit).index(fc['trigger'])
      dephash['M'][fc['code']] << [type,code] if fc['type'] == 'M' && %w(Fail Pass Skip Omit Yes No).index(fc['trigger'])
    end
  end

  # -----
  # (Re)calculate workflow enforcements
  # -----

  def recalc_enforce
    pall,plast = true,true
    self.checkdata['procedures'].each do |p|
      # procs
      p['locked'] = (plast ? 0 : 1) if p['enforce'] == PSM_ENFORCE_LASTONE
      p['locked'] = (pall ? 0 : 1) if p['enforce'] == PSM_ENFORCE_ALLPRIOR
      pall = false if CHK_CDSTATUS_ALL_UNFINISHED.index(p['status'])
      plast = !CHK_CDSTATUS_ALL_UNFINISHED.index(p['status'])
      sall,slast = true,true
      p['steps'].each do |s|
        s['locked'] = p['locked']
        s['locked'] = (slast ? 0 : 1) if s['enforce'] == PSM_ENFORCE_LASTONE
        s['locked'] = (sall ? 0 : 1) if s['enforce'] == PSM_ENFORCE_ALLPRIOR
        sall = false if CHK_CDSTATUS_ALL_UNFINISHED.index(s['status'])
        slast = !CHK_CDSTATUS_ALL_UNFINISHED.index(s['status'])
        mall,mlast = true,true
        s['measures'].each do |m|
          m['locked'] = s['locked']
          m['locked'] = (mlast ? 0 : 1) if m['enforce'] == PSM_ENFORCE_LASTONE
          m['locked'] = (mall ? 0 : 1) if m['enforce'] == PSM_ENFORCE_ALLPRIOR
          mall = false if CHK_CDSTATUS_ALL_UNFINISHED.index(m['status'])
          mlast = !CHK_CDSTATUS_ALL_UNFINISHED.index(m['status'])
        end
      end
    end
  end

end