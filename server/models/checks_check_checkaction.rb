class Check < ActiveRecord::Base

  def checkaction(data)
    if data['action'] == 'comment'
      return checkaction_comment(data)
    end
    if data['action'] == 'schedule'
      return checkaction_schedule(data)
    end
    if data['action'] == 'reschedule'
      return checkaction_reschedule(data)
    end
    if data['action'] == 'reassign'
      return checkaction_reassign
    end
    if data['action'] == 'changeassign'
      return checkaction_changeassign(data)
    end
    if data['action'] == 'takeover'
      return checkaction_takeover(data)
    end
    if data['action'] == 'delete'
      return checkaction_delete
    end
    if data['action'] == 'start'
      return checkaction_start
    end
    if data['action'] == 'register'
      return checkaction_register(data)
    end
    if data['action'] == 'unregister'
      return checkaction_unregister(data)
    end
    if data['action'] == 'commit'
      return checkaction_commit(data)
    end
    if data['action'] == 'close'
      return checkaction_close(data)
    end
    if data['action'] == 'cancel'
      return checkaction_cancel(data)
    end
    if data['action'] == 'reopen'
      return checkaction_reopen(data)
    end
    assemble_error('CHECKACTION','INVCOMMAND',{ },[]).rest_fail
  end

  def checkaction_reopen(data)
    return assemble_error('CHECKACTION', 'INVALIDPHASE', {}, []).rest_fail if !self.status || CHK_STATUS_ALL_UNFINISHED.index(self.status)
    return assemble_error('CHECKACTION', 'REQFAILED', {}, []).rest_fail if !data['assignees'] || data['assignees'].size < 1
    self.status = CHK_STATUS_STARTED
    self.started = Time.now
    checkcomplete, hasfails, haswarnings = true, false, false
    self.checkdata['procedures'].each do |p|
      allcommitted = true
      p['steps'].each do |s|
        s['committed'] = nil if data['assignees'].index(s['assignee'])
        allcommitted = false if !s['committed']
        haswarnings = true if s['status'] == CHK_CDSTATUS_WARNINGS
        hasfails = true if s['status'] == CHK_CDSTATUS_FAILED
      end
      p['committed'] = allcommitted
      checkcomplete = false if !allcommitted
    end
    self.status = (hasfails ? CHK_STATUS_FIN_SUCCESS : (haswarnings ? CHK_STATUS_FIN_WARNINGS : CHK_STATUS_FIN_SUCCESS)) if checkcomplete
    self.finished = nil if !checkcomplete
    self.checkdata['closedby'] = nil if !checkcomplete
    save!
    return {}.rest_success
  end

  def checkaction_cancel(data)
    return assemble_error('CHECKACTION', 'INVALIDPHASE', {}, []).rest_fail if !self.status || (self.status != CHK_STATUS_SCHEDULED && (CHK_STATUS_ALL_UNFINISHED.index(self.status) || CHK_STATUS_ALL_CLOSED.index(self.status)))
    self.status = CHK_STATUS_CANCELLED
    self.checkdata['closedby'] = data['userid']
    save!
    return {}.rest_success
  end

  def checkaction_close(data)
    return assemble_error('CHECKACTION', 'INVALIDPHASE', {}, []).rest_fail if !self.status || CHK_STATUS_ALL_UNFINISHED.index(self.status) || CHK_STATUS_ALL_CLOSED.index(self.status)
    self.status = {
        CHK_STATUS_FIN_FAILED => CHK_STATUS_CLOSED_FAILED,
        CHK_STATUS_FIN_WARNINGS => CHK_STATUS_CLOSED_WARNINGS,
        CHK_STATUS_FIN_SUCCESS => CHK_STATUS_CLOSED_SUCCESS
    }[self.status]
    self.checkdata['closedby'] = data['userid']
    save!
    return {}.rest_success
  end

  def checkaction_commit(data)
    return assemble_error('CHECKACTION', 'INVALIDPHASE', {}, []).rest_fail if !self.status || CHK_STATUS_ALL_PRESTART.index(self.status)
    checkcomplete, hasfails, haswarnings = true, false, false
    self.checkdata['procedures'].each do |p|
      allcommitted = true
      p['steps'].each do |s|
        s['committed'] = data['userid'] if (CHK_CDSTATUS_ALL_FINISHED.index(s['status']) && s['completem'] && s['completet'] && !s['committed'] && s['assignee'] >= 0 && self.checkdata['assignees'][s['assignee']]['user_id'] == data['userid'])
        s['committed'] = data['userid'] if (CHK_CDSTATUS_ALL_LEFTOUT_HARD.index(s['status']) && !s['committed'])
        s['committed'] = data['userid'] if (CHK_CDSTATUS_ALL_LEFTOUT_SOFT.index(s['status']) && !s['committed'] && s['assignee'] >= 0 && self.checkdata['assignees'][s['assignee']]['user_id'] == data['userid'])
        allcommitted = false if (!s['committed'])
        haswarnings = true if s['status'] == CHK_CDSTATUS_WARNINGS
        hasfails = true if s['status'] == CHK_CDSTATUS_FAILED
      end
      p['committed'] = allcommitted
      checkcomplete = false if !allcommitted
    end
    if checkcomplete
      self.status = (hasfails ? CHK_STATUS_FIN_FAILED : (haswarnings ? CHK_STATUS_FIN_WARNINGS : CHK_STATUS_FIN_SUCCESS))
      self.finished = Time.now
    else
      self.finished = nil
    end
    save!
    return {}.rest_success
  end

  def checkaction_unregister(data)
    return assemble_error('CHECKACTION', 'INVALIDPHASE', {}, []).rest_fail if !self.status || self.status == CHK_STATUS_INIT
    ass = data['assignment']
    return assemble_error('CHECKACTION', 'REQFAILED', {}, []).rest_fail unless self.checkdata['assignees'][ass]['selfassign']
    return assemble_error('CHECKACTION', 'REQFAILED', {}, []).rest_fail if !self.checkdata['assignees'][ass]['user_id'] || self.checkdata['assignees'][ass]['user_id'] != data['userid']
    assignment = self.checkdata['assignees'][ass]
    assignment.delete('user_id')
    assignment.delete('selfassign')
    if assignment['subof']
      self.checkdata['procedures'].each do |po|
        po['assignee'] = assignment['sub_of'] if po['assignee'] == ass
        po['steps'].each { |so| so['assignee'] = assignment['subof'] if so['assignee'] == ass }
      end
      assign({'remove' => ass})
    end

    # Prüfen, ob Vorabzuweisungen vorhanden sind bei Blöcken, welche nichtmehr
    # zugewiesen bzw. keinem User/Group zugewiesen sind
    check_for_preallocate

    save!
    return {}.rest_success
  end

  def checkaction_register(data)
    return assemble_error('CHECKACTION', 'INVALIDPHASE', {}, []).rest_fail if !self.status || self.status == CHK_STATUS_INIT
    register(data['assignment'], data['proclist'], data['userid'])
    save!
    return {}.rest_success
  end

  def checkaction_start
    return assemble_error('CHECKACTION', 'INVALIDPHASE', {}, []).rest_fail if !self.status || self.status != CHK_STATUS_SCHEDULED
    hasunassigned = false
    self.checkdata['procedures'].each do |po|
      hasunassigned = true if (po['assignee'] == CHK_ASSIGN_UNASSIGNED && po['status'] != CHK_CDSTATUS_OMITTED_HARD)
      po['steps'].each { |so| hasunassigned = true if (so['assignee'] == CHK_ASSIGN_UNASSIGNED && so['status'] != CHK_CDSTATUS_OMITTED_HARD) }
    end
    return assemble_error('CHECKACTION', 'REQFAILED', {}, []).rest_fail if hasunassigned
    self.status = CHK_STATUS_STARTED
    self.started = Time.now
    save!
    return {}.rest_success
  end

  def checkaction_delete
    self.destroy
    return {}.rest_success
  end

  def checkaction_takeover(data)
    assignee = self.checkdata['assignees'][data['assignment']]
    return assemble_error('CHECKACTION', 'REQFAILED', {}, []).rest_fail unless assignee
    assignee['user_id'] = data['userid']
    save!
    return {}.rest_success
  end

  def checkaction_changeassign(data)
    assignee = self.checkdata['assignees'][data['num']]
    return assemble_error('CHECKACTION', 'REQFAILED', {}, []).rest_fail unless assignee
    assignee['user_id'] = data['user_id']
    assignee['group_id'] = data['group_id']
    assignee['regmode'] = data['regmode']
    assignee.delete('user_id') if !assignee['user_id']
    assignee.delete('group_id') if !assignee['group_id']
    assignee.delete('regmode') if !assignee['regmode']

    # Prüfen, ob Vorabzuweisungen vorhanden sind, wenn 'irgendeine Gruppe' und 'irgendein Benutzer'
    # ausgewählt wurden
    if !assignee['user_id'] && !assignee['group_id']
      # Wenn Vorabzuweisungen definiert sind, sollen diese wieder geholt werden
      check_for_preallocate
    end

    save!
    return {}.rest_success
  end

  def checkaction_reassign
    return assemble_error('CHECKACTION', 'INVALIDPHASE', {}, []).rest_fail if !self.status || CHK_STATUS_ALL_PRESTART.index(self.status)
    self.status = CHK_STATUS_SCHEDULED
    self.started = nil
    save!
    return {}.rest_success
  end

  def checkaction_reschedule(data)
    return assemble_error('CHECKACTION', 'INVALIDPHASE', {}, []).rest_fail if self.status != CHK_STATUS_SCHEDULED
    self.scheduled = data['scheduled'] if !$application_parameters[:skipcheckschedule]
    self.dueby = data['dueby'] if !$application_parameters[:skipcheckschedule]
    save!
    return {}.rest_success
  end

  def checkaction_schedule(data)
    return assemble_error('CHECKACTION', 'INVALIDPHASE', {}, []).rest_fail if self.status != CHK_STATUS_INIT
    self.scheduled = data['scheduled'] if !$application_parameters[:skipcheckschedule]
    self.dueby = data['dueby'] if !$application_parameters[:skipcheckschedule]
    if data['assignment'] == CHK_ASSMODE_FREE
      self.status = CHK_STATUS_STARTED
      self.started = Time.now
      assign({'add' => {'regmode' => data['regmode']}, 'assign' => {'assignee' => CHK_ASSIGN_UNASSIGNED, 'target' => {'all' => true}}})
    elsif data['assignment'] == CHK_ASSMODE_FULL
      self.status = CHK_STATUS_STARTED
      self.started = Time.now
      assignee = data['assignee']
      assignee['regmode'] = data['regmode']
      assign({'add' => assignee, 'assign' => {'assignee' => CHK_ASSIGN_UNASSIGNED, 'target' => {'all' => true}}})
    elsif data['assignment'] == CHK_ASSMODE_DETAILED
      self.status = CHK_STATUS_SCHEDULED
    elsif data['assignment'] == CHK_ASSMODE_FREEPREALLOC
      self.status = CHK_STATUS_STARTED
      self.started = Time.now
      assign({'assign' => {'assignee' => CHK_ASSIGN_UNASSIGNED, 'regmode' => data['regmode'], 'target' => {'prealloc' => true}}})
      assign({'add' => {'regmode' => data['regmode']}, 'assign' => {'assignee' => CHK_ASSIGN_UNASSIGNED, 'target' => {'fill' => true}}})
    elsif data['assignment'] == CHK_ASSMODE_DETAILEDPREALLOC
      self.status = CHK_STATUS_SCHEDULED
      assign({'assign' => {'assignee' => CHK_ASSIGN_UNASSIGNED, 'regmode' => data['regmode'], 'target' => {'prealloc' => true}}})
    end
    save!
    return {}.rest_success
  end

  def checkaction_comment(data)
    self.comment = data['comment']
    save!
    return {}.rest_success
  end

  # Prüfen, ob Vorabzuweisungen vorhanden sind bei Blöcken, welche nichtmehr
  # zugewiesen bzw. keinem User/Group zugewiesen sind -- wenn ja, die Prozeduren der
  # Vorabzuweisungen zuweisen
  def check_for_preallocate
    preallocate = self.model.metadata['_preallocate']

    # return value
    has_preallocate = false

    # Vorabzuweisungen vorhanden?
    if preallocate

      has_preallocate = true

      # Prozeduren dieses Checks durchgehen und prüfen, ob diese einem Block zugewiesen sind
      # welche nun keinen assignee (User oder Group) mehr haben
      self.checkdata['procedures'].each do |po|
        proc_preallocate = preallocate[po['rid']]

        # Vorabzuweisungen für diese Prozedur innerhalb dieses Checktypes vorhanden
        if proc_preallocate && proc_preallocate[self.checktype.id]

          # Die hinterlegten Vorabzuweisungen für diese Prozedur im selben Checktype
          used_preallocate = proc_preallocate[self.checktype.id]
          back_to_preallocate = false

          # Assignees-(Blöcke) dieses Checks durchgehen
          self.checkdata['assignees'].each_with_index do |a, i|

            # Überspringen, wenn assignee in der Prozedur nicht der Selbe wie in checkdata.assignees ist (Prozedur
            # gehört nicht zu diesem Assignee)
            next if po['assignee'] != i

            # Prüfen, ob bei momentanem Block kein Assignee hinterlegt ist
            # Wenn der Block nicht assigned ist, user_id oder group_id der Vorabzuweisung assignen
            if !a['user_id'] && !a['group_id']
              if used_preallocate && used_preallocate['user_id'] >= 0
                a['user_id'] = used_preallocate['user_id']
                back_to_preallocate = true
              elsif used_preallocate && used_preallocate['group_id'] >= 0
                a['group_id'] = used_preallocate['group_id']
                back_to_preallocate = true
              end

              # Vorabzuweisungen zuweisen mithilfe des Index, da mit diesem referenziert wird
              if back_to_preallocate
                po['assignee'] = i
                po['steps'].each { |so| so['assignee'] = i }
              end
            end
          end
        end
      end
    end

    return has_preallocate

  end
end