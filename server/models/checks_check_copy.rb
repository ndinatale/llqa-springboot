require_relative '../lib/constants'

class Check < ActiveRecord::Base

  def copy_check(blueprint, proclist)
    self.comment = blueprint.comment
    self.scheduled = blueprint.scheduled
    self.dueby = blueprint.dueby
    self.started = nil
    self.finished = nil
    self.status = CHK_STATUS_SCHEDULED
    self.metadata = JSON.parse(blueprint.metadata.to_json)
    blueprint.images.each do |image|
      Image.merge_data({ owner_id: self.id,
                         owner_type: image.owner_type,
                         fullimage_id: image.fullimage_id,
                         preview_id: image.preview_id,
                         thumbnail_id: image.thumbnail_id,
                         caption: image.caption })
    end
    blueprint.documents.each do |doc|
      Document.merge_data({ owner_id: self.id,
                            owner_type: doc.owner_type,
                            binaryfile_id: doc.binaryfile_id,
                            caption: doc.caption,
                            doctype: doc.doctype })
    end
    self.checkdata['assignees'] = blueprint.checkdata['assignees']
    procmap = {}
    self.checkdata['procedures'].each_with_index { |proc,idx| procmap[proc['code']] = idx }
    blueprint.checkdata['procedures'].each_with_index do |proc,idx|
      next if !proclist.index(proc['code'])
      tgtidx = procmap[proc['code']]
      next if !tgtidx
      next if self.checkdata['procedures'][tgtidx]['id'] != proc['id']
      self.checkdata['procedures'][tgtidx] = JSON.parse(proc.to_json)
    end
    checkall = Hash.new { |h,k| h[k] = [] }
    self.checkdata['procedures'].each do |proc|
      checkall['P'] << proc['code']
      proc['steps'].each do |step|
        checkall['S'] << step['code']
        step['measures'].each do |measure|
          checkall['M'] << measure['code']
          next if !measure['mid'] || measure['mid'] < 0
          mbp = Measurement.find_by(id: measure['mid'])
          return assemble_error('NOTFOUND', 'TEXT', {class: 'Measurement', id: measure['mid']}, []).rest_fail if !mbp
          newmeas = Measurement.merge_data({ check_id: self.id,
                                             measure_id: mbp.measure_id,
                                             toolunit_id: mbp.toolunit_id,
                                             rawvalues: JSON.parse(mbp.rawvalues.to_json),
                                             value: mbp.value,
                                             comment: mbp.comment,
                                             metadata: mbp.metadata,
                                             savedon: mbp.savedon,
                                             savedby: mbp.savedby,
                                             status: mbp.status }).last
          measure['mid'] = newmeas.id
        end
      end
    end
    recalc_workflow(checkall)
    save!
  end

end