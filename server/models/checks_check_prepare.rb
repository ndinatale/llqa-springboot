require_relative '../lib/constants'

class Check < ActiveRecord::Base

  def preinit_check(type, item)
    self.metadata = { 'testrun' => { 'type' => type, 'item' => item } }
    data = {
        'action' => 'schedule',
        'scheduled' => Time.now-86400,
        'dueby' => Time.now+86400*14,
        'assignment' => 1,
        'assignee' => get_current_user,
        'regmode' => 2 }
    puts self.checkaction(data)
  end

  def prepare_check
    self.status = 0
    dependencies = Hash.new { |ho,ko| ho[ko] = Hash.new { |hi,ki| hi[ki] = [] }}
    self.checkdata = {
        'assignees' => [],
        'procedures' => [],
        'procids' => [],
        'stepids' => [],
        'measids' => [],
        'dependencies' => dependencies
    }
    self.model.activeprocedures.each do |ap|
      pinfo = {
          'assignee' => -1,
          'ap_id' => ap.id,
          'rid' => ap.procedure.realid,
          'id' => ap.procedure.id,
          'status' => CHK_CDSTATUS_TODO,
          'code' => ap.procedure.code,
          'committed' => false,
          'steps' => [],
          'locked' => 0,
          'enforce' => ap.enforce
      }
      Check.flowcontrol_dependencies(ap.procedure,'P',dependencies)
      ap.procedure.steps.each do |s|
        sinfo = {
            'assignee' => -1,
            'id' => s.id,
            'status' => CHK_CDSTATUS_TODO,
            'code' => s.code,
            'committed' => nil,
            'measures' => [],
            'completem' => false,
            'completet' => false,
            'locked' => 0,
            'enforce' => s.enforce,
            'steptype' => s.steptype,
        }
        Check.flowcontrol_dependencies(s,'S',dependencies)
        s.measures.each do |m|
          minfo = {
              'id' => m.id,
              'mid' => -1,
              'status' => CHK_CDSTATUS_TODO,
              'cstatus' => CHK_CDSTATUS_TODO,
              'mtype' => m.measuretype,
              'code' => s.code+'.'+m.code,
              'needstool' => m.tooltype_id,
              'cmplcode' => m.calculation['complcode'],
              'locked' => 0,
              'enforce' => m.enforce
          }
          Check.flowcontrol_dependencies(m,'M',dependencies)
          sinfo['measures'] << minfo
          self.checkdata['measids'] << m.id
        end
        pinfo['steps'] << sinfo
        self.checkdata['stepids'] << s.id
      end
      self.checkdata['procedures'] << pinfo
      self.checkdata['procids'] << ap.procedure.id
    end
    recalc_workflow(nil)
    save!
  end

end
