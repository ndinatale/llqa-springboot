require_relative '../lib/constants'

class Check < ActiveRecord::Base

  # -----
  # Assign (users to steps/procedures in a check)
  # -----

  def assign(assigndata)
    if assigndata['remove']
      return assign_remove(assigndata)
    end
    if assigndata['unassign']
      return assign_unassign(assigndata)
    end
    if assigndata['assign']
      assign_assign(assigndata)
    end
  end

  def assign_assign(assigndata)
    self.checkdata['assignees'] << assigndata['add'] if (assigndata['add'])
    who, to = (assigndata['add'] ? self.checkdata['assignees'].size - 1 : assigndata['assign']['assignee']), assigndata['assign']['target']
    if to['prealloc']
      assign_assign_prealloc(assigndata)
    end
    if to['fill']
      assign_assign_fill(assigndata, who)
    end
    if to['all']
      assign_assign_all(who)
    end
    if to['procedures']
      assign_assign_procedures(to, who)
    end
    if to['steps']
      assign_assign_steps(to, who)
    end
    save!
  end

  def assign_assign_steps(to, who)
    self.checkdata['procedures'].each do |po|
      poass = nil
      po['steps'].each do |so|
        so['assignee'] = who if to['steps'].index(so['id'])
        poass = so['assignee'] unless poass
        poass = CHK_ASSIGN_DETAILED if poass != so['assignee']
      end
      po['assignee'] = poass || CHK_ASSIGN_UNASSIGNED
    end
  end

  def assign_assign_procedures(to, who)
    self.checkdata['procedures'].each do |po|
      next unless to['procedures'].index(po['id'])
      po['assignee'] = who
      po['steps'].each { |so| so['assignee'] = who }
    end
  end

  def assign_assign_all(who)
    self.checkdata['procedures'].each do |po|
      po['assignee'] = who
      po['steps'].each { |so| so['assignee'] = who }
    end
  end

  def assign_assign_fill(assigndata, who)
    remadd = true
    self.checkdata['procedures'].each do |po|
      next if po['assignee'] != CHK_ASSIGN_UNASSIGNED
      remadd = false
      po['assignee'] = who
      po['steps'].each { |so| so['assignee'] = who }
    end
    self.checkdata['assignees'].pop if (assigndata['add'] && remadd)
  end

  def assign_assign_prealloc(assigndata)
    preallocate = self.model.metadata['_preallocate']
    if preallocate
      self.checkdata['procedures'].each do |po|
        proc_preall = preallocate[po['rid']]
        if proc_preall && proc_preall[self.checktype.id]
          used_preall = proc_preall[self.checktype.id]
          assignee = nil
          self.checkdata['assignees'].each_with_index do |ass, i|
            assignee = i if (used_preall['user_id'] >= 0 && ass['user_id'] == used_preall['user_id'])
            break if assignee
            assignee = i if (used_preall['group_id'] >= 0 && ass['group_id'] == used_preall['group_id'])
            break if assignee
            assignee = i if (!ass['user_id'] && !ass['group_id'])
            break if assignee
          end
          if !assignee
            if used_preall['user_id'] >= 0
              self.checkdata['assignees'] << {'user_id' => used_preall['user_id']}
            elsif used_preall['group_id'] >= 0
              self.checkdata['assignees'] << {'group_id' => used_preall['group_id'], 'regmode' => assigndata['assign']['regmode']}
            else
              self.checkdata['assignees'] << {'regmode' => assigndata['assign']['regmode']}
            end
            assignee = self.checkdata['assignees'].size - 1
          end
          po['assignee'] = assignee
          po['steps'].each { |so| so['assignee'] = assignee }
        end
      end
    end
  end

  def assign_unassign(assigndata)
    who = assigndata['remove']
    self.checkdata['procedures'].each do |po|
      poass = nil
      po['steps'].each do |so|
        so['assignee'] = CHK_ASSIGN_UNASSIGNED if so['assignee'] == who
        poass = so['assignee'] unless poass
        poass = CHK_ASSIGN_DETAILED if poass != so['assignee']
      end
      po['assignee'] = poass || CHK_ASSIGN_UNASSIGNED
    end
    save!
    return
  end

  def assign_remove(assigndata)
    who = assigndata['remove']
    self.checkdata['assignees'].each do |ass|
      next if !ass['subof']
      if ass['subof'] == who
        ass.delete('subof')
        ass['regmode'] = self.checkdata['assignees'][who]['regmode']
      else
        ass['subof'] -= 1 if ass['subof'] > who
      end
    end
    self.checkdata['assignees'].delete_at(who)
    self.checkdata['procedures'].each do |po|
      poass = nil
      po['steps'].each do |so|
        so['assignee'] = (so['assignee'] == who ? -1 : (so['assignee'] > who ? so['assignee']-1 : so['assignee']))
        poass = so['assignee'] unless poass
        poass = CHK_ASSIGN_DETAILED if poass != so['assignee']
      end
      po['assignee'] = poass || CHK_ASSIGN_UNASSIGNED
    end
    save!
    return
  end

  # -----
  # Registering (users to allocated blocks)
  # -----

  def register(assignment, proclist, user_id)
    return assemble_error('CHECKACTION','REQFAILED',{ },[]).rest_fail if self.checkdata['assignees'][assignment]['user_id']
    regprocs,regsteps,has_remaining = [],[],false
    self.checkdata['procedures'].each do |po|
      if po['assignee'] == assignment
        if proclist.index(po['code'])
          regprocs << po['id']
        else
          has_remaining = true
        end
      elsif po['assignee'] == CHK_ASSIGN_DETAILED
        po['steps'].each do |so|
          if so['assignee'] == assignment
            if proclist.index(po['code'])
              regsteps << so['id']
            else
              has_remaining = true
            end
          end
        end
      end
    end
    append, has_subs = CHK_ASSIGN_UNASSIGNED,false
    self.checkdata['assignees'].each_with_index do |ass,idx|
      if ass['subof'] == assignment
        has_subs = true
        append = idx if ass['user_id'] == user_id
      end
    end
    if !has_remaining && !has_subs
      self.checkdata['assignees'][assignment]['user_id'] = user_id
      self.checkdata['assignees'][assignment]['selfassign'] = true
    else
      if append < 0
        assign({ 'add' => { 'user_id' => user_id, 'regmode' => CHK_REGMODE_APPENDED, 'subof' => assignment, 'selfassign' => true }, 'assign' => { 'assignee' => CHK_ASSIGN_UNASSIGNED, 'target' => { 'procedures' => regprocs, 'steps' => regsteps } } } )
      else
        assign({ 'assign' => { 'assignee' => append, 'target' => { 'procedures' => regprocs, 'steps' => regsteps } } } )
      end
      if !has_remaining
        assign({ 'remove' => assignment })
      end
    end
  end

end