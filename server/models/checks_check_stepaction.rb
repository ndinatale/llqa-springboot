require_relative '../lib/constants'

class Check < ActiveRecord::Base

  def stepaction(data,entrystep)
    if data['action'] == 'savemeasure'
      return stepaction_savemeasure(data, entrystep)
    end
    if data['action'] == 'savetool'
      return stepaction_savetool(data, entrystep)
    end
    assemble_error('CHECKACTION','INVCOMMAND',{ },[]).rest_fail
  end

  def stepaction_savetool(data, entrystep)
    return assemble_error('STEPACTION', 'INVALIDPHASE', {}, []).rest_fail if !self.status || CHK_STATUS_ALL_PRESTART.index(self.status)
    return assemble_error('STEPACTION', 'REQFAILED', {}, []).rest_fail if CHK_CDSTATUS_ALL_LEFTOUT.index(entrystep['status'])
    changes = {'M' => []}
    entrystep['measures'].each do |cdm|
      measure = Measure.find_by(id: cdm['id'])
      return assemble_error('NOTFOUND', 'TEXT', {class: 'Measures', id: data['measure_id']}, []).rest_fail if !measure
      next if !measure.tooltype_id || measure.tooltype_id != data['tooltype_id']
      if cdm['mid'] < 0
        _, _, mmnt = Measurement.merge_data({check_id: self.id, measure_id: measure.id, status: MMNT_STATUS_UNPROC, rawvalues: {}, value: nil, comment: nil, toolunit_id: data['toolunit_id']})
        cdm['mid'] = mmnt.id
        mmnt
      else
        mmnt = Measurement.find_by(id: cdm['mid'])
        return assemble_error('NOTFOUND', 'TEXT', {class: 'Measurement', id: cdm['mid']}, []).rest_fail if !measure
        mmnt.toolunit_id = data['toolunit_id']
        mmnt.save!
        mmnt
      end
      cdm['needstool'] = data['toolunit_id'] ? nil : measure.tooltype_id
      changes['M'] << cdm['code']
    end
    recalc_workflow(changes)
    entrystep['committed'] = false
    self.checkdata['procedures'].each { |p| p['committed'] = false if p['steps'].index { |s| s['id'] == entrystep['id'] } }
    save!
    return {}.rest_success
  end

  def stepaction_savemeasure(data, entrystep)
    return assemble_error('STEPACTION', 'INVALIDPHASE', {}, []).rest_fail if !self.status || CHK_STATUS_ALL_PRESTART.index(self.status)
    return assemble_error('STEPACTION', 'REQFAILED', {}, []).rest_fail if CHK_CDSTATUS_ALL_LEFTOUT.index(entrystep['status'])
    measure = Measure.find_by(id: data['measure_id'])
    return assemble_error('NOTFOUND', 'TEXT', {class: 'Measures', id: data['measure_id']}, []).rest_fail if !measure
    entrymeas = nil
    entrystep['measures'].each { |m| entrymeas = m if m['id'] == measure.id }
    return assemble_error('NOTFOUND', 'CHECKMTEXT', {cid: self.id, sid: entrystep['id'], mid: measure.id}, []).rest_fail if !entrymeas
    return assemble_error('STEPACTION', 'REQFAILED', {}, []).rest_fail if CHK_CDSTATUS_ALL_OMITTED.index(entrymeas['status'])

    measgroup = []
    if entrymeas['cmplcode']
      entrystep['measures'].each do |m|
        next if m['cmplcode'] != entrymeas['cmplcode']
        objmmnt = nil
        if m['mid'] >= 0
          objmmnt = Measurement.find_by(id: m['mid'])
          return assemble_error('NOTFOUND', 'TEXT', {class: 'Measurement', id: m['mid']}, []).rest_fail if !objmmnt
        end
        if m['id'] == entrymeas['id']
          measgroup.unshift({entry: m, mobj: objmmnt, obj: measure})
        else
          objmeas = Measure.find_by(id: m['id'])
          return assemble_error('NOTFOUND', 'TEXT', {class: 'Measures', id: m['id']}, []).rest_fail if !objmeas
          measgroup.push({entry: m, mobj: objmmnt, obj: objmeas})
        end
      end
    else
      objmmnt = nil
      if entrymeas['mid'] >= 0
        objmmnt = Measurement.find_by(id: entrymeas['mid'])
        return assemble_error('NOTFOUND', 'TEXT', {class: 'Measurement', id: entrymeas['mid']}, []).rest_fail if !objmmnt
      end
      measgroup.push({entry: entrymeas, mobj: objmmnt, obj: measure})
    end

    #dump_checkdata
    measgroup.each_with_index do |measpart, idx|
      p measpart
      rawdata = idx == 0 ? data['rawvalues'] : (measpart[:mobj] ? measpart[:mobj].rawvalues : nil)
      #next if !rawdata
      vt = ValueTester.new(rawdata, measpart[:entry], measpart[:obj], measgroup)
      next if !vt.check
      mmnt = if !measpart[:mobj]
               _, _, mmnt = Measurement.merge_data({check_id: self.id, measure_id: measpart[:obj].id, status: vt.newstatus, rawvalues: rawdata, value: vt.stringify_value, comment: data['comment'], metadata: {'_commentby' => get_current_user}})
               measpart[:entry]['mid'] = mmnt.id
               measpart[:mobj] = mmnt
               mmnt
             else
               measpart[:mobj].status = vt.newstatus
               measpart[:mobj].value = vt.stringify_value
               if idx == 0
                 measpart[:mobj].metadata['_commentby'] = get_current_user if measpart[:mobj].comment != data['comment']
                 measpart[:mobj].comment = data['comment']
                 measpart[:mobj].rawvalues = data['rawvalues']
               end
               measpart[:mobj].save!
               measpart[:mobj]
             end
      measpart[:entry]['status'] = measpart[:entry]['cstatus'] = {
          MMNT_STATUS_UNPROC => CHK_CDSTATUS_TODO,
          MMNT_STATUS_PASSED => CHK_CDSTATUS_PASSED,
          MMNT_STATUS_FAILED => (measpart[:obj].calculation['internal'] && measpart[:obj].calculation['internal'] == 1) ? CHK_CDSTATUS_WARNINGS : CHK_CDSTATUS_FAILED,
          MMNT_STATUS_SKIPPED => CHK_CDSTATUS_SKIPPED,
          MMNT_STATUS_INVALID => CHK_CDSTATUS_TODO,
          MMNT_STATUS_ERROR => CHK_CDSTATUS_TODO }[mmnt.status]
      if measpart[:entry]['mid'] == entrymeas['mid']
        measpart[:mobj].savedby = data['userid']
        measpart[:mobj].savedon = Time.now.to_i
        measpart[:mobj].save!
      end
      recalc_workflow({'M' => [measpart[:entry]['code']]}, {'M' => [measpart[:entry]['code']]})
      entrystep['committed'] = false
      self.checkdata['procedures'].each { |p| p['committed'] = false if p['steps'].index { |s| s['id'] == entrystep['id'] } }
    end

    #dump_checkdata
    save!
    return {}.rest_success
  end

end