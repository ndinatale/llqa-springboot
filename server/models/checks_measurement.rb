require_relative '../lib/constants'

class Measurement < ActiveRecord::Base
  include ModelBase
  include Searching

  # -----
  # Table setup
  # -----

  serialize :rawvalues
  serialize :metadata, Hash

  belongs_to :check
  belongs_to :measure
  belongs_to :toolunit
  has_many :images, -> { order('seqnum ASC') }, as: :owner, dependent: :destroy
  has_many :documents, -> { order('seqnum ASC') }, as: :owner, dependent: :destroy

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.references :check, null: false
      t.references :measure, null: false
      t.references :toolunit
      t.text :rawvalues
      t.string :value
      t.string :comment
      t.integer :status, null: false, default: MMNT_STATUS_UNPROC 
      augment_table_usertag t
    end
  end
  def self.update_table_01(m,tn)
    m.change_table tn do |t|
      t.text :metadata
    end
  end
  def self.update_table_02(m,tn)
    m.change_table tn do |t|
      t.integer :savedon
      t.integer :savedby
    end
    Check.all.each do |check|
      check.checkdata['procedures'].each do |p|
        p['steps'].each do |s|
          s['measures'].each do |m|
            if m['mid'] >= 0
              msmnt = Measurement.find_by(id: m['mid'])
              if msmnt
                msmnt.savedon = m['savedon']
                msmnt.savedby = m['savedby']
                msmnt.save!
              end
              m.delete 'savedon'
              m.delete 'savedby'
            end
          end
        end
      end
      check.save!
    end
  end

  # -----
  # Global search
  # -----

  def self.searchable_fields
    {
        comment: :text
    }
  end
  def self.search_result_extend
    %w(check.unit.model measure.step.procedure)
  end

  # -----
  # Aggregation and helper methods
  # -----

  def applicable_unit
    measure.applicable_unit
  end

  def get_checkdata_block
    self.check.checkdata['procedures'].each do |p|
      p['steps'].each do |s|
        s['measures'].each do |m|
          return m if m['mid'] == self.id
        end
      end
    end
    return nil
  end

end
