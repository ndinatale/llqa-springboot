class Activechecktype < ActiveRecord::Base
  include ModelBase

  # -----
  # Table setup
  # -----

  belongs_to :model
  belongs_to :checktype

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.references :model
      t.references :checktype
      augment_table_usertag t
    end
  end

  # -----
  # DB hooks
  # -----

  def has_been_created(init)
    return if !model || !checktype
    model.log_change(checktype.code, 'checktype', 'createactct', { initdata: init }, true)
  end

  def has_been_updated(changes)
    # no action
  end

  def has_been_deleted
    return if !model || !checktype
    model.log_change(checktype.code, 'checktype', 'deleteactct', {}, true)
  end

end
