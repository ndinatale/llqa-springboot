class Activeprocedure < ActiveRecord::Base
  include ModelBase
  include Ordering

  # -----
  # Table setup
  # -----

  belongs_to :model
  belongs_to :procedure
  serialize :flowcontrol, Array

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.references :model
      t.references :procedure
      t.text :flowcontrol
      augment_table_usertag t
      augment_table_ordering t
    end
  end
  def self.update_table_01(m,tn)
    m.change_table tn do |t|
      t.integer :enforce, null:false, default:0
    end
  end

  # -----
  # DB hooks
  # -----

  def has_been_created(init)
    return if !model || !procedure
    model.log_change(procedure.code, 'procedure', 'createactp', { initdata: init }, true)
  end

  def has_been_updated(changes)
    return if !model || !procedure
    changes.each do |k,v|
      if k == 'procedure_id'
        newproc = Procedure.find(v[1])
        model.log_change(procedure.code, 'procedure', 'changepid', { version: newproc.version }, true)
      elsif k == 'enforce'
        model.log_change(procedure.code, 'procedure', 'changefield', { field: k, olddata: v[0] ? v[0].to_json : nil, newdata: v[1] ? v[1].to_json : v[1] }, true)
      elsif k == 'seqnum'
        model.log_change(procedure.code, 'procedure', 'changeseq', { changes: v }, true)
      end
    end
  end

  def has_been_deleted
    return if !model || !procedure
    model.log_change(procedure.code, 'procedure', 'deleteactp', {}, true)
  end

  # -----
  # Reordering
  # -----

  def reorder_parentset
    self.model.activeprocedures
  end

end

