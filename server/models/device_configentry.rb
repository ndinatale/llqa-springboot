class Configentry < ActiveRecord::Base
  include ModelBase
  include Searching

  # -----
  # Table setup
  # -----

  belongs_to :configtable

  serialize :blocked_by, Hash

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.integer :code_id # this used to be string and will break the "Teilauslieferung"!
                        # also renaming because there's a lot of logic behind "code"
      t.references :configtable
      t.boolean :active, default: false
      t.boolean :deleted, default: false # flag if no longer in use
      t.string :col1
      t.string :col2
      t.string :col3
      t.string :col4
      t.string :col5
      t.string :col6
      t.text :blocked_by
      t.boolean :blocked, default: false
      t.text :timeline
      augment_table_usertag t
    end
  end

  # -----
  # Global search
  # -----

  def self.searchable_fields
    {
        col1: :text,
        col2: :text,
        col3: :text,
        col4: :text,
        col5: :text,
        col6: :text,
        deleted: :boolean
    }
  end

  def set_status(status, comment = nil)
    p status,comment
    self.status = status
    self.timeline = [] if !(self.timeline.is_a? Array)
    return if timeline.size > 0 && timeline[-1]['new_status'] == status
    self.timeline << {
        'new_status' => status,
        'timestamp' => Time.now.to_i,
        'user' => get_current_user_id,
        'comment' => comment
    }
    self.save!
  end

  def prepare_rest(hash_out)
    hash_out['users'] = uhash = {}
    self.timeline.each do |event|
      next if !event || !event['user']
      begin
        user = User.find(event['user'])
        uhash[user.id] = { 'realname' => user.realname, 'username' => user.username, 'status' => user.status }
      rescue
      end
    end
  end

  def self.export_to_hash(id, forexport=true)
    get_single(id, forexport).serialize do |h,o|
      h.clean_export_hash
    end
  end

  def self.import_from_hash(hash)
    configentry = Configentry.new hash
    configentry.save!
    configentry
  end

  def self.get_new_entry_code(table_id)
    # Get new code_id for a entry to be persisted. This only has table scope and will start at 1 by default.
    sql = "SELECT coalesce(max(code_id) + 1, 1) FROM configentries WHERE configtable_id = " + table_id.to_s
    connection.select_all(sql)[0][0]
  end

  def self.find_by_table(table_id, entry_id)
    return Configentry.find_by(configtable: table_id, code_id: entry_id)
  end

  def add_blocker(id, type)
    # Adds a new blocking procedure, step, measure
    # type must be in 'p', 's' or 'm'
    if self.blocked_by.empty?
      # initializing blocked_by as a hash of sets
      self.blocked_by = {
          'p' => [],
          's' => [],
          'm' => []
      }
    end

    self.blocked_by[type] << id.to_i
    self.blocked = true
    self.save!
  end

  def remove_blocker(id, type)
    # Removes a blocker from the entry.
    # type must be in 'p', 's' or 'm'

    if self.blocked_by != nil && !self.blocked_by.empty?
      self.blocked_by[type].delete(id)

      # delete completely if last blocker removed
      if self.blocked_by['p'].empty? && self.blocked_by['s'].empty? && self.blocked_by['m'].empty?
        self.blocked_by = nil
        self.blocked = false
      end
    else
      self.blocked = false
    end
    self.save!
  end

end