class Configtable < ActiveRecord::Base
  include ModelBase
  include Searching

  # -----
  # Table setup
  # -----

  #serialize :title
  #serialize :description
  #serialize :metadata, Hash

  has_many :configentries, dependent: :destroy

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.string :title
      t.string :parent # parent type: device / unit / check (Modell / Einheit / Prüfung)
      t.integer :parentid
      t.string :parentcode
      t.string :colheader1
      t.string :colheader2
      t.string :colheader3
      t.string :colheader4
      t.string :colheader5
      t.string :colheader6
      t.text :timeline
      t.boolean :editable, null: false, default: true # Tabelle editierbar? <-> Prüfungen verknüpft
      t.boolean :deleted, null:false, default: false # Tabelle gelöscht?
      augment_table_usertag t # this will add timestamps, :created_by, :updated_by
      #augment_table_version t # this will add :realid, :version, :disabled
    end
  end

  # -----
  # Global search
  # -----

  def self.searchable_fields
    {
        title: :text
    }
  end

  def set_status(status, comment = nil)
    p status,comment
    self.status = status
    self.timeline = [] if !(self.timeline.is_a? Array)
    return if timeline.size > 0 && timeline[-1]['new_status'] == status
    self.timeline << {
        'new_status' => status,
        'timestamp' => Time.now.to_i,
        'user' => get_current_user_id,
        'comment' => comment
    }
    self.save!
  end

  def prepare_rest(hash_out)
    hash_out['users'] = uhash = {}
    self.timeline.each do |event|
      next if !event || !event['user']
      begin
        user = User.find(event['user'])
        uhash[user.id] = { 'realname' => user.realname, 'username' => user.username, 'status' => user.status }
      rescue
      end
    end
  end

  def self.export_to_hash(id, forexport=true, complete=false)
    get_single(id, forexport).serialize(%w(configentries)) do |h,o|
      h.clean_export_hash
    end
  end

  def self.import_from_hash(hash)
    configentries = hash.delete 'configentries'

    configtable = Configtable.new hash
    stamp_realid(configtable)
    configtable.parent = nil
    configtable.parentid = nil
    configtable.parentcode = nil
    configtable.deleted = false
    configtable.editable = true

    configtable.save!

    configentries.each do |configentry|
      # removing blocking procedures
      configentry['blocked_by'] = nil
      configentry['blocked'] = false

      # setting new configtable id
      configentry['configtable_id'] = configtable.id
      Configentry.import_from_hash(configentry)
    end

    configtable
  end

end