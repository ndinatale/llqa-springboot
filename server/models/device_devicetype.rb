class Devicetype < ActiveRecord::Base
  include ModelBase
  include Searching

  # -----
  # Table setup
  # -----

  before_validation { complete_languageblock(false, :title, :description) }

  validates :code, codevalid: true
  validates :title, language: { complete: :all, minlength: 3 }
  validates :description, language: { complete: :none }

  serialize :title
  serialize :description
  serialize :metadata

  has_many :models
  has_many :images, -> { order('seqnum ASC') }, as: :owner, dependent: :destroy
  has_many :documents, -> { order('seqnum ASC') }, as: :owner, dependent: :destroy

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.string :code, null:false
      t.text :title, null:false
      t.text :description
      t.text :metadata
      t.boolean :deleted, null:false, default:false
      augment_table_usertag t
    end
  end

  # -----
  # Global search
  # -----

  def self.searchable_fields
    {
        code: :text,
        title: :localized_text,
        description: :localized_text
    }
  end

end
