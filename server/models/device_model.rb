require_relative 'device_model_preallocate'

class Model < ActiveRecord::Base
  include ModelBase
  include Versioning
  include Searching

  # -----
  # Table setup
  # -----

  before_validation { complete_languageblock(false, :title, :description) }

  validates :code, codevalid: { scope: 'realid' }
  validates :title, language: { complete: :all, minlength: 3 }
  validates :description, language: { complete: :none }
  validates :devicetype_id, reference: { notnull: true, refclass: 'Devicetype' }

  serialize :title
  serialize :description
  serialize :metadata, Hash

  belongs_to :devicetype
  has_many :units, dependent: :destroy
  has_many :images, -> { order('seqnum ASC') }, as: :owner, dependent: :destroy
  has_many :documents, -> { order('seqnum ASC') }, as: :owner, dependent: :destroy
  has_many :activeprocedures, -> { order('seqnum ASC') }, dependent: :destroy
  has_many :activechecktypes, dependent: :destroy
  has_many :datachanges, -> { order('timestamp ASC') }, as: :source

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.references :devicetype, null:false
      t.string :code, null:false
      t.text :title, null:false
      t.text :description
      t.text :metadata
      augment_table_version t
      augment_table_usertag t
    end
  end

  # -----
  # Global search
  # -----

  def self.searchable_fields
    {
        code: :text,
        title: :localized_text,
        description: :localized_html
    }
  end

  def self.get_entity_array(fields, limitation = :none)
    fields += %w(realid version)
    return self.order(updated_at: :desc).select(fields).all if limitation == :none
    return self.order(updated_at: :desc).where(version: nil).select(fields).all if limitation == :edit_version
    return self.order(updated_at: :desc).where.not(version: nil).select(fields).all if limitation == :finalized
    ret = {}
    self.order(updated_at: :desc).where.not(version: nil).select(fields).all.each do |entity|
      ret[entity.realid] = entity if(!ret[entity.realid] || (entity.version > ret[entity.realid].version))
    end
    ret.values
  end

  # -----
  # DB hooks
  # -----

  def has_been_created(init)
    log_change(self.code, 'model', 'create', { initdata: init }, true)
  end

  def has_been_updated(changes)
    changes.each do |k,v|
      if k == 'code'
        log_change(self.code, 'model', 'changecode', { oldcode: v[0], newcode: v[1]}, true)
      elsif (%w(title description realid).index(k))
        log_change(self.code, 'model', 'changefield', { field: k, olddata: v[0] ? v[0].to_json : nil, newdata: v[1] ? v[1].to_json : v[1] }, true)
      elsif k == 'prealloc'
        log_change(self.code, 'model', 'preallocation', { proc_realid: v }, true)
      elsif k == 'version'
        log_change(self.code, 'model', 'finalize', { version: v[1] }, false)
      elsif k == 'disabled'
        log_change(self.code, 'model', 'delete', { type: 'disabled' }, false)
      end
    end
  end

  def has_been_deleted
    log_change(self.code, 'model', 'delete', { type: 'deleted' }, false)
  end

  # -----
  # Versioning
  # -----

  def log_change(obj, objtype, type, details, dirty)
    Datachange.log_change(self, obj, objtype, type, details, (self.version != nil) && dirty, (self.version == nil) && dirty)
  end

  def is_dirty?
    Datachange.where(source: self, dirty: true).size > 0
  end

  def recurse_finalize
    [[:images,:owner_id], [:documents,:owner_id], [:activeprocedures,:model_id], [:activechecktypes,:model_id]]
  end

  def update_after_finalize
    Unit.all.each do |unit|
      if unit.model.realid == self.realid && unit.model.version != self.version
        unit.model_id = self.id
        unit.save!
      end
    end
  end

  def effective_version
    self.version
  end

  def effective_realid
    self.realid
  end

  # -----
  # Import/Export
  # -----

  def self.import_from_hash(hash,changecode = true)
    activeprocedures = hash.delete 'activeprocedures'
    activechecktypes = hash.delete 'activechecktypes'
    images = hash.delete 'images'
    documents = hash.delete 'documents'
    if changecode
      cnt = 1
      hash['code'].gsub!(/\(\d+\)\z/,'');
      cnt += 1 while Model.where(code: '%s(%d)' % [hash['code'],cnt]).count > 0
      hash['code'] = '%s(%d)' % [hash['code'],cnt]
    end
    model = Model.new hash
    stamp_realid(model)
    stamp_seqnum(model)
    model.has_been_created(hash)
    model.save!
    activechecktypes.each { |act| act['model_id'] = model.id; Activechecktype.import_from_hash(act,false) }
    activeprocedures.each { |apr| apr['model_id'] = model.id; Activeprocedure.import_from_hash(apr,false) }
    images.each { |image| image['owner_id'] = step.id; Image.import_from_hash(image,false,blobs) }
    documents.each { |document| document['owner_id'] = step.id; Document.import_from_hash(document,false,blobs) }
    model
  end

  def self.export_to_hash(id, forexport=true, complete=false)
    get_single(id, forexport).serialize(%w(activeprocedures activechecktypes images documents)) do |h,o|
      if complete
        h['blobs'] = {}
        o.images.each { |img| img.export_blob(h['blobs']) }
        o.documents.each { |doc| doc.export_blob(h['blobs']) }
      end
      h.clean_export_hash
    end
  end

  # -----
  # Aggregation and helper methods
  # -----

  def counters
    ret = { 'total' => 0, 'open' => 0, 'closed' => 0, 'discarded' => 0 }
    curv = Model.find_current(self.realid)
    return ret if !curv || curv.size < 1
    curv[0].units.each do |u|
      next if u.status >= 100

      # fix for LLQA-200
      # puts "unit: #{u.inspect}"
      next if u.code.end_with? "(TR)"

      ret['total'] += 1
      case u.status
        when 10
          ret['closed'] += 1
        when 20
          ret['discarded'] += 1
        when 100
          ret['closed'] += 1
        when 110
          ret['closed'] += 1
        when 120
          ret['discarded'] += 1
        else
          ret['open'] += 1
      end
    end
    return ret
  end

  def self.using_procedure(pri = nil, detailed = false)
    ret = Hash.new { |h,k| h[k] = [[],[]] }
    lvers = Hash.new { |h,k| h[k] = Procedure.latest_version(k) }
    Model.trunk_objects.each do |mod|
      next if mod.code.end_with?'(TR)'
      mod.activeprocedures.each do |apr|
        next if pri && apr.procedure.realid != pri
        ret[apr.procedure.realid][0] << (detailed ? [mod.id, mod.serializable_hash, apr.procedure.version] : [mod.id, mod.code])
        if apr.procedure.version != lvers[apr.procedure.realid]
          ret[apr.procedure.realid][1] << [mod.id, mod.code]
        end
      end
    end
    return ret[pri] if pri
    ret
  end

  def self.update_procedure(pri)
    lvobject = Procedure.find_current(pri)[0]
    Model.trunk_objects.each do |mod|
      mod.activeprocedures.each do |apr|
        if apr.procedure.realid == pri && apr.procedure.version != lvobject.version
          apr.has_been_updated({'procedure_id' => [apr.procedure_id, lvobject.id]})
          apr.procedure_id = lvobject.id
          apr.save!
          lvers = Model.find_current(mod.realid)[0]
          next if !lvers
          clone = lvers.clone_version
          clone.activeprocedures.each do |iapr|
            if iapr.procedure.realid == pri
              iapr.has_been_updated({'procedure_id' => [iapr.procedure_id, lvobject.id]})
              iapr.procedure_id = lvobject.id
              iapr.save!
            end
          end
        end
      end
    end
  end

  # -----
  # Comments
  # -----

  def self.all_comments(id)
    comments = []
    element = Model.find(id)
    element = Model.latest_version_object(element.realid)
    ret = { comments: comments, element: element }
    element.units.each do |unit|
      comments << { type: 'unit', obj: unit, text: unit.comment, owner: unit.metadata['_commentby'], children: [] }
    end
    ret
  end

  # -----
  # Configtable
  # -----

  def self.get_table_id(model_code)
    # get the configuration table associated with this model
    table = Configtable.select('id').where("parent = 'model' AND parentcode = ?", model_code).first
    return table.id if table
    return nil
  end

end
