class Model < ActiveRecord::Base

  def preallocate(proc_rid, ctype_id, user_id, group_id)
    group_id = -1 if user_id >= 0
    valid_actprocs,valid_actctype = {},{}
    self.activeprocedures.each { |ap| valid_actprocs[ap.procedure.realid] = true }
    self.activechecktypes.each { |ac| valid_actctype[ac.checktype.id] = true }
    preallocate = self.metadata['_preallocate']
    preallocate = {} if !preallocate
    preallocate[proc_rid] = {} if !preallocate[proc_rid]
    preallocate[proc_rid][ctype_id] = user_id == -2 ? nil : { 'user_id' => user_id, 'group_id' => group_id }
    preallocate = preallocate.delete_if { |prid,_| !valid_actprocs[prid] }
    preallocate.each { |_,subpa| subpa.delete_if { |ctid,v| !valid_actctype[ctid] || !v } }
    self.metadata['_preallocate'] = preallocate
  end

  # 0: not allocated, 1: fully allocated, 2: partially allocated, 3: allocation invalid
  def preallocated_status(proc_rid)
    preallocate = self.metadata['_preallocate']
    return { 'status' => 0, 'allocations' => {} } if !preallocate
    proc_pa = preallocate[proc_rid]
    return { 'status' => 0, 'allocations' => {} } if !proc_pa
    status,userlist,grouplist = 0,{},{}
    self.activechecktypes.each_with_index do |ac,idx|
      proc_pa_ac = proc_pa[ac.checktype.id]
      if proc_pa_ac
        status = (idx > 0 ? 2 : 1) if status == 0
        if proc_pa_ac['user_id'] >= 0
          user = User.find_by(id: proc_pa_ac['user_id'])
          if (!user || user.status == 0)
            status = 3
            userlist[proc_pa_ac['user_id']] = '???'
          else
            userlist[proc_pa_ac['user_id']] = sprintf('%s (%d:%s)', user.realname, user.id, user.username)
          end
        end
        if proc_pa_ac['group_id'] >= 0
          group = Usergroup.find_by(id: proc_pa_ac['group_id'])
          if !group
            status = 3
            grouplist[proc_pa_ac['group_id']] = '???'
          else
            grouplist[proc_pa_ac['group_id']] = group.name
          end
        end
      else
        status = 2 if status == 1
      end
    end
    { 'status' => status, 'allocations' => proc_pa, 'userlist' => userlist, 'grouplist' => grouplist }
  end

end
