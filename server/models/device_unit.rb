class Unit < ActiveRecord::Base
  include ModelBase
  include Searching

  # -----
  # Table setup
  # -----

  validates :code, codevalid: true
  validates :model_id, reference: { notnull: true, refclass: 'Model' }

  serialize :metadata, Hash

  belongs_to :model
  has_many :checks, dependent: :destroy
  has_many :images, -> { order('seqnum ASC') }, as: :owner, dependent: :destroy
  has_many :documents, -> { order('seqnum ASC') }, as: :owner, dependent: :destroy

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.references :model, null:false
      t.string :code, null:false
      t.string :customer
      t.text :comment
      t.date :commissioned
      t.date :finished
      t.date :delivered
      t.date :approved
      t.integer :status
      t.text :metadata
      augment_table_usertag t
    end
  end

  # -----
  # Global search
  # -----

  def self.search_result_extend
    %w(model)
  end

  def self.searchable_fields
    {
        code: :text,
        customer: :text,
        comment: :text
    }
  end

  # -----
  # DB hooks
  # -----

  def has_been_created(_)
    metadata['_commentby'] = get_current_user
  end

  def has_been_updated(changes)
    changes.each do |k,_|
      if k == 'comment'
        metadata['_commentby'] = get_current_user
      end
    end
  end

  # -----
  # Aggregation and helper methods
  # -----

  def self.using_model
    ret = Hash.new { |h,k| h[k] = [] }
    Unit.all.each do |unit|
      # filtering units which were created for model testing
      if !unit.code.end_with?('(TR)')
        ret[unit.model.realid] << [unit.id, unit.code]
      end
    end
    ret
  end

  def archive
    if $system_parameters[:unitarchive] && self.status && self.status >= 10 && self.status < 100 && self.updated_at && $system_parameters[:unitarchive] < (Time.now - self.updated_at)
      self.status += 100
      save!
      return true
    end
    false
  end

  # -----
  # Comments
  # -----

  def self.all_comments(id, includemsmnts)
    comments = []
    element = Unit.find(id)
    ret = { comments: comments, element: element }
    element.checks.each do |check|
      children = []
      children = Check.all_comments(check.id, false)[:comments] if includemsmnts
      comments << { type: 'check', obj: check, text: check.comment, owner: check.metadata['_commentby'], children: children }
    end
    ret
  end

end

# Status:
#   0: Open
#  10: Closed
#  20: Discarded
# 100: Archived
# 110: Closed & Archived
# 120: Discarded & Archived