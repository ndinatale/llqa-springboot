class Binaryfile < ActiveRecord::Base
  include ModelBase

  # -----
  # Table setup
  # -----

  serialize :metadata, Hash

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.string :filename
      t.string :original_filename
      t.string :hexhash
      t.string :mimetype
      t.integer :size
      t.text :metadata
      augment_table_usertag t
    end
  end

  # -----
  # Aggregation and helper methods
  # -----

  def self.generate_filename(ext)
    return sprintf('%0.6f',Time.now.to_f).gsub('.','_')+".#{ext}"
  end

end
