require_relative 'import_document_import'

class Document < ActiveRecord::Base
  include ModelBase
  include Ordering

  # -----
  # Table setup
  # -----

  serialize :metadata

  belongs_to :owner, polymorphic: true
  belongs_to :binaryfile

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.references :owner, polymorphic: true
      t.references :binaryfile
      t.string :caption
      t.integer :doctype # 1: Textdocuments, 2: Videos
      augment_table_usertag t
      augment_table_ordering t
    end
  end

  # -----
  # DB hooks
  # -----

  def has_been_created(init)
    obj = set_source
    return if !obj
    obj.log_change(self.binaryfile.original_filename, 'document', 'createatt', { initdata: init, tgt: owner.code }, true)
  end

  def has_been_updated(changes)
    obj = set_source
    return if !obj
    changes.each do |k,v|
      if (%w(caption).index(k))
        obj.log_change(self.binaryfile.original_filename, 'document', 'changefield', { field: k, olddata: v[0] ? v[0].to_json : nil, newdata: v[1] ? v[1].to_json : v[1] }, true)
      elsif k == 'seqnum'
        obj.log_change(self.binaryfile.original_filename, 'document', 'changeseq', { changes: v }, true)
      end
    end
  end

  def has_been_deleted
    obj = set_source
    return if !obj
    obj.log_change(self.binaryfile.original_filename, 'document', 'deleteatt', { tgt: owner.code }, true)
  end

  # -----
  # Reordering
  # -----

  def reorder_parentset
    Document.where(owner_type: self.owner_type, owner_id: self.owner_id).order(:seqnum).to_a
  end

  # -----
  # Aggregation and helper methods
  # -----

  def set_source
    obj = nil
    obj = owner.procedure if ((owner_type == 'Step') && owner && owner.procedure)
    obj = owner if ((owner_type == 'Model') && owner)
    obj
  end

  # -----
  # Import/Export
  # -----

  def export_blob(thash)
    hid = "DOC/#{self.binaryfile_id}"
    return if thash.has_key?(hid)
    blob = {
        oname: self.binaryfile.original_filename,
        type: self.binaryfile.mimetype,
        data: Base64.encode64(File.read("../uploads/#{self.binaryfile.filename}"))
    }
    thash[hid] = blob
  end

  def self.import_from_hash(hash,changecode = true, blobhash = nil)
    blob = blobhash ? blobhash["DOC/#{hash['binaryfile_id']}"] : nil
    if !blob
      obj = self.new hash
      obj.has_been_created(hash)
      obj.save!
      return obj
    end
    if %w(video/mkv video/avi video/mp4 video/quicktime).index(blob[:type])
      return Document.importVideo(Base64.decode64(blob['data']), { owner: hash['owner_type'], ownerid: hash['owner_id'], caption: hash['caption']}, blob['oname'], blob['oname'].gsub(/\A.*\./,''), blob['type'], true)
    end
    if %w(application/pdf).index(blob[:type])
      return Document.importPdf(Base64.decode64(blob['data']), { owner: hash['owner_type'], ownerid: hash['owner_id'], caption: hash['caption']}, blob['oname'], blob['oname'].gsub(/\A.*\./,''), blob['type'], true)
    end
    return Document.importText(Base64.decode64(blob['data']), { owner: hash['owner_type'], ownerid: hash['owner_id'], caption: hash['caption']}, blob['oname'], blob['oname'].gsub(/\A.*\./,''), blob['type'], true)
  end


end
