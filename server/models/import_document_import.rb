class Document < ActiveRecord::Base

  def self.importVideo(file,params,oname,ext,type,retobj = false)
    hash = Digest::SHA1.hexdigest(file)
    video_id = nil
    duplicate = Binaryfile.find_by(hexhash: hash)
    video_id = duplicate.id if duplicate
    if !video_id
      fname = Binaryfile.generate_filename(ext)
      File.open("../uploads/#{fname}", 'wb') { |f| f.print file }
      movie = FFMPEG::Movie.new("../uploads/#{fname}")
      if !movie.valid?
        begin
          File.unlink("../uploads/#{fname}")
        rescue
        end
        return assemble_error('INVMOVIE', 'TEXT', {}, []).rest_fail
      end
      metadata = { width: movie.width, height: movie.height, duration: movie.duration, bitrate: movie.bitrate, fps: movie.frame_rate }
      _,_,obj = Binaryfile.merge_data({ filename: fname, original_filename: oname, hexhash: hash, mimetype: type, size: file.size, metadata: metadata })
      video_id = obj.id
    end
    _,_,document = Document.merge_data({ owner_type: params[:owner], owner_id: params[:ownerid], binaryfile_id: video_id, title: nil, caption: params[:caption], doctype: 2 })
    return document if retobj
    return { type: 'Video', id: document.id }.rest_success
  end

  def self.importPdf(file,params,oname,ext,type,retobj = false)
    hash = Digest::SHA1.hexdigest(file)
    pdf_id = nil
    duplicate = Binaryfile.find_by(hexhash: hash)
    pdf_id = duplicate.id if duplicate
    if !pdf_id
      fname = Binaryfile.generate_filename(ext)
      File.open("../uploads/#{fname}", 'wb') { |f| f.print file }
      metadata = nil
      begin
        pdf = PDF::Reader.new("../uploads/#{fname}")
        metadata = { version: pdf.pdf_version, pages: pdf.page_count }
      rescue => e
        p e
        begin
          File.unlink("../uploads/#{fname}")
        rescue
        end
        return assemble_error('INVPDF', 'TEXT', {}, []).rest_fail
      end
      _,_,obj = Binaryfile.merge_data({ filename: fname, original_filename: oname, hexhash: hash, mimetype: type, size: file.size, metadata: metadata })
      pdf_id = obj.id
    end
    _,_,document = Document.merge_data({ owner_type: params[:owner], owner_id: params[:ownerid], binaryfile_id: pdf_id, title: nil, caption: params[:caption], doctype: 1 })
    return document if retobj
    return { type: 'Document', id: document.id }.rest_success
  end

  def self.importText(file,params,oname,ext,type,retobj = false)
    hash = Digest::SHA1.hexdigest(file)
    text_id = nil
    duplicate = Binaryfile.find_by(hexhash: hash)
    text_id = duplicate.id if duplicate
    if !text_id
      fname = Binaryfile.generate_filename(ext)
      File.open("../uploads/#{fname}", 'wb') { |f| f.print file }
      lines = file.split("\n").size
      metadata = { pages: lines/25, lines: lines }
      _,_,obj = Binaryfile.merge_data({ filename: fname, original_filename: oname, hexhash: hash, mimetype: type, size: file.size, metadata: metadata })
      text_id = obj.id
    end
    _,_,document = Document.merge_data({ owner_type: params[:owner], owner_id: params[:ownerid], binaryfile_id: text_id, title: nil, caption: params[:caption], doctype: 1 })
    return document if retobj
    return { type: 'Document', id: document.id }.rest_success
  end

end
