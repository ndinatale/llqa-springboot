require_relative 'import_image_import'

class Image < ActiveRecord::Base
  include ModelBase
  include Ordering

  # -----
  # Table setup
  # -----

  serialize :metadata

  belongs_to :owner, polymorphic: true
  belongs_to :fullimage, foreign_key: 'fullimage_id', class_name: 'Binaryfile'
  belongs_to :preview, foreign_key: 'preview_id', class_name: 'Binaryfile'
  belongs_to :thumbnail, foreign_key: 'thumbnail_id', class_name: 'Binaryfile'

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.references :owner, polymorphic: true
      t.references :fullimage
      t.references :preview
      t.references :thumbnail
      t.string :caption
      augment_table_usertag t
      augment_table_ordering t
    end
  end

  # -----
  # DB hooks
  # -----

  def has_been_created(init)
    obj = set_source
    return if !obj
    obj.log_change(self.fullimage.original_filename, 'image', 'createatt', { initdata: init, tgt: owner.code }, true)
  end

  def has_been_updated(changes)
    obj = set_source
    return if !obj
    changes.each do |k,v|
      if (%w(caption).index(k))
        obj.log_change(self.fullimage.original_filename, 'image', 'changefield', { field: k, olddata: v[0] ? v[0].to_json : nil, newdata: v[1] ? v[1].to_json : v[1] }, true)
      elsif k == 'seqnum'
        obj.log_change(self.fullimage.original_filename, 'image', 'changeseq', { changes: v }, true)
      end
    end
  end

  def has_been_deleted
    obj = set_source
    return if !obj
    obj.log_change(self.fullimage.original_filename, 'image', 'deleteatt', { tgt: owner.code }, true)
  end

  # -----
  # Reordering
  # -----

  def reorder_parentset
    Image.where(owner_type: self.owner_type, owner_id: self.owner_id).order(:seqnum).to_a
  end

  # -----
  # Aggregation and helper methods
  # -----

  def set_source
    obj = nil
    p owner, owner_type
    obj = owner.procedure if ((owner_type == 'Step') && owner && owner.procedure)
    obj = owner if ((owner_type == 'Model') && owner)
    obj
  end

  # -----
  # Import/Export
  # -----

  def export_blob(thash)
    hid = "IMG/#{self.fullimage_id}"
    return if thash.has_key?(hid)
    blob = {
        oname: self.fullimage.original_filename,
        type: self.fullimage.mimetype,
        data: Base64.encode64(File.read("../uploads/#{self.fullimage.filename}"))
    }
    thash[hid] = blob
  end

  def self.import_from_hash(hash,changecode = true, blobhash = nil)
    blob = blobhash ? blobhash["IMG/#{hash['fullimage_id']}"] : nil
    if !blob
      obj = self.new hash
      obj.has_been_created(hash)
      obj.save!
      return obj
    end
    return Image.importImage(Base64.decode64(blob['data']), { owner: hash['owner_type'], ownerid: hash['owner_id'], caption: hash['caption']}, blob['oname'], blob['oname'].gsub(/\A.*\./,''), blob['type'], true)
  end

end
