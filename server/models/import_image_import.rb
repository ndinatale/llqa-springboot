class Image < ActiveRecord::Base

  def self.importImage(file,params,oname,ext,type,retobj = false)
    hash = Digest::SHA1.hexdigest(file)
    fullimage_id,preview_id,thumbnail_id,fullimage_filename = nil,nil,nil,nil
    duplicate = Binaryfile.find_by(hexhash: hash)
    fullimage_id,fullimage_filename = duplicate.id,duplicate.filename if duplicate
    duplicate = Binaryfile.find_by(hexhash: hash+'[P]')
    preview_id = duplicate.id if duplicate
    duplicate = Binaryfile.find_by(hexhash: hash+'[T]')
    thumbnail_id = duplicate.id if duplicate
    if !fullimage_id
      fname = Binaryfile.generate_filename(ext)
      File.open("../uploads/#{fname}", 'wb') { |f| f.print file }
      image = MiniMagick::Image.open("../uploads/#{fname}")
      metadata = { width: image[:width], height: image[:height] }
      _,_,obj = Binaryfile.merge_data({ filename: fname, original_filename: oname, hexhash: hash, mimetype: type, size: file.size, metadata: metadata })
      fullimage_id = obj.id
      fullimage_filename = fname
    end
    if !preview_id
      fname = Binaryfile.generate_filename(ext)
      image = MiniMagick::Image.open("../uploads/#{fullimage_filename}")
      image.resize '450x350'
      image.write  "../uploads/#{fname}"
      metadata = { width: image[:width], height: image[:height] }
      _,_,obj = Binaryfile.merge_data({ filename: fname, original_filename: oname, hexhash: hash+'[P]', mimetype: type, size: test(?s,"../uploads/#{fname}"), metadata: metadata })
      preview_id = obj.id
    end
    if !thumbnail_id
      fname = Binaryfile.generate_filename(ext)
      image = MiniMagick::Image.open("../uploads/#{fullimage_filename}")
      image.resize '120x120'
      image.write  "../uploads/#{fname}"
      metadata = { width: image[:width], height: image[:height] }
      _,_,obj = Binaryfile.merge_data({ filename: fname, original_filename: oname, hexhash: hash+'[T]', mimetype: type, size: test(?s,"../uploads/#{fname}"), metadata: metadata })
      thumbnail_id = obj.id
    end
    _,_,image = Image.merge_data({ owner_type: params[:owner], owner_id: params[:ownerid], fullimage_id: fullimage_id, preview_id: preview_id, thumbnail_id: thumbnail_id, caption: params[:caption] })
    return image if retobj
    return { type: 'Image', id: image.id }.rest_success
  end

end
