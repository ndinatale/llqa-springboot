require_relative '../lib/constants'

require_relative 'proc_measure_calcvalidate'
require_relative 'proc_measure_valuetest'
require_relative 'proc_measure_matrix'

class Measure < ActiveRecord::Base
  include ModelBase
  include Ordering
  include Searching

  # -----
  # Table setup
  # -----

  before_validation { complete_languageblock(false, :title, :description) }

  validates :title, language: { complete: :all, minlength: 3 }
  validates :description, language: { complete: :none }
  validates :calculation, calculation: true

  serialize :title
  serialize :description
  serialize :calculation
  serialize :metadata, Hash
  serialize :flowcontrol, Array

  belongs_to :step
  belongs_to :tooltype
  has_many :measurements

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.references :step, null:false
      t.references :tooltype
      t.string :code, null:false
      t.text :title, null:false
      t.text :description
      t.integer :measuretype, null:false
      t.text :calculation, null:false
      t.text :metadata
      t.text :flowcontrol
      augment_table_usertag t
      augment_table_ordering t
    end
  end
  def self.update_table_01(m,tn)
    m.change_table tn do |t|
      t.integer :enforce, null:false, default:0
    end
  end

  # -----
  # Global search
  # -----

  def self.searchable_fields
    {
        code: :text,
        title: :localized_text,
        description: :localized_text,
        calculation: :choicelist # new search type specifically for measure, will search in 't17_list' on measure, see models_searching.rb
    }
  end

  def self.search_result_extend
    %w(step.procedure)
  end

  def self.get_entity_array(fields, limitation = :none)
    fields += %w(step_id code)
    ret = []
    Step.get_entity_array(%w(id), limitation).each do |step|
      ret += step.measures.select(fields).to_a
    end
    ret
  end

  # -----
  # DB hooks
  # -----

  def has_been_created(init)
    return if !step || !step.procedure
    step.procedure.log_change(step.code + '.' + self.code, 'measure', 'create', { initdata: init }, true)
  end

  def has_been_updated(changes)
    return if !step || !step.procedure
    changes.each do |k,v|
      # LLQA-86 editing code in measure is now disabled
      #if k == 'code'
      #  step.procedure.log_change(step.code+'.'+self.code, 'measure', 'changecode', { oldcode: v[0], newcode: v[1]}, true)
      # 15.02.2016, ast: bugfix https://suisse.jira.com/browse/LLQA-71 log changes for metadata attribute
      if (%w(title description flowcontrol tooltype_id measuretype calculation step_id tooltype_id enforce metadata).index(k))
        step.procedure.log_change(step.code+'.'+self.code, 'measure', 'changefield', { field: k, olddata: v[0] ? v[0].to_json : nil, newdata: v[1] ? v[1].to_json : v[1] }, true)
      elsif k == 'seqnum'
        step.procedure.log_change(step.code + '.'+self.code, 'measure', 'changeseq', { changes: v }, true)
      end
    end
  end

  def has_been_deleted
    return if !step || !step.procedure
    step.procedure.log_change(step.code+'.'+self.code, 'measure', 'delete', {}, true)
  end

  # -----
  # Aggregation and helper methods
  # -----

  def self.measuretypes
    [
        { id:  1, name: 'MEASURE.TYPES.THRESHOLD',         type: 'thold', input: 'numeric', unit: 't1_tholdu', link: 'none'   },
        { id:  6, name: 'MEASURE.TYPES.THRESHOLDMATRIX',   type: 'thold', input: 'matrix',  unit: 't1_tholdu', link: 'none'   },
        { id:  2, name: 'MEASURE.TYPES.ABSOLUTERNG',       type: 'absrng',input: 'numeric', unit: 't2_unit'  , link: 'none'   },
        { id:  7, name: 'MEASURE.TYPES.ABSOLUTERNGMATRIX', type: 'absrng',input: 'matrix',  unit: 't2_unit'  , link: 'none'   },
        { id:  8, name: 'MEASURE.TYPES.ABSOLUTE',          type: 'abs',   input: 'numeric', unit: 't8_unit'  , link: 'none'   },
        { id:  9, name: 'MEASURE.TYPES.ABSOLUTEMATRIX',    type: 'abs',   input: 'matrix',  unit: 't8_unit'  , link: 'none'   },
        { id:  3, name: 'MEASURE.TYPES.TEXT',              type: 'frtext',input: 'text',    unit: nil        , link: 'none'   },
        { id:  4, name: 'MEASURE.TYPES.REGEXP',            type: 'regexp',input: 'text',    unit: nil        , link: 'none'   },
        { id:  5, name: 'MEASURE.TYPES.BOOL',              type: 'flag',  input: 'bool',    unit: nil        , link: 'none'   },
        { id: 10, name: 'MEASURE.TYPES.RESCHECK',          type: 'reschk',input: 'bool',    unit: nil        , link: 'none'   },
        { id: 11, name: 'MEASURE.TYPES.STATISTICAL',       type: 'statst',input: 'numeric', unit: 't11_unit' , link: 'none'   },
        { id: 12, name: 'MEASURE.TYPES.STATISTICALMATRIX', type: 'statst',input: 'matrix',  unit: 't11_unit' , link: 'none'   },
        { id: 13, name: 'MEASURE.TYPES.TIMERSTART',        type: 'timera',input: 'timest',  unit: nil        , link: 'bycode' },
        { id: 14, name: 'MEASURE.TYPES.TIMERSTOP',         type: 'timers',input: 'timestxm',unit: nil        , link: 'bycode' },
        { id: 15, name: 'MEASURE.TYPES.TIMERSTOPQ',        type: 'timerq',input: 'timestxf',unit: nil        , link: 'bycode' },
        { id: 16, name: 'MEASURE.TYPES.TIMERSTOPC',        type: 'timerc',input: 'none',    unit: nil        , link: 'bycode' },
        { id: 17, name: 'MEASURE.TYPES.CHOICELIST',        type: 'choice',input: 'list',    unit: nil        , link: 'none'   }
    ]
  end

  def self.get_measuretype_by_id(id)
    self.measuretypes.each { |type| return type if type[:id] == id }
    return nil
  end

  def measuretype_info
    Measure.get_measuretype_by_id(self.measuretype)
  end

  def applicable_unit
    field = measuretype_info[:unit]
    return '' if !field
    unit = calculation[field]
    unit = field if !unit or unit == ''
    " #{unit}"
  end

  # -----
  # Versioning
  # -----

  def version
    step.procedure.version
  end

  def effective_version
    self.step.procedure.version
  end

  def effective_realid
    "#{self.step.effective_realid}.#{self.code}"
  end

  # -----
  # Reordering
  # -----

  def reorder_parentset
    self.step.measures
  end

  # -----
  # Import/Export
  # -----

  def self.import_from_hash(hash,changecode = true)
    measure = Measure.new hash
    stamp_seqnum(measure)
    measure.save!
    # always set code to generated id, no matter what 'changecode' is
    measure['code'] = measure.id.to_s
    measure.save
    measure.has_been_created(hash)
    measure
  end

  def self.export_to_hash(id, forexport=true, _=false)
    get_single(id, forexport).serialize() do |h,_|
      h.clean_export_hash
    end
  end

  # -----
  # Comments
  # -----

  def self.all_comments(id)
    comments = []
    element = Measure.find(id)
    ret = { comments: comments, element: element }
    element.measurements.each do |msmnt|
      comments << { type: 'check', obj: msmnt.check, text: msmnt.comment, owner: msmnt.metadata['_commentby'] || msmnt.updated_by, children: [] }
    end
    ret
  end

end

