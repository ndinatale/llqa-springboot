class CalculationValidator < ActiveModel::EachValidator

  def initialize(options)
    super(options)
    @klass = options[:class]
  end

  def valid_number?(data,field,positive = false)
    return false unless data.has_key?(field)
    return false unless data[field]
    return false unless data[field].is_a? Numeric
    return false if positive && data[field] < 0
    true
  end

  def valid_integer?(data,field,positive = false)
    return false unless data.has_key?(field)
    return false unless data[field]
    return false unless data[field].is_a? Fixnum
    return false if positive and data[field] < 0
    true
  end

  def text_exists?(data,field,minlen)
    return false unless data.has_key?(field)
    return false unless data[field]
    return false unless data[field].is_a? String
    return false if data[field].size < minlen
    true
  end

  def validate_each(record, attribute, value)
    record.errors[attribute] << ['OPTMISS', @klass.name, {}] unless valid_integer?(value,'optional',true)
    mtinfo = Measure.get_measuretype_by_id(record.measuretype)
    if !mtinfo
      record.errors[attribute] << ['INVMTYPE', @klass.name, { } ]
      return
    end

    if mtinfo[:type] == 'thold'
      record.errors[attribute] << ['AINVVAL', @klass.name, { } ] if !valid_number?(value,'t1_targetv') || !valid_number?(value,'t1_tholdv',true)
    end

    if mtinfo[:type] == 'absrng'
      record.errors[attribute] << ['BINVVAL', @klass.name, { } ] if !valid_number?(value,'t2_minv') || !valid_number?(value,'t2_maxv')
    end

    if mtinfo[:type] == 'frtext'
      record.errors[attribute] << ['CINVVAL', @klass.name, {}] unless valid_integer?(value, 't3_minlen', true)
    end

    # validate list choices, must not be empty
    if mtinfo[:type] == 'choice'
      list = value['t17_list']
      nr = value['t17_len']

      # check all list values if they're empty
      (0..nr-1).each do |i|
        if list[i].to_s == ''
          record.errors[attribute] << ['EMPTYVAL', @klass.name, {}]
          break
        end
      end

    end

    if mtinfo[:type] == 'regexp'
      if text_exists?(value, 't4_regexp', 1)
        begin
          Regexp.new(value['t4_regexp'])
        rescue
          record.errors[attribute] << ['DINVREGEXP', @klass.name, {}]
        end
      else
        record.errors[attribute] << ['DMISSING', @klass.name, {}]
      end
    end

    if mtinfo[:type] == 'flag'
      record.errors[attribute] << ['EINVVAL', @klass.name, {}] unless valid_integer?(value, 't5_expected', true)
    end

    if mtinfo[:type] == 'reschk'
      record.errors[attribute] << ['JINVVAL', @klass.name, {}] unless valid_number?(value, 't10_value')
      record.errors[attribute] << ['JINVCMP', @klass.name, {}] unless valid_integer?(value, 't10_comparator', true)
    end

    if mtinfo[:type] == 'abs'
      record.errors[attribute] << ['HINVVAL', @klass.name, {}] unless valid_number?(value, 't8_value')
      record.errors[attribute] << ['HINVCMP', @klass.name, {}] unless valid_integer?(value, 't8_comparator', true)
    end

    if mtinfo[:link] == 'bycode'
      record.errors[attribute] << ['MCODEMISSING', @klass.name, {}] unless text_exists?(value, 'complcode', 3)
    end

    if mtinfo[:input] == 'matrix'
      if !valid_integer?(value,'mx_xsize',true) || !valid_integer?(value,'mx_ysize',true)
        record.errors[attribute] << ['XINVMTRX', @klass.name, { } ]
        return
      end
      unless text_exists?(value, 'mx_formula', 1)
        record.errors[attribute] << ['XINVFORM', @klass.name, {}]
        return
      end
      begin
        mc = MatrixCalculator.new(value['mx_xsize'],value['mx_ysize'],nil,value['mx_formula'])
        res = mc.calculate
        value['mx_usedfields'] = mc.usage_matrix
        record.errors[attribute] << ['X'+res, @klass.name, { } ] if res.is_a? String
      rescue Exception => e
        record.errors[attribute] << ['XVARIOUS', @klass.name, { } ]
      end
    end

    if mtinfo[:type] == 'timerq' || mtinfo[:type] == 'timerc'
      record.errors[attribute] << ['OINVVAL', @klass.name, {}] unless valid_number?(value, 't15_value')
      record.errors[attribute] << ['OINVCMP', @klass.name, {}] unless valid_integer?(value, 't15_comparator', true)
    end
  end

end
