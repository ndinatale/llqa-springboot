# -----
# Matrix calculator
# -----

class MatrixCalculator
  attr_reader :result, :error

  # Put submitted data into ruby hashes for eval access
  def initialize(xs,ys,values,formula)
    @values = values
    @cols = %w(a b c d e f g h i)[0..xs-1]
    @rows = %w(1 2 3 4 5 6 7 8 9)[0..ys-1]
    @result,@error = nil,nil
    if values == nil
      @values,@used = {},Hash.new(0)
      @cols.each_with_index { |c,ci| @rows.each { |r| key = "#{c}#{r}"; @values[key]=ci*10+r.to_i; } }
    end
    @formula = formula.downcase
  end

  # Calculates the formula via eval; if exception occurs, check exception type and return proper message
  # TODO: Sanitize formula string!
  def calculate
    begin
      return @result = eval(@formula)
    rescue Exception => e
      return @error = 'SYNTAX' if 'SyntaxError' == e.class.name
      return @error = 'INVVAR' if 'NameError' == e.class.name
      return @error = 'INVFUN' if 'NoMethodError' == e.class.name
      return @error = 'INVUSE' if 'TypeError' == e.class.name
      return nil if 'ZeroDivisionError' == e.class.name
      return nil if e.class.name =~ /\AMath/
      return @error = 'VARIOUS'
    end
  end

  # Return usage matrix (i.e. which fields in the x*y array are actually used by the formula)
  def usage_matrix
    @used.keys
  end

  # Return value matrix
  def value_matrix
    @values
  end

  # Provides all methods that may be used in the formula string
  def method_missing(method, *argsin)
    args = argsin.flatten
    case method
      when :conste
        return Math.E
      when :constpi
        return Math.PI
      when :abs
        return args[0].abs
      when :floor
        return args[0].floor
      when :ceil
        return args[0].ceil
      when :rad
        return args[0] * Math::PI / 180.0
      when :deg
        return args[0] / Math::PI * 180.0
      when :acos
        return Math.acos(args[0])
      when :acosh
        return Math.acosh(args[0])
      when :asin
        return Math.asin(args[0])
      when :asinh
        return Math.asinh(args[0])
      when :atan
        return Math.atan(args[0])
      when :atan2
        return Math.atan2(args[0],args[1])
      when :atanh
        return Math.atanh(args[0])
      when :cbrt
        return Math.cbrt(args[0])
      when :cos
        return Math.cos(args[0])
      when :cosh
        return Math.cosh(args[0])
      when :erf
        return Math.erf(args[0])
      when :erfc
        return Math.erfc(args[0])
      when :exp
        return Math.exp(args[0])
      when :gamma
        return Math.gamma(args[0])
      when :hypot
        return Math.hypot(args[0],args[1])
      when :ldexp
        return Math.ldexp(args[0],args[1])
      when :lgamma
        return Math.lgamma(args[0])
      when :log
        return args.size > 1 ? Math.log(args[0],args[1]) : Math.log(args[0])
      when :log10
        return Math.log10(args[0])
      when :log2
        return Math.log2(args[0])
      when :sin
        return Math.sin(args[0])
      when :sinh
        return Math.sinh(args[0])
      when :sqrt
        return Math.sqrt(args[0])
      when :tan
        return Math.tan(args[0])
      when :max
        return args.max
      when :min
        return args.min
      when :sum
        return args.inject :+
      when :pot
        return args.inject :*
      when :mean
        return args.inject(:+)/args.size.to_f
      when :mode
        return args.group_by { |e| e }.values.max_by(&:size).first
      when :median
        sorted = args.sort
        rank = 0.5 * (args.size - 1)
        lrank = rank.floor
        d = rank - lrank
        lower = sorted[lrank].to_f
        upper = sorted[lrank+1].to_f
        return lower + (upper - lower) * d
      when :range
        return args.max - args.min
      when :var
        mean = args.inject(:+)/args.size.to_f
        return args.map { |s| (mean-s) ** 2 }.inject(:+) / args.size.to_f
      when :stddev
        mean = args.inject(:+)/args.size.to_f
        return Math.sqrt(args.map { |s| (mean-s) ** 2 }.inject(:+) / args.size.to_f)
      when :straightness
        xs,ys = args[1..args[0]],args[args[0]+1..args[0]*2]
        slr = SimpleLinearRegression.new(xs, ys)
        return slr.straightness
      when :squareness
        xs1,ys1,xs2,ys2 = args[2..args[0]+1],args[args[0]+2..args[0]*2+1],args[args[0]*2+2..args[0]*2+args[1]+1],args[args[0]*2+args[1]+2..args[0]*2+args[1]*2+1]
        slr1 = SimpleLinearRegression.new(xs1, ys1)
        slr2 = SimpleLinearRegression.new(xs2, ys2)
        return slr1.squareness(slr2)
      else
        # ...
    end
    if @values.has_key?(method.to_s)
      @used[method.to_s] += 1 if @used
      return @values[method.to_s]
    end
    if method.to_s =~ /\A(col|c_|col_)([abcdefghi])\z/
      return super(method, argsin) unless @cols.index($2)
      return @rows.to_a.map do |r|
        @used[($2+r)] += 1 if @used
        @values[($2+r)]
      end.compact
    end
    if method.to_s =~ /\A(row|r_|row_)([123456789])\z/
      return super(method, argsin) unless @rows.index($2)
      return @cols.to_a.map do |c|
        @used[(c+$2)] += 1 if @used
        @values[(c+$2)]
      end.compact
    end
    if method.to_s == 'all'
      return @cols.to_a.map do |c|
        @rows.to_a.map do |r|
          @used[(c+r)] += 1 if @used
          @values[(c+r)]
        end
      end.flatten.compact
    end
    super(method,argsin)
  end

end

# -----
# Linear regression (simple)
# -----

class SimpleLinearRegression
  attr_reader :xs,:ys

  def initialize(xs, ys)
    @xs, @ys = xs, ys
    if @xs.length != @ys.length
      raise 'Unbalanced data. xs need to be same length as ys'
    end
  end

  def y_intercept
    mean(@ys) - (slope * mean(@xs))
  end

  def slope
    x_mean = mean(@xs)
    y_mean = mean(@ys)

    numerator = (0...@xs.length).reduce(0) do |sum, i|
      sum + ((@xs[i] - x_mean) * (@ys[i] - y_mean))
    end

    denominator = @xs.reduce(0) do |sum, x|
      sum + ((x - x_mean) ** 2)
    end

    (numerator / denominator)
  end

  def mean(values)
    total = values.reduce(0) { |sum, x| x + sum }
    Float(total) / Float(values.length)
  end

  def deviation(pt)
    if (pt>@xs.size || pt < 1)
      raise 'Deviation point outside array length'
    end
    @ys[pt-1]-(@xs[pt-1]*slope+y_intercept)
  end

  def range
    @xs.max-@xs.min
  end

  def straightness
    d = []
    @xs.size.times { |i| d << deviation(i+1) }
    d.max-d.min
  end

  def alignment
    range*slope
  end

  def squareness(lrc)
    rmax = [range,lrc.range].max
    rmax * (slope+lrc.slope)
  end
end
