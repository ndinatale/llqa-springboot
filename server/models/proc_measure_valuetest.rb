require_relative '../lib/constants'

class ValueTester
  attr_reader :calcvalue,:newstatus

  def initialize(rawvalues,measure_entry,measure,complgroup)
    # measuretype,calculation,metadata
    @measure_obj = measure
    @measure_entry = measure_entry
    @complgroup = complgroup
    @values = rawvalues
    @mtype = measure.measuretype
    @mtinfo = Measure.get_measuretype_by_id(measure.measuretype)
    @calc = measure.calculation
    @calcvalue = nil
    @metadata = measure.metadata
    @newstatus = 0
  end

  # -----
  # Check value, set status
  # -----

  def check
    if !@values
      if @mtinfo[:type] == 'timerc'
        @values = { 'val' => -1 }
      else
        return nil
      end
    end

    @calcvalue,@newstatus = nil,MMNT_STATUS_ERROR
    if @values['skip']
      if @calc['optional']
        return @newstatus = MMNT_STATUS_SKIPPED
      end
    end

    if @mtinfo[:input] == 'matrix'
      @calc['mx_usedfields'].each { |uf| return @newstatus = 0 unless @values[uf] }
      mc = MatrixCalculator.new(@calc['mx_xsize'], @calc['mx_ysize'], @values, @calc['mx_formula'])
      mc.calculate
      return @newstatus = MMNT_STATUS_ERROR if mc.error
      @calcvalue = mc.result
      return @newstatus = MMNT_STATUS_INVALID unless @calcvalue
    else
      @calcvalue = @values['val']
    end

    return @newstatus = MMNT_STATUS_UNPROC unless @calcvalue
    return @newstatus = MMNT_STATUS_UNPROC if @calcvalue == ''

    if @mtinfo[:input] == 'matrix' || @mtinfo[:input] == 'numeric'
      unless @calcvalue.is_a? Numeric
        @calcvalue = nil
        return @newstatus = MMNT_STATUS_INVALID
      end
    end

    if @mtinfo[:input] == 'text'
      unless @calcvalue.is_a? String
        @calcvalue = nil
        return @newstatus = MMNT_STATUS_INVALID
      end
    end

    # The value should be a string value from a list, thus a string is required
    if @mtinfo[:input] == 'list'
      unless @calcvalue.is_a? String
        @calcvalue = nil
        return @newstatus = MMNT_STATUS_INVALID
      end
    end

    if @mtinfo[:input] == 'bool'
      unless [0, 1, 2].index(@calcvalue)
        @calcvalue = nil
        return @newstatus = MMNT_STATUS_INVALID
      end
    end

    if @mtinfo[:type] == 'timera' || @mtinfo[:type] == 'timers' || @mtinfo[:type] == 'timerq' || @mtinfo[:type] == 'timerc'
      @calcvalue = compile_timer_value
      return @newstatus = MMNT_STATUS_INVALID if !@calcvalue
    end

    if @mtinfo[:type] == 'thold'
      return @newstatus = (@calcvalue.abs <= @calc['t1_tholdv'] ? MMNT_STATUS_PASSED : MMNT_STATUS_FAILED)
    end

    if @mtinfo[:type] == 'absrng'
      return @newstatus = (@calcvalue <= @calc['t2_maxv'] && @calcvalue >= @calc['t2_minv'] ? MMNT_STATUS_PASSED : MMNT_STATUS_FAILED)
    end

    if @mtinfo[:type] == 'abs'
      case @calc['t8_comparator']
        when COMP_LESSTHAN
          @newstatus = (@calcvalue < @calc['t8_value'] ? MMNT_STATUS_PASSED : MMNT_STATUS_FAILED)
        when COMP_LESSEQUAL
          @newstatus = (@calcvalue <= @calc['t8_value'] ? MMNT_STATUS_PASSED : MMNT_STATUS_FAILED)
        when COMP_EQUAL
          @newstatus = (@calcvalue == @calc['t8_value'] ? MMNT_STATUS_PASSED : MMNT_STATUS_FAILED)
        when COMP_MOREEQUAL
          @newstatus = (@calcvalue >= @calc['t8_value'] ? MMNT_STATUS_PASSED : MMNT_STATUS_FAILED)
        when COMP_MORETHAN
          @newstatus = (@calcvalue > @calc['t8_value'] ? MMNT_STATUS_PASSED : MMNT_STATUS_FAILED)
        else
          @newstatus = MMNT_STATUS_INVALID
      end
    end

    if @mtinfo[:type] == 'frtext'
      return @newstatus = (@calcvalue.size >= @calc['t3_minlen'] ? MMNT_STATUS_PASSED : MMNT_STATUS_FAILED)
    end

    if @mtinfo[:type] == 'regexp'
      return @newstatus = (@calcvalue =~ Regexp.new(@calc['t4_regexp']) ? MMNT_STATUS_PASSED : MMNT_STATUS_FAILED)
    end

    # always accept if type is list choice
    if @mtinfo[:type] == 'choice'
      return @newstatus = MMNT_STATUS_PASSED
    end

    if @mtinfo[:type] == 'flag'
      return @newstatus = ((@calc['t5_expected'] == 2 || @calc['t5_expected'] == @calcvalue) ? MMNT_STATUS_PASSED : MMNT_STATUS_FAILED)
    end

    if @mtinfo[:type] == 'reschk'
      return @newstatus = (@calcvalue == 1 ? MMNT_STATUS_PASSED : MMNT_STATUS_FAILED)
    end

    if @mtinfo[:type] == 'statst'
      return @newstatus = MMNT_STATUS_PASSED
    end

    if @mtinfo[:type] == 'timera'
      return @newstatus = MMNT_STATUS_PASSED
    end

    if @mtinfo[:type] == 'timers'
      return @newstatus = MMNT_STATUS_PASSED
    end

    if @mtinfo[:type] == 'timerq' || @mtinfo[:type] == 'timerc'
      return (@newstatus = MMNT_STATUS_PASSED) if @calcvalue == -2
      case @calc['t15_comparator']
        when COMP_LESSTHAN
          @newstatus = (@calcvalue <= @calc['t15_value']*60+59 ? MMNT_STATUS_PASSED : MMNT_STATUS_FAILED)
        when COMP_MORETHAN
          @newstatus = (@calcvalue >= @calc['t15_value']*60 ? MMNT_STATUS_PASSED : MMNT_STATUS_FAILED)
        else
          @newstatus = MMNT_STATUS_INVALID
      end
    end
    @newstatus
  end

  # -----
  # Timer handling
  # -----

  def compile_timer_value
    return -2 if @complgroup.size == 0
    minstart,maxfinish = nil,nil
    if Measure.get_measuretype_by_id(@measure_obj.measuretype)[:type] == 'timera'
      minstart = @values['val']
    else
      maxfinish = @values['val']
    end
    @complgroup.each do |compl|
      next if compl[:obj] == @measure_obj
      next if !CHK_CDSTATUS_ALL_FINISHED.index(compl[:entry]['status'])
      next if !compl[:mobj]
      type = Measure.get_measuretype_by_id(compl[:obj].measuretype)
      if type[:type] == 'timera'
        minstart = compl[:mobj].rawvalues['val'] if (!minstart || minstart > compl[:mobj].rawvalues['val'])
      end
      if type[:type] == 'timers' || type[:type] == 'timerq'
        maxfinish = compl[:mobj].rawvalues['val'] if (!maxfinish || maxfinish < compl[:mobj].rawvalues['val'])
      end
    end
    return -1 if (!minstart || !maxfinish)
    diff = maxfinish - minstart - ((@values['reduce'] || 0) * 60)
    @values['val'] = maxfinish if @mtinfo[:type] == 'timerc'
    return nil if diff < 0
    diff
  end

  # -----
  # Create string from value/type
  # -----

  def stringify_value
    return nil unless @calcvalue

    if @mtinfo[:input] == 'matrix' || @mtinfo[:input] == 'numeric'
      format = @metadata['floatformat']
      if !format || format == 'default'
        format = $application_parameters[:defaultfloatformat]
      end
      return sprintf(format,@calcvalue)
    end

    if @mtinfo[:input] == 'text'
      return @calcvalue
    end

    if @mtinfo[:input] == 'list'
      return @calcvalue
    end

    if @mtinfo[:input] == 'bool'
      return %w(NO YES ANY)[@calcvalue]
    end

    if @mtinfo[:type] == 'timera'
      time = "[TIMS:#{@values['val']}]"
      return "(#{time})"
    end

    if @mtinfo[:type].start_with? 'timer'
      time = "[TIMS:#{@values['val']}]"
      span = @calcvalue < 0 ? (@calcvalue == -2 ? '*h **min' : '---') : "[SPAN:#{@calcvalue}]"
      return "(#{time}, -[SPAN:#{@values['reduce']*60}]) #{span}" if @values['reduce'] && @values['reduce'] > 0
      return "(#{time}) #{span}"
    end

    nil
  end

end
