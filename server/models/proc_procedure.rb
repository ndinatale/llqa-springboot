class Procedure < ActiveRecord::Base
  include ModelBase
  include Versioning
  include Searching

  # -----
  # Table setup
  # -----

  before_validation { complete_languageblock(false, :title, :description) }

  validates :title, language: { complete: :all, minlength: 3 }
  validates :description, language: { complete: :none }

  serialize :title, Hash
  serialize :description, Hash
  serialize :metadata, Hash
  serialize :flowcontrol, Array

  has_many :steps, -> { order('seqnum ASC') }, dependent: :destroy
  has_many :activeprocedures, dependent: :destroy

  has_many :datachanges, -> { order('timestamp ASC') }, as: :source

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.string :code, null:false
      t.text :title, null:false
      t.text :description
      t.text :metadata
      t.text :flowcontrol
      augment_table_usertag t
      augment_table_version t
    end
  end
  # LLQA-85: add custom model attributes
  def self.add_custom_attrs(m,tn)
    m.change_table tn do |t|
      t.text :tag_1
      t.text :tag_2
      t.text :tag_3
      t.text :tag_4
      t.text :tag_5
      t.text :tag_6
    end
  end

  # -----
  # Global search
  # -----

  def self.searchable_fields
    {
        code: :text,
        title: :localized_text,
        description: :localized_html
    }
  end

  def self.get_entity_array(fields, limitation = :none)
    fields += %w(realid version)
    return self.order(updated_at: :desc).select(fields).all if limitation == :none
    return self.order(updated_at: :desc).where(version: nil).select(fields).all if limitation == :edit_version
    return self.order(updated_at: :desc).where.not(version: nil).select(fields).all if limitation == :finalized
    ret = {}
    self.order(updated_at: :desc).where.not(version: nil).select(fields).all.each do |entity|
      ret[entity.realid] = entity if(!ret[entity.realid] || (entity.version > ret[entity.realid].version))
    end
    ret.values
  end

  # -----
  # DB hooks
  # -----

  def has_been_created(init)
    log_change(self.code, 'procedure', 'create', { initdata: init }, true)
  end

  def has_been_updated(changes)
    changes.each do |k,v|
      if k == 'code'
        log_change(self.code, 'procedure', 'changecode', { oldcode: v[0], newcode: v[1]}, true)
      elsif (%w(title description flowcontrol realid tag_1 tag_2 tag_3 tag_4 tag_5 tag_6).index(k))
        log_change(self.code, 'procedure', 'changefield', { field: k, olddata: v[0] ? v[0].to_json : nil, newdata: v[1] ? v[1].to_json : v[1] }, true)
      elsif k == 'version'
        log_change(self.code, 'procedure', 'finalize', { version: v[1] }, false)
      elsif k == 'disabled'
        log_change(self.code, 'procedure', 'delete', { type: 'disabled' }, false)
      end
    end
  end

  def has_been_deleted
    log_change(self.code, 'procedure', 'delete', { type: 'deleted' }, false)
  end

  # -----
  # Versioning
  # -----

  def log_change(obj, objtype, type, details, dirty)
    Datachange.log_change(self, obj, objtype, type, details, (self.version != nil) && dirty, (self.version == nil) && dirty)
  end

  def is_dirty?
    Datachange.where(source: self, dirty: true).size > 0
  end

  def recurse_finalize
    [[:steps,:procedure_id]]
  end

  def effective_version
    self.version
  end

  def effective_realid
    self.realid
  end

  # -----
  # Import/Export
  # -----

  def self.import_from_hash(hash,changecode = true)
    steps = hash.delete 'steps'
    blobs = hash.delete 'blobs' if !blobs
    procedure = Procedure.new hash
    stamp_realid(procedure)
    procedure.save!
    # always set code to generated id, no matter what 'changecode' is
    procedure['code'] = procedure.id.to_s
    procedure.save
    procedure.has_been_created(hash)
    steps.each { |step| step['procedure_id'] = procedure.id; Step.import_from_hash(step,false,blobs) }
    procedure
  end

  def self.export_to_hash(id, forexport=true, complete=false)
    get_single(id, forexport).serialize(%w(steps steps.measures steps.images steps.documents)) do |h,o|
      if complete
        h['blobs'] = {}
        o.steps.each do |step|
          step.images.each { |img| img.export_blob(h['blobs']) }
          step.documents.each { |doc| doc.export_blob(h['blobs']) }
        end
      end
      h.clean_export_hash
    end
  end

end

