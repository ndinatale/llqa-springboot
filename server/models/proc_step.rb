class Step < ActiveRecord::Base
  include ModelBase
  include Ordering
  include Searching

  # -----
  # Table setup
  # -----

  before_validation { complete_languageblock(false, :title, :description) }

  validates :title, language: { complete: :all, minlength: 3 }
  validates :description, language: { complete: :none }

  serialize :title, Hash
  serialize :description, Hash
  serialize :metadata, Hash
  serialize :flowcontrol, Array

  belongs_to :procedure
  has_many :measures, -> { order('seqnum ASC') }, dependent: :destroy
  has_many :images, -> { order('seqnum ASC') }, as: :owner, dependent: :destroy
  has_many :documents, -> { order('seqnum ASC') }, as: :owner, dependent: :destroy

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.references :procedure, null:false
      t.string :code, null:false
      t.text :title, null:false
      t.text :description
      t.text :metadata
      t.text :flowcontrol
      augment_table_usertag t
      augment_table_ordering t
    end
  end

  def self.update_table_01(m,tn)
    m.change_table tn do |t|
      t.integer :enforce, null:false, default:0
    end
  end

  def self.update_table_02_new_step_type(m,tn)
    m.change_table tn do |t|
      # 'type' is a reserved keyword, so we'll call it 'steptype'
      # type 0 (or nil): normal
      # type 1: instruction
      t.integer :steptype, default: 0
    end
  end

  # -----
  # Global search
  # -----

  def self.searchable_fields
    {
        code: :text,
        title: :localized_text,
        description: :localized_html
    }
  end
  def self.search_result_extend
    %w(procedure)
  end

  def self.get_entity_array(fields, limitation = :none)
    fields += %w(procedure_id code)
    ret = []
    Procedure.get_entity_array(%w(id), limitation).each do |proc|
      ret += proc.steps.select(fields).to_a
    end
    ret
  end

  # -----
  # DB hooks
  # -----

  def has_been_created(init)
    return if !procedure

    procedure.log_change(self.code, 'step', 'create', { initdata: init }, true)
  end

  def has_been_updated(changes)
    return if !procedure
    changes.each do |k,v|
      # changing the code is no longer possible
      #if k == 'code'
      #  procedure.log_change(self.code, 'step', 'changecode', { oldcode: v[0], newcode: v[1]}, true)
      if (%w(title description flowcontrol procedure_id enforce steptype).index(k))
        procedure.log_change(self.code, 'step', 'changefield', { field: k, olddata: v[0] ? v[0].to_json : nil, newdata: v[1] ? v[1].to_json : v[1] }, true)
      elsif k == 'seqnum'
        procedure.log_change(self.code, 'step', 'changeseq', { changes: v }, true)
      end
    end
  end

  def has_been_deleted
    return if !procedure
    procedure.log_change(self.code, 'step', 'delete', {}, true)
  end

  # -----
  # Versioning
  # -----

  def version
    procedure.version
  end

  def recurse_finalize
    [[:measures,:step_id], [:images,:owner_id], [:documents,:owner_id]]
  end

  def effective_version
    self.procedure.version
  end

  def effective_realid
    "#{self.procedure.realid}.#{self.code}"
  end

  # -----
  # Reordering
  # -----

  def reorder_parentset
    self.procedure.steps
  end

  # -----
  # Import/Export
  # -----

  def self.import_from_hash(hash,changecode = true,blobs = nil)
    measures = hash.delete 'measures'
    images = hash.delete 'images'
    documents = hash.delete 'documents'
    blobs = hash.delete 'blobs' if !blobs
    step = Step.new hash
    stamp_seqnum(step)
    step.save!
    # always set code to generated id, no matter what 'changecode' is
    step['code'] = step.id.to_s
    step.save
    step.has_been_created(hash)
    measures.each { |measure| measure['step_id'] = step.id; Measure.import_from_hash(measure) }
    images.each { |image| image['owner_id'] = step.id; Image.import_from_hash(image,false,blobs) }
    documents.each { |document| document['owner_id'] = step.id; Document.import_from_hash(document,false,blobs) }
    step
  end

  def self.export_to_hash(id, forexport=true, complete=false)
    get_single(id, forexport).serialize(%w(measures images documents)) do |h,o|
      if complete
        h['blobs'] = {}
        o.images.each { |img| img.export_blob(h['blobs']) }
        o.documents.each { |doc| doc.export_blob(h['blobs']) }
      end
      h.clean_export_hash
    end
  end

  # -----
  # Comments
  # -----

  def self.all_comments(id)
    comments = []
    element = Step.find(id)
    begin
      element = Procedure.latest_version_object(element.procedure.realid).steps.to_a.keep_if { |s| s.code == element.code }.compact.first
    rescue => e
    end if !element.procedure.version

    return { comments: [], element: Step.find(id) } if !element

    ret = { comments: comments, element: element }
    element.measures.each do |measure|
      children = Measure.all_comments(measure.id)[:comments]
      comments << { type: 'meas', obj: measure, text: nil, children: children }
    end
    ret
  end

end

