class Settings < ActiveRecord::Base
  include ModelBase
  include Ordering

  # -----
  # Table setup
  # -----
  before_validation { complete_languageblock(false, :value) }

  validates :value, language: { complete: :all, minlength: 3 }

  # serialize json object (value) as newline delimited string in the database
  serialize :value
  serialize :sub_values

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.string :key
      t.string :value
      t.integer :seqnum
      augment_table_usertag t
    end
  end

  def self.add_subvalues(m,tn)
    m.change_table tn do |t|
      t.string :sub_values
    end
  end

end