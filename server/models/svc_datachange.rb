class Datachange < ActiveRecord::Base
  serialize :changedetails, Hash

  # -----
  # Table setup
  # -----

  belongs_to :source, polymorphic: true

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.references :source, polymorphic: true
      t.string :subobject, null:false
      t.string :subobjtype, null:false
      t.string :changetype, null:false
      t.text :changedetails
      t.integer :user
      t.integer :timestamp
      t.boolean :post_finalize
      t.boolean :dirty
    end
  end

  # -----
  # Aggregation and helper methods
  # -----

  def self.log_change(source, subobj, subobjtype, changetype, changedetails, post_finalize, dirty)
    details = {}
    changedetails.each do |k,v|
      v = v.to_json if v.is_a?(Hash)
      v = v.to_json if v.is_a?(Array)
      details[k] = v
    end
    entry = new({ source: source, subobject: subobj, subobjtype: subobjtype, changetype: changetype, changedetails: details, post_finalize: post_finalize, dirty: dirty, user: get_current_user_id, timestamp: Time.now.to_i })
    entry.save!
  end

  def extend_user
    user = User.find_by(id: self.user)
    {
        username: user ? user.username : 'Anonymous',
        realname: user ? user.realname : '',
        userid: user ? user.id : -1
    }
  end
end
