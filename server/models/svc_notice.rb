class Notice < ActiveRecord::Base
  include ModelBase
  include Searching

  # -----
  # Table setup
  # -----

  augment_class_usertag

  serialize :timeline, Array
  serialize :path, Hash

  belongs_to :category, class_name: 'Snippet'

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.string :text
      t.references :category, null: false
      t.text :path
      t.string :artno
      t.string :artdesc
      t.integer :timeloss
      t.integer :status # 1 = offen, 2 = in Bearbeitung, 5 = geschlossen, 9 = archiviert
      t.text :timeline
      augment_table_usertag t
    end
  end

  # -----
  # Global search
  # -----

  def self.searchable_fields
    {
        text: :text
    }
  end

  # -----
  # Aggregation and helper methods
  # -----

  def archive
    if $system_parameters[:noticearchive] && self.status && self.status == 5 && self.updated_at && $system_parameters[:noticearchive] < (Time.now - self.updated_at)
      self.status = 9
      save!
      return true
    end
    false
  end

  def self.editorinfo
    {
      textproposals: Snippet.where(category: 'notice_textprop', status: 1).order(seqnum: :asc).select(:id, :text).to_a.map { |s| s.serializable_hash },
      categories: Snippet.where(category: 'notice_category', status: 1).order(seqnum: :asc).select(:id, :text).to_a.map { |s| s.serializable_hash },
      last_artno: Notice.where.not(artno: nil).order(updated_at: :desc).limit(200).select(:artno).to_a.map { |s| s.artno }.uniq,
      last_artdesc: Notice.where.not(artdesc: nil).order(updated_at: :desc).limit(200).select(:artdesc).to_a.map { |s| s.artdesc }.uniq
    }
  end

  def set_status(status, comment = nil)
    p status,comment
    self.status = status
    self.timeline = [] if !(self.timeline.is_a? Array)
    return if timeline.size > 0 && timeline[-1]['new_status'] == status
    self.timeline << {
        'new_status' => status,
        'timestamp' => Time.now.to_i,
        'user' => get_current_user_id,
        'comment' => comment
    }
    self.save!
  end

  def prepare_rest(hash_out)
    hash_out['users'] = uhash = {}
    self.timeline.each do |event|
      next if !event || !event['user']
      begin
        user = User.find(event['user'])
        uhash[user.id] = { 'realname' => user.realname, 'username' => user.username, 'status' => user.status }
      rescue
      end
    end
    hash_out['pathobj'] = phash = []
    self.path['path'].each do |pathelement|
      begin
        obj = Kernel.const_get(pathelement['type']).find(pathelement['id']).serializable_hash.delete_if { |key,value| !(%w(id code title description).index(key)) }
        phash << {
            'type' => pathelement['type'],
            'obj' => obj
        }
      rescue
      end
    end
  end

  def self.export_data(data)
    out = {
        'type' => 'list',
        'notices' => []
    }

    q = Notice.all
    q = q.where('status < 9') if data['scope'] == 'unarchived'
    q = q.where(id: data['showids'].split(',').map { |x| x.to_i }) if data['scope'] == 'filtered'
    q.each do |notice|
      ohash = notice.serializable_hash
      ohash['type'] = 'notice'
      ohash['path']['pathtype'] = ohash['path']['type']
      ohash['path']['type'] = 'path'
      ohash['path']['url'] = nil
      ohash['path']['path'].each { |p| p['segmenttype'],p['type'] = p['type'],'segment' } if (ohash['path'] && ohash['path']['path'] && ohash['path']['path'].is_a?(Array))
      ohash['timeline'].each { |t| t['type'] = 'timeline' }
      ohash['category'] = notice.category.text
      out['notices'] << ohash
    end

    case data['format']
      when 'xml' then return out.format_as_xml
      when 'csv' then return out.format_as_csv_stringkeys(%w(type id text category_id category artno artdesc timeloss status pathtype segmenttype timestamp user new_status))
    end
    return out.format_as_json
  end

end

