class Snippet < ActiveRecord::Base
  include ModelBase
  include Ordering

  # -----
  # Table setup
  # -----

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.string :category
      t.string :text
      t.integer :status, default: 1   # 1 = active, 2 = locked, 9 = discarded
      augment_table_usertag t
    end
  end
  def self.update_table_01(m,tn)
    m.change_table tn do |t|
      augment_table_ordering t
    end
    all.to_a.each do |snippet|
      snippet.seqnum = (maximum(:seqnum) || 0)+1
      snippet.save!
    end
  end

  # -----
  # Reordering
  # -----

  def reorder_parentset
    Snippet.where(category: self.category, status: [1,2]).order(seqnum: :asc)
  end

end