class Tooltype < ActiveRecord::Base
  include ModelBase
  include Searching

  # -----
  # Table setup
  # -----

  validates :code, codevalid: true
  validates :title, language: { complete: :all, minlength: 3 }
  validates :description, language: { complete: :none }

  serialize :title
  serialize :description
  serialize :metadata, Hash

  has_many :images, -> { order('seqnum ASC') }, as: :owner, dependent: :destroy
  has_many :documents, -> { order('seqnum ASC') }, as: :owner, dependent: :destroy
  has_many :toolunits, dependent: :destroy
  has_many :measures, dependent: :nullify

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.string :code, null:false
      t.text :title, null:false
      t.text :description
      t.text :metadata
      t.boolean :deleted, null:false, default:false
      augment_table_usertag t
    end
  end

  # -----
  # Global search
  # -----

  def self.searchable_fields
    {
        code: :text,
        title: :localized_text,
        description: :localized_text
    }
  end

  # -----
  # Comments
  # -----

  def self.all_comments(id)
    comments = []
    element = Tooltype.find(id)
    ret = { comments: comments, element: element }
    element.toolunits.each do |unit|
      comments << { type: 'tool', obj: unit, text: unit.comment, owner: unit.metadata['_commentby'], children: [] }
    end
    ret
  end
end
