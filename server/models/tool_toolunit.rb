class Toolunit < ActiveRecord::Base
  include ModelBase
  include Searching

  # -----
  # Table setup
  # -----

  validates :code, codevalid: { scope: [:tooltype_id] }

  serialize :metadata, Hash

  belongs_to :tooltype
  has_many :measurements, dependent: :nullify

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.references :tooltype, null:false
      t.string :code, null:false
      t.text :comment
      t.text :metadata
      t.boolean :deleted, null:false, default:false
      augment_table_usertag t
    end
  end

  # -----
  # Global search
  # -----

  def self.searchable_fields
    {
        code: :text,
        comment: :text
    }
  end

  def self.search_result_extend
    %w(tooltype)
  end

  # -----
  # DB Hooks
  # -----

  def has_been_created(_)
    metadata['_commentby'] = get_current_user
  end

  def has_been_updated(changes)
    changes.each do |k,_|
      if k == 'comment'
        metadata['_commentby'] = get_current_user
      end
    end
  end

  # -----
  # Aggregation and helper methods
  # -----

  def all_uses(sortedby, from, to)
    ret = {}
    self.measurements.each do |mmnt|
      next if from && mmnt.savedon && mmnt.savedon < from
      next if to && mmnt.savedon && mmnt.savedon > to
      sortvalue =
          if sortedby == 'check'
            sprintf('%s/%s/%s/%s/%s/%s',mmnt.check.unit.model.code,mmnt.check.unit.code,mmnt.check.id,mmnt.measure.step.procedure.code,mmnt.measure.step.code,mmnt.measure.code)
          else
            "#{mmnt.savedon}"
          end
      ret[sortvalue] = mmnt
    end
    ret.sort.map { |t| t[1] }
  end

end
