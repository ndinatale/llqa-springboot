class Affiliation < ActiveRecord::Base
  include ModelBase

  # -----
  # Table setup
  # -----

  belongs_to :user
  belongs_to :usergroup

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.references :user
      t.references :usergroup
      augment_table_usertag t
    end
  end

end

