class Grant < ActiveRecord::Base
  include ModelBase

  # -----
  # Table setup
  # -----

  belongs_to :grantee, polymorphic: true
  belongs_to :privilege

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.references :grantee, polymorphic: true
      t.references :privilege
      augment_table_usertag t
    end
  end

  # -----
  # Aggregation and helper methods
  # -----

  def self.manage_grants(klass,objid,data)
    obj = klass.find_by(id:objid)
    return assemble_error('NOTFOUND', 'TEXT', {class: klass.name+'s', id: objid}, []).rest_fail if !obj
    data['addlist'].each do |gr|
      has = false
      obj.grants.each { |eg| has = true if eg.privilege_id == gr }
      next if has
      obj.grants.create!({ privilege_id: gr })
    end if (data['addlist'])
    data['dellist'].each do |gr|
      has = nil
      obj.grants.each { |eg| has = eg if eg.privilege_id == gr }
      next if !has
      has.destroy
    end if (data['dellist'])
    return {}.rest_success
  end

  def self.add_grants_by_name(obj,grantcodes)
    grantcodes.each do |grc|
      gr = Privilege.find_by(code:grc)
      next if !gr
      grid = gr.id
      has = false
      obj.grants.each { |eg| has = true if eg.privilege_id == grid }
      next if has
      obj.grants.create!({ privilege_id: grid })
    end
  end

end
