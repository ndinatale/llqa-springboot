require_relative 'usrmgt_privilege_validator'

class Privilege < ActiveRecord::Base
  include ModelBase

  # -----
  # Table setup
  # -----

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.string :name, null:false
      t.text :description
      t.string :code, null:false
      augment_table_usertag t
    end
  end

  # -----
  # Aggregation and helper methods
  # -----

  def self.setup_privileges
    %w(MNGMUC MNGPSM MNGTOL MNGPAR MNGUSR MNGALL EDTMOD EDTUNT EDTPSM EDTTTY EDTALL CRTMOD CRTUNT CRTPSM CRTTOL CRTTTY CRTCHK CRTALL DELMOD DELUNT DELPRC DELTTY DELTOL DELCHK DELALL USRMGO USRMGA GRTPRO GRTPRA GRPCRT GRTTOG WFLMNG WFLREG CHGCOD FINALZ MODVRS TRUSER TKOVER GLSRCH MNGNTC MNGCFG MNGCFE).each do |grnt|
      unless Privilege.find_by(code: grnt)
        puts "Creating new grant #{grnt}"
        Privilege.create(name: 'PRV.'+grnt+'.TTL', description: 'PRV.'+grnt+'.DSC', code: grnt)
        %w(admin root).each do |poweruser|
          userobj = User.find_by(username: poweruser)
          if userobj
            puts " - adding new grant to #{poweruser}"
            Grant.add_grants_by_name(userobj, [grnt])
          end
        end
      end
    end
  end

end
