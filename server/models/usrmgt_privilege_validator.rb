class UsermgtValidator < ActiveModel::EachValidator
  def initialize(options)
    super(options)
    @klass = options[:class]
  end

  def validate_each(record, attribute, value)
    if (!value)
      record.errors[attribute] << ['NOTNULL', @klass.name, {}]
      return
    end
    res = @klass.all.where(attribute => value).where.not(id: record.id)
    record.errors[attribute] << ['UNIQUE', @klass.name, {}] if options[:unique] && res.count > 0
    record.errors[attribute] << ['MISMATCH', @klass.name, {}] if options[:match] && value !~ options[:match]
    record.errors[attribute] << ['TOOSHORT', @klass.name, {}] if options[:minlen] && (!value.is_a?(String) || value.size < options[:minlen])
    record.errors[attribute] << ['NUMBER', @klass.name, {}] if options[:number] && (!value.is_a?(Fixnum) || !options[:number].include?(value))
  end
end
