class User < ActiveRecord::Base
  include ModelBase
  include Searching

  # -----
  # Table setup
  # -----

  validates :username, usermgt: { unique: true, match: /\A[a-zA-Z\d_]{3,}\z/ }
  validates :passhash, usermgt: { match: /\A(\*|[a-f\d]{40})\z/ }
  validates :realname, usermgt: { minlen: 3 }

  serialize :userinfo, Hash
  serialize :metadata, Hash
  serialize :dashboardinfo, Array

  has_many :grants, as: :grantee, dependent: :destroy
  has_many :affiliations, dependent: :destroy

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.string :username, null:false
      t.string :passhash, null:false
      t.text :realname, null:false
      t.text :comment
      t.text :userinfo
      t.integer :status, null:false, default: 1
      t.references :usergroup
      augment_table_usertag t
    end
  end
  def self.update_table_01(m,tn)
    m.change_table tn do |t|
      t.text :metadata
    end
  end
  def self.update_table_02(m,tn)
    all.to_a.each do |user|
      Affiliation.merge_data({ user_id: user.id, usergroup_id: user.usergroup_id })
    end
    m.change_table tn do |t|
      t.remove :usergroup_id
    end
  end
  def self.update_table_03(m,tn)
    m.change_table tn do |t|
      t.text :dashboardinfo
    end
  end

  # -----
  # Global search
  # -----

  def self.searchable_fields
    {
        username: :text,
        realname: :text,
        comment: :text
    }
  end

  # -----
  # DB Hooks
  # -----

  def has_been_created(_)
    metadata['_commentby'] = get_current_user
  end

  def has_been_updated(changes)
    changes.each do |k,_|
      if k == 'comment'
        metadata['_commentby'] = get_current_user
      end
    end
  end

  # -----
  # Comments
  # -----

  def self.all_comments
    comments = []
    ret = { comments: comments, element: nil }
    User.all.each do |user|
      comments << { type: 'user', obj: user, text: user.comment, owner: user.metadata['_commentby'], children: [] }
    end
    ret
  end

  # -----
  # Aggregation and helper methods
  # -----

  def get_all_privileges
    priv_collection = {}
    self.affiliations.each do |aff|
      aff.usergroup.grants.each do |grant|
        priv_collection[grant.privilege.code] = grant.privilege
      end
    end
    self.grants.each do |grant|
      priv_collection[grant.privilege.code] = grant.privilege
    end
    priv_collection
  end

  def get_rank
    rank = 1
    self.affiliations.each do |aff|
      rank = aff.usergroup.level if aff.usergroup.level > rank
    end
    rank
  end

end
