class Usergroup < ActiveRecord::Base
  include ModelBase

  # -----
  # Table setup
  # -----

  validates :name, usermgt: { unique: true, match: /\A[a-zA-Z\d_ ]{3,}\z/ }
  validates :level, usermgt: { number: (1..1000) }

  has_many :grants, as: :grantee, dependent: :destroy
  has_many :affiliations, dependent: :destroy

  augment_class_usertag

  def self.create_table(m,tn)
    m.create_table tn do |t|
      t.string :name, null:false
      t.integer :level
      t.text :description
      augment_table_usertag t
    end
  end

  def self.update_table_01(m,tn)
    m.change_table tn do |t|
      t.boolean :deleted, null: false, default: false
    end
  end

end
