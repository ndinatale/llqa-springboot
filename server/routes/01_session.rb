# interne Sessionverwaltung; Sessions gehen bei Serverstart verloren; gespeicherte Daten per Session unlimitiert
use Rack::Session::Pool, :expire_after => $expiry_time

# Sessions werden als Cookies im Browser gespeichert, bleiben über Serverneustart erhalten. Unsicherer und gespeicherte
# Daten sind auf 4KB limitert
#use Rack::Session::Cookie, :key => 'rack.session',
#                           :expire_after => 3600, # In seconds
#                           :secret => 'lean.logic.qa'

# Generiert einen Seed für eine Session
# Route SESSION_1
get '/api/auth/seed' do
  seed = SecureRandom.hex
  session[:seed] = seed
  return { seed: seed }.rest_success
end

# Login
# Route SESSION_2
post '/api/auth/login' do
  session[:logininfo] = nil
  request.body.rewind
  data = JSON.parse request.body.read

  # no username provided
  if !data['username'] || data['username'] == ''
    return assemble_error('LOGIN', 'USERUNKNOWN', {}, []).rest_fail
  end

  userdata = User.find_by_username(data['username'])
  if !session[:seed]
    return assemble_error('LOGIN', 'NOTINITIALIZED', {}, []).rest_fail
  end

  if !userdata
    return assemble_error('LOGIN', 'USERUNKNOWN', {}, []).rest_fail
  end

  # check if user is deactivated
  if userdata['status'] != 1
    return assemble_error('LOGIN', 'USERLOCKED', {}, []).rest_fail
  end

  checkpw = Digest::SHA1.hexdigest("#{userdata.passhash}.#{session[:seed]}")
  if !$debugging_api && (userdata['passhash'] != '*' && checkpw != data['passcode'])
    return assemble_error('LOGIN', 'WRONGPASSWORD', {}, []).rest_fail
  end
  ret = {
      username: userdata.username,
      userid: userdata.id,
      realname: userdata.realname,
      comment: userdata.comment,
      userinfo: userdata.userinfo,
      groups: userdata.affiliations.to_a.collect { |aff| { group: aff.usergroup.name, groupdesc: aff.usergroup.description, groupid: aff.usergroup.id, level: aff.usergroup.level } }.sort { |a,b| a[:level] <=> b[:level] },
      level: userdata.get_rank,
      grants: userdata.get_all_privileges.keys,
      dashboardinfo: userdata.dashboardinfo,
      allgrants: Privilege.order(:code).map { |e| e.serializable_hash }
  }
  session[:logininfo] = ret
  return ret.rest_success
end

# Logout
# Route SESSION_3
get '/api/auth/logout' do
  session[:logininfo] = nil
  return { }.rest_success
end

# Speichert permanente Benutzerinformationen (in Session und DB)
# Route SESSION_4
post '/api/auth/userinfo' do
  request.body.rewind
  data = JSON.parse request.body.read
  begin
    user = User.find(session[:logininfo][:userid])
    user.userinfo = data['userinfo'] if data['userinfo']
    user.dashboardinfo = data['dashboardinfo'] if data['dashboardinfo']
    user.save!
    session[:logininfo][:userinfo] = data['userinfo'] if data['userinfo']
    session[:logininfo][:dashboardinfo] = data['dashboardinfo'] if data['dashboardinfo']
  rescue => e
  end if session[:logininfo]
  return { }.rest_success
end

# Prüft, ob Benutzer eingeloggt ist
# Route SESSION_5
get '/api/auth/check' do
  if !session[:logininfo]
    return assemble_error('LOGIN', 'NOTLOGGEDIN', {}, []).rest_fail
  end
  return session[:logininfo].rest_success
end

#  Properties

# Gibt Application properties an Webapp (s. servercfg.rb, bzw. servercfg_def.rb)
# Route SESSION_6
get '/api/applicationproperties' do
  # Testenvironment setzen - wird für den Versionsnamen und die Headerfarbe gebraucht
  if $debugging_api
    $version_parameters['testenvironment'] = true
  end
  app_props = $application_parameters.merge($version_parameters)
  app_props.rest_success
end

# customer-specific features
get '/api/customerfeatures' do
  $features_array.to_json
end

# Gatekeeper

# Gatekeeper-Routen: Prüfen, ob die Webapp (Benutzer) eingeloggt ist, melden sonst ein 403. Wenn $delaytest in
# servercfg.rb auf true gesetzt ist, kann die Webapp mit (extremen) künstlichen Delays getestet werden.
get '/*' do |url|
  sleep 0.2 if $delaytest
  pass
end

get '/api/*' do
  return not_logged_in unless session[:logininfo]
  sleep 1 if $delaytest
  Thread.current[:session] = session
  pass
end

post '/api/*' do
  return not_logged_in unless session[:logininfo]
  sleep 1 if $delaytest
  Thread.current[:session] = session
  pass
end

delete '/api/*' do
  return not_logged_in unless session[:logininfo]
  sleep 1 if $delaytest
  Thread.current[:session] = session
  pass
end

get '/media/*' do
  return not_logged_in unless session[:logininfo]
  sleep 2 if $delaytest
  Thread.current[:session] = session
  pass
end

get '/servlet/*' do
  return not_logged_in unless session[:logininfo]
  sleep 2 if $delaytest
  Thread.current[:session] = session
  pass
end