require_relative '../lib/utils'
require_relative '../lib/constants'

# Checks & Workflow

# Check editieren oder neu hinzufügen
# Route CHECK_1
post '/api/checks' do
  request.body.rewind
  data = JSON.parse request.body.read

  isnew_saved = false
  ActiveRecord::Base.transaction do
    Check.merge_data(data) do |obj,isnew|
      isnew_saved = isnew
    end.first
  end

  if isnew_saved
    ActiveRecord::Base.transaction do
      #Check.find_by(model_id: data['model_id']).last.prepare_check
      Check.last.prepare_check
    end
  end
end

# Kopiert eine Prüfung
# Route CHECK_2
post '/api/checks/:cid/copy' do
  request.body.rewind
  copyinfo = JSON.parse request.body.read
  proclist = copyinfo['procs']
  upgrade = copyinfo['upgrade']
  blueprint = Check.find_by(id: params[:cid])

  if $features['configtable']
    table = Configtable.find_by(parent: 'check', parentid: blueprint.id)
    table_id = (table != nil ? table.id : 0)
  else
    table_id = 0
  end

  return assemble_error('NOTFOUND', 'TEXT', {class: 'Checks', id: cid}, []).rest_fail if !blueprint
  ActiveRecord::Base.transaction do
    data = { unit_id: blueprint.unit.id,
             model_id: upgrade < 0 ? blueprint.model.id : upgrade,
             checktype_id: blueprint.checktype.id,
             'configtable' => table_id}
    newcheck = Check.merge_data(data) do |obj,_|
      obj.prepare_check
    end.last
    newcheck.copy_check(blueprint,proclist)
    p newcheck.checkdata
    return newcheck.serializable_hash.rest_success
  end
end

# Checkinfo (Übersicht)
# Route CHECK_3
get '/api/workflow/:cid' do
  save_check_progress_to_db(params[:cid])
  Check.get_single(params[:cid]).serialize(%w(images.fullimage documents.binaryfile unit model model.devicetype.images.fullimage model.devicetype.images.preview model.devicetype.documents.binaryfile model.images.fullimage model.images.preview model.documents.binaryfile checktype)) do |h,o|
    h['procedures'] = Hash[Procedure.where(id:o.checkdata['procids']).select(:id,:code,:title).collect { |o| [o.id, { 'code' => o.code, 'title' => o.title }]}]
    h['steps'] = Hash[Step.where(id:o.checkdata['stepids']).select(:id,:code,:title).collect { |o| [o.id, { 'code' => o.code, 'title' => o.title }]}]
    h['measures'] = Hash[Measure.where(id:o.checkdata['measids']).select(:id,:code,:title).collect { |o| [o.id, { 'code' => o.code, 'title' => o.title }]}]
    h['users'] = Hash[(User.where(id:o.checkdata['assignees'].collect { |o| o['user_id'] }.compact).select(:id,:username,:realname) || []).collect { |o| [o.id, { 'username' => o.username, 'realname' => o.realname, 'level' => o.get_rank }]}]
    h['groups'] = Hash[(Usergroup.where(id:o.checkdata['assignees'].collect { |o| o['group_id'] }.compact).select(:id,:name,:level) || []).collect { |o| [o.id, { 'name' => o.name, 'level' => o.level }]}]
    tooearly = false
    if $application_parameters[:skipcheckschedule]
      h['timing'] = { tooearly: false, toolate: false }
    else
      tooearly = o.scheduled && o.scheduled > Time.now
      h['timing'] = { tooearly: tooearly, toolate: (o.dueby && o.dueby < Time.now) }
    end
    h['nextstep'] = tooearly ? nil : o.get_current_and_next_step(nil,params[:userid].to_i)[1]
    h['newerversions'] = o.newer_model_versions
    h['configtable_id'] = Check.get_table_id(o.id)
    h
  end.first
end

# Checkinfo (Schritt)
# Route CHECK_4
get '/api/workflow/:cid/step/:num' do

  # save_check_progress_to_db(params[:cid])

  _,_,cobj,chash = Check.get_single(params[:cid]).serialize(%w(unit model model.devicetype checktype))
  if params[:num].start_with?('MM')
    cdentry,cdnextentry,cdpreventry,numsteps = cobj.get_current_and_next_step_by_measurement_id(params[:num][2..-1].to_i, params[:userid].to_i)
  else
    cdentry,cdnextentry,cdpreventry,numsteps = cobj.get_current_and_next_step(params[:num].to_i, params[:userid].to_i)
  end
  return assemble_error('NOTFOUND', 'CHECKSTEXT', {cid: params['cid'], sid: params['num']}, []).rest_fail if !cdentry
  measurements,tools,admindata = {},Hash.new { |h,k| h[k] = [nil,nil] },{ 'measurers' => [] }
  cdentry['measures'].each do |m|
    mobj = Measure.find(m['id'])
    hastool = mobj.tooltype_id && !CHK_CDSTATUS_ALL_LEFTOUT.index(m['status'])
    tools[mobj.tooltype_id][0] = mobj.tooltype.serializable_hash(include: %w(toolunits images.fullimage documents.binaryfile).incllist) if hastool
    if m['mid'] < 0
      measurements[mobj.id] = {}
      tools[mobj.tooltype_id][1] = nil if hastool
    else
      msobj = Measurement.find(m['mid'])
      measurements[mobj.id] = msobj.serializable_hash
      measurements[mobj.id]['unit'] = msobj.applicable_unit
      tools[mobj.tooltype_id][1] = msobj.toolunit_id ? msobj.toolunit.serializable_hash : nil if hastool
      if msobj.savedby
        user = User.find(msobj.savedby)
        admindata['measurers'] << { 'mcode'=>mobj.code, 'username'=>user.username, 'realname'=>user.realname, 'userid'=>user.id, 'savedate'=>msobj.savedon }
      else
        admindata['measurers'] << { 'mcode'=>mobj.code, 'username'=>nil, 'realname'=>nil, 'userid'=>nil, 'savedate'=>nil }
      end
    end
  end
  if cdentry['committed']
    user = User.find(cdentry['committed'])
    admindata['committer'] = { 'username'=>user.username, 'realname'=>user.realname, 'userid'=>user.id }
  else
    admindata['committer'] = { 'username'=>nil, 'realname'=>nil, 'userid'=>nil }
  end
  tools = tools.values.sort { |a,b| a[0]['id'] <=> b[0]['id'] }
  tooearly = $application_parameters[:skipcheckschedule] ? false : cobj.scheduled && cobj.scheduled > Time.now
  Step.get_single(cdentry['id']).serialize(%w(procedure images.fullimage images.preview images.thumbnail documents.binaryfile measures)) do |h,o|
    h['check'] = chash
    h['cdentry'] = cdentry
    h['timing'] = { tooearly: tooearly, toolate: $application_parameters[:skipcheckschedule] ? false : (cobj.dueby && cobj.dueby < Time.now) }
    h['readonly'] = !(!tooearly && cobj.status == CHK_STATUS_STARTED && !(CHK_CDSTATUS_ALL_LEFTOUT_SOFT.index(cdentry['status']) && CHK_CDSTATUS_ALL_OMITTED.index(cdentry['status'])) && cdentry['assignee'] >= 0 && cobj.checkdata['assignees'][cdentry['assignee']]['user_id'] == params[:userid].to_i)
    h['nextstepid'] = cdnextentry
    h['prevstepid'] = cdpreventry
    h['measurements'] = measurements
    h['tools'] = tools
    h['admindata'] = admindata
    h['numsteps'] = numsteps
    h
  end.first
end

# (Neu)zuweisung von Prozeduren/Schritten
# Route CHECK_5
post '/api/workflow/:cid/assign' do
  request.body.rewind
  data = JSON.parse request.body.read
  cobj = Check.find_by(id:params['cid'])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Checks', id: params['cid']}, []).rest_fail if !cobj
  ActiveRecord::Base.transaction do
    cobj.assign(data)
    return { }.rest_success
  end
end

# Aktion auf Check ausführen
# Route CHECK_6
post '/api/workflow/:cid/action' do
  request.body.rewind
  data = JSON.parse request.body.read
  cobj = Check.find_by(id:params['cid'])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Checks', id: params['cid']}, []).rest_fail if !cobj
  ActiveRecord::Base.transaction do
    return cobj.checkaction(data)
  end
end

# Aktion auf Schritt im Check ausführen
# Route CHECK_7
post '/api/workflow/:cid/step/:num/action' do
  request.body.rewind
  data = JSON.parse request.body.read
  cobj = Check.find_by(id:params['cid'])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Checks', id: params['cid']}, []).rest_fail if !cobj
  cdentry,_,_ = cobj.get_current_and_next_step(params[:num].to_i, params[:userid].to_i)
  return assemble_error('NOTFOUND', 'CHECKSTEXT', {cid: params['cid'], sid: params['num']}, []).rest_fail if !cdentry
  ActiveRecord::Base.transaction do
    return cobj.stepaction(data,cdentry)
  end
end

# Gibt für ein Element die Workflow-Modifikatoren zurück
# Route CHECK_8
get '/api/wflowmod' do
  proc = params[:proc]
  ret = {
      'checktypes' => Checktype.get_list2() { |q| q.select(:code, :title).order(code: :asc, id: :desc) }.serialize()[2][:rows].collect { |i| [ i['code'], i['title'] ] } .uniq { |s| s.first } ,
      'models' => Model.get_list2() { |q| q.select(:code, :title).order(code: :asc, id: :desc) }.serialize()[2][:rows].collect { |i| [ i['code'], i['title'] ] }.uniq { |s| s.first } ,
      'devicetypes' => Devicetype.get_list2() { |q| q.select(:code, :title).order(code: :asc, id: :desc) }.serialize()[2][:rows].collect { |i| [ i['code'], i['title'] ] }.uniq { |s| s.first } ,
      'procedures' => Procedure.get_list2() { |q| q.select(:code, :title).order(code: :asc, id: :desc) }.serialize()[2][:rows].collect { |i| [ i['code'], i['title'] ] }.uniq { |s| s.first },
      'configtables' => Configentry.get_list2() { |q|
        q.select('configtable_id, code_id, col1, col2').joins(:configtable).where("configtables.parent = 'model'").order(:configtable_id, :code_id)
      }.serialize()[2][:rows].collect{ |i|
        [
            # using "nil coalescing" to prevent null pointer when first or second column are empty
            i['configtable_id'].to_s + "." + i['code_id'].to_s,
            (i['col1'] || '') + ' ' + (i['col2'] || '')
        ]
      }
  }
  if proc
    ret['steps'] = Step.get_list2() { |q|
      q.where(procedure_id: proc).select(:code, :title).order(code: :asc, id: :desc)
    }.serialize()[2][:rows].collect { |i|
      [ i['code'], i['title'] ]
    }.uniq { |s| s.first }

    ret['measures'] = []
    Step.get_list { |q| q.where(procedure_id: proc) }.serialize(%w(measures))[2][:rows].collect do |s|
      s['measures'].each { |m| ret['measures'] << [ "#{s['code']}.#{m['code']}", m['title'] ] }
    end

    ret['measures'].uniq!
  end
  ret.rest_success
end

# Route CHECK_9
get '/api/procedures/:prid/models/:mrid/checks' do
  prid = params[:prid].to_i
  obj = Model.find_current(params[:mrid].to_i)
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Models', id: params['mid']}, []).rest_fail if !obj || obj.size < 1
  midlv = obj[0].id
  cnt = 0
  Unit.get_list() do |q|
    q = q.where(model_id: midlv)
    params[:search].downcase.gsub(/[^a-z\d ]/,'_').split(' ').each do |str|
      q = q.where('lower(code) like lower(?) or lower(customer) like lower(?)',"%#{str}%","%#{str}%")
    end
    q = q.limit(25)
    q
  end.serialize(%w(checks.checktype checks.model checks.unit)) do |h,o|
    h['checks'] = h['checks'].delete_if { |check| !Check.find(check['id']).uses_procedure?(prid) }
    cnt += 1
    h['num'] = cnt
    h = nil if h['checks'].empty?
    h
  end.first
end

# Measurements

# Route CHECK_10
post '/api/measurements/test' do
  request.body.rewind
  data = JSON.parse request.body.read
  measure = Measure.find_by(id: data['measure_id'])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Measures', id: data['measure_id']}, []).rest_fail if !measure
  vt = ValueTester.new(data['rawvalues'], nil, measure, [])
  vt.check
  return { status: vt.newstatus, value: vt.stringify_value }.rest_success
end

# Testruns

# Route CHECK_11
get '/api/testrun/procedure/:prid/ctype/:ctypeid' do
  pobj = Procedure.find_by(id: params[:prid].to_i)
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Procedures', id: params['prid']}, []).rest_fail if !pobj
  ActiveRecord::Base.transaction do
    usercode = "#{get_current_username.upcase.gsub('??','NYM')}(TR)"
    Model.where(code: usercode).each do |mobj|
      mobj.destroy
    end
    modeltitle = {}
    $system_languages.map { |a| a[:code] }.each { |lng| modeltitle[lng] = "TESTMODEL (#{lng})" }
    mmerge = Model.merge_data({ devicetype_id: Devicetype.all[0].id, code: usercode, title: modeltitle, description: {}, metadata: {}, version: 1 })
    mobj = mmerge.last
    Activeprocedure.merge_data({ model_id: mobj.id, procedure_id: pobj.id, flowcontrol: [] })
    Activechecktype.merge_data({ model_id: mobj.id, checktype_id: Checktype.all[0].id })
    Unit.where(code: usercode).each do |uobj|
      uobj.destroy
    end
    uobj = Unit.merge_data({ model_id: mobj.id, code: usercode, customer: 'TESTCUSTOMER', status: 0, metadata: {} }).last
    return Check.merge_data({ unit_id: uobj.id, model_id: mobj.id, checktype_id: params[:ctypeid].to_i }) do |cobj,_|
      cobj.prepare_check
      cobj.preinit_check('procedure', pobj.code)
    end.first
  end
end

# Route CHECK_12
get '/api/testrun/model/:modid/ctype/:ctypeid' do
  mobj = Model.find_by(id: params[:modid].to_i)
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Models', id: params['modid']}, []).rest_fail if !mobj
  ActiveRecord::Base.transaction do
    usercode = "#{get_current_username.upcase.gsub('??','NYM')}(TR)"
    Unit.where(code: usercode).each do |uobj|
      uobj.destroy
    end
    uobj = Unit.merge_data({ model_id: mobj.id, code: usercode, customer: 'TESTCUSTOMER', status: 0, metadata: {} }).last
    return Check.merge_data({ unit_id: uobj.id, model_id: mobj.id, checktype_id: params[:ctypeid].to_i }) do |cobj,_|
      cobj.prepare_check
      cobj.preinit_check('model', mobj.code)
    end.first
  end
end

# revalidate check from configtable
put '/api/checks/:checkid/revalidate' do
  ActiveRecord::Base.transaction do
    check = Check.find(params[:checkid])
    check.revalidate
    return {}.rest_success
  end
end

# Route CHECK_14
def save_check_progress_to_db(cid, deduct_skipped = true)

  # if !$lastSave
  #   $lastSave = {}
  # end

  # Letzte Speicherung länger als 5min her?
  # if !$lastSave[cid] ||  $lastSave[cid] && (Time.now - $lastSave[cid] > 300) # Sekunden

    # $lastSave[cid] = Time.now

    check = Check.find(cid)

    stats,total = Hash.new(0),0
    check.checkdata['procedures'].each do |proc|
      proc['steps'].each do |step|
        step['measures'].each do |measure|
          stats[measure['status']] += 1
          total += 1
        end
      end
    end

    # cnt_unfin = stats[CHK_CDSTATUS_TODO] not needed
    cnt_passed = stats[CHK_CDSTATUS_PASSED]
    cnt_failed = stats[CHK_CDSTATUS_WARNINGS]+stats[CHK_CDSTATUS_FAILED]
    cnt_skipped = stats[CHK_CDSTATUS_OMITTED_HARD]+stats[CHK_CDSTATUS_OMITTED_SOFT]+stats[CHK_CDSTATUS_SKIPPED]
    total -= cnt_skipped if deduct_skipped
    perc = total == 0 ? 0.0 : (100.0/total)
    check.progress_finished = perc*cnt_passed
    check.progress_error = perc*cnt_failed


    # assignments löschen und anschliessend wieder einfügen (einfacherer Weg als zu Prüfen
    # ob user oder group XY noch assignt sind und wenn nein, diese rauslöschen...)
    check.assign_data['user_ids'] = []
    check.assign_data['group_ids'] = []

    # assignees durchgehen und die user_ids und group_ids im assign_data speichern
    check.checkdata['assignees'].each do |ass|

      if ass['user_id']                                 # Wenn user_id schon vorhanden ist, nicht einfügen (Redundanzen)
        check.assign_data['user_ids'] << ass['user_id'] if !check.assign_data['user_ids'].include? ass['user_id']
      end
      if ass['group_id']                                  # Wenn group_id schon vorhanden ist, nicht einfügen (Redundanzen)
        check.assign_data['group_ids'] << ass['group_id'] if !check.assign_data['group_ids'].include? ass['group_id']
      end

    end

    check.save!
end