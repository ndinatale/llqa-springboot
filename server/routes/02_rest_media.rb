# Mediafiles

# Route MEDIA_1
get '/api/media/:owner/:ownerid/images' do
  Image.get_list() do |q|
    q.where(owner_id: params['ownerid'], owner_type: params['owner']).order(seqnum: :asc)
  end.serialize(%w(fullimage preview thumbnail)).first
end

# Route MEDIA_2
delete '/api/media/images/:id' do
  img = Image.find_by(id:params[:id])

  # Zum Image dazugehörige Binaryfiles finden (3)
  binaryfiles = Binaryfile.find(img.fullimage_id, img.preview_id, img.thumbnail_id)
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Images', id: params['id']}, []).rest_fail if !img
  ActiveRecord::Base.transaction do

    # Image Eintrag löschen
    fullimage_id = img.fullimage_id
    preview_id = img.preview_id
    thumbnail_id = img.thumbnail_id
    img.destroy
    puts "Image has been deleted:\n#{img.inspect}"

    # Alle 3 Einträge in der Binaryfiles Tabelle löschen und die Bilddateien
    # aus dem /uploads Verzeichnis löschen
    binaryfiles.each do |bf|

      # Überprüfen, ob andere Einträge in IMAGES die binaryfiles referenzieren, da beim Hochladen
      # eines Bildes, welches schon vorhanden sind, die binaryfiles Einträge "wiederbenutzt" werden
      bfs_still_in_use = Image.where("fullimage_id = ? or preview_id = ? or thumbnail_id = ?", fullimage_id, preview_id, thumbnail_id)

      # Einträge NICHT löschen, wenn andere Records darauf referenzieren
      break if !bfs_still_in_use.empty?

      if File.exist?("../uploads/#{bf.filename}")
        File.delete("../uploads/#{bf.filename}")
        puts "File has been deleted:\n#{bf.filename}"
      end
      puts "Binaryfile has been deleted:\n#{bf.inspect}"
      bf.destroy
    end
  end
  return {}.rest_success
end

# Route MEDIA_3
post '/api/media/images/:id/action_reorder' do
  obj = Image.find_by(id:params['id'])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Images', id: params['id']}, []).rest_fail if !obj
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    obj.reorder(data['neworder'])
  end
  return { }.rest_success
end

# Route MEDIA_4
post '/api/media/images' do
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    return Image.merge_data(data).first
  end
end

# Route MEDIA_5
get '/api/media/:owner/:ownerid/docs' do
  Document.get_list() do |q|
    q.where(owner_id: params['ownerid'], owner_type: params['owner']).order(seqnum: :asc)
  end.serialize(%w(binaryfile)).first
end

# Route MEDIA_6
delete '/api/media/docs/:id' do
  doc = Document.find_by(id:params[:id])

  # Zum Document dazugehöriges Binaryfile finden (1)
  binaryfile = Binaryfile.find(doc.binaryfile_id)
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Documents', id: params['id']}, []).rest_fail if !doc
  ActiveRecord::Base.transaction do

    # Document Eintrag löschen
    binaryfile_id = doc.binaryfile_id
    doc.destroy_logged

    # Prüfen, ob andere Einträge auf diesen Binaryfile-Record referenzieren
    bfs_still_in_use = Document.where("binaryfile_id = ?", binaryfile_id)

    # Wenn keine Document-Einträge darauf referenzieren, kann der Binaryfile-Eintrag
    # und die dazugehörige Datei gelöscht werden
    if bfs_still_in_use.empty?

      # Eintrag in der Binaryfiles Tabelle löschen und die Documentdatei
      # aus dem /uploads Verzeichnis löschen
      if File.exist?("../uploads/#{binaryfile.filename}")
        File.delete("../uploads/#{binaryfile.filename}")
      end
      binaryfile.destroy_logged
    end
  end
  return {}.rest_success
end

# Route MEDIA_7
post '/api/media/docs/:id/action_reorder' do
  obj = Document.find_by(id:params['id'])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Documents', id: params['id']}, []).rest_fail if !obj
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    obj.reorder(data['neworder'])
  end
  return { }.rest_success
end

# Route MEDIA_8
post '/api/media/docs' do
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    return Document.merge_data(data).first
  end
end

# Route MEDIA_9
post '/api/media/:owner/:ownerid/upload' do
  oname = params['file'][:filename]
  ext = oname.gsub(/\A.*\./,'')
  type = params['file'][:type]
  file = File.read(params['file'][:tempfile])
  ActiveRecord::Base.transaction do
    if %w(video/mkv video/avi video/mp4 video/quicktime).index(type)
      return Document.importVideo(file,params,oname,ext,type)
    end
    if %w(application/pdf).index(type)
      return Document.importPdf(file,params,oname,ext,type)
    end
    #if %w(text/plain).index(type)
    #  return Document.importText(file,params,oname,ext,type)
    #end
    if %w(image/jpeg image/tiff image/png).index(type)
      return Image.importImage(file,params,oname,ext,type)
    end
  end
  return assemble_error('INVMEDIA', 'TEXT', {}, []).rest_fail
end
