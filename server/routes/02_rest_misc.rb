# Tooltypes

# Route MISC_1
get '/api/tooltypes' do
  if params['selector']
    obj = Tooltype.joins(:toolunits).where(Tooltype.table_name => {deleted: false}).where(toolunits: { deleted: false}).distinct
  else
    obj = Tooltype.joins(:toolunits).distinct
  end
  obj.get_list().serialize.first
end

# Route MISC_2
get '/api/tools' do
  Tooltype.get_list().serialize() { |h,o| h['unitcnt'] = o.toolunits.count; h['measurecnt'] = o.measures.count; h }.first
end

# Route MISC_3
get '/api/tools/:id' do
  tmp = Tooltype.get_single(params[:id])
  if (params[:foredit])
    return tmp.serialize().first
  end
  tmp.serialize(params[:foredit] ? nil : %w(images documents measures toolunits.measurements)).first
end

# Route MISC_4
delete '/api/tools/:id' do
  obj = Tooltype.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Tooltype', id: params['id']}, []).rest_fail if !obj
  ActiveRecord::Base.transaction do
    obj.destroy
  end
  return {}.rest_success
end

# Route MISC_5
post '/api/tools' do
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    return Tooltype.merge_data(data).first
  end
end

# Route MISC_6
post '/api/toolunits' do
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    return Toolunit.merge_data(data).first
  end
end

# Route MISC_7
delete '/api/toolunits/:id' do
  obj = Toolunit.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Toolunit', id: params['id']}, []).rest_fail if !obj
  ActiveRecord::Base.transaction do
    obj.destroy
  end
  return {}.rest_success
end

# Devicetypes

# Route MISC_8
get '/api/devicetypes' do
  if params['formodelsel']
    return Devicetype.get_list().serialize(%w(models)) do |h,o|
      h['models'].delete_if { |model| model['version'] != 1 }
      h = nil if h['models'].empty?
      h
    end.first
  end
  Devicetype.get_list() do |q|
    q.where(deleted: false).order(code: :asc) unless params['all'] == 'true'
    q
  end.serialize(%w(images documents)) { |h,o| h['modelcnt'] = o.models.to_a.collect { |c| c.realid }.uniq.size; h }.first
end

# Route MISC_9
delete '/api/devicetypes/:id' do
  obj = Devicetype.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Devicetypes', id: params['id']}, []).rest_fail if !obj
  ActiveRecord::Base.transaction do
    obj.destroy
  end
  return {}.rest_success
end

# Route MISC_10
post '/api/devicetypes' do
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    return Devicetype.merge_data(data).first
  end
end

# Checktypes

# Route MISC_11
get '/api/checktypes' do
  Checktype.get_list() do |q|
    q.where(deleted: false).order(code: :asc) unless params['all'] == 'true'
    q
  end.serialize() { |h,o|
    checkcnt = 0
    o.checks.each do |check|
      # only count if not a check from a testunit
      if !check.unit.code.end_with?('(TR)')
        checkcnt += 1
      end
    end
    h['checkcnt'] = checkcnt

    # counting models
    h['modelcnt'] = o.activechecktypes.to_a.collect { |c|
      # count realid, so it should be only one per model
      if !c.model.code.end_with?('(TR)')
        c.model.realid
      end
    }.uniq.size

    h
  }.first
end

# Route MISC_12
delete '/api/checktypes/:id' do
  obj = Checktype.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Checktypes', id: params['id']}, []).rest_fail if !obj
  ActiveRecord::Base.transaction do
    obj.destroy
  end
  return {}.rest_success
end

# Route MISC_13
post '/api/checktypes' do
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    return Checktype.merge_data(data).first
  end
end

# Global search

# Route MISC_14
get '/api/globalsearch' do
  return { searchable: Searching.searchable_classes }.rest_success
end

# Route MISC_15
post '/api/globalsearch' do
  request.body.rewind
  data = JSON.parse request.body.read
  return { result: Searching.perform_search(data) }.rest_success
end

# Dashboards

# Route MISC_16
get '/api/dashboard/list' do
  { list: get_all_dashboard_types }.rest_success
end

# Route MISC_17
get '/api/dashboard/:dbnum/block/:blnum' do
  user = get_current_userobject
  return assemble_error('LOGIN', 'NOTLOGGEDIN', {}, []).rest_fail if !user

  dbnum,blnum = params[:dbnum].to_i, params[:blnum].to_i
  return { rows: [] }.rest_success if !user.dashboardinfo || !user.dashboardinfo[dbnum] || !user.dashboardinfo[dbnum]['blocks'] || !(block = user.dashboardinfo[dbnum]['blocks'][blnum]) || !respond_to?("dashboard_type_#{block['type']}", true)

  method("dashboard_type_#{block['type']}").call(block['filters'], false)
end

# Snippets

# Route MISC_18
get '/api/snippets/:cat' do
  Snippet.get_list() do |q|
    q = q.where(category: params[:cat], status: [1,2]).order(seqnum: :asc)
    q
  end.serialize().first
end

# Route MISC_19
delete '/api/snippets/:id' do
  obj = Snippet.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Snippet', id: params['id']}, []).rest_fail if !obj
  ActiveRecord::Base.transaction do
    obj.destroy
  end
  return {}.rest_success
end

# Route MISC_20
post '/api/snippets' do
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    return Snippet.merge_data(data) do |obj,isnew|
      if isnew && data['seqnum']
        oldsn = Snippet.find_by(seqnum: data['seqnum'])
        if (oldsn)
          oldsn.seqnum = obj.seqnum
          oldsn.save!
        end
        obj.seqnum = data['seqnum']
      end
    end.first
  end
end

# Route MISC_24
post '/api/snippets/:id/action_reorder' do
  obj = Snippet.find_by(id:params['id'])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Snippets', id: params['id']}, []).rest_fail if !obj
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    obj.reorder(data['neworder'])
  end
  return { }.rest_success
end

# Notices

# Route MISC_21
get '/api/notices/editinfo' do
  Notice.editorinfo.rest_success
end

# Route MISC_22
post '/api/notices' do
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    return Notice.merge_data(data) do |notice,isnew|
      notice.set_status(1, nil) if isnew
      notice.set_status(data['status'], data['comment']) if data['status']
    end.first
  end
end

# Route MISC_23
get '/api/notices' do
  Notice.get_list() do |q|
    q = q.where.not(status: 9).order(created_at: :desc)
    q
  end.serialize(%w(category)) do |h,o|
    if o.archive
      h = nil
    else
      o.prepare_rest(h)
    end
    h
  end.first
end

get '/api/configtables' do
  Configtable.get_list do |q|
    # don't show tables which belong to unit
    q.where("parent IS NULL OR parent = '' OR parent = 'model'")
    #q.where.not('parent = ?', 'unit') # somehow this isn't working like it should...
  end.serialize.first
end

get '/api/configtables/:id' do
  table = Configtable.get_single(params[:id])
  table.serialize.first
end

get '/api/configtables/:id/entries' do
  Configentry.get_list() do |q|
    q = q.where(configtable_id: params[:id])
    q
  end.serialize.first
end

delete '/api/configentries/:id' do
  obj = Configentry.find_by(id: params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Toolunit', id: params['id']}, []).rest_fail if !obj
  # don't delete, just set flag
  obj['deleted'] = true
  obj.save
  return {}.rest_success
end

post '/api/configentries' do
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    return Configentry.merge_data(data).first
  end
end

get '/api/configentries/:id' do
  tmp = Configentry.get_single(params[:id])
  tmp.serialize.first
end

post '/api/configtables' do
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    return Configtable.merge_data(data).first
  end
end

post '/api/configtables/:id/clone' do
  ActiveRecord::Base.transaction do
    hash = Configtable.export_to_hash(params[:id], false)[3]
    ret = Configtable.import_from_hash(hash)
    return { new_id: ret.id }.rest_success
  end
end

# link a configuration table to a model
put '/api/configtables/:tableid/linktomodel/:modelid' do
  ActiveRecord::Base.transaction do
    table = Configtable.find(params[:tableid])
    model = Model.find(params[:modelid])

    table.parent = 'model'
    table.parentid = model.id
    table.parentcode = model.code
    table.save

    return {}.rest_success
  end
end

# unlink a configuration table
delete '/api/configtables/:tableid/linktomodel' do
  ActiveRecord::Base.transaction do
    table = Configtable.find(params[:tableid])
    table.parent = nil
    table.parentid = nil
    table.parentcode = nil
    table.save
    return {}.rest_success
  end
end

delete '/api/configtables/:id' do
  obj = Configtable.find_by(id: params[:id])
  # don't delete, just set flag
  obj['deleted'] = true
  obj.save
  # also mark entries belonging to table as deleted
  Configentry.where(configtable_id: params[:id]).find_each do |entry|
    entry.deleted = true
    entry.save
  end
  return {}.rest_success
end

post '/api/configtables/import' do
  data = check_import_hash(File.read(params['file'][:tempfile]), 'Configtable', params['file'][:filename])
  return data if data.is_a? Array
  #data['procedure_id'] = params['id'].to_i
  ActiveRecord::Base.transaction do
    ret = Configtable.import_from_hash(data)
    return { new_id: ret.id }.rest_success
  end
end

post '/api/configtables/:id/reactivate' do
  obj = Configtable.find_by(id: params[:id])
  obj['deleted'] = false
  obj.save
  Configentry.where(configtable_id: params[:id]).find_each do |entry|
    entry.deleted = false
    entry.save
  end
  return {}.rest_success
end

get '/api/settings' do
  Settings.get_list() do |q|
    q.order(key: :asc)
  end.serialize.first
end

post '/api/settings' do
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    return Settings.merge_data(data).first
  end
end

get '/api/settings/:id' do
  tmp = Settings.get_single(params[:id])
  return tmp.serialize().first
end

delete '/api/settings/:id' do
  obj = Settings.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Settings', id: params['id']}, []).rest_fail if !obj
  ActiveRecord::Base.transaction do
    obj.destroy
  end
  return {}.rest_success
end

# Only for testing - add a blocking rule on a config entry
#get '/api/configentries/addrule/:id/procedure/:procid' do
#  tmp = Configentry.find(params[:id])
#  tmp.add_blocker(params[:procid], 'p')
#end

# remove a blocking rule on a config entry
delete '/api/configentry/:code/blocker/:type/id/:id' do
  table_id = params[:code].split('.').first.to_i
  entry_id = params[:code].split('.').last.to_i
  entry = Configentry.find_by_table(table_id, entry_id)
  entry.remove_blocker(params[:id].to_i, params[:type])
end
