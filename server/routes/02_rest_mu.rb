# Models

# Route MU_1
get '/api/models' do
  usagehash = Unit.using_model if !params['selector']
  Model.get_list() do |q|
    if params['selector']
      q = q.latest_version_objects
      q.order(code: :asc)
    else
      q.trunk_and_disabled_objects.order(realid: :asc, version: :asc)
    end
  end.serialize(%w(devicetype units)) do |h,o|
    h['versions'] = Model.all_versions(o.realid) if !params['selector']
    h['usage'] = usagehash[o.realid] if !params['selector']
    h['dirty'] = o.is_dirty?
    h
  end.first
end

# Route MU_2
post '/api/models/import' do
  data = check_import_hash(File.read(params['file'][:tempfile]), 'Model', params['file'][:filename])
  return data if data.is_a? Array
  ActiveRecord::Base.transaction do
    ret = Model.import_from_hash(data)
    return { new_id: ret.id }.rest_success
  end
end

# Route MU_3
get '/api/models/:id' do
  tmp = Model.get_single(params[:id])
  if (params[:foredit])
    return tmp.serialize().first
  end
  tmp.serialize(params[:foredit] ? nil : %w(activeprocedures.procedure devicetype activechecktypes.checktype images.fullimage images.preview images.thumbnail documents.binaryfile)) do |h,o|
    h['counters'] = o.counters
    h['activeprocedures'].each do |p|
      p['procedure']['scnt'] = Step.where(procedure_id: p['procedure']['id']).count
      lvers = Procedure.latest_version(p['procedure']['realid'])
      lvers = Procedure.version_object(p['procedure']['realid'],lvers) if lvers
      p['procedure']['vinfo'] = lvers ? { latest: lvers.version, latest_id: lvers.id, disabled: Procedure.is_disabled?(p['procedure']['realid']) } : { latest: nil, disabled: false }
      p['preallocation'] = o.preallocated_status(p['procedure']['realid'])
    end
    h['versions'] = Model.all_versions(o.realid)
    h['dirty'] = o.is_dirty?
    h['configtable_id'] = Model.get_table_id(o.code)
    h
  end.first
end

# Route MU_4
get '/api/models/:id/changelog' do
  obj = Model.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Models', id: params['id']}, []).rest_fail if !obj
  ret = []
  obj.datachanges.each do |change|
    line = change.serializable_hash
    line['user'] = change.extend_user
    ret << line
  end
  { changelog: ret }.rest_success
end

# Route MU_5
delete '/api/models/:id' do
  obj = Model.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Models', id: params['id']}, []).rest_fail if !obj
  ActiveRecord::Base.transaction do
    obj.destroy_vsafe
  end
  return {}.rest_success
end

# Route MU_6
post '/api/models' do
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    return Model.merge_data(data).first
  end
end

# Route MU_7
get '/api/models/:id/action_finalize' do
  obj = Model.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Models', id: params['id']}, []).rest_fail if !obj
  ActiveRecord::Base.transaction do
    obj.finalize_trunk
  end
  return { trunk_id: Model.trunk_id(obj.realid) }.rest_success
end

# Route MU_8
get '/api/models/:id/action_reset' do
  obj = Model.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Models', id: params['id']}, []).rest_fail if !obj
  ActiveRecord::Base.transaction do
    obj.reset_trunk
  end
  return { trunk_id: Model.trunk_id(obj.realid) }.rest_success
end

# Route MU_9
get '/api/models/action_auto_update/:procrid' do
  ActiveRecord::Base.transaction do
    Model.update_procedure(params[:procrid].to_i)
  end
  return { }.rest_success
end

# Route MU_10
post '/api/models/:id/preallocate' do
  obj = Model.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Models', id: params['id']}, []).rest_fail if !obj
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    data['checktypes'].each do |ct|
      obj.preallocate(data['proc_rid'], ct, data['user'], data['group'])
    end
    obj.has_been_updated({'prealloc' => data['proc_rid']})
    obj.save!
  end
  return { }.rest_success
end

# Models (Subtables)

# Route MU_11
post '/api/activeprocedures/:id/action_reorder' do
  obj = Activeprocedure.find_by(id:params['id'])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Activeprocedures', id: params['id']}, []).rest_fail if !obj
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    obj.reorder(data['neworder'])
  end
  return { }.rest_success
end

# Route MU_12
delete '/api/activeprocedures/:id' do
  obj = Activeprocedure.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Activeprocedures', id: params['id']}, []).rest_fail if !obj
  ActiveRecord::Base.transaction do
    obj.destroy_logged
  end
  return {}.rest_success
end

# Route MU_13
delete '/api/activechecktypes/:id' do
  obj = Activechecktype.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Activechecktypes', id: params['id']}, []).rest_fail if !obj
  ActiveRecord::Base.transaction do
    obj.destroy_logged
  end
  return {}.rest_success
end

# Route MU_14
post '/api/models/:id/activeprocedures' do
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    data.each do |pid|
      res,succ = Activeprocedure.merge_data({model_id: params[:id], procedure_id: pid})
      return res unless succ
    end
  end
  return {}.rest_success
end

# Route MU_15
post '/api/activeprocedures' do
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    return Activeprocedure.merge_data(data).first
  end
end

# Route MU_16
post '/api/models/:id/activechecktypes' do
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    data.each do |pid|
      res,succ = Activechecktype.merge_data({model_id: params[:id], checktype_id: pid})
      return res unless succ
    end
  end
  return {}.rest_success
end

# Units

# Route MU_17
get '/api/models/:mrid/units' do
  obj = Model.find_current(params[:mrid].to_i)
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Models', id: params['mid']}, []).rest_fail if !obj || obj.size < 1
  midlv = obj[0].id
  Unit.get_list() do |q|
    q = q.where(model_id: midlv)
    if (params[:archive])
      q = q.where('status >= 100')
      params[:archive].downcase.gsub(/[^a-z\d ]/,'_').split(' ').each do |str|
        q = q.where('lower(code) like lower(?) or lower(customer) like lower(?)',"%#{str}%","%#{str}%")
      end
      q = q.limit(100)
    else
      q = q.where('status < 100')
    end
    q
  end.serialize(%w(model.devicetype)) do |h,o|
    h['ckcnt'] = o.checks.count
    h = nil if !params[:archive] && o.archive
    h
  end.first
end

# Route MU_18
get '/api/units/:id' do
  tmp = Unit.get_single(params[:id])
  if (params[:foredit])
    return tmp.serialize().first
  end
  tmp.serialize(params[:foredit] ? nil : %w(model.devicetype model.activeprocedures.procedure model.activechecktypes.checktype checks.checktype checks.model)).first
end

# Route MU_19
delete '/api/units/:id' do
  obj = Unit.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Units', id: params['id']}, []).rest_fail if !obj
  ActiveRecord::Base.transaction do
    obj.destroy_logged
  end
  return {}.rest_success
end

# Route MU_20
post '/api/units' do
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    return Unit.merge_data(data).first
  end
end

# Alle Konfigurationstabellen von einer Einheit
get '/api/units/:id/configtables' do
  obj = Unit.find(params[:id])
  unitcode = obj.code

  Configtable.get_list() do |q|
    q.where(parentcode: unitcode)
    #q.where('parentcode = ?', unitcode)
  end.serialize.first
end
