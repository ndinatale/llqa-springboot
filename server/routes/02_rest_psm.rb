# Procedures

# Route PSM_1
get '/api/procedures' do
  if (params['copysel'])
    return Procedure.get_list() do |q|
      q.where(disabled: false).order(realid: :asc, version: :asc)
    end.serialize { |h,_| h.delete_if { |k,_| !%w(id realid title code version).index(k) } }.first
  end
  if (params['selector'])
    return Procedure.get_list() do |q|
      q = q.latest_version_objects
      q.order(code: :asc)
    end.serialize.first
  end
  usagemap = Model.using_procedure
  Procedure.get_list() do |q|
    q.trunk_and_disabled_objects.order(realid: :asc, version: :asc)
  end.serialize do |h,o|
    h['stepcnt'] = o.steps.count
    h['usage'] = usagemap[o.realid]
    h['versions'] = Procedure.all_versions(o.realid)
    h['dirty'] = o.is_dirty?
    h
  end.first
end

# Route PSM_2
get '/api/procedures/:id' do
  tmp = Procedure.get_single(params[:id])
  if (params[:foredit])
    return tmp.serialize().first
  end
  tmp.serialize(%w(steps activeprocedures.model)) do |h,o|
    usagemap = Model.using_procedure(o.realid,true)
    h['steps'].each { |s| s['mcnt'] = Step.find(s['id']).measures.count }
    h['versions'] = Procedure.all_versions(o.realid)
    h['updatablemodels'] = usagemap[1]
    h['usage'] = usagemap[0]
    h['dirty'] = o.is_dirty?
    h
  end.first
end

# Route PSM_3
get '/api/procedures/:id/changelog' do
  obj = Procedure.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Procedures', id: params['id']}, []).rest_fail if !obj
  ret = []
  obj.datachanges.each do |change|
    line = change.serializable_hash
    line['user'] = change.extend_user
    ret << line
  end
  { changelog: ret }.rest_success
end

# Route PSM_4
post '/api/procedures' do
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    return Procedure.merge_data(data).first
  end
end

# Route PSM_5
post '/api/procedures/import' do
  data = check_import_hash(File.read(params['file'][:tempfile]), 'Procedure', params['file'][:filename])
  return data if data.is_a? Array
  ActiveRecord::Base.transaction do
    ret = Procedure.import_from_hash(data)
    return { new_id: ret.id }.rest_success
  end
end

# Route PSM_6
post '/api/procedures/clone' do
  request.body.rewind
  data = JSON.parse request.body.read
  proc_ids = []
  ActiveRecord::Base.transaction do
    data.each do |id|
      hash = Procedure.export_to_hash(id,false)[3]
      ret = Procedure.import_from_hash(hash)
      proc_ids << ret.id
    end
    return { newprocs: proc_ids }.rest_success
  end
end

# Route PSM_7
delete '/api/procedures/:id' do
  obj = Procedure.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Procedures', id: params['id']}, []).rest_fail if !obj
  ActiveRecord::Base.transaction do
    obj.destroy_vsafe
  end
  return {}.rest_success
end

# Route PSM_8
get '/api/procedures/:id/action_finalize' do
  obj = Procedure.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Procedures', id: params['id']}, []).rest_fail if !obj
  ActiveRecord::Base.transaction do
    obj.finalize_trunk
  end
  return { trunk_id: Procedure.trunk_id(obj.realid) }.rest_success
end

# Route PSM_9
get '/api/procedures/:id/action_reset' do
  obj = Procedure.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Procedures', id: params['id']}, []).rest_fail if !obj
  ActiveRecord::Base.transaction do
    obj.reset_trunk
  end
  return { trunk_id: Procedure.trunk_id(obj.realid) }.rest_success
end

# Route PSM_10
post '/api/procedures/:pid/checkmeasureids' do
  request.body.rewind
  data = JSON.parse request.body.read
  input,list = data['ids'],[]
  obj = Procedure.find_by(id:params[:pid])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Procedures', id: params['id']}, []).rest_fail if !obj
  begin
    input.gsub("\n",'').gsub("\t",'').gsub(/[^\d;]/,'').split(';').each do |id|
      next if !id || id == ''
      mobj = Measure.find_by(id:id)
      next unless mobj
      next if mobj.step.procedure.code != obj.code
      list << id
    end
  rescue
  end
  return { valid_ids: list }.rest_success
end

# Route PSM_11
post '/api/procedures/:pid/checkcheckids' do
  request.body.rewind
  data = JSON.parse request.body.read
  input,list = data['ids'],[]
  obj = Procedure.find_by(id:params[:pid])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Procedures', id: params['id']}, []).rest_fail if !obj
  begin
    input.gsub("\n",'').gsub("\t",'').gsub(/[^\d;]/,'').split(';').each do |id|
      next if !id || id == ''
      cobj = Check.find_by(id:id)
      next unless cobj
      next unless cobj.uses_procedure?(obj.realid)
      list << { id: cobj.id, mcode: cobj.model.code, ucode: cobj.unit.code, ctcode: cobj.checktype.code }
    end
  rescue
  end
  return { valid_ids: list }.rest_success
end

# Steps

# Route PSM_12
get '/api/procedures/:pid/steps' do
  if (params['copysel'])
    return Step.get_list() do |q|
      q.where(procedure_id: params[:pid]).order(seqnum: :asc)
    end.serialize { |h,_| h.delete_if { |k,_| !%w(id title code).index(k) } }.first
  end
  if (params['copyselmeasure'])
    return Step.get_list() do |q|
      q.where(procedure_id: params[:pid]).order(seqnum: :asc)
    end.serialize(%w(measures)) { |h,_| h['measures'].each { |m| m.delete_if { |k,_| !%w(id title code).index(k) } }; h.delete_if { |k,_| !%w(id title code measures).index(k) } }.first
  end
  Step.get_list() do |q|
    q.where(procedure_id: params[:pid]).order(seqnum: :asc)
  end.serialize() { |h,o| h['mcnt'] = o.measures.count; h['icnt'] = o.images.count; h['dcnt'] = o.documents.count; h }.first
end

# Route PSM_13
post '/api/steps/import/:procid' do
  data = check_import_hash(File.read(params['file'][:tempfile]), 'Step', params['file'][:filename])
  return data if data.is_a? Array
  data['procedure_id'] = params['procid'].to_i
  ActiveRecord::Base.transaction do
    ret = Step.import_from_hash(data)
    return { new_id: ret.id }.rest_success
  end
end

# Route PSM_14
post '/api/steps/clone/:procid' do
  request.body.rewind
  data = JSON.parse request.body.read
  step_ids = []
  ActiveRecord::Base.transaction do
    data.each do |id|
      hash = Step.export_to_hash(id,false)[3]
      hash['procedure_id'] = params['procid'].to_i
      ret = Step.import_from_hash(hash)
      step_ids << ret.id
    end
    return { newsteps: step_ids }.rest_success
  end
end

# Route PSM_15
get '/api/steps/:id' do
  tmp = Step.get_single(params[:id])
  tmp.serialize(params[:foredit] ? %w(procedure measures) : %w(procedure measures images.fullimage images.preview images.thumbnail documents.binaryfile)) do |h,o|
    h['procedure']['versions'] = Procedure.all_versions(h['procedure']['realid'])

    # if "for edit", don't expose measures, but get a count for the type setting
    if params[:foredit]
      h['measurecnt'] = h['measures'].to_a.count
      h.delete('measures')
    end

    h['measures'].each { |m| m['typedesc'] = Measure.get_measuretype_by_id(m['measuretype'])[:name] } unless params[:foredit]
    h
  end.first
end

# Route PSM_16
post '/api/steps/:id/action_reorder' do
  obj = Step.find_by(id:params['id'])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Steps', id: params['id']}, []).rest_fail if !obj
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    obj.reorder(data['neworder'])
  end
  return { }.rest_success
end

# Route PSM_17
delete '/api/steps/:id' do
  obj = Step.find_by(id: params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Steps', id: params['id']}, []).rest_fail if !obj
  ActiveRecord::Base.transaction do
    obj.destroy_logged
  end
  return {}.rest_success
end

# Route PSM_18
post '/api/steps' do
  request.body.rewind
  data = JSON.parse request.body.read

  # existing step: check if measures on it & change enforce if set
  if data['id'] && data['steptype'] == 1
    # reset enforcement
    data['enforce'] = 0

    # check if there are measures on this step, then the type can not be changed
    measure = Measure.find_by(step: data['id'])
    if measure != nil
      # Error translation: SERVER.ERROR.STEPTYPE.HASMEASURES
      return assemble_error('STEPTYPE', 'HASMEASURES', {class: 'Steps', id: data['id']}, []).rest_fail
    end
  end

  ActiveRecord::Base.transaction do
    return Step.merge_data(data).first
  end
end

# Measures

# Route PSM_19
post '/api/measures/:id/action_reorder' do
  obj = Measure.find_by(id:params['id'])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Measures', id: params['id']}, []).rest_fail if !obj
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    obj.reorder(data['neworder'])
  end
  return { }.rest_success
end

# Route PSM_20
delete '/api/measures/:id' do
  obj = Measure.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Measure', id: params['id']}, []).rest_fail if !obj
  ActiveRecord::Base.transaction do
    obj.destroy_logged
  end
  return {}.rest_success
end

# Route PSM_21
post '/api/measures/import/:stepid' do
  data = check_import_hash(File.read(params['file'][:tempfile]), 'Measure', params['file'][:filename])
  return data if data.is_a? Array
  data['step_id'] = params['stepid'].to_i
  ActiveRecord::Base.transaction do
    ret = Measure.import_from_hash(data)
    return { new_id: ret.id }.rest_success
  end
end

# Route PSM_22
post '/api/measures/clone/:stepid' do
  request.body.rewind
  data = JSON.parse request.body.read
  meas_ids = []
  ActiveRecord::Base.transaction do
    data.each do |id|
      hash = Measure.export_to_hash(id,false)[3]
      hash['step_id'] = params['stepid'].to_i
      ret = Measure.import_from_hash(hash)
      meas_ids << ret.id
    end
    return { newmeass: meas_ids }.rest_success
  end
end

# Route PSM_23
post '/api/measures' do
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    return Measure.merge_data(data).first
  end
end

# Route PSM_24
get '/api/measures/:id' do
  tmp = Measure.get_single(params[:id])
  return tmp.serialize(params[:foredit] ? nil : %w(tooltype step.procedure)).first
end

# Route PSM_25
get '/api/measuretypes' do
  return { rows: Measure.measuretypes, rowcnt: Measure.measuretypes.size, page: 1, pages: 1, limit: 0 }.rest_success
end
