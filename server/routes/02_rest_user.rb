# Users and Groups

# Route USER_1
get '/api/users/shortlist' do
  return { 'groups' => Usergroup.select(:id, :name, :level).order(:level, :name).map { |e| e.serializable_hash }, 'users' => User.select(:id, :realname, :username).where("status = '1'").order(:username).map do |e|
      hash = e.serializable_hash
      hash['group_ids'] = e.affiliations.collect { |aff| aff.usergroup.id }
      hash
    end }.rest_success
end

# Route USER_2
get '/api/users/full' do
  return illegal_access if(!has_privilege('MNGUSR'))
  return {
      'groups' => Usergroup.order(:level, :name).map { |e| e.serializable_hash(include: %w(grants).incllist) },
      'users' => User.order(:username).map do |e|
        hash = e.serializable_hash(include: %w(grants affiliations).incllist)
        hash['rank'] = e.get_rank
        hash
      end,
      'privileges' => Privilege.order(:code).map { |e| e.serializable_hash }
  }.rest_success
end

# Route USER_3
post '/api/users' do
  request.body.rewind
  ActiveRecord::Base.transaction do
    data = JSON.parse request.body.read
    puts data
    access_allowed = (data['id'] && data['id'] == get_current_user_id && data.size == 2 && data['passhash']) # let users change their own password
    access_allowed = true if !access_allowed && has_privilege('USRMGA','USRMGO')
    return illegal_access if !access_allowed
    return User.merge_data(data).first
  end
end

# Route USER_4
post '/api/groups' do
  return illegal_access if(!has_privilege('USRMGA','USRMGO'))
  request.body.rewind
  ActiveRecord::Base.transaction do
    data = JSON.parse request.body.read
    return Usergroup.merge_data(data).first
  end
end

# Route USER_5
delete '/api/groups/:id' do
  return illegal_access if(!has_privilege('USRMGA','USRMGO'))
  obj = Usergroup.find_by(id:params[:id])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Usergroups', id: params['id']}, []).rest_fail if !obj

  # instead deleting, remove affilliations to users and set flag
  obj.deleted = true
  obj.save!

  ActiveRecord::Base.transaction do
    Affiliation.where(usergroup_id: params[:id]).each do |a|
      a.destroy
    end
    return {}.rest_success
  end
end

# Route USER_6
delete '/api/affiliations/:uid/group/:gid' do
  return illegal_access if(!has_privilege('USRMGA','USRMGO'))
  obj = Affiliation.find_by(user_id:params[:uid], usergroup_id:params[:gid])
  return assemble_error('NOTFOUND', 'TEXT', {class: 'Activeprocedures', id: params[:id]}, []).rest_fail if !obj
  cuobj = User.find_by(id:get_current_user)
  return assemble_error('NOTFOUND', 'TEXT', {class: 'User', id: get_current_user}, []).rest_fail if !cuobj
  return illegal_access if ((cuobj.get_rank < obj.usergroup.level) && !cuobj.get_all_privileges['USRMGA'])
  ActiveRecord::Base.transaction do
    obj.destroy_logged
  end
  return {}.rest_success
end

# Route USER_7
post '/api/affiliations' do
  return illegal_access if(!has_privilege('USRMGA','USRMGO'))
  cuobj = User.find_by(id:get_current_user)
  return assemble_error('NOTFOUND', 'TEXT', {class: 'User', id: get_current_user}, []).rest_fail if !cuobj
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    data.each do |aff|
      gobj = Usergroup.find_by(id:aff['gid'])
      return assemble_error('NOTFOUND', 'TEXT', {class: 'Usergroup', id: aff['gid']}, []).rest_fail if !gobj
      uobj = User.find_by(id:aff['uid'])
      return assemble_error('NOTFOUND', 'TEXT', {class: 'User', id: aff['uid']}, []).rest_fail if !uobj
      return assemble_error('ACCDENIED', 'TEXT', {}, []).rest_fail if ((cuobj.get_rank < gobj.level) && !cuobj.get_all_privileges['USRMGA'])
      res,succ = Affiliation.merge_data({user_id: aff['uid'], usergroup_id: aff['gid']})
      return res unless succ
    end
  end
  return {}.rest_success
end

# Route USER_8
post '/api/users/:id/grants' do
  return illegal_access if(!has_privilege('GRTPRO','GRTPRA'))
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    Grant.manage_grants(User,params[:id],data)
    return {}.rest_success
  end
end

# Route USER_9
post '/api/groups/:id/grants' do
  return illegal_access if(!has_privilege('GRTPRO','GRTPRA') || !has_privilege('GRTTOG'))
  request.body.rewind
  data = JSON.parse request.body.read
  ActiveRecord::Base.transaction do
    Grant.manage_grants(Usergroup,params[:id],data)
    return {}.rest_success
  end
end

# Route USER_10
get '/api/users/:id' do
  { 'user' => User.select(:realname).where('id = ?', params[:id]).map { |e| e.serializable_hash } }.rest_success
end