get '/servlet/mediadownload/:type/:id' do
  binfile = nil
  if params[:type] == 'image'
    img = Image.find_by(id: params[:id].to_i)
    return 404 if !img
    binfile = img.fullimage
  end
  if params[:type] == 'doc'
    doc = Document.find_by(id: params[:id].to_i)
    return 404 if !doc
    binfile = doc.binaryfile
    return 404 if !doc
  end
  return 404 if !binfile
  data = File.read("../uploads/#{binfile.filename}")
  return [200, { 'Content-Disposition' => "attachment; filename=#{binfile.original_filename}", 'Content-Type' => 'application/octet-stream', 'Content-Transfer-Encoding' => 'binary' }, data ]
end

get '/servlet/export/procedures/:id' do
  Procedure.export_to_hash(params[:id],true,params[:complete])
end

get '/servlet/export/steps/:id' do
  Step.export_to_hash(params[:id],true,params[:complete])
end

get '/servlet/export/measures/:id' do
  Measure.export_to_hash(params[:id],true,params[:complete])
end

get '/servlet/export/models/:id' do
  Model.export_to_hash(params[:id],true,params[:complete])
end

get '/servlet/pdf/checkreport/:id' do
  data = CheckReport.gen_pdf(params[:id].to_i, params)
  fname = sprintf('checkreport_%d.pdf',params[:id].to_i)
  return [200, { 'Content-Disposition' => "filename=#{fname}", 'Content-Type' => 'application/pdf', 'Content-Transfer-Encoding' => 'binary' }, data ]
end

get '/servlet/export/checkreport/:id' do
  data = CheckReport.export_data(params[:id].to_i, params)
  fname = sprintf('checkreport_%d.%s',params[:id].to_i,params[:format] || 'json')
  return [200, { 'Content-Disposition' => "attachment; filename=#{fname}", 'Content-Type' => 'application/text', 'Content-Transfer-Encoding' => 'binary' }, data ]
end

get '/servlet/pdf/msrstatistics/:pid' do
  data = MeasureStatistics.gen_pdf(params[:pid].to_i, params)
  fname = sprintf('statistics_%s.pdf',Time.now.strftime('%Y%m%d_%H%M%S'))
  return [200, { 'Content-Disposition' => "filename=#{fname}", 'Content-Type' => 'application/pdf', 'Content-Transfer-Encoding' => 'binary' }, data ]
end

get '/servlet/export/msrstatistics/:pid' do
  data = MeasureStatistics.export_data(params[:pid].to_i, params)
  fname = sprintf('statistics_%s.%s',Time.now.strftime('%Y%m%d_%H%M%S'),params[:format] || 'json')
  return [200, { 'Content-Disposition' => "attachment; filename=#{fname}", 'Content-Type' => 'application/text', 'Content-Transfer-Encoding' => 'binary' }, data ]
end

get '/servlet/pdf/comments' do
  data = CommentReport.gen_pdf(params)
  fname = sprintf('comments_%s_%s.pdf',params['type'],Time.now.strftime('%Y%m%d_%h%m%s'))
  return [200, { 'Content-Disposition' => "filename=#{fname}", 'Content-Type' => 'application/pdf', 'Content-Transfer-Encoding' => 'binary' }, data ]
end

get '/servlet/export/notices' do
  data = Notice.export_data(params)
  fname = sprintf('notices_%s.%s',Time.now.strftime('%Y%m%d_%H%M%S'),params[:format] || 'json')
  return [200, { 'Content-Disposition' => "attachment; filename=#{fname}", 'Content-Type' => 'application/text', 'Content-Transfer-Encoding' => 'binary' }, data ]
end

get '/servlet/pdf/unitreport/:uid' do
  data = ToolunitReport.gen_pdf(params)
  fname = sprintf('toolunit_%s_%s.pdf',params['uid'],Time.now.strftime('%Y%m%d_%h%m%s'))
  return [200, { 'Content-Disposition' => "filename=#{fname}", 'Content-Type' => 'application/pdf', 'Content-Transfer-Encoding' => 'binary' }, data ]
end

get '/servlet/export/configtables/:id' do
  Configtable.export_to_hash(params[:id], true, params[:complete])
end
