get '/' do
  redirect to('/index.html')
end

# Transformiert die .less-Dateien in das entsprechende .css und liefert es dem Browser
get '/css/:style.css' do
  if test(?f,"../public/css/#{params[:style]}.css")
    return send_file "../public/css/#{params[:style]}.css"
  end
  content_type :css
  return serve_cached("less_#{params[:style]}") do
    fnam = "../public/less/#{params[:style]}.less"
    parser = Less::Parser.new :paths => %w(../public/less ../components/bootstrap/less), :filename => fnam

    # Wenn auf Testserver, die Header-Farbe ändern
    if $debugging_api
      file_contents = File.read(fnam)
      new_file_contents = file_contents.gsub(/@navbar-inverse-bg: @theme-color;/, "@navbar-inverse-bg: #99783D;")
      tree = parser.parse(new_file_contents)
    else
      tree = parser.parse(File.read(fnam))
    end
    tree.to_css(:compress => true)
  end
end

# Baut den in servercfg.rb definierten Sprachblock in public/js/services/i18n.js ein, wenn dieses abgerufen wird
def insert_languageblock(buf)
  langblock = $system_languages.inspect[1..-2].gsub(/:([a-z]+)=\>/,'\1: ')
  buf.gsub('/* INSERT_LANGUAGEBLOCK */', langblock)
end

# Prepariert die app.js-Datei, bevor diese dem Browser geliefert wird:
# Debugmode: keine Änderung
# Prod.mode: alle .js-Dateien der Webapp (nicht: Libraries) werden an app.js angehängt, Kommentare entfernt, uglified, Systemsprachen definiert
get '/js/app.js' do
  content_type :js
  serve_cached('app-js') do
    buf = File.read('../public/js/app.js')
    buf += append_constants()
    if !$debugging_api
      %w(controllers directives filters services).each do |area|
        Dir.glob("../public/js/#{area}/*").each { |m| buf += "\n\n"+File.read(m) }
      end
      buf = insert_languageblock(buf)
      buf = buf.gsub(/\/\/[^\n]+/,'')
      buf = buf.gsub(/\/\*.*?\*\//m,'')
      buf = Uglifier.compile(buf, :mangle => false)
    end
    buf
  end
end

# Präpariert die im Server verwendeten Konstanten (lib/constants.rb) für die Verwendung im Client
def append_constants
  buf = "angular.module('leanLogic.constants', []).constant('ConstantBundle', {\n"
  File.foreach('lib/constants.rb') do |line|
    line = line.chomp.gsub(/#.*\z/,'')
    buf += "\"#{$1}\": #{$2},\n" if line =~ /\A(.*) += +(.*)\z/
  end
  buf + '"DEFAULT_YES": 1, "DEFAULT_NO": 0 })'
end

# Prepariert i18n.js (s. insert_languageblock):
# Debugmode: Definition der Systemsprachen
# Prod.mode: keine Änderung
get '/js/services/i18n.js' do
  content_type :js
  serve_cached('i18n') do
    buf = File.read('../public/js/services/i18n.js')
    insert_languageblock(buf)
  end
end

def get_vendor_library_list(minified = false)
  liblist = nil
  File.foreach('../public/index.html') do |line|
    line = line.chomp
    liblist = [] if line =~ /LIB_LIST/
    next if !liblist
    if line =~ /\A \- (.*)\z/
      libfile = $1.split(' : ')
      libfile = libfile.size == 1 ? libfile[0] : libfile[minified ? 1 : 0]
      libfile = libfile.gsub(/\A */,'').gsub(/ *\z/,'')
      libfile = nil if libfile == '-'
      liblist << libfile if libfile
    end
  end
  liblist
end

# Prepariert index.html:
# Debugmode: Für alle .js-Dateien der Webapp (nicht: Libraries) wird ein <script src=...>-Eintrag erstellt, Für alle
#    .js-Dateien der externen Libraries wird ein <script src=...>-Eintrag zur nicht-minified-Version (falls vorhangen)
#    erstellt
# Prod.mode: <script>-Eintrag zu vendor.js wird erstellt (dieses wird dynamisch aus den minified-Versionen zusammen
#    gesetzt, s. prepare_vendor_lib_file)
def prepare_index
  serve_cached('index') do
    buf = File.read('../public/index.html')
    if $debugging_api
      insrt = ''
      %w(controllers directives filters services).each do |area|
        Dir.glob("../public/js/#{area}/*").each { |m| insrt += "<script src=\"#{m.gsub('../public/','')}\"></script>\n" }
      end
      buf.gsub! '<!-- AUTOINSERT_WEBAPP -->',insrt
      insrt = ''
      get_vendor_library_list.each { |libfile| insrt += "<script src=\"#{libfile}\"></script>\n" }
      buf.gsub! '<!-- AUTOINSERT_EXTLIB -->',insrt
    else
      buf.gsub! '<!-- AUTOINSERT_EXTLIB -->','<script src="vendor.js"></script>'
    end
    buf
  end
end

# Setzt vendor.js aus allen minified-Versionen der externen Libraries (wie in index.html definiert) zusammen
def prepare_vendor_lib_file
  serve_cached('vendor.js') do
    buf = ''
    get_vendor_library_list.each do |libfile|
      if test(?f,"../override/#{libfile}")
        buf += File.read("../override/#{libfile}")
      elsif test(?f,"../components/#{libfile}")
        buf += File.read("../components/#{libfile}")
      else
        buf += File.read("../public/#{libfile}")
      end
      buf += "\n"
    end
    buf
  end
end

# Prepariert templates:
# Debugmode: keine Änderung
# Prod.mode: Kommentare werden entfernt
def prepare_template(tmpl)
  serve_cached("template_#{tmpl.gsub('/','_').gsub('.html','')}") do
    buf = File.read("../public/templates/#{tmpl}")
    if !$debugging_api
      buf = buf.gsub(/\<\!\-\-.*?\-\-\>/m,'')
    end
    buf
  end
end

# Liefert mediendaten aus dem Uploads-VZ
get '/media/*' do |url|
  send_file "../uploads/#{url}"
end

# Liefert vendor.js aus den zusammengesetzten .js-Dateien der externen Bibliotheken (minified)
get '/vendor.js' do
  content_type :js
  prepare_vendor_lib_file
end

get '/i18n/*' do |url|
  lang = nil
  lang = $1 if url =~ /lang\-(..)\.json/
  halt 404 if !lang
  load_localization(lang).to_json
end

# Liefert public HTML Dateien
get '/*' do |url|
  pass if url =~ /\A(api|media|servlet)\//
  # Sucht nach der geforderten Datei zunächst im override-VZ, dann in components, schliesslich in public
  sfile = %w(override components public).each do |basedir|
    fnam = "../#{basedir}/#{url}"
    next if !test(?f,fnam)
    break fnam
  end
  # Wenn nicht gefunden und URL beginnt mit "app/" (i.e. Route wird von Webapp intern verarbeitet), liefere index.html
  sfile = '../public/index.html' if sfile.is_a?(Array) && url =~ /\Aapp\//
  # Liefere 404 wenn URL nicht mit "app/" beginnt und nicht gefunden wurde
  halt 404 if sfile.is_a? Array
  # Wenn index.html geliefert werden soll, zunächst preparieren
  if sfile == '../public/index.html'
    content_type :html
    prepare_index
  # Wenn ein template geliefert werden soll, zunächst preparieren
  elsif sfile =~ /\A\.\.\/public\/templates\/(.*)/
    content_type :html
    prepare_template $1
  else
    send_file sfile
  end
end