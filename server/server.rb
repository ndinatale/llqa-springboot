$logger = [ STDOUT ]

require_relative 'lib/utils'

$logger.info 'Starting up'

require 'json'
require 'securerandom'
require 'digest/sha1'
require 'csv'
require 'base64'

require 'bundler/setup'
Bundler.require(:default)

require 'prawn/measurement_extensions'
require 'active_support/core_ext'

require_relative 'config/servercfg_def'
if test(?f,'config/servercfg.rb')
  $logger.info 'Using local server configuration'
  require_relative 'config/servercfg'
else
  $logger.info 'Using standard server configuration'
  require_relative 'config/servercfg_std'
end

configure do
  set :lock, true
  set :static_cache_control, [:public, :max_age => 1]
  set :server, 'puma'
  set :server_settings, { :Threads => '0:16',
                          :Verbose => false,
                          :environment => 'production'
                        }
end

$logger.info 'Using DEBUGGING API' if $debugging_api
$logger.info "Session expiry set to #{$expiry_time}s"

set :database_file, 'config/database.yml'

configure do
  disable :logging
  disable :static
end

begin
  ActiveRecord::Base.logger.level = 1
rescue
end

include Rake::DSL

require_relative 'lib/models'
begin
  Privilege.setup_privileges if !$testmode
rescue
end

require_relative 'lib/routes' if !$testmode

require_relative 'lib/servlets'

$logger.info 'Reading features'
$features = {}
$features_array = JSON.parse(File.read('config/features.json'))
$features_array.each do |feature|
  $features[feature['key']] = feature['active']
end

before do
  @request_ts = Time.now.to_f
end

after do
  uri = request.env['REQUEST_URI']
  bytes = response.body.is_a?(Array) ? (response.body[0] ? response.body[0].size : 0) : test(?s,response.body.path)
  $logger.info('%-80.80s : %7d   (%3.3f)' % [uri,bytes,Time.now.to_f-@request_ts])
end