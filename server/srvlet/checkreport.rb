# encoding: UTF-8
require_relative '../lib/constants'

module CheckReport

  def CheckReport.check_status(status, lcat, params)
    return lcat.pdf_status_cancelled if status == CHK_STATUS_CANCELLED
    if CHK_STATUS_ALL_WARNINGS.index(status)
      return lcat.pdf_status_warnext if params['customer']
      return lcat.pdf_status_warnint
    end
    CHK_STATUS_ALL_FAILED.index(status) ? lcat.pdf_status_failed : (CHK_STATUS_ALL_SUCCESS.index(status) ? lcat.pdf_status_passed : lcat.pdf_status_open)
  end

  def CheckReport.unit_status(status, lcat)
    case status
      when UNIT_STATUS_CLOSED then lcat.pdf_ustatus_closed
      when UNIT_STATUS_DISCARDED then lcat.pdf_ustatus_discarded
      when UNIT_STATUS_ARCHCLOSED then lcat.pdf_ustatus_aclosed
      when UNIT_STATUS_ARCHDISCARDED then lcat.pdf_ustatus_adiscarded
      else lcat.pdf_ustatus_open
    end
  end

  def CheckReport.insert_header(pdf, params, lcat, check)
    pdf.font 'FontHdr'
    pdf.fill_color '005499'
    pdf.fill {
      pdf.rectangle [20.mm,287.mm], 180.mm, 15.mm
      pdf.rectangle [20.mm,20.mm], 180.mm, 10.mm
    }
    pdf.fill_color 'ffffff'
    # header
    text = lcat.pdf_mainttl+' '+lcat.formatd(Time.now)
    pdf_text pdf, 25, 10, 170, 15, text, true, valign: :center, fontsize: 24
    text = check_status(check.status,lcat,params)
    pdf_text pdf, 25, 10, 170, 15, text, true, style: :bold, fontsize: 24, valign: :center, halign: :right, inline_format: true
    # footer
    text = sprintf('%s (%s)',check.unit.code,check.model.title[lcat.code])
    pdf_text pdf, 25, -20, 170, 10, text, true, fontsize: 16, valign: :center, style: :bold
    #text = check.checktype.title[lcat.code]
    params['pageno'] += 1
    pdf_text pdf, 25, -20, 170, 10, "#{params['pageno']}", true, fontsize: 16, valign: :center, halign: :right
  end

  def CheckReport.insert_infoblock(pdf, params, lcat, check)
    pdf.fill_color '000000'
    ypos = 30

    datefrm = "<font size='9'><i>%s: </i></font>%s"

    pdf_text(pdf, 25, ypos, 20, 20, lcat.pdf_check, false, fontsize: 12, halign: :right, style: :bold)
    ypos += pdf_text(pdf, 50, ypos, 75, 20, "#{check.checktype.title[lcat.code]} (#{lcat.pdf_version} #{check.model.version})", false, fontsize: 12)[0]
    ypos += pdf_text(pdf, 50, ypos, 75, 30, check.comment, false, fontsize: 9, inline_format: true, autofit: true)[0]
    ypos += 2
    if !$application_parameters[:skipcheckschedule]
      ypos += pdf_text(pdf, 50, ypos, 75, 20, datefrm % [lcat.pdf_date_sched,(check.scheduled ? lcat.formatd(check.scheduled) : lcat.pdf_date_nosched)], false, fontsize: 12, inline_format: true)[0]
      ypos += pdf_text(pdf, 50, ypos, 75, 20, datefrm % [lcat.pdf_date_due,(check.dueby ? lcat.formatd(check.dueby) : lcat.pdf_date_nodue)], false, fontsize: 12, inline_format: true)[0]
    end
    ypos += pdf_text(pdf, 50, ypos, 75, 20, datefrm % [lcat.pdf_date_start,(check.started ? lcat.formatd(check.started) : lcat.pdf_date_nostart)], false, fontsize: 12, inline_format: true)[0]
    ypos += pdf_text(pdf, 50, ypos, 75, 20, datefrm % [lcat.pdf_date_fin,(check.finished ? lcat.formatd(check.finished) : lcat.pdf_date_nofin)], false, fontsize: 12, inline_format: true)[0]
    ypos += 3
    ypos = 75 if ypos < 75

    pdf_text(pdf, 25, ypos, 20, 10, lcat.pdf_model, false, fontsize: 12, halign: :right, style: :bold)
    ypos += pdf_text(pdf, 50, ypos, 145, 10, check.model.title[lcat.code]+' ('+check.model.devicetype.title[lcat.code]+')'+" <sup>#{lcat.pdf_version} #{check.model.version}</sup>", false, fontsize: 12, autofit: true, inline_format: true)[0]
    ypos += pdf_text(pdf, 50, ypos, 145, 30, check.model.devicetype.description[lcat.code], false, fontsize: 9, autofit: true)[0]
    ypos += pdf_text(pdf, 50, ypos, 145, 30, check.model.description[lcat.code].html_to_pdf, false, fontsize: 9, autofit: true)[0]
    ypos += 3

    pdf_text(pdf, 25, ypos, 20, 15, lcat.pdf_unit, false, fontsize: 12, halign: :right, style: :bold)
    ypos += pdf_text(pdf, 50, ypos, 145, 15, check.unit.code+' ('+check.unit.customer+'), '+unit_status(check.unit.status,lcat), false, fontsize: 12, autofit: true)[0]
    ypos += pdf_text(pdf, 50, ypos, 145, 30, check.unit.comment, false, fontsize: 9, inline_format: true, autofit: true)[0]

    if check.model.images.size > 0 && test(?f, '../uploads/'+check.model.images[0].fullimage.filename)
      pdf.image '../uploads/'+check.model.images[0].fullimage.filename, at: [130.mm,(297-30).mm], fit: [65.mm,40.mm]
    end

    ypos
  end

  def CheckReport.paint_block(pdf,ypos,color,params,lcat,check,height,margin)
    if height+margin*2+ypos > 272
      pdf.start_new_page
      insert_header(pdf, params, lcat, check)
      ypos = 30
    end
    pdf.fill_color color
    pdf.fill { pdf.rectangle [20.mm,(297-ypos).mm], 180.mm, (height+margin*2+1).mm }
    ypos+margin+1
  end

  def CheckReport.status_name(lcat,data,params,measure = nil,measurement = nil,short = false)
    return lcat.pdf_status_unfin if data['status'] == CHK_CDSTATUS_TODO
    if measurement && measurement.value && measurement.value != '' && CHK_CDSTATUS_ALL_FINISHED.index(data['status'])
      text = lcat.replacedatetag("#{measurement.value}#{measurement.applicable_unit}", params['tzoff'])
      if measure.measuretype_info[:type] =~ /\Atimer/
        if short
          text = "<b>#{$1}</b>" if text =~ /\A\(.*\) (.*)\z/
          text = "<b>#{$1}</b>" if text =~ /\A\((.*)\)\z/
        else
          text = "#{$1}\n<b>#{$2}</b>" if text =~ /\A\((.*)\) (.*)\z/
          text = "#{$1}<font size='6'>\n#{$2}\n</font>#{$3}" if text =~ /\A(.*), (\-.*)\n(.*)/
          text = "<b>#{$1}</b>" if text =~ /\A\((.*)\)\z/
        end
      else
        text = "<b>#{text}</b>"
      end
      return text
    end
    return '<b>'+lcat.pdf_status_pass+'</b>' if measurement && data['status'] == CHK_CDSTATUS_PASSED
    return '<b>'+lcat.pdf_status_warnint+'</b>' if measurement && data['status'] == CHK_CDSTATUS_WARNINGS && !params['customer']
    return '<b>'+lcat.pdf_status_warnext+'</b>' if measurement && data['status'] == CHK_CDSTATUS_WARNINGS && params['customer']
    return '<b>'+lcat.pdf_status_fail+'</b>' if measurement && data['status'] == CHK_CDSTATUS_FAILED
    return lcat.pdf_status_pass if data['status'] == CHK_CDSTATUS_PASSED
    return lcat.pdf_status_warnint if data['status'] == CHK_CDSTATUS_WARNINGS && !params['customer']
    return lcat.pdf_status_warnext if data['status'] == CHK_CDSTATUS_WARNINGS && params['customer']
    return lcat.pdf_status_fail if data['status'] == CHK_CDSTATUS_FAILED
    return lcat.pdf_status_skip
  end

  def CheckReport.status_color(data)
    return '009000' if data['status'] == CHK_CDSTATUS_PASSED
    return '707000' if data['status'] == CHK_CDSTATUS_WARNINGS
    return '900000' if data['status'] == CHK_CDSTATUS_FAILED
    return '000000'
  end

  def CheckReport.user_name(lcat, check, id)
    return nil if !id
    if check
      return lcat.pdf_user_noone if id < 0
      assdata = check.checkdata['assignees'][id]
      return lcat.pdf_user_any if !assdata['group_id'] && !assdata['user_id']
      return lcat.pdf_user_grp % Usergroup.find(assdata['group_id']).name if !assdata['user_id']
      id = assdata['user_id']
    end
    user = User.find(id)
    return "#{user.realname} (#{user.id}:#{user.username})"
  end

  def CheckReport.insert_warntext(pdf, params, lcat, ypos)
    pdf.fill_color '000000'
    prerun = ypos == nil
    ypos = 0 if prerun
    height = 10
    ypos += 10

    text = params['customer'] ? lcat.pdf_hint_checkwarn_customer : lcat.pdf_hint_checkwarn_internal

    height += pdf_text(pdf, 25, ypos, 170, 40, "<sup>*)</sup> #{text}", false, style: :italic, valign: :top, fontsize: 8, inline_format: true, autofit: true, dry: prerun)[0]
    height
  end

  def CheckReport.insert_procedure(pdf, params, lcat, check, proc, pdata, ypos)
    pdf.fill_color '000000'
    prerun = ypos == nil
    ypos = 0 if prerun
    height = 0
    vpos = :top

    if params['showv']
      height += pdf_text(pdf, 50, ypos, 120, 5, proc.title[lcat.code]+" <sup>#{lcat.pdf_version} #{proc.version}</sup>", false, style: :bold, fontsize: 14, autofit: true, inline_format: true, dry: prerun)[0]
    else
      height += pdf_text(pdf, 50, ypos, 120, 5, proc.title[lcat.code], false, style: :bold, fontsize: 14, autofit: true, dry: prerun)[0]
    end

    pdf_text(pdf, 25, ypos, 20, height, proc.code, false, valign: vpos, fontsize: 14, autofit: true, dry: prerun)
    pdf_text(pdf, 175, ypos, 20, height, status_name(lcat,pdata,params), false, valign: vpos, style: :bold, color: status_color(pdata), halign: :right, fontsize: 14, inline_format: true, autofit: true, dry: prerun)
    height
  end

  def CheckReport.insert_step(pdf, params, lcat, check, step, sdata, ypos)
    pdf.fill_color '000000'
    prerun = ypos == nil
    ypos = 0 if prerun
    height = 0
    vpos = :top

    height += pdf_text(pdf, 50, ypos, 120, 20, step.title[lcat.code], false, style: :bold, fontsize: 12, autofit: true, dry: prerun)[0]
    if params['showadm']
      text = sdata['committed'] ? sprintf(lcat.pdf_step_assc, user_name(lcat, check, sdata['assignee']), user_name(lcat, nil, sdata['committed'])) : sprintf(lcat.pdf_step_assnc, user_name(lcat, check, sdata['assignee']))
      height += pdf_text(pdf, 50, ypos+height, 120, 20, text, false, fontsize: 8, inline_format: true, autofit: true, dry: prerun)[0]
      vpos = :center
    end

    pdf_text(pdf, 25, ypos, 20, height, step.code, false, valign: vpos, fontsize: 12, autofit: true, dry: prerun)
    pdf_text(pdf, 175, ypos, 20, height, status_name(lcat,sdata,params), false, valign: vpos, color: status_color(sdata), style: :bold, halign: :right, fontsize: 12, inline_format: true, autofit: true, dry: prerun)

    height
  end

  def CheckReport.format_rawvalues(measure, measurement)
    floatformat = $application_parameters[:defaultfloatformat]
    floatformat = measure.metadata['floatformat'] if measure.metadata && measure.metadata['floatformat'] && measure.metadata['floatformat'].start_with?('%')
    measurement.rawvalues.to_a.sort { |a,b| a[0] <=> b[0] }.map { |v| sprintf("%s = #{floatformat}",v[0].upcase, v[1]) }.join(', ')
  end

  def CheckReport.insert_measure(pdf, params, lcat, check, measure, measurement, mdata, ypos)
    pdf.fill_color '000000'
    prerun = ypos == nil
    ypos = 0 if prerun
    height = 0
    vpos = :top

    reswidth = ((measure.measuretype_info[:input] == 'text' || measure.measuretype_info[:input] == 'timest')&& measurement && [1,2].index(mdata['status']) && measurement.value.size > 8) ? 60 : 25

    height += pdf_text(pdf, 50, ypos, 140-reswidth, 10, measure.title[lcat.code], false, style: :bold, fontsize: 10, autofit: true, dry: prerun)[0]
    if params['showrule'] && measurement && measure
      fformat = $application_parameters[:defaultfloatformat]
      fformat = measure.metadata['floatformat'] if measure.metadata && measure.metadata['floatformat'] && measure.metadata['floatformat'].start_with?('%')
      text = MeasureStatistics.get_rule_desc(measure.calculation, lcat, measure.measuretype_info[:type], fformat)
      height += pdf_text(pdf, 50, ypos+height, 140-reswidth, 10, text, false, fontsize: 7, autofit: true, inline_format: true, dry: prerun)[0]
      vpos = :center
    end
    if params['showtool'] && measure.tooltype_id && CHK_CDSTATUS_ALL_FINISHED.index(mdata['status'])
      if measurement && measurement.toolunit_id && measurement.toolunit
        height += pdf_text(pdf, 50, ypos+height, 140-reswidth, 10, lcat.pdf_measure_tool % [measure.tooltype.code, measure.tooltype.title[lcat.code], measurement.toolunit.code], false, fontsize: 7, autofit: true, inline_format: true, dry: prerun)[0]
      else
        height += pdf_text(pdf, 50, ypos+height, 140-reswidth, 10, lcat.pdf_measure_toolnosel % [measure.tooltype.code, measure.tooltype.title[lcat.code]], false, fontsize: 7, autofit: true, inline_format: true, dry: prerun)[0]
      end
      vpos = :center
    end
    if params['showadm'] && measurement && measurement.savedby
      height += pdf_text(pdf, 50, ypos+height, 140-reswidth, 10, lcat.pdf_measure_rep % [user_name(lcat,nil,measurement ? measurement.savedby : nil), lcat.formatt(measurement ? Time.at_with_tzoff(measurement.savedon, params['tzoff']) : nil)], false, fontsize: 7, autofit: true, inline_format: true, dry: prerun)[0]
      vpos = :center
    end
    if params['showcomm'] && measurement && measurement.comment && measurement.comment.size > 0
      height += pdf_text(pdf, 50, ypos+height, 140-reswidth, 10, lcat.pdf_measure_comm % [measurement.comment], false, fontsize: 7, autofit: true, inline_format: true, dry: prerun)[0]
      vpos = :center
    end
    if params['showraw'] && measurement && CHK_CDSTATUS_ALL_FINISHED.index(mdata['status']) && measure.measuretype_info[:input] == 'matrix'
      height += pdf_text(pdf, 50, ypos+height, 140-reswidth, 10, lcat.pdf_measure_raw % format_rawvalues(measure, measurement), false, fontsize: 7, autofit: true, inline_format: true, dry: prerun)[0]
      vpos = :center
    end

    pdf_text(pdf, 25, ypos, 20, height, measure.code, false, valign: vpos, fontsize: 10, autofit: true, dry: prerun)
    pheight = pdf_text(pdf, 195-reswidth, ypos, reswidth, 20, status_name(lcat,mdata,params,measure,measurement), false, valign: :top, color: status_color(mdata), inline_format: true, halign: :right, fontsize: 10, autofit: true, dry: prerun)[0]

    [height,pheight].max
  end

  def CheckReport.gen_pdf(check_id, params)
    p params
    pdf = Prawn::Document.new(page_size: "A4", margin: 0, page_layout: :portrait, compress: true, print_scalign: :none) do |pdf|
      pdf_setup_fonts(pdf,'os')

      lcat = pdf_read_language(params['lang'] || $system_languages[0][:code])

      check = Check.find(check_id)
      params['pageno'] = 0

      insert_header(pdf, params, lcat, check)
      ypos = insert_infoblock(pdf, params, lcat, check)
      ypos += 20

      wmap,warn_text = [],false
      check.checkdata['procedures'].each do |pdata|
        next if CHK_CDSTATUS_ALL_OMITTED.index(pdata['status'])
        proc = Procedure.find(pdata['id'])
        height = insert_procedure(pdf, params, lcat, check, proc, pdata, nil)
        wmap << (pentry = [:procedure, pdata, proc, height, 2, false])
        pdata['steps'].each do |sdata|
          next if CHK_CDSTATUS_ALL_OMITTED.index(sdata['status'])
          step = Step.find(sdata['id'])
          height = insert_step(pdf, params, lcat, check, step, sdata, nil)
          wmap << (sentry = [:step, sdata, step, height, 1, false])
          sdata['measures'].each do |mdata|
            next if CHK_CDSTATUS_ALL_OMITTED.index(mdata['status'])
            measure = Measure.find(mdata['id'])
            next if params['customer'] && measure.calculation['internal']
            measurement = mdata['mid'] >= 0 ? Measurement.find(mdata['mid']) : nil
            height = insert_measure(pdf, params, lcat, check, measure, measurement, mdata, nil)
            wmap << [:measure, mdata, measure, measurement, height, 1]
            pentry[5],sentry[5] = true,true
          end
          warn_text = true if(!warn_text && sentry[5] && sdata['status'] == CHK_CDSTATUS_WARNINGS)
        end
        warn_text = true if(!warn_text && pentry[5] && pdata['status'] == CHK_CDSTATUS_WARNINGS)
      end
      p warn_text
      if warn_text
        height = insert_warntext(pdf, params, lcat, nil)
        wmap << [:warntext, height]
      end

      wmap.each do |entry|
        if entry[0] == :procedure
          next unless entry[5]
          ypos = paint_block(pdf, ypos, 'b0b0b0', params,lcat,check, entry[3], entry[4])
          insert_procedure(pdf, params, lcat, check, entry[2], entry[1], ypos)
          ypos += entry[3]+entry[4]
        end
        if entry[0] == :step
          next unless entry[5]
          ypos = paint_block(pdf, ypos, 'e7e7e7', params,lcat,check, entry[3], entry[4])
          insert_step(pdf, params, lcat, check, entry[2], entry[1], ypos)
          ypos += entry[3]+entry[4]
        end
        if entry[0] == :measure
          ypos = paint_block(pdf, ypos, 'ffffff', params,lcat,check, entry[4], entry[5])
          insert_measure(pdf, params, lcat, check, entry[2], entry[3], entry[1], ypos)
          ypos += entry[4]+entry[5]
        end
        if entry[0] == :warntext
          insert_warntext(pdf, params, lcat, ypos)
          ypos += entry[1]
        end
      end
    end

    pdf.render
  end

  def CheckReport.export_data(check_id, params)
    lcat = pdf_read_language(params['lang'] || $system_languages[0][:code])
    check = Check.find(check_id)
    unit = {
      type: 'unit',
      code: check.unit.code,
      comment: check.unit.comment,
      customer: check.unit.customer,
      commissioned: lcat.formatd(check.unit.commissioned),
      finished: lcat.formatd(check.unit.finished),
      delivered: lcat.formatd(check.unit.delivered),
      approved: lcat.formatd(check.unit.approved),
      status: unit_status(check.unit.status, lcat)
    }
    devicetype = {
      type: 'devicetype',
      code: check.model.devicetype.code,
      title: check.model.devicetype.title[lcat.code],
      description: check.model.devicetype.description[lcat.code],
    }
    model = {
      type: 'model',
      code: check.model.code,
      title: check.model.title[lcat.code],
      description: check.model.description[lcat.code],
      devicetype: devicetype
    }
    checktype = {
      type: 'checktype',
      code: check.checktype.code,
      title: check.checktype.title[lcat.code],
      description: check.checktype.description[lcat.code],
    }
    procedures = check.checkdata['procedures'].map do |pdata|
      next nil if CHK_CDSTATUS_ALL_OMITTED.index(pdata['status'])
      pobj = Procedure.find(pdata['id'])
      steps = pdata['steps'].map do |sdata|
        next nil if CHK_CDSTATUS_ALL_OMITTED.index(sdata['status'])
        sobj = Step.find(sdata['id'])
        measures = sdata['measures'].map do |mdata|
          next nil if CHK_CDSTATUS_ALL_OMITTED.index(mdata['status'])
          mobj = Measure.find(mdata['id'])
          msobj = mdata['mid'] >= 0 ? Measurement.find(mdata['mid']) : nil
          tooltype_code,tooltype_title,toolunit = nil,nil,nil
          if mobj.tooltype_id && CHK_CDSTATUS_ALL_FINISHED.index(mdata['status'])
            if msobj && msobj.toolunit_id && msobj.toolunit
              tooltype_code,tooltype_title,toolunit = mobj.tooltype.code, mobj.tooltype.title[lcat.code], msobj.toolunit.code
            else
              tooltype_code,tooltype_title,toolunit = mobj.tooltype.code, mobj.tooltype.title[lcat.code], nil
            end
          end
          {
            type: 'measure',
            status: status_name(lcat,mdata,params),
            value: status_name(lcat,mdata,params,msobj),
            saved_by: user_name(lcat,nil,msobj ? msobj.savedby : nil),
            saved_on: lcat.formatt(msobj ? Time.at_with_tzoff(msobj.savedon, params['tzoff']) : nil),
            tooltype_code: tooltype_code,
            tooltype_title: tooltype_title,
            toolunit: toolunit,
            rawvalues: (msobj && CHK_CDSTATUS_ALL_FINISHED.index(mdata['status']) && mobj.measuretype_info[:input] == 'matrix') ? format_rawvalues(mobj, msobj) : nil,
            comment: msobj ? msobj.comment : nil,
            code: mobj.code,
            title: mobj.title[lcat.code],
            description: mobj.description[lcat.code],
            input_type: mobj.measuretype_info[:input],
            value_type: mobj.measuretype_info[:type],
          }
        end.compact
        {
          type: 'step',
          status: status_name(lcat,sdata,params),
          assignee: user_name(lcat, check, sdata['assignee']),
          committed: user_name(lcat, nil, sdata['committed']),
          measures_complete: sdata['completem'] ? lcat.pdf_bool_yes : lcat.pdf_bool_no,
          tools_complete: sdata['completet'] ? lcat.pdf_bool_yes : lcat.pdf_bool_no,
          code: sobj.code,
          title: sobj.title[lcat.code],
          description: sobj.description[lcat.code],
          measures: measures
        }
      end.compact
      {
        type: 'procedure',
        status: status_name(lcat,pdata,params),
        code: pobj.code,
        title: pobj.title[lcat.code],
        description: pobj.description[lcat.code],
        steps: steps
      }
    end.compact
    checkdata = {
      type: 'checkdata',
      procedures: procedures
    }
    check = {
      type: 'check',
      comment: check.comment,
      scheduled: $application_parameters[:skipcheckschedule] ? nil : lcat.formatd(check.scheduled),
      dueby: $application_parameters[:skipcheckschedule] ? nil : lcat.formatd(check.dueby),
      started: lcat.formatd(check.started),
      finished: lcat.formatd(check.finished),
      status: check_status(check.status, lcat, params),
      unit: unit,
      model: model,
      checktype: checktype,
      checkdata: checkdata
    }

    case params['format']
      when 'xml' then return check.format_as_xml
      when 'csv' then return check.format_as_csv(%w(type code status value title description customer comment scheduled started dueby commissioned finished delivered approved assignee committed saved_by saved_on tooltype_code tooltype_title toolunit rawvalues input_type value_type measures_complete tools_complete))
    end
    return check.format_as_json
  end

end










