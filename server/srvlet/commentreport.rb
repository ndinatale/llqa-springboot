# encoding: UTF-8

module CommentReport

  def CommentReport.insert_header(pdf, lcat, type, element, params)
    pdf.font 'FontHdr'
    pdf.fill_color '005499'
    pdf.fill {
      pdf.rectangle [20.mm,287.mm], 180.mm, 15.mm
      pdf.rectangle [20.mm,20.mm], 180.mm, 10.mm
    }
    pdf.fill_color 'ffffff'
    # header
    text = lcat.pdf_comment_ttl
    pdf_text pdf, 25, 10, 170, 15, text, true, valign: :center, fontsize: 24
    text = lcat.formatd(Time.now)
    pdf_text pdf, 25, 10, 170, 15, text, true, style: :bold, fontsize: 16, valign: :center, halign: :right
    # footer
    text = case type
             when 'users' then lcat.pdf_comment_footer_users
             when 'tooltype' then sprintf(lcat.pdf_comment_footer_tooltype, element.title[lcat.code], element.code)
             when 'model' then sprintf(lcat.pdf_comment_footer_model, element.title[lcat.code], element.code)
             when 'unit' then sprintf(lcat.pdf_comment_footer_unit, element.code)
             when 'unitfull' then sprintf(lcat.pdf_comment_footer_unit, element.code)
             when 'check' then sprintf(lcat.pdf_comment_footer_check, element.id, element.unit.code, element.unit.model.code)
             when 'step' then sprintf(lcat.pdf_comment_footer_step, element.title[lcat.code], element.version || 0, element.procedure.code, element.code)
             when 'measure' then sprintf(lcat.pdf_comment_footer_measure, element.title[lcat.code], element.version || 0, element.step.procedure.code, element.step.code, element.code)
             when 'demo' then 'Demoreport'
             else '???'
           end
    pdf_text pdf, 25, -19, 160, 8, text, true, fontsize: 14, valign: :center, style: :bold, autofit: true
    params['pageno'] += 1
    pdf_text pdf, 25, -20, 170, 10, "#{params['pageno']}", true, fontsize: 16, valign: :center, halign: :right
  end

  def CommentReport.paint_block(pdf, lcat, ypos, level, height, type, element, params)
    if height+ypos > 272
      pdf.start_new_page
      insert_header(pdf, lcat, type, element, params)
      ypos = 30
    end

    pdf.fill_color 'b0b0b0'
    pdf.fill { pdf.rectangle [20.mm, (297-ypos+(level>1 ? 2 : 0)).mm], 2.mm, (height+(level>1 ? 1 : -1)).mm }
    pdf.fill_color 'cbcbcb' if level > 1
    pdf.fill { pdf.rectangle [23.mm, (297-ypos+(level>2 ? 2 : 0)).mm], 2.mm, (height+(level>2 ? 1 : -1)).mm } if level > 1
    pdf.fill_color 'e7e7e7' if level > 2
    pdf.fill { pdf.rectangle [26.mm, (297-ypos).mm], 2.mm, (height-1).mm } if level > 2

    pdf.fill { pdf.rectangle [(18+level*3).mm, (297-ypos).mm], (200-(18+level*3)).mm, 5.mm }

    ypos
  end

  def CommentReport.comment_title(lcat, commobj)
    obj = commobj[:obj]
    case commobj[:type]
      when 'user' then sprintf(lcat.pdf_comment_element_user, obj.realname, obj.username)
      when 'tooltype' then sprintf(lcat.pdf_comment_element_tooltype, obj.code)
      when 'tool' then sprintf(lcat.pdf_comment_element_tool, obj.code)
      when 'unit' then sprintf(lcat.pdf_comment_element_unit, obj.code, obj.customer)
      when 'check' then sprintf(lcat.pdf_comment_element_check, obj.id, obj.unit.code, obj.unit.model.code)
      when 'msmnt' then sprintf(lcat.pdf_comment_element_msmnt, obj.measure.code, obj.measure.step.code, obj.measure.step.procedure.code)
      when 'proc' then sprintf(lcat.pdf_comment_element_proc, obj.code)
      when 'step' then sprintf(lcat.pdf_comment_element_step, obj.code)
      when 'msmnts' then sprintf(lcat.pdf_comment_element_msmnts, obj.measure.code)
      when 'meas' then sprintf(lcat.pdf_comment_element_msmnts, obj.code)
      when 'demo' then 'Demoelement'
      else '???'
    end
  end

  def CommentReport.user_name(id)
    return '' if !id || id == '??'
    user = User.find(id)
    " <i>(#{user.realname}; #{user.id}:#{user.username})</i>"
  end


  def CommentReport.insert_comment(pdf, lcat, ypos, level, commobj)
    pdf.fill_color '000000'
    prerun = ypos == nil
    ypos = 0 if prerun
    height = 0
    xpos = 20+level*3
    width = 200-xpos

    pdf_text(pdf, xpos, ypos+height+0.3, width, 5, comment_title(lcat, commobj), true, fontsize: 10, valign: :center, autofit: true, dry: prerun)[0]
    height += 6
    height += pdf_text(pdf, xpos, ypos+height, width, 50, commobj[:text]+user_name(commobj[:owner]), false, inline_format: true, fontsize: 8, halign: :justify, dry: prerun)[0] if commobj[:text]
    height += 1

    height
  end

  def CommentReport.demo_comments(min, max, level)
    ret = []
    (rand(max-min+1)+min).times do
      ret << { type: 'demo', obj: nil, text: level == 1 ? nil : loremipsum(10,1000), children: demo_comments(0,30-level*10,level+1) }
    end
    ret
  end

  def CommentReport.gen_pdf(params)
    pdf = Prawn::Document.new(page_size: 'A4', margin: 0, page_layout: :portrait, compress: true, print_scalign: :none) do |pdf|
      pdf_setup_fonts(pdf,'os')

      lcat = pdf_read_language(params['lang'] || $system_languages[0][:code])

      data = case params['type']
                   when 'users' then User.all_comments # all users
                   when 'tooltype' then Tooltype.all_comments(params['id'].to_i) # all toolunits of one type
                   when 'model' then Model.all_comments(params['id'].to_i) # all units of a model
                   when 'unit' then Unit.all_comments(params['id'].to_i, false) # all checks of a unit
                   when 'unitfull' then Unit.all_comments(params['id'].to_i, true) # all checks and measurements of a unit
                   when 'check' then Check.all_comments(params['id'].to_i, true) # all measurements of a check
                   when 'step' then Step.all_comments(params['id'].to_i) # all measurements of a step
                   when 'measure' then Measure.all_comments(params['id'].to_i) # all measurements of a measure
                   when 'demo' then { comments: demo_comments(1,10,1), element: nil }
                   else nil
                 end
      break if !data

      params['pageno'] = 0

      comments = data[:comments]
      element = data[:element]

      insert_header(pdf, lcat, params['type'], element, params)
      ypos = 30

      wmap = []
      comments.each do |comm1|
        comm1[:text] = loremipsum(10,1000) if (!comm1[:text] && params['demo'] && rand(100)<50)
        height = insert_comment(pdf, lcat, nil, 1, comm1)
        wmap << (map1 = [comm1, height, 1, comm1[:text] == nil || comm1[:text] == ''])
        comm1[:children].each do |comm2|
          comm2[:text] = loremipsum(10,1000) if (!comm2[:text] && params['demo'] && rand(100)<30)
          height = insert_comment(pdf, lcat, nil, 2, comm2)
          wmap << (map2 = [comm2, height, 2, comm2[:text] == nil || comm2[:text] == ''])
          map1[3] = false if !map2[3]
          comm2[:children].each do |comm3|
            comm3[:text] = loremipsum(10,1000) if (!comm3[:text] && params['demo'] && rand(100)<20)
            height = insert_comment(pdf, lcat, nil, 3, comm3)
            wmap << (map3 = [comm3, height, 3, comm3[:text] == nil || comm3[:text] == ''])
            map1[3],map2[3] = false,false if !map3[3]
          end
        end
      end

      wmap.each do |entry|
        next if entry[3]
        ypos = paint_block(pdf, lcat, ypos, entry[2], entry[1], params['type'], element, params)
        insert_comment(pdf, lcat, ypos, entry[2], entry[0])
        ypos += entry[1]
      end
    end

    pdf.render
  end

end










