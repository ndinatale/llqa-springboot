require_relative '../lib/constants'

module MeasureStatistics

  def MeasureStatistics.get_rule_desc(calc, lcat, valtype, fformat)
    case valtype
      when 'thold'
        return lcat.pdf_minput_thresh % [
            "#{fformat}%s" % [calc['t1_targetv'], calc['t1_targetu'] ? ' '+calc['t1_targetu'] : ''],
            "#{fformat}%s" % [calc['t1_tholdv'], calc['t1_tholdu'] ? ' '+calc['t1_tholdu'] : '']
        ]
      when 'absrng'
        return lcat.pdf_minput_valuerng % [
            "#{fformat}%s" % [calc['t2_minv'], calc['t2_unit'] ? ' '+calc['t2_unit'] : ''],
            "#{fformat}%s" % [calc['t2_maxv'], calc['t2_unit'] ? ' '+calc['t2_unit'] : '']
        ]
      when 'abs'
        return lcat.pdf_minput_valuecmp % [
            lcat.pdf_minput_comp['T'+calc['t8_comparator'].to_s],
            "#{fformat}%s" % [calc['t8_value'], calc['t8_unit'] ? ' '+calc['t8_unit'] : '']
        ]
      when 'reschk'
        return lcat.pdf_minput_checkcmp % [
            lcat.pdf_minput_comp['T'+calc['t10_comparator'].to_s],
            "#{fformat}%s" % [calc['t10_value'], calc['t10_unit'] ? ' '+calc['t10_unit'] : '']
        ]
      when 'frtext'
        return lcat.pdf_minput_textlen % calc['t3_minlen']
      when 'regexp'
        return lcat.pdf_minput_textpat % calc['t4_regexp']
      when 'choice'
        return lcat.pdf_minput_choice
      when 'flag'
        return calc['t5_expected'] == 0 ? lcat.pdf_minput_exp_no : (calc['t5_expected'] == 1 ? lcat.pdf_minput_exp_yes : lcat.pdf_minput_exp_both)
      when 'statst'
        return lcat.pdf_minput_statistical
      when 'timera'
        return lcat.pdf_minput_timera
      when 'timers'
        return lcat.pdf_minput_timers
      when 'timerq'
        return lcat.pdf_minput_timerq['T'+calc['t15_comparator'].to_s] % [calc['t15_value']]
      when 'timerc'
        return lcat.pdf_minput_timerc['T'+calc['t15_comparator'].to_s] % [calc['t15_value']]
    end
    return '???'
  end

  def MeasureStatistics.prepare_data(proc_rid, params, lcat, removeobj)
    measures,checks = [],[]

    params['measures'].split(';').each do |mid|
      mid = mid.to_i
      mobj = Measure.find_by(id:mid)
      next unless mobj
      mtype = mobj.measuretype_info
      fformat = $application_parameters[:defaultfloatformat]
      fformat = mobj.metadata['floatformat'] if mobj.metadata && mobj.metadata['floatformat'] && mobj.metadata['floatformat'].start_with?('%')
      measures << {
        type: 'measure',
        id: mobj.id,
        code: mobj.code,
        title: mobj.title[lcat.code],
        description: (mobj.description[lcat.code] || '').html_to_pdf,
        step_code: mobj.step.code,
        step_title: mobj.step.title[lcat.code],
        procedure_code: mobj.step.procedure.code,
        procedure_title: mobj.step.procedure.title[lcat.code],
        input_type: mtype[:input],
        value_type: mtype[:type],
        tooltype_code: mobj.tooltype_id ? mobj.tooltype.code : nil,
        tooltype_title: mobj.tooltype_id ? mobj.tooltype.title[lcat.code] : nil,
        optional: mobj.calculation['optional'] == 1,
        rule: get_rule_desc(mobj.calculation, lcat, mtype[:type], fformat),
        fformat: fformat,
        obj: mobj
      }
    end

    params['checks'].split(';').each do |cid|
      cid = cid.to_i
      cobj = Check.find_by(id:cid)
      next unless cobj
      checks << {
        type: 'check',
        id: cobj.id,
        status: CheckReport.check_status(cobj.status, lcat, params),
        unit_code: cobj.unit.code,
        unit_customer: cobj.unit.customer,
        unit_status: CheckReport.unit_status(cobj.unit.status, lcat),
        model_code: cobj.model.code,
        model_title: cobj.model.title[lcat.code],
        devicetype_code: cobj.model.devicetype.code,
        devicetype_title: cobj.model.devicetype.title[lcat.code],
        checktype_code: cobj.checktype.code,
        checktype_title: cobj.checktype.title[lcat.code],
        scheduled: $application_parameters[:skipcheckschedule] ? nil : lcat.formatd(cobj.scheduled),
        dueby: $application_parameters[:skipcheckschedule] ? nil : lcat.formatd(cobj.dueby),
        started: lcat.formatd(cobj.started),
        finished: lcat.formatd(cobj.finished),
        obj: cobj
      }
    end

    measures.each do |measure|
      mobj = measure[:obj]
      mments = []
      checks.each_with_index do |check,cnum|
        cobj = check[:obj]
        mmdata = nil
        # only collect data if cobj.checkdata['procedures'] exists
        if cobj.checkdata['procedures']
          cobj.checkdata['procedures'].each do |pdata|
            next if mmdata || pdata['code'] != measure[:procedure_code]
            pdata['steps'].each do |sdata|
              next if mmdata || sdata['code'] != measure[:step_code]
              sdata['measures'].each do |mdata|
                next if mmdata || mdata['code'] != measure[:step_code]+'.'+measure[:code]
                msobj = mdata['mid'] >= 0 ? Measurement.find(mdata['mid']) : nil
                mmdata = {
                    type: 'measurement',
                    check_num: cnum+1,
                    check_id: check[:id],
                    has_data: true,
                    is_old_version: mdata['id'] != mobj.id,
                    status: CheckReport.status_name(lcat,mdata,params),
                    status_code: mdata['status'],
                    value: CheckReport.status_name(lcat,mdata,params,mobj,msobj,true),
                    tool: mobj.tooltype_id && CHK_CDSTATUS_ALL_FINISHED.index(mdata['status']) && msobj && msobj.toolunit_id && msobj.toolunit ? msobj.toolunit.code : nil,
                    comment: msobj ? msobj.comment : nil,
                    saved_by: CheckReport.user_name(lcat,nil,msobj ? msobj.savedby : nil),
                    saved_on: lcat.formatt((msobj && msobj.savedon) ? Time.at_with_tzoff(msobj.savedon, params['tzoff']) : nil),
                    assignee: CheckReport.user_name(lcat, cobj, sdata['assignee']),
                    committed: CheckReport.user_name(lcat, nil, sdata['committed'])
                }
              end
            end
          end
        end
        mmdata = {
            type: 'measurement',
            check_num: cnum+1,
            check_id: check[:id],
            has_data: false,
            is_old_version: false,
            status: nil,
            status_code: nil,
            value: nil,
            tool: nil,
            comment: nil,
            saved_by: nil,
            saved_on: nil,
            assignee: nil,
            committed: nil
        } if !mmdata
        mments << mmdata
      end
      measure[:measurements] = mments
    end
    checks.each { |check| check.delete :obj } if removeobj
    measures.each { |measure| measure.delete :obj } if removeobj

    { checks: checks, measures: measures }
  end

  def MeasureStatistics.new_page(pdf, pagetitle, pagenum, pagetotal, lcat)
    pdf.start_new_page
    pdf.font 'FontHdr'
    pdf.fill_color '005499'
    pdf.fill {
      pdf.rectangle [20.mm,287.mm], 180.mm, 15.mm
      pdf.rectangle [20.mm,20.mm], 180.mm, 10.mm
    }
    pdf.fill_color 'ffffff'
    # header
    text = lcat.pdf_statreport
    pdf_text pdf, 25, 10, 170, 15, text, true, valign: :center, fontsize: 24
    text = lcat.formatd(Time.now)
    pdf_text pdf, 25, 10, 170, 15, text, true, style: :bold, fontsize: 24, valign: :center, halign: :right
    # footer
    text = pagetitle
    pdf_text pdf, 25, -20, 170, 10, text, true, fontsize: 16, valign: :center, style: :bold
    text = lcat.pdf_pageno % [pagenum,pagetotal]
    pdf_text pdf, 25, -20, 170, 10, text, true, fontsize: 16, valign: :center, halign: :right
  end

  def MeasureStatistics.insert_front_page(pdf, data, lcat)
    MeasureStatistics.new_page pdf, lcat.pdf_stat_title_front, 1, data[:totalpages], lcat
    pdf.fill_color '000000'
    pdf_text pdf, 25, 30, 170, 20, lcat.pdf_stat_checkintro, false, fontsize: 14
    data[:checks].each_with_index do |check,idx|
      pdf.fill_color 'e0e0e0'
      pdf.fill {
        pdf.rectangle [20.mm,(258-idx*7.5).mm], 180.mm, 7.5.mm
      } if idx&1>0
      pdf.fill_color '000000'
      pdf_text pdf, 25, 40+idx*7.5, 8, 7.5, "#{idx+1}", false, fontsize: 14, halign: :right
      text = lcat.pdf_stat_check_line1 % [check[:id], check[:checktype_title], check[:unit_code], check[:unit_customer], check[:model_code], check[:model_title]]
      pdf_text pdf, 35, 40+idx*7.5, 160, 5, text, false, fontsize: 10, autofit: true, inline_format: true
      text = lcat.pdf_stat_check_line2 % [check[:status], check[:unit_status]]
      pdf_text pdf, 35, 43.5+idx*7.5, 160, 5, text, false, fontsize: 8, autofit: true, inline_format: true
    end
  end

  def MeasureStatistics.insert_stats_page(pdf, data, idx, lcat)
    msr = data[:measures][idx]
    MeasureStatistics.new_page pdf, lcat.pdf_stat_title_stats % msr[:code], idx+2, data[:totalpages], lcat
    pdf.fill_color '000000'
    pdf_text pdf, 25, 30, 170, 8, lcat.pdf_stat_msr1 % [msr[:code], msr[:title]], false, fontsize: 14, autofit: true, inline_format: true
    pdf_text pdf, 25, 38, 170, 6, lcat.pdf_stat_msr2 % [msr[:step_code], msr[:step_title], msr[:procedure_code], msr[:procedure_title]], false, fontsize: 11, autofit: true, inline_format: true
    pdf_text pdf, 25, 44, 170, 6, lcat.pdf_stat_msr3o % msr[:rule], false, fontsize: 11, autofit: true, inline_format: true if msr[:optional]
    pdf_text pdf, 25, 44, 170, 6, lcat.pdf_stat_msr3s % msr[:rule], false, fontsize: 11, autofit: true, inline_format: true unless msr[:optional]
    pdf_text pdf, 25, 50, 170, 6, lcat.pdf_stat_msr4t % [msr[:tooltype_code], msr[:tooltype_title]], false, fontsize: 11, autofit: true, inline_format: true if msr[:tooltype_code]
    pdf_text pdf, 25, 50, 170, 6, lcat.pdf_stat_msr4n, false, fontsize: 11, autofit: true, inline_format: true unless msr[:tooltype_code]

    vallen,grphtype = 91,:none
    case msr[:value_type]
      when 'thold'
        vallen,grphtype = 21,:thold
      when 'absrng'
        vallen,grphtype = 21,:absrng
      when 'abs'
        vallen,grphtype = 21,:abs
      when 'statst'
        vallen,grphtype = 21,:stat
      when 'timers'
        vallen,grphtype = 21,:tbar
      when 'timerq'
        vallen,grphtype = 21,:tbar
      when 'timerc'
        vallen,grphtype = 21,:tbar
    end

    pdf_text pdf, 33, 66,  15, 6, lcat.pdf_stat_hdr_status, false, style: :bold, fontsize: 8
    pdf_text pdf, 50, 66,  35, 6, lcat.pdf_stat_hdr_user, false, style: :bold, fontsize: 8
    pdf_text pdf, 87, 66,  15, 6, lcat.pdf_stat_hdr_tool, false, style: :bold, fontsize: 8
    pdf_text pdf, 104, 66,  vallen, 6, lcat.pdf_stat_hdr_value, false, style: :bold, halign: :right, fontsize: 8
    has_old_v = false
    msr[:measurements].each_with_index do |mmnt,mnum|
      pdf.fill_color 'e0e0e0'
      pdf.fill {
        pdf.rectangle [20.mm,(227.5-mnum*6.5).mm], (vallen+89).mm, 6.5.mm
      } if mnum&1>0
      pdf.fill_color '000000'
      has_old_v = true if mmnt[:is_old_version]
      pdf_text pdf, 25, 70+mnum*6.5,  6, 6, "<sup>*</sup>#{mnum+1}", false, valign: :center, halign: :right, fontsize: 10, autofit: true, inline_format: true if mmnt[:is_old_version]
      pdf_text pdf, 25, 70+mnum*6.5,  6, 6, "#{mnum+1}", false, valign: :center, halign: :right, fontsize: 10, autofit: true, inline_format: true unless mmnt[:is_old_version]
      if !mmnt[:has_data]
        pdf_text pdf, 33, 70+mnum*6.5,  92, 6, lcat.pdf_stat_nodata, false, valign: :center, style: :italic, fontsize: 10
        next
      end
      pdf_text pdf, 33, 70+mnum*6.5,  15, 6, mmnt[:status], false, valign: :center, style: :bold, fontsize: 10, autofit: true, inline_format: true
      pdf_text pdf, 50, 70+mnum*6.5,  35, 6, mmnt[:saved_by], false, valign: :center, fontsize: 8, autofit: true
      pdf_text pdf, 87, 70+mnum*6.5,  15, 6, mmnt[:tool], false, valign: :center, fontsize: 8, autofit: true
      color = mmnt[:status_code] == CHK_CDSTATUS_PASSED ? '00a000' : 'a00000'
      pdf_text pdf, 104, 70+mnum*6.5,  vallen, 6, mmnt[:value], false, color: color, inline_format: true, valign: :center, halign: :right, fontsize: 10, autofit: true if mmnt[:status] != mmnt[:value]
      # check_num, (has_data), (is_old_version), status, value, tool, saved_by, assignee, committed
      if grphtype != :none
        mmnt[:numvalue] = mmnt[:status] == mmnt[:value] ? nil : mmnt[:value].gsub(/.*(\d+)h (\d+)min.*/){|_|$1.to_i*60+$2.to_i}.gsub(/[^\-\d\.]/,'').to_f
      end
    end
    pdf_text pdf, 25, 273, 170, 6, lcat.pdf_stat_oldv, false, fontsize: 9, autofit: true, inline_format: true if has_old_v

    return if grphtype == :none

    calc = msr[:obj].calculation
    case grphtype
      when :thold
        thold = calc['t1_tholdv']
        min,max = -thold*1.5,thold*1.5
        vrng = msr[:measurements].map { |m| m[:numvalue] }.compact
        min,max = [vrng,min].flatten.min,[vrng,max].flatten.max
        centerlbl = "0 = #{msr[:fformat]}%s" % [calc['t1_targetv'], calc['t1_targetu'] ? ' '+calc['t1_targetu'] : '']
        minlbl = "#{msr[:fformat]}%s" % [min, calc['t1_tholdu'] ? ' '+calc['t1_tholdu'] : '']
        maxlbl = "#{msr[:fformat]}%s" % [max, calc['t1_tholdu'] ? ' '+calc['t1_tholdu'] : '']
        negthlbl = "#{msr[:fformat]}%s" % [-calc['t1_tholdv'], calc['t1_tholdu'] ? ' '+calc['t1_tholdu'] : '']
        posthlbl = "#{msr[:fformat]}%s" % [calc['t1_tholdv'], calc['t1_tholdu'] ? ' '+calc['t1_tholdu'] : '']
        MeasureStatistics.draw_graph(pdf, min, max, 0, minlbl, maxlbl, centerlbl, -thold, thold, { value: -thold, label: negthlbl }, { value: thold, label: posthlbl })
        msr[:measurements].each_with_index do |mmnt,mnum|
          next if !mmnt[:numvalue]
          vals = [mmnt[:numvalue],0]
          colb = mmnt[:status_code] == CHK_CDSTATUS_PASSED ? '00a000' : 'a00000'
          pdf_rectangle pdf, get_graph_x(min,max,vals.min), 71+mnum*6.5, get_graph_x(min,max,vals.max)-get_graph_x(min,max,vals.min), 4.5, colb, '000000', 0.2
        end
      when :absrng
        minv,maxv = calc['t2_minv'],calc['t2_maxv']
        min,max = minv-(maxv-minv)*0.25,maxv+(maxv-minv)*0.25
        vrng = msr[:measurements].map { |m| m[:numvalue] }.compact
        min,max = [vrng,min].flatten.min,[vrng,max].flatten.max
        center = min+(max-min)/2.0
        centerlbl = "#{msr[:fformat]}%s" % [center, calc['t2_unit'] ? ' '+calc['t2_unit'] : '']
        minlbl = "#{msr[:fformat]}%s" % [min, calc['t2_unit'] ? ' '+calc['t2_unit'] : '']
        maxlbl = "#{msr[:fformat]}%s" % [max, calc['t2_unit'] ? ' '+calc['t2_unit'] : '']
        minvlbl = "#{msr[:fformat]}%s" % [minv, calc['t2_unit'] ? ' '+calc['t2_unit'] : '']
        maxvlbl = "#{msr[:fformat]}%s" % [maxv, calc['t2_unit'] ? ' '+calc['t2_unit'] : '']
        MeasureStatistics.draw_graph(pdf, min, max, center, minlbl, maxlbl, centerlbl, minv, maxv, { value: minv, label: minvlbl }, { value: maxv, label: maxvlbl })
        msr[:measurements].each_with_index do |mmnt,mnum|
          next if !mmnt[:numvalue]
          colb = mmnt[:status_code] == CHK_CDSTATUS_PASSED ? '00a000' : 'a00000'
          pdf_ellipse pdf, get_graph_x(min,max,mmnt[:numvalue]), 73.25+mnum*6.5, 1.25, 2.25, colb, '000000', 0.2
        end
      when :abs
        cmpv,cmpt = calc['t8_value'],calc['t8_comparator']
        vrng = msr[:measurements].map { |m| m[:numvalue] }.compact
        min,max = [vrng,cmpv].flatten.min,[vrng,cmpv].flatten.max
        min,max = min-(max-min)*0.1,max+(max-min)*0.1
        centerlbl = "#{msr[:fformat]}" % [cmpv]
        minlbl = "#{msr[:fformat]}" % [min]
        maxlbl = "#{msr[:fformat]}" % [max]
        minv,maxv = cmpv,max if cmpt > 3
        minv,maxv = cmpv-(max-min)*0.01,cmpv+(max-min)*0.01 if cmpt == 3
        minv,maxv = min,cmpv if cmpt < 3
        MeasureStatistics.draw_graph(pdf, min, max, nil, minlbl, maxlbl, nil, minv, maxv, { value: cmpv, label: centerlbl })
        msr[:measurements].each_with_index do |mmnt,mnum|
          next if !mmnt[:numvalue]
          colb = mmnt[:status_code] == CHK_CDSTATUS_PASSED ? '00a000' : 'a00000'
          pdf_ellipse pdf, get_graph_x(min,max,mmnt[:numvalue]), 73.25+mnum*6.5, 1.25, 2.25, colb, '000000', 0.2
        end
      when :tbar
        cmpv = (msr[:value_type] == 'timerq' || msr[:value_type] == 'timerc') ? calc['t15_value'] : 0
        vrng = msr[:measurements].map { |m| m[:numvalue] }.compact
        max = [vrng,cmpv].flatten.max
        max += max % 60 == 0 ? 60 : (60-(max%60))
        gf,gt,lines = 0,max,[]
        if (msr[:value_type] == 'timerq' || msr[:value_type] == 'timerc')
          lines << { value: cmpv, label: cmpv.to_s }
          if calc['t15_comparator'] == 1
            gt = cmpv
          else
            gf = cmpv
          end
        end
        MeasureStatistics.draw_graph(pdf, 0, max, nil, '0', max.to_i.to_s, nil, gf, gt, *lines)
        msr[:measurements].each_with_index do |mmnt,mnum|
          next if !mmnt[:numvalue]
          vals = [mmnt[:numvalue],0]
          colb = mmnt[:status_code] == CHK_CDSTATUS_PASSED ? '00a000' : 'a00000'
          pdf_rectangle pdf, get_graph_x(0,max,vals.min), 71+mnum*6.5, get_graph_x(0,max,vals.max)-get_graph_x(0,max,vals.min), 4.5, colb, '000000', 0.2
        end
      when :stat
        vrng = msr[:measurements].map { |m| m[:numvalue] }.compact
        min,max = vrng.min || 0,vrng.max || 0
        min,max = min-(max-min)*0.1,max+(max-min)*0.1
        minlbl = "#{msr[:fformat]}" % [min]
        maxlbl = "#{msr[:fformat]}" % [max]
        minv,maxv = min,max
        MeasureStatistics.draw_graph(pdf, min, max, nil, minlbl, maxlbl, nil, minv, maxv)
        msr[:measurements].each_with_index do |mmnt,mnum|
          next if !mmnt[:numvalue]
          colb = mmnt[:status_code] == CHK_CDSTATUS_PASSED ? '00a000' : 'a00000'
          pdf_ellipse pdf, get_graph_x(min,max,mmnt[:numvalue]), 73.25+mnum*6.5, 1.25, 2.25, colb, '000000', 0.2
        end
    end

  end

  def MeasureStatistics.get_graph_x(min, max, value)
    135.0 + 65.0/(max-min)*(value-min)
  end

  def MeasureStatistics.draw_graph(pdf, min, max, center, minlbl, maxlbl, centerlbl, greenfrom, greento, *lines)
    pdf_rectangle pdf, 135, 67, 65, 200, 'fff0f0', nil, nil
    pdf_rectangle pdf, get_graph_x(min,max,greenfrom), 67, get_graph_x(min,max,greento)-get_graph_x(min,max,greenfrom), 200, 'f0fff0', nil, nil
    pdf_text pdf, 135, 60, 20, 3.5, minlbl, false, valign: :bottom, halign: :left, fontsize: 7, autofit: true
    pdf_text pdf, 180, 60, 20, 3.5, maxlbl, false, valign: :bottom, halign: :right, fontsize: 7, autofit: true
    pdf_text pdf, get_graph_x(min,max,center)-10, 60, 20, 3.5, centerlbl, false, valign: :bottom, halign: :center, fontsize: 7, autofit: true if center
    pdf_line pdf, 135, 63, 135, 267, '000000', 0.2
    pdf_line pdf, 200, 63, 200, 267, '000000', 0.2
    pdf_line pdf, get_graph_x(min,max,center), 63, get_graph_x(min,max,center), 267, '000000', 0.2 if center
    lines.each do |line|
      pdf_text pdf, get_graph_x(min,max,line[:value])-8, 64, 16, 3.5, line[:label], false, valign: :bottom, halign: :center, fontsize: 7, autofit: true
      pdf_line pdf, get_graph_x(min,max,line[:value]), 67, get_graph_x(min,max,line[:value]), 267, '000000', 0.2
    end
  end

  def MeasureStatistics.insert_final_page(pdf, data, checks, measures, lcat)
    MeasureStatistics.new_page pdf, '', data[:totalpages], data[:totalpages], lcat
    pdf.fill_color '000000'
    pdf_text pdf, 25, 30, 170, 20, lcat.pdf_stat_final_intro, false, fontsize: 14
    pdf_text pdf, 25, 50, 170, 20, lcat.pdf_stat_final_checks + "\n<b>" + checks + '</b>', false, fontsize: 12, inline_format: true
    pdf_text pdf, 25, 70, 170, 20, lcat.pdf_stat_final_measures + "\n<b>" + measures + '</b>', false, fontsize: 12, inline_format: true
  end

  def MeasureStatistics.export_data(proc_rid, params)
    lcat = pdf_read_language(params['lang'] || $system_languages[0][:code])
    data = MeasureStatistics.prepare_data proc_rid,params,lcat,true

    case params['format']
      when 'xml' then return data.format_as_xml
      when 'csv' then return data.format_as_csv(%w(type id code check_num check_id has_data is_old_version status value tool comment saved_by saved_on assignee committed title description step_code procedure_code input_type value_type tooltype_code tooltype_title optional rule unit_code unit_customer unit_status model_code model_title devicetype_code devicetype_title checktype_code checktype_title scheduled dueby started finished))
    end
    return data.format_as_json
  end

  def MeasureStatistics.gen_pdf(proc_rid, params)
    lcat = pdf_read_language(params['lang'] || $system_languages[0][:code])
    data = MeasureStatistics.prepare_data proc_rid,params,lcat,false

    data[:totalpages] = data[:measures].size+2

    pdf = Prawn::Document.new(page_size: "A4", margin: 0, page_layout: :portrait, compress: true, print_scalign: :none, skip_page_creation: true) do |pdf|
      pdf_setup_fonts(pdf,'os')

      MeasureStatistics.insert_front_page(pdf, data, lcat)
      data[:measures].size.times { |i| MeasureStatistics.insert_stats_page(pdf, data, i, lcat) }
      MeasureStatistics.insert_final_page(pdf, data, params['checks'], params['measures'], lcat)
    end

    pdf.render
  end

end