# encoding: UTF-8

module ToolunitReport

  def ToolunitReport.insert_header(pdf, lcat, unit, params)
    pdf.font 'FontHdr'
    pdf.fill_color '005499'
    pdf.fill {
      pdf.rectangle [20.mm,287.mm], 180.mm, 15.mm
      pdf.rectangle [20.mm,20.mm], 180.mm, 10.mm
    }
    pdf.fill_color 'ffffff'
    # header
    text = lcat.pdf_tureport_header
    pdf_text pdf, 25, 10, 170, 15, text, true, valign: :center, fontsize: 24
    text = lcat.formatd(Time.now)
    pdf_text pdf, 25, 10, 170, 15, text, true, style: :bold, fontsize: 16, valign: :center, halign: :right
    # footer
    text = (lcat.pdf_tureport_footer % [unit.tooltype.code, unit.code])
    pdf_text pdf, 25, -19, 160, 8, text, true, fontsize: 14, valign: :center, style: :bold, autofit: true, inline_format: true
    params['pageno'] += 1
    pdf_text pdf, 25, -20, 170, 10, "#{params['pageno']}", true, fontsize: 16, valign: :center, halign: :right
  end

  def ToolunitReport.check_new_page(pdf,ypos,lcat,toolunit,params,height,margin)
    if height+margin+ypos > 272
      pdf.start_new_page
      insert_header(pdf, lcat, toolunit, params)
      return true
    end
    false
  end

  def ToolunitReport.paint_block(pdf,ypos,color,height)
    pdf.fill_color color
    pdf.fill { pdf.rectangle [20.mm,(297-ypos).mm], 180.mm, height.mm }
    ypos+2
  end

  def ToolunitReport.insert_title(pdf, lcat, ypos, type, useobj, continued = false)
    pdf.fill_color '000000'
    prerun = ypos == nil
    ypos = 0 if prerun
    height = 0
    xpos = 22
    width = 200-xpos

    text = if type == :model
             (lcat.pdf_tureport_title_model % [useobj.check.unit.model.code, useobj.check.unit.code])
           else
             time = Time.at(useobj.savedon)
             (lcat.pdf_tureport_title_time % [time.strftime('%m'), time.strftime('%Y')])
           end
    text += ' '+lcat.pdf_tureport_continued if continued
    height += pdf_text(pdf, xpos, ypos+height, width, 10, text, true, fontsize: 12, valign: :top, autofit: true, inline_format: true, dry: prerun)[0]
    height + 2
  end

  def ToolunitReport.insert_tooluse(pdf, lcat, ypos, type, useobj, drawline = false)
    pdf.fill_color '000000'
    prerun = ypos == nil
    ypos = 0 if prerun
    height = 0
    xpos = 22
    width = 138-xpos

    pdf_text(pdf, 150, ypos+height, 50, 20, lcat.formatt(Time.at(useobj.savedon)), true, fontsize: 8, valign: :top, halign: :right, autofit: true, dry: prerun)
    if type == :model
      text = (' • '+lcat.pdf_tureport_entry_model % [useobj.measure.step.procedure.code, useobj.measure.step.code, useobj.measure.code])
      height += pdf_text(pdf, xpos, ypos+height, width, 20, text, false, fontsize: 8, valign: :top, autofit: true, inline_format: true, dry: prerun)[0]
    else
      text = (' • '+lcat.pdf_tureport_entry_timet % [useobj.check.unit.model.code, useobj.check.unit.code])
      height += pdf_text(pdf, xpos, ypos+height, width, 20, text, false, fontsize: 8, valign: :top, autofit: true, inline_format: true, dry: prerun)[0]
      text = (lcat.pdf_tureport_entry_timeb % [useobj.measure.step.procedure.code, useobj.measure.step.code, useobj.measure.code])
      height += pdf_text(pdf, xpos+2, ypos+height, width-4, 20, text, false, fontsize: 6, valign: :top, autofit: true, inline_format: true, dry: prerun)[0]
    end
    if !prerun && drawline
      pdf_line(pdf,24,ypos+height-0.5,200,ypos+height-0.5,'a0a0a0',0.2)
    end
    height + 1
  end

  def ToolunitReport.prepare_check_sorted(pdf, data, lcat)
    last_unit_code = nil
    wmap = []
    data.each do |use|
      next if use.savedon == nil
      unit_code = use.check.unit.model.code + '_' + use.check.unit.code
      if last_unit_code != unit_code
        height = insert_title(pdf, lcat, nil, :model, use)
        wmap << [use, height, true]
        last_unit_code = unit_code
      end
      height = insert_tooluse(pdf, lcat, nil, :model, use)
      wmap << [use, height, false]
    end
    wmap
  end

  def ToolunitReport.prepare_time_sorted(pdf, data, lcat)
    last_savedon = nil
    wmap = []
    data.each do |use|
      next if use.savedon == nil
      savedon = Time.at(use.savedon)
      savedon = savedon.strftime('%m%Y')
      if last_savedon != savedon
        height = insert_title(pdf, lcat, nil, :time, use)
        wmap << [use, height, true]
        last_savedon = savedon
      end
      height = insert_tooluse(pdf, lcat, nil, :time, use)
      wmap << [use, height, false]
    end
    wmap
  end

  def ToolunitReport.gen_pdf(params)
    pdf = Prawn::Document.new(page_size: 'A4', margin: 0, page_layout: :portrait, compress: true, print_scalign: :none) do |pdf|
      pdf_setup_fonts(pdf,'os')

      lcat = pdf_read_language(params['lang'] || $system_languages[0][:code])

      unit = Toolunit.find_by(id: params['uid'].to_i)
      data = unit ? unit.all_uses(params['sort'],params['dfrom'] ? params['dfrom'].to_i : nil,params['dto'] ? params['dto'].to_i : nil) : []

      params['pageno'] = 0

      insert_header(pdf, lcat, unit, params)
      ypos = 30

      wmap = params['sort'] == 'check' ? prepare_check_sorted(pdf, data, lcat) : prepare_time_sorted(pdf, data, lcat)
      lastheader = nil
      type = (params['sort'] == 'check' ? :model : :time)
      wmap.each_with_index do |entry,idx|
        if entry[2]
          ypos = 30 if check_new_page(pdf, ypos, lcat, unit, params, entry[1]+wmap[idx+1][1], 2)
          ypos = paint_block(pdf, ypos, 'e7e7e7', entry[1])
          insert_title(pdf, lcat, ypos, type, entry[0], false)
          lastheader = entry
          ypos += entry[1]
        else
          if check_new_page(pdf, ypos, lcat, unit, params, entry[1], 0)
            ypos = 30
            ypos = paint_block(pdf, ypos, 'e7e7e7', lastheader[1])
            insert_title(pdf, lcat, ypos, type, lastheader[0], true)
            ypos += lastheader[1]
          end
          insert_tooluse(pdf, lcat, ypos, type, entry[0], !wmap[idx+1] || !wmap[idx+1][2])
          ypos += entry[1]
        end
      end
    end

    pdf.render
  end

end

