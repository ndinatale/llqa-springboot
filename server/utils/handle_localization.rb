require 'json'

de = JSON.parse(File.read('../../public/i18n/lang-de.json'))
en = JSON.parse(File.read('../../public/i18n/lang-en.json'))
cn = JSON.parse(File.read('../../public/i18n/lang-cn.json'))
fr = JSON.parse(File.read('../../public/i18n/lang-fr.json'))

def recurse(block, prefix='')
  block.each do |key,value|
    if value.is_a? String
      yield prefix+key, value
    else
      recurse(value, prefix+key+'.', &Proc.new)
    end
  end
end

def insert_value(map,key,value)
  pos,tagparts = map,key.split('.')
  tagparts[0..-2].each do |tagpart|
    pos[tagpart] = {} if !pos[tagpart]
    pos = pos[tagpart]
  end
  if pos[tagparts[-1]]
    puts "text in #{key} exists!"
    return
  end

  pos[tagparts[-1]] = value
end

deh,enh,cnh,frh = {},{},{},{}
alllang = Hash.new { |h,k| h[k] = [0,0,0] }
recurse(de) { |k,v| deh[k]=v; alllang[k][0] = (v.start_with?('@:') ? 1 : 2) }
recurse(en) { |k,v| enh[k]=v; alllang[k][1] = (v.start_with?('@:') ? 1 : 2) }
recurse(cn) { |k,v| cnh[k]=v; alllang[k][2] = (v.start_with?('@:') ? 1 : 2) }
recurse(fr) { |k,v| frh[k]=v; alllang[k][2] = (v.start_with?('@:') ? 1 : 2) }

begin
  alllang.each do |key,found|
    if found[0] > 0 && found[1] == 0
      puts "#{key} found in DE, not EN!"
      insert_value(en,key,"[TTL]#{deh[key]}")
    end
  end
  File.open('../../public/i18n/lang-en.json','w') { |f| f.puts JSON.pretty_generate(en) }
end

