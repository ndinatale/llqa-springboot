(function () {
  'use strict';

  describe("AlertModalCtrl", function () {

    beforeEach(module('leanLogic.controllers'));
    beforeEach(module('restangular'));

    var AlertModalCtrl, scope;

    beforeEach(inject(function ($controller, _$rootScope_) {
      scope = _$rootScope_.$new();

      AlertModalCtrl = $controller('AlertModalCtrl', {
        $scope: scope,
        type: 1,
        head: "head",
        intro: "intro",
        reasons: "reasons",
        $modalInstance: {}
      });
    }));

    it('should instantiate Controller', function () {
      expect(AlertModalCtrl).not.toBeNull();
    });

    it('should provide ok and cancel methods', function () {
      expect(scope.ok).toBeDefined();
      expect(scope.ok).toEqual(jasmine.any(Function));

      expect(scope.cancel).toBeDefined();
      expect(scope.cancel).toEqual(jasmine.any(Function));
    });

    it('should provide type', function () {
      expect(scope.gettypestr).toBeDefined();
      expect(scope.gettypestr).toEqual(jasmine.any(Function));

      scope.type = 1;
      var result = scope.gettypestr();
      expect(result).toEqual("primary");
    });

  });
}());