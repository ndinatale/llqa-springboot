(function () {
  'use strict';

  describe("ChangeLogViewCtrl", function () {

    beforeEach(module('leanLogic.controllers'));
    beforeEach(module('restangular'));

    var ChangeLogViewCtrl, scope, Restangular, $httpBackend;

    beforeEach(inject(function ($controller, _$httpBackend_, _$rootScope_, _Restangular_) {
      scope = _$rootScope_.$new();

      Restangular = _Restangular_;
      $httpBackend = _$httpBackend_;

      $httpBackend.whenGET('/model/changelog').respond({
        changelog: [
          {changetype: "foobar", subobjtype: "title"}, {changetype: "description", subobjtype: "asd"}
        ]
      });

      ChangeLogViewCtrl = $controller('ChangeLogViewCtrl', {
        $scope: scope,
        Restangular: Restangular,
        $httpBackend: $httpBackend,
        type: "model",
        item: {},
        $modalInstance: {}
      });

      $httpBackend.flush();
    }));

    afterEach(function () {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should instantiate Controller', function () {
      expect(ChangeLogViewCtrl).not.toBeNull();
    });

    it('should prepare lines & item on refreshData()', function () {
      expect(scope.lines).toBeDefined();
      expect(scope.hasDirty).toBeDefined();
      expect(scope.hasDirty).toBeFalsy();
      expect(scope.hasPostFinalize).toBeDefined();
      expect(scope.hasPostFinalize).toBeFalsy();

      expect(scope.lines.length).toEqual(2);
      expect(scope.lines[0].description).toEqual('CHANGELOG.TYPE.FOOBAR');
      expect(scope.lines[0].itemtype).toEqual('CHANGELOG.ITEM.TITLE');
      expect(scope.lines[0].marked).toEqual('normal');
    });

  });
}());