(function () {
  'use strict';

  describe("ErrorMsgCtrl", function () {

    beforeEach(module('leanLogic.controllers'));
    beforeEach(module('restangular'));

    var ErrorMsgCtrl, scope, $location;

    beforeEach(inject(function ($controller, _$rootScope_) {
      scope = _$rootScope_.$new();

      $location = {
        url: function () {
        }
      };

      ErrorMsgCtrl = $controller('ErrorMsgCtrl', {
        $scope: scope,
        $rootScope: _$rootScope_,
        $stateParams: {code: 42},
        $location: $location
      });
    }));

    it('should instantiate Controller', function () {
      expect(ErrorMsgCtrl).not.toBeNull();
    });

    it('should set provided code', function () {
      expect(scope.errCode).toBeDefined();
      expect(scope.errCode).toEqual(42);
    });

    it('should provide function leave()', function () {
      expect(scope.leave).toBeDefined();
      expect(scope.leave).toEqual(jasmine.any(Function));

      // install spy and fire
      spyOn($location, 'url');
      scope.leave();

      expect($location.url).toHaveBeenCalled();
      expect($location.url).toHaveBeenCalledWith("/app/login");
    });

  });
}());