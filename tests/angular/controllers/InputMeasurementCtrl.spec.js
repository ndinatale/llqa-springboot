(function () {
  'use strict';

  describe("InputMeasurementCtrl", function () {

    beforeEach(module('leanLogic.controllers'));
    //beforeEach(module('leanLogic.filters'));
    //beforeEach(module('leanLogic.services'));
    //beforeEach(module('leanLogic.directives'));
    //beforeEach(module('ui.bootstrap'));
    beforeEach(module('restangular'));

    var InputMeasurementCtrl, $httpBackend, measure, scope, rootScope, mainscope, testmode, measurement, UsersService, Restangular, i18n, $q, deferred;

    beforeEach(inject(function ($controller, _$httpBackend_, _Restangular_, _$q_) {
      $httpBackend = _$httpBackend_;

      // inject fake list measure
      measure = {
        "id": 12415,
        "step_id": 6699,
        "tooltype_id": null,
        "code": "12415",
        "title": {
          "en": "Auswahlliste2",
          "de": "Auswahlliste2",
          "cn": "Auswahlliste2",
          "fr": "Auswahlliste2"
        },
        "description": {
          "en": "Auswahlliste2",
          "de": "Auswahlliste2",
          "cn": "Auswahlliste2",
          "fr": "Auswahlliste2"
        },
        "measuretype": 17,
        "calculation": {
          "t17_len": 3,
          "t17_list": [
            "SKF",
            "BAG",
            "NSK"
          ],
          "optional": 0
        },
        "metadata": {},
        "flowcontrol": []
      };

      $q = _$q_;
      deferred = $q.defer();

      scope = {};
      rootScope = {appProperties: {}};
      mainscope = {};
      testmode = {};
      measurement = {};
      UsersService = {
        addGrantMethods: function () {
          return deferred.promise;
        }
      };
      Restangular = _Restangular_;
      i18n = {selectedLanguage: {}};

      // example for responding to request
      //$httpBackend.whenGET('app/common/services/enums.json').respond({});

      InputMeasurementCtrl = $controller('InputMeasurementCtrl', {
        $scope: scope,
        $rootScope: rootScope,
        mainscope: mainscope,
        $modalInstance: {},
        measure: measure,
        testmode: testmode,
        measurement: measurement,
        mtinfo: {},
        ConstantService: {
          installConstants: function () {
          }
        },
        ModalEditorService: {},
        EditorManagementService: {
          installEditorManagement: function () {
          }
        },
        InputMeasurementCtrl: {},
        Restangular: Restangular,
        UsersService: UsersService,
        i18n: i18n,
        AlertService: {},
        $modal: {},
        sprintf: {},
        _: {},
        KeyboardShortcutService: {
          installKeyboardShortcuts: function () {
          }
        }
      });
    }));

    afterEach(function () {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should instantiate Controller', function () {
      expect(InputMeasurementCtrl).not.toBeNull();
    });

    it('should have a scope', function () {
      expect(scope).not.toBeNull();
      expect(scope).toEqual(scope);
    });

    it('should have injected refreshModalData-function', function () {
      expect(scope.refreshModalData).toBeDefined();
      expect(scope.refreshModalData).toEqual(jasmine.any(Function));
    });

    it('should have a list of choices for measuretype 17', function () {
      expect(scope.choiceList).toEqual(jasmine.any(Function));

      // because the data in our measure is type 17 (choice list), we should get a list here
      var list = scope.choiceList();
      expect(list).not.toBeNull();
      expect(list.length).toEqual(3);
    });

    it('should have injected keyboard', function () {
      expect(scope.keyboard).toBeDefined();
      expect(scope.keyboard).toEqual(jasmine.any(Object));
      expect(scope.keyboard.needed).toBeDefined();
    });

    it('should have parsed measure into item', function () {
      // at the beginning, item is not defined
      expect(scope.item).not.toBeDefined();

      scope.refreshModalData();

      // after calling refresh, it should be defined
      expect(scope.item).toBeDefined();
      expect(scope.item).toEqual(jasmine.any(Object));

      // check item attributes
      expect(scope.item.comment).toBeDefined();
      expect(scope.item.comment).toEqual("");
      expect(scope.item.rawvalues).toBeDefined();
      expect(scope.item.rawvalues).toEqual(jasmine.any(Object));
      expect(scope.item.value).toBeDefined();
      expect(scope.item.value).toEqual(null);
      expect(scope.item.status).toBeDefined();
      expect(scope.item.status).toEqual(0);
    });


    it('should clear matrix', function () {
      scope.refreshModalData();

      var myItem = {foo: 'bar'};
      scope.item.rawvalues = myItem;
      expect(scope.item.rawvalues).toBeDefined();
      expect(scope.item.rawvalues).toEqual(myItem);

      scope.clearMatrix();
      expect(scope.item.rawvalues).toBeDefined();
      expect(scope.item.rawvalues).toEqual({});

    });

    it('should run outside testmode', function () {
      // inject magic number
      measurement.value = 42;
      scope.testmode = false;

      scope.refreshModalData();

      expect(scope.item.value).toBeDefined();
      expect(scope.item.value).toEqual(42);
    });

    it('should accept keyboard input', function () {
      var key = 'C';
      scope.refreshModalData();
      scope.keyboardAction(key);

      expect(scope.keyboard.pressed).toBeDefined();
      expect(scope.keyboard.curdot).toBeFalsy();
      expect(scope.keyboard.curpostdot).toEqual('');
      expect(scope.keyboard.curpredot).toEqual('');
      expect(scope.keyboard.curminus).toBeFalsy();
      expect(scope.keyboard.preview).toEqual('');
    });

  });
}());