(function () {
  'use strict';

  describe("PdfViewerCtrl", function () {

    beforeEach(module('leanLogic.controllers'));
    beforeEach(module('restangular'));

    var PdfViewerCtrl, scope, $location, medium, $modalInstance;

    beforeEach(inject(function ($controller, _$rootScope_) {
      scope = _$rootScope_.$new();

      medium = {caption: 'foobar', binaryfile: {filename: 'foobar'}};
      $modalInstance = {
        close: function () {
        }
      };

      PdfViewerCtrl = $controller('PdfViewerCtrl', {
        $scope: scope,
        $rootScope: _$rootScope_,
        medium: medium,
        $modalInstance: $modalInstance
      });
    }));

    it('should instantiate Controller', function () {
      expect(PdfViewerCtrl).not.toBeNull();
    });

    it('should set provide function setupConfig & close', function () {
      expect(scope.setupConfig).toBeDefined();
      expect(scope.setupConfig).toEqual(jasmine.any(Function));

      expect(scope.close).toBeDefined();
      expect(scope.close).toEqual(jasmine.any(Function));
    });

    it('should close modal', function () {
      expect(scope.close).toEqual(jasmine.any(Function));

      spyOn($modalInstance, 'close');
      scope.close();

      expect($modalInstance.close).toHaveBeenCalled();
    });

  });
}());