(function () {
  'use strict';

  describe("ToolListCtrl", function () {

    beforeEach(module('leanLogic.controllers'));
    beforeEach(module('restangular'));

    var ToolListCtrl, $httpBackend, scope, rootScope, mainscope, testmode, UsersService, Restangular, i18n, $q, deferred, location, ConstantService;

    beforeEach(inject(function ($controller, _$httpBackend_, _Restangular_, _$q_, _$rootScope_) {
      $httpBackend = _$httpBackend_;
      $q = _$q_;
      deferred = $q.defer();

      scope = _$rootScope_.$new();
      UsersService = {
        addGrantMethods: function () {
          return deferred.promise;
        },
        updateUserInfo: function () {
        }
      };
      Restangular = _Restangular_;
      i18n = {selectedLanguage: {}};
      location = {
        url: function () {
        }
      };

      ToolListCtrl = $controller('ToolListCtrl', {
        $scope: scope,
        $rootScope: rootScope,
        Restangular: Restangular,
        UsersService: UsersService,
        i18n: i18n,
        AlertService: {},
        $modal: {},
        VersioningService: {},
        FileUploader: {},
        ListManagementService: {
          installListManagement: function () {
          }
        },
        $location: location
      });
    }));

    afterEach(function () {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should instantiate Controller', function () {
      expect(ToolListCtrl).not.toBeNull();
    });

    it('should call location.url on goto()', function () {
      expect(scope.goto).toBeDefined();
      expect(scope.goto).toEqual(jasmine.any(Function));

      spyOn(location, 'url');
      scope.goto(2);

      var expectedUrl = "/app/tools/view/2";
      expect(location.url).toHaveBeenCalled();
      expect(location.url).toHaveBeenCalledWith(expectedUrl);
    });

    it('should call location.url on newTooltype', function () {
      expect(scope.newTooltype).toBeDefined();
      expect(scope.newTooltype).toEqual(jasmine.any(Function));

      spyOn(location, 'url');
      scope.newTooltype();

      var expectedUrl = "/app/tools/edit/new";
      expect(location.url).toHaveBeenCalled();
      expect(location.url).toHaveBeenCalledWith(expectedUrl);
    });


    it('should call refreshListData', function () {
      expect(scope.refreshListData).toBeDefined();
      expect(scope.refreshListData).toEqual(jasmine.any(Function));

      spyOn(Restangular, 'all').and.callThrough();

      // expect GET for tools
      $httpBackend.expectGET('/tools').respond([]);
      scope.refreshListData();
      $httpBackend.flush();

      expect(Restangular.all).toHaveBeenCalled();
    });

  });
}());