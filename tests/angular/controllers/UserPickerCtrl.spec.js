(function () {
  'use strict';

  describe("UserPickerCtrl", function () {

    beforeEach(module('leanLogic.controllers'));
    beforeEach(module('restangular'));

    var UserPickerCtrl, scope, Restangular, $httpBackend, usersShortlist;

    beforeEach(inject(function ($controller, _$httpBackend_, _$rootScope_, _Restangular_) {
      scope = _$rootScope_.$new();

      Restangular = _Restangular_;
      $httpBackend = _$httpBackend_;

      usersShortlist = [{id: 0}, {id: 1}];
      $httpBackend.whenGET('/users/shortlist').respond(usersShortlist);

      UserPickerCtrl = $controller('UserPickerCtrl', {
        $scope: scope,
        Restangular: Restangular,
        $httpBackend: $httpBackend,
        type: "model",
        item: {},
        $modalInstance: {},
        i18n: {},
        title: "Titel",
        text: "Text",
        presel: "Prezel",
        param: "param",
        AlertService: {}
      });

      $httpBackend.flush();
    }));

    afterEach(function () {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should instantiate Controller', function () {
      expect(UserPickerCtrl).not.toBeNull();
      expect(scope.setupPicker).toBeTruthy(scope.userPickLoaded);
    });

    it('should provide some modal function, refresh & setupPicker', function () {
      expect(scope.refreshPickerData).toEqual(jasmine.any(Function));
      expect(scope.setupPicker).toEqual(jasmine.any(Function));
      expect(scope.ok).toEqual(jasmine.any(Function));
      expect(scope.cancel).toEqual(jasmine.any(Function));
    });


    it('should load users into scope.ressource', function () {
      expect(scope.ressource[0]).toEqual(usersShortlist[0]);
      expect(scope.ressource[1]).toEqual(usersShortlist[1]);
    });

  });
}());