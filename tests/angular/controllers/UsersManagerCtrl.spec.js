(function () {
  'use strict';

  describe("UserManagerCtrl", function () {

    beforeEach(module('leanLogic.controllers'));
    beforeEach(module('restangular'));

    var UserManagerCtrl, $httpBackend, scope, rootScope, mainscope, testmode, UsersService, Restangular, i18n, $q, deferred, location, ConstantService, ModalEditorService, AlertService;

    beforeEach(inject(function ($controller, _$httpBackend_, _Restangular_, _$q_, _$rootScope_) {
      $httpBackend = _$httpBackend_;
      $q = _$q_;
      deferred = $q.defer();

      scope = _$rootScope_.$new();
      scope.userinfo = {userid: 49};
      rootScope = {
        appProperties: {}, activateNoticeButton: function () {
        }, session: {userinfo: {}}
      };
      ModalEditorService = {
        showTextLineEditor: function () {
          return {result: deferred.promise};
        }
      };
      mainscope = {};
      testmode = {};
      UsersService = {
        addGrantMethods: function () {
          return deferred.promise;
        },
        updateUserInfo: function () {
        }
      };
      Restangular = _Restangular_;
      i18n = {selectedLanguage: {}};
      location = {
        url: function () {
        }
      };
      AlertService = {
        createConfirmMessage: function () {
          return {
            show: function () {
              return {result: deferred.promise};
            }
          }
        }
      };

      UserManagerCtrl = $controller('UserManagerCtrl', {
        $scope: scope,
        $rootScope: rootScope,
        mainscope: mainscope,
        ConstantService: ConstantService,
        ModalEditorService: ModalEditorService,
        Restangular: Restangular,
        UsersService: UsersService,
        i18n: i18n,
        AlertService: AlertService,
        $modal: {},
        $stateParams: {},
        VersioningService: {},
        MediaService: {},
        $location: location,
        FileUploader: {},
        ListManagementService: {
          installListManagement: function () {
          }
        }
      });

      // not needed here
      //$httpBackend.flush();
    }));

    afterEach(function () {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should instantiate Controller', function () {
      expect(UserManagerCtrl).not.toBeNull();
    });

    it('should hide deleted groups by default', function () {
      expect(!!scope.showDeletedGroups).toEqual(false);
    });

    it('should show deleted groups if togled', function () {
      expect(scope.toggleShowDeletedGroups).toBeDefined();
      expect(scope.toggleShowDeletedGroups).toEqual(jasmine.any(Function));

      // toggle 'show deleted groups'
      scope.toggleShowDeletedGroups();
      expect(scope.showDeletedGroups).toBeDefined();
      expect(scope.showDeletedGroups).toEqual(true);
    });

    it('should ask before deleting group', function () {
      expect(scope.deleteGroup).toBeDefined();
      expect(scope.deleteGroup).toEqual(jasmine.any(Function));

      // install spy & call function
      spyOn(AlertService, 'createConfirmMessage').and.callThrough();
      scope.deleteGroup();

      expect(AlertService.createConfirmMessage).toHaveBeenCalled();
    });

    it('should set deleted flag to true if reactivating group', function () {
      expect(scope.reactivateGroup).toBeDefined();
      expect(scope.reactivateGroup).toEqual(jasmine.any(Function));

      spyOn(AlertService, 'createConfirmMessage').and.callThrough();
      scope.reactivateGroup();

      expect(AlertService.createConfirmMessage).toHaveBeenCalled();
    });

    it('should fire request on updating user', function () {
      expect(scope.updateUser).toBeDefined();
      expect(scope.updateUser).toEqual(jasmine.any(Function));

      // preparing some data for request
      var item = {};
      scope.seluser = {id: 1};

      // install spy
      spyOn(Restangular, 'all').and.callThrough();

      // expect POST for user update and a refresh
      $httpBackend.expectPOST('/users').respond({});
      $httpBackend.expectGET('/users/full').respond({});
      scope.updateUser(item);
      $httpBackend.flush();

      expect(Restangular.all).toHaveBeenCalled();
    });

    it('should select user', function () {
      expect(scope.selectUser).toBeDefined();
      expect(scope.selectUser).toEqual(jasmine.any(Function));

      var fakeUser = {id: 2};

      scope.selectUser(fakeUser);

      expect(scope.seluser).toBeDefined();
      expect(scope.seluser).toEqual(fakeUser);
      expect(scope.seluserid).toBeDefined();
      expect(scope.seluserid).toEqual(fakeUser.id);

      // should also set mode to 'user'
      expect(scope.selMode).toBeDefined();
      expect(scope.selMode).toEqual('user');
    });


    it('should select group', function () {
      expect(scope.selectGroup).toBeDefined();
      expect(scope.selectGroup).toEqual(jasmine.any(Function));

      var fakeGroup = {id: 2};

      scope.selectGroup(fakeGroup);

      expect(scope.selgroup).toBeDefined();
      expect(scope.selgroup).toEqual(fakeGroup);
      expect(scope.selgroupid).toBeDefined();
      expect(scope.selgroupid).toEqual(fakeGroup.id);

      // should also set mode to 'user'
      expect(scope.selMode).toBeDefined();
      expect(scope.selMode).toEqual('group');

      // additionally, groups should be shown
      expect(scope.selMode).toBeDefined();
      expect(scope.showGroups).toEqual(true);
    });

    it('should show deleted groups if togled', function () {
      expect(scope.toggleGroups).toBeDefined();
      expect(scope.toggleGroups).toEqual(jasmine.any(Function));

      // toggle 'show deleted groups'
      scope.toggleGroups();
      expect(scope.showGroups).toBeDefined();
      expect(scope.showGroups).toEqual(true);
    });

    it('should provide edit username function', function () {
      scope.seluser = {username: 'foo'};
      expect(scope.editUserUsername).toEqual(jasmine.any(Function));
      spyOn(ModalEditorService, 'showTextLineEditor').and.callThrough();

      scope.editUserUsername();

      expect(ModalEditorService.showTextLineEditor).toHaveBeenCalled();
    });

    it('should provide edit real name function', function () {
      scope.seluser = {username: 'bar'};
      expect(scope.editUserRealname).toEqual(jasmine.any(Function));
      spyOn(ModalEditorService, 'showTextLineEditor').and.callThrough();
      scope.editUserRealname();
      expect(ModalEditorService.showTextLineEditor).toHaveBeenCalled();
    });

    it('should provide edit user comment function', function () {
      scope.seluser = {username: 'foobar'};
      expect(scope.editUserComment).toEqual(jasmine.any(Function));
      spyOn(ModalEditorService, 'showTextLineEditor').and.callThrough();
      scope.editUserComment();
      expect(ModalEditorService.showTextLineEditor).toHaveBeenCalled();
    });

    it('should provide edit group name function', function () {
      scope.selgroup = {name: 'my-group'};
      expect(scope.editGroupName).toEqual(jasmine.any(Function));
      spyOn(ModalEditorService, 'showTextLineEditor').and.callThrough();
      scope.editGroupName();
      expect(ModalEditorService.showTextLineEditor).toHaveBeenCalled();
    });

    it('should provide edit group level function', function () {
      scope.selgroup = {name: 'my-group', level: 1};
      expect(scope.editGroupLevel).toEqual(jasmine.any(Function));
      spyOn(ModalEditorService, 'showTextLineEditor').and.callThrough();
      scope.editGroupLevel();
      expect(ModalEditorService.showTextLineEditor).toHaveBeenCalled();
    });

    it('should provide edit group description function', function () {
      scope.selgroup = {name: 'my-group', description: 'best-group-ever'};
      expect(scope.editGroupDescription).toEqual(jasmine.any(Function));
      spyOn(ModalEditorService, 'showTextLineEditor').and.callThrough();
      scope.editGroupDescription();
      expect(ModalEditorService.showTextLineEditor).toHaveBeenCalled();
    });

    it('should provide edit password function', function () {
      expect(scope.changePassword).toEqual(jasmine.any(Function));
      spyOn(ModalEditorService, 'showTextLineEditor').and.callThrough();
      scope.changePassword();
      expect(ModalEditorService.showTextLineEditor).toHaveBeenCalled();
    });

  });
}());