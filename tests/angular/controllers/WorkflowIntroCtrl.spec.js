(function () {
  'use strict';

  describe("WorkflowIntroCtrl", function () {

    beforeEach(module('leanLogic.controllers'));
    beforeEach(module('restangular'));

    var WorkflowIntroCtrl, $httpBackend, scope, rootScope, mainscope, testmode, UsersService, Restangular, i18n, $q, deferred, location, ConstantService;

    beforeEach(inject(function ($controller, _$httpBackend_, _Restangular_, _$q_, _$rootScope_) {
      $httpBackend = _$httpBackend_;
      $q = _$q_;
      deferred = $q.defer();

      scope = _$rootScope_.$new();
      scope.userinfo = {userid: 49};
      rootScope = {
        appProperties: {}, activateNoticeButton: function () {
        }, session: {userinfo: {}}
      };
      mainscope = {};
      testmode = {};
      UsersService = {
        addGrantMethods: function () {
          return deferred.promise;
        },
        updateUserInfo: function () {
        }
      };
      Restangular = _Restangular_;
      i18n = {selectedLanguage: {}};
      location = {
        url: function () {
        }
      };
      ConstantService = {
        installConstants: function (scope, C, arr) {
          // adding constants like this as they are generated from Ruby source in the application
          C.CHK_CDSTATUS_TODO = 0;
          C.CHK_CDSTATUS_PASSED = 10;
          C.CHK_CDSTATUS_WARNINGS = 13;
          C.CHK_CDSTATUS_FAILED = 15;
          C.CHK_CDSTATUS_OMITTED_HARD = 20;
          C.CHK_CDSTATUS_OMITTED_SOFT = 21;
          C.CHK_CDSTATUS_SKIPPED = 30;

          C.matchConst = function (status, code) {
          };
        }
      };

      // prepare mocked data
      var wflowData = getWflowMock();
      $httpBackend.whenGET('/workflow?userid=49').respond(wflowData); //? userid=49

      WorkflowIntroCtrl = $controller('WorkflowIntroCtrl', {
        $scope: scope,
        $rootScope: rootScope,
        mainscope: mainscope,
        ConstantService: ConstantService,
        ModalEditorService: {},
        Restangular: Restangular,
        UsersService: UsersService,
        i18n: i18n,
        AlertService: {},
        $modal: {},
        $stateParams: {},
        VersioningService: {},
        MediaService: {},
        $location: location
      });
    }));

    afterEach(function () {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should instantiate Controller', function () {
      expect(WorkflowIntroCtrl).not.toBeNull();
    });

    it('should set icon for step type instruction correctly', function () {
      var step = {
        id: 1,
        status: 20,
        steptype: 1
      };

      // get icon for step with type instruction
      var icon = scope.listStatusIcon(step);
      expect(icon).toEqual("fa-times-circle-o");
    });

    it('should initialize image options', function () {
      expect(scope.zoomableImageOptions).toBeDefined();
      expect(scope.zoomableImageOptions).toEqual(jasmine.any(Object));

      expect(scope.selImage).toBeDefined();
      expect(scope.selectedZoomImage).toBeDefined();
      expect(scope.selImageData).toBeDefined();
    });

    it('should count images', function () {
      // first a simple test with undefined check
      scope.imagelist = [];
      expect(scope.imageCount()).toEqual(0);

      // now with a check, but an empty list
      scope.check = {};
      expect(scope.imageCount()).toEqual(0);

      // now with some more useful data
      scope.imagelist = [{id: 1}, {id: 2}, {id: 3}];
      expect(scope.imageCount()).toEqual(3);
    });

    it('should redirect user to model / unit', function () {
      // preparation for calls
      scope.check = {model: {}, unit: {}};
      // install spy and call function
      spyOn(location, 'url');

      scope.gotoModel();
      expect(location.url).toHaveBeenCalled();

      scope.gotoUnit();
      expect(location.url).toHaveBeenCalled();
    });


    it('should calculate statistics', function () {
      // refresh data, mocked workflow data is defined
      scope.refreshData();
      $httpBackend.flush();

      expect(scope.statistics).toBeDefined();

      // check the calculation of stats

      // procedures
      var procTotal = scope.statistics.proc.todo + scope.statistics.proc.pass + scope.statistics.proc.fail + scope.statistics.proc.skip;
      expect(procTotal).toEqual(1);

      // steps
      var stepTotal = scope.statistics.step.todo + scope.statistics.step.pass + scope.statistics.step.fail + scope.statistics.step.skip;
      expect(stepTotal).toEqual(2);

      // instructions
      expect(scope.statistics.instruction).toEqual(1);

      // measures
      var measTotal = scope.statistics.meas.todo + scope.statistics.meas.pass + scope.statistics.meas.fail + scope.statistics.meas.skip;
      expect(measTotal).toEqual(3);
    });


    //  ------------  helper  ------------

    function getWflowMock() {
      return {
        "id": 353,
        "unit_id": 310,
        "model_id": 348,
        "checktype_id": 1,
        "comment": null,
        "scheduled": "2016-12-15",
        "dueby": "2016-12-30",
        "started": "2016-12-16",
        "finished": null,
        "status": 20,
        "metadata": {
          "testrun": {
            "type": "procedure",
            "item": "941"
          }
        },
        "checkdata": {
          "assignees": [
            {
              "regmode": 2
            }
          ],
          "procedures": [
            {
              "assignee": 0,
              "ap_id": 8990,
              "rid": 183,
              "id": 942,
              "status": 0,
              "code": "941",
              "committed": false,
              "steps": [
                {
                  "assignee": 0,
                  "id": 6718,
                  "status": 0,
                  "code": "6717",
                  "committed": null,
                  "measures": [
                    {
                      "id": 12461,
                      "mid": -1,
                      "status": 0,
                      "cstatus": 0,
                      "mtype": 3,
                      "code": "6717.12459",
                      "needstool": null,
                      "cmplcode": null,
                      "locked": 0,
                      "enforce": 0
                    },
                    {
                      "id": 12462,
                      "mid": -1,
                      "status": 0,
                      "cstatus": 0,
                      "mtype": 3,
                      "code": "6717.12460",
                      "needstool": null,
                      "cmplcode": null,
                      "locked": 0,
                      "enforce": 0
                    }
                  ],
                  "completem": false,
                  "completet": true,
                  "locked": 0,
                  "enforce": 0,
                  "steptype": 0
                },
                {
                  "assignee": 0,
                  "id": 6719,
                  "status": 10,
                  "code": "6715",
                  "committed": null,
                  "measures": [],
                  "completem": true,
                  "completet": true,
                  "locked": 0,
                  "enforce": 0,
                  "steptype": 1
                },
                {
                  "assignee": 0,
                  "id": 6720,
                  "status": 0,
                  "code": "6716",
                  "committed": null,
                  "measures": [
                    {
                      "id": 12463,
                      "mid": -1,
                      "status": 0,
                      "cstatus": 0,
                      "mtype": 3,
                      "code": "6716.12458",
                      "needstool": null,
                      "cmplcode": null,
                      "locked": 0,
                      "enforce": 0
                    }
                  ],
                  "completem": false,
                  "completet": true,
                  "locked": 0,
                  "enforce": 0,
                  "steptype": 0
                }
              ],
              "locked": 0,
              "enforce": 0
            }
          ],
          "procids": [
            942
          ],
          "stepids": [
            6718,
            6719,
            6720
          ],
          "measids": [
            12461,
            12462,
            12463
          ],
          "dependencies": {}
        },
        "created_at": "2016-12-16 09:31:37 UTC",
        "updated_at": "2016-12-16 09:31:37 UTC",
        "created_by": "49",
        "updated_by": "49",
        "images": [],
        "documents": [],
        "unit": {
          "id": 310,
          "model_id": 348,
          "code": "ADI(TR)",
          "customer": "TESTCUSTOMER",
          "comment": null,
          "commissioned": null,
          "finished": null,
          "delivered": null,
          "approved": null,
          "status": 0,
          "metadata": {},
          "created_at": "2016-12-16 09:31:37 UTC",
          "updated_at": "2016-12-16 09:31:37 UTC",
          "created_by": "49",
          "updated_by": "49"
        },
        "model": {
          "id": 348,
          "devicetype_id": 1,
          "code": "ADI(TR)",
          "title": {
            "de": "TESTMODEL (de)"
          },
          "description": {
            "de": ""
          },
          "metadata": {},
          "realid": 45,
          "version": null,
          "disabled": false,
          "created_at": "2016-12-16 09:31:37 UTC",
          "updated_at": "2016-12-16 09:31:37 UTC",
          "created_by": "49",
          "updated_by": "49",
          "devicetype": {
            "id": 1,
            "code": "01",
            "title": {
              "de": "Fräsmaschine"
            },
            "description": {
              "de": ""
            },
            "metadata": null,
            "deleted": false,
            "created_at": "2014-12-01 07:07:06 UTC",
            "updated_at": "2014-12-08 14:30:36 UTC",
            "created_by": "9",
            "updated_by": "10",
            "images": [],
            "documents": []
          },
          "images": [],
          "documents": []
        },
        "checktype": {
          "id": 1,
          "code": "CH001",
          "title": {
            "de": "Produktionsprozess"
          },
          "description": {
            "de": ""
          },
          "metadata": {},
          "deleted": false,
          "created_at": "2014-12-01 07:09:49 UTC",
          "updated_at": "2014-12-01 07:09:49 UTC",
          "created_by": "9",
          "updated_by": "9"
        },
        "procedures": {
          "942": {
            "code": "941",
            "title": {
              "de": "LLQA-135 Schritt Typ Anleitung"
            }
          }
        },
        "steps": {
          "6718": {
            "code": "6717",
            "title": {
              "de": "Normaler Schritt"
            }
          },
          "6719": {
            "code": "6715",
            "title": {
              "de": "Anleitungs-Schritt"
            }
          },
          "6720": {
            "code": "6716",
            "title": {
              "de": "Testschritt2"
            }
          }
        },
        "measures": {
          "12461": {
            "code": "12459",
            "title": {
              "de": "Messung1"
            }
          },
          "12462": {
            "code": "12460",
            "title": {
              "de": "Messung2"
            }
          },
          "12463": {
            "code": "12458",
            "title": {
              "de": "teasd"
            }
          }
        },
        "users": {},
        "groups": {},
        "timing": {
          "tooearly": false,
          "toolate": false
        },
        "nextstep": null,
        "newerversions": [],
        "configtable_id": null
      }
    };

  });
}());