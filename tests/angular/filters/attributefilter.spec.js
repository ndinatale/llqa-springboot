(function () {
  'use strict';

  describe("attributefilter", function () {

    var filter;
    var elements;
    var searchtext;
    var attributes;
    var filteredList;

    beforeEach(function () {
      module('leanLogic.filters');

      inject(function (_$filter_) {
        filter = _$filter_;
      });
    });

    it('should return no elements on empty attributes to filter', function () {
      elements = [{id: 1, code: "abc"}, {id: 2, code: "ab"}, {id:3, code: "bc"}];
      searchtext = 'abc';
      attributes = null;

      filteredList = filter('attributefilter')(elements, attributes, searchtext);
      expect(filteredList.length).toEqual(0);
    });

    it('should filter array on correct attribute', function () {
      elements = [{id: 1, code: "abc"}, {id: 2, code: "ab"}, {id:3, code: "bc"}];
      searchtext = 'abc';
      attributes = ['code'];

      filteredList = filter('attributefilter')(elements, attributes, searchtext);
      expect(filteredList.length).toEqual(1);
    });

    it('should filter array only by code, not other attributes', function () {
      elements = [{id: 1, code: "abc"}, {id: 2, code: "ab", foo: "abc"}, {id:3, code: "bc", bar: "abc"}];
      searchtext = 'abc';
      attributes = ['code'];

      filteredList = filter('attributefilter')(elements, attributes, searchtext);
      expect(filteredList.length).toEqual(1);
    });

    it('should filter array without duplicating elements', function () {
      elements = [{id: "abc", code: "abc", foo: "abc", bar: "abc"}];
      searchtext = 'abc';
      attributes = ['id', 'code', 'foo', 'bar'];

      filteredList = filter('attributefilter')(elements, attributes, searchtext);
      expect(filteredList.length).toEqual(1);
    });

    it('should handle empty objects', function () {
      elements = [{id: "abc", code: "abc"}, {}, 0, 1];
      searchtext = 'abc';
      attributes = ['code'];

      filteredList = filter('attributefilter')(elements, attributes, searchtext);
      expect(filteredList.length).toEqual(1);
    });

    it('should handle mixed text', function () {
      elements = [{id: 1, code: "abc"}, {id: 2, code: "86tgj8t&%87igba"}, {id: 3, code: "aaaaaaaaaaaaa"}, {id: 4, code: "kuggfa"}];
      searchtext = 'a';
      attributes = ['code'];

      filteredList = filter('attributefilter')(elements, attributes, searchtext);
      expect(filteredList.length).toEqual(4);
    });

    it('should handle numbers', function () {
      elements = [{id: 1}, {id: 2}, {id: 3}, {id: 4}];
      searchtext = '2';
      attributes = ['id'];

      filteredList = filter('attributefilter')(elements, attributes, searchtext);
      expect(filteredList.length).toEqual(1);
    });

    it('should handle capitalization', function () {
      elements = [{code: '123A'}, {code: 'AB'}, {code: 'CDAEF'}, {code: 'AAG'}, {code: 'Ä'}, {code: ''}];
      searchtext = 'a';
      attributes = ['code'];

      filteredList = filter('attributefilter')(elements, attributes, searchtext);
      expect(filteredList.length).toEqual(4);
    });

    it('should handle translations', function () {
      elements = [{id: 1, title: { de: 'abc', en: '123', fr: '789'}}, {id: 2, title: {en: 'abc1'}}, {id: 3, title: {en: 'abc2'}}];
      searchtext = 'abc';
      attributes = ['title'];
      var lang = 'de';

      filteredList = filter('attributefilter')(elements, attributes, searchtext, lang);
      expect(filteredList.length).toEqual(1);

      // now another language
      lang = 'en';
      filteredList = filter('attributefilter')(elements, attributes, searchtext, lang);
      expect(filteredList.length).toEqual(2);
    });

    it('should handle missing translations on object', function () {
      // element without german translation
      elements = [{id: 2, title: {en: '123', fr: '789'}}];
      searchtext = 'abc';
      attributes = ['code'];
      var lang = 'de';

      filteredList = filter('attributefilter')(elements, attributes, searchtext, lang);
      expect(filteredList.length).toEqual(0);
    });

  });
}());