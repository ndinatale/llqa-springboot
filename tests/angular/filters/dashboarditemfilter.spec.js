(function () {
    'use strict';

    describe("dashboarditemfilter", function () {

        var filter;
        var items;
        var filteredList;

        beforeEach(function () {
            module('leanLogic.filters');

            inject(function (_$filter_) {
                filter = _$filter_;
            });
        });

        it('should return all elements', function () {
            items = [{id: 1, code: "abcde"}, {id: 2, code: "edcba"}, {id: 3, code: "aebcd"}];

            filteredList = filter('dashboarditemfilter')(items);
            expect(filteredList.length).toEqual(3);
        });

        it('should filter out the code with the empty parentheses at the end', function () {
            items = [{id: 1, code: "abcde"}, {id: 2, code: "edcba()"}, {id: 3, code: "aebcd"}, {
                id: 4,
                code: "abcde()"
            }];

            filteredList = filter('dashboarditemfilter')(items);
            expect(filteredList.length).toEqual(2);
        });

        it('should filter out the code with the "TR"-filled parentheses at the end', function () {
            items = [{id: 1, code: "abcde"}, {id: 2, code: "edcba(TR)"}, {id: 3, code: "aebcd"}, {
                id: 4,
                code: "abcde(TR)"
            }];

            filteredList = filter('dashboarditemfilter')(items);
            expect(filteredList.length).toEqual(2);
        });

        it('should filter out the code with the empty and the "TR"-filled parentheses at the end', function () {
            items = [{id: 1, code: "abcde"}, {id: 2, code: "edcba()"}, {id: 3, code: "aebcd"}, {
                id: 4,
                code: "abcde(TR)"
            }];

            filteredList = filter('dashboarditemfilter')(items);
            expect(filteredList.length).toEqual(2);
        });

        it('should NOT filter out the code with the empty or "TR"-filled parentheses which are not at the end', function () {
            items = [{id: 1, code: "abc()de"}, {id: 2, code: "edcba()"}, {id: 3, code: "aeb(TR)cd"}, {
                id: 4,
                code: "abcde(TR)"
            }, {id: 4, code: "abcde"}, {id: 4, code: "abcde"}];

            filteredList = filter('dashboarditemfilter')(items);
            expect(filteredList.length).toEqual(4);
        });

        it('should NOT filter out the code with the parentheses which are not empty and not filled with "TR"', function () {
            items = [{id: 1, code: "abcde"}, {id: 2, code: "edcba(AB)"}, {id: 3, code: "aebcd"}, {
                id: 4,
                code: "abcde(CD)"
            }, {id: 5, code: "abcde(TR)"}, {id: 6, code: "abcde()"}];

            filteredList = filter('dashboarditemfilter')(items);
            expect(filteredList.length).toEqual(4);
        });

    });
}());