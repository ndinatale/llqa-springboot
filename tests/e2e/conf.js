exports.config = {
    directConnect: true,
    baseUrl: 'http://localhost:4567',
    framework: 'jasmine2',
    specs: ['specs/general/*spec.js',
            'specs/configtable/*spec.js'],

    capabilities: {
        'browserName': 'chrome'
    },

    jasmineNodeOpts: {
        defaultTimeoutInterval: 60000
    }
};
