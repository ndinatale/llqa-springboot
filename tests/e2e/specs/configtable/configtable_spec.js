describe('Configtable', function () {
  it('should startup and show some content on header', function () {
    browser.get('/app/configmgt');

    var firstListEntry = element.all(by.repeater('table in items')).first();
    firstListEntry.click();

    var tableEntries = element.all(by.repeater('entry in entries'));
    expect(tableEntries.first().getText()).toContain('Zylinder');
  });

  it('should remove entry if delete button clicked', function () {
    var tableEntries = element.all(by.repeater('entry in entries'));
    expect(tableEntries.count()).toEqual(9);

    // remove first entry
    tableEntries.first().all(by.css('span')).get(1).click();

    // confirm deleting
    element(by.buttonText('Ja')).click();

    expect(tableEntries.first().isDisplayed()).toBeFalsy();
  });

  it('should show deleted elements if clicked on eye', function () {
    // click second eye, revealing deleted entries
    var eyes = element.all(by.css('.fa-eye'));
    eyes.get(1).click();

    // expect one deleted entry (with 2x class ll-disabled)
    var deletedEntries = element.all(by.css('.ll-disabled'));
    expect(deletedEntries.count()).toEqual(2);
  });

  it('should succesfully rename table', function () {
    var eyes = element.all(by.css('.ll-nbaction'));
    eyes.get(0).click();

    // renaming table to some random string
    var name = 'Table ' + Math.round(Math.random()*1000);

    var title = element(by.model('item.title'));
    title.clear();
    title.sendKeys(name);
    element(by.partialButtonText('Speichern')).click();

    expect(element.all(by.css('.ll-fullheight')).first().getText()).toContain(name);
  });

});
