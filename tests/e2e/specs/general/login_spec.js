describe('Login', function () {
  it('should open login form', function () {
    browser.get('/');

    var loginform = element(by.css('body > div.ll-topview.ng-scope > div > div > form'));
    expect(loginform.getText()).toContain('Login');
  });

  it('should login user and show application', function () {
    element(by.model('logininfo.username')).sendKeys('adi');
    element(by.model('logininfo.password')).sendKeys('adi');

    var btn = element(by.css('body > div.ll-topview.ng-scope > div > div > form > div.flexcnt-r.flexitem-0 > button'));
    btn.click();

    var frame = element(by.css('body > div.ll-topview.ng-scope > div > div > div.flexvcnt.ll-fullheight'));
    expect(frame.getText()).toContain('Dashboard');

    var navbar = element(by.css('body > nav'));
    expect(navbar.getText()).toContain('LeanLogic QA');
    expect(navbar.getText()).toContain('Dashboard');
    expect(navbar.getText()).toContain('Logout');
  });
});
