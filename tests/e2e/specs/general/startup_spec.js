describe('Application', function () {
  it('should startup and show LeanLogic header', function () {
    //browser.manage().window().maximize(); // not working correctly on Mac
    browser.get('/');

    var header = element(by.css('body > nav > div > span > span.ng-scope'));
    expect(header.getText()).toContain('LeanLogic QA');
  });
});
