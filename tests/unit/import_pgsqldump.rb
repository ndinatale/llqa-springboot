def readdump(dumpfile)
  datahash = {}
  curcopy = nil
  File.foreach(dumpfile) do |line|
    line.chomp!
    if line =~ /\ACOPY ([^ ]+) \(([^\)]+)\) FROM stdin;/
      if curcopy
        puts "Found COPY (#{$1}) although copy still running"
        exit
      end
      datahash[$1] = {}
      curcopy = [datahash[$1],$2.split(', ')]
      next
    end
    if line =~ /\A\\\.\z/
      if !curcopy
        puts "Found COPY-End although copy not running"
        exit
      end
      curcopy = nil
    end
    if curcopy
      data = line.split("\t",-1)
      if (data.size != curcopy[1].size)
        puts "Found line with data size not matching current copy op"
        p curcopy
        p line
        p data
        exit
      end
      obj = {}
      curcopy[1].each_with_index { |k,i| obj[k] = data[i] }
      curcopy[0][obj['id']] = obj
    end
  end
  datahash
end

def inserttexthashes(dumphash)
  dumphash['texts'].each do |k,v|
    obj,col = v['text_type'].split('.')
    next if !dumphash[obj][v['text_id']]
    dumphash[obj][v['text_id']][col] = {} if !dumphash[obj][v['text_id']][col]
    dumphash[obj][v['text_id']][col][v['language']] = v['text'].gsub('\n',"\n").gsub('\t',"\t")
    dumphash[obj][v['text_id']][col][v['language']] += '.' while (dumphash[obj][v['text_id']][col][v['language']].length<3)
  end
end

def category(dh,cat,sorted = false)
  data = dh[cat].to_a
  data = data.sort { |a,b| a[1]['sequence'].to_i <=> b[1]['sequence'].to_i }
  data.each { |v| yield(v[0],v[1]) }
end

def import_pgsql(file)
  Measure.delete_all
  Step.delete_all
  Procedure.delete_all
  dh = readdump(file)
  inserttexthashes(dh)
  stddesc = { 'de' => '', 'en' => '', 'cn' => '', 'fr' => '' }
  pcnt = 0
  category(dh,'procedures',false) do |pid,pdata|
    pcnt += 1
    ret,res,procedure = Procedure.merge_data({ code: sprintf('IP%02d',pcnt), title: pdata['name'], description: stddesc, metadata: {}, flowcontrol: [] })
    begin puts ret[1]; exit; end if !res
    npid = procedure.id
    printf("PROCEDURE  %05d -> %02d\n",pid.to_i,npid)
    scnt = 0
    category(dh,'steps',true) do |sid,sdata|
      next if sdata['procedure_id'] != pid
      scnt += 1
      ret,res,step = Step.merge_data({ procedure_id: npid, code: sprintf('IS%02d',scnt), title: sdata['name'], description: sdata['description'], metadata: {}, flowcontrol: [] })
      begin puts ret[1]; exit; end if !res
      nsid = step.id
      printf("  STEP      %05d -> %02d  %s\n",sid.to_i,nsid,sdata['sequence'])
      mcnt = 0
      category(dh,'measures',true) do |mid,mdata|
        next if mdata['step_id'] != sid
        mcnt += 1
        mtype,mcalc = nil,{}
        otype = mdata['type'].to_i
        case otype
          when 1,5
            mtype = 1
            mcalc = { 't1_targetv' => mdata['measurand'].to_f, 't1_tholdv' => mdata['threshold'].to_f, 't1_targetu' => mdata['measurand_unit'], 't1_tholdu' => mdata['threshold_unit'] }
          when 2,6
            mtype = 2
            tgt,thld = mdata['measurand'].to_f,mdata['threshold'].to_f
            mcalc = { 't2_minv' => tgt-thld, 't2_maxv' => tgt+thld, 't2_unit' => mdata['measurand_unit'] }
          when 3
            mtype = 5
            mcalc = { 't5_expected' => 1 }
          when 4,7
            mtype = 3
            mcalc = { 't3_minlen' => 1 }
        end
        mcalc['optional'] = (otype > 4 ? 1 : 0)
        ret,res,measure = Measure.merge_data({ step_id: nsid, code: sprintf('IM%02d',mcnt), tooltype_id: nil, title: mdata['name'], description: stddesc, measuretype: mtype, calculation: mcalc, metadata: {}, flowcontrol: [] })
        begin puts ret[1]; exit; end if !res
        nmid = measure.id
        printf("    MEASURE  %05d -> %02d   %s\n",mid.to_i,nmid,mdata['sequence'])
      end
      dh['images'].each do |id,idata|
        next if idata['image_type'] != 'step'
        next if idata['image_id'] != sid
        img = idata['image'].gsub('\\\\x','').scan(/../).map { |x| x.hex.chr }.join
        Image.importImage(img,{ owner:'Step', ownerid:nsid}, 'Dataimport',((idata['type'].end_with? 'jpeg') ? 'jpg' : 'png'),idata['type'])
        printf("    IMAGE    %05d\n",id.to_i)
      end
    end
    procedure.finalize_trunk
  end
end

__END__

dh = readdump('../temp/db_dump_20140821.sql')
cntr = Hash.new(0)
dh['images'].each do |id,data|
  s = data['image'].gsub('\\\\x','')
  d = s.scan(/../).map { |x| x.hex.chr }.join
  #File.open("test#{id}.jpg","wb") { |f| f.print d }
  cntr[data['image_type']] += 1
  puts "#{id} - #{data['size']} - #{d.size} - #{data['type']}"
end
p cntr


