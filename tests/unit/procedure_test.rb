require './test/test_helper'
require 'active_record'

class ProcedureTest < ActiveSupport::TestCase
    test "first test should work" do
        assert_equal 42, 42
    end

    test "saving a procedure without title should fail" do
        procedure = Procedure.new
        assert_not procedure.save, "Saved the procedure without a title"
    end

    test "saving a procedure with proper title in all languages" do
        procedure = Procedure.new
        procedure['title']['en'] = 'foo'
        procedure['title']['de'] = 'bar'
        procedure['title']['cn'] = 'foobar'
        procedure['title']['fr'] = 'barfoo'
        procedure['code'] = '123'
        procedure['realid'] = '123'

        assert procedure.save, "Saving with a title failed"
    end

end
