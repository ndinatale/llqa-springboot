require 'benchmark'

$testmode = true
require_relative '../server'

require_relative 'testfuns'

begin
  # tooltypes, toolunits_per_type
  seed_demodata_tools(5, (5..10))
  # procedures, steps_per_procedure, measures_per_step, finalizations_per_procedure
  seed_demodata_procedures(5, (5..7), (2..5), (1..2))
  # devicetypes, models_per_devicetype, finalizations_per_model, units_per_model, checktypes, perc_checktype_in_unit, perc_procedure_in_unit
  seed_demodata_devices(3, (2..5), (1..2), (5..10), 5, 40, 60)

  seed_demodata_users

  # tooltypes, toolunits_per_type
  #seed_demodata_tools(3, 3)
  # procedures, steps_per_procedure, measures_per_step, finalizations_per_procedure
  #seed_demodata_procedures(3, 3, 6, 1)
  #proc,step = Procedure.trunk_object(1),nil
  #proc.steps.each { |i| step = i if i.code == 'STP0102' }
  #step.flowcontrol.push({ 'type' => 'M', 'trigger' => 'Yes', 'code' => 'STP0101.MSR010102', 'res' => 'skip', 'inverse' => false })
  #step.save!
  #proc.steps.each { |i| step = i if i.code == 'STP0103' }
  #step.flowcontrol.push({ 'type' => 'M', 'trigger' => 'No', 'code' => 'STP0101.MSR010102', 'res' => 'skip', 'inverse' => false })
  #step.save!
  #proc.finalize_trunk
  # devicetypes, models_per_devicetype, finalizations_per_model, units_per_model, checktypes, perc_checktype_in_unit, perc_procedure_in_unit
  #seed_demodata_devices(3, 1, 1, 1, 5, 100, 100)

  #import_pgsql('temp/db_dump_20140821.sql')
end


exit


# type trigger   parameters
#  CT   Select    code, inverse
#  U    Set       key, inverse
#  AP   Set       key, inverse
#  MD   Select    code, inverse
#  DT   Select    code, inverse
#  P    InQueue   code, inverse
#  P    P/F/S/O   code, res, inverse (S/O only)
#  S    P/F/S/O   code, res, inverse (S/O only)
#  M    P/F/S/O   code, res, inverse (S/O only)
#  M    Yes/No    code, res