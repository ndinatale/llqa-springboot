require 'benchmark'
require_relative '../lib/routes_dashboard'

$testmode = true
require_relative '../server'

require_relative 'testfuns'

def temp(data, max)

end

begin
  proc_rid = 1
  params = {"lang"=>"de", "tzoff"=>"-120", "showv"=>"1", "showadm"=>"1", "showcomm"=>"1", "showraw"=>"1", "showtool"=>"1", "showrule"=>"1", "customerx"=>"1", "splat"=>[], "captures"=>["1"], "id"=>"1"}
  pdf = CheckReport.gen_pdf(proc_rid, params)
  File.open('test.pdf', 'w') { |f| f.print pdf }
  system 'open test.pdf'
end if nil

begin
  chk = Check.find(405)
  p chk.get_current_and_next_step(nil,10)
end

exit

__END__

      t.string :text
      t.references :category, null: false
      t.text :path
      t.string :artno
      t.string :artdesc
      t.integer :timeloss
      t.integer :status # 1 = offen, 2 = in Bearbeitung, 5 = geschlossen, 9 = archiviert
      t.text :timeline
