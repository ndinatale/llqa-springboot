require 'minitest/autorun'
require 'active_support'
require 'active_record'
require './lib/models_base.rb'
require './lib/models_versioning.rb'
require './lib/models_searching.rb'
require './lib/models_validators.rb'
require './models/proc_procedure.rb'
require './lib/utils.rb'

db_options = {adapter: 'sqlite3', database: 'db/db_dev_db.sqlite3'}
ActiveRecord::Base.establish_connection(db_options)

# some more constants
$system_languages = [
    {code: 'en', name: 'English', dformat: 'MM/dd/yyyy', tformat: 'MM/dd/yyyy hh:mm:ss a', dformatr: '%m/%d/%Y', tformatr: '%m/%d/%Y %I:%M:%S %p' },
    {code: 'de', name: 'Deutsch', dformat: 'dd.MM.yyyy', tformat: 'dd.MM.yyyy HH:mm:ss',   dformatr: '%d.%m.%Y', tformatr: '%d.%m.%Y %H:%M:%S'    },
    {code: 'cn', name: '中文',     dformat: 'MM/dd/yyyy', tformat: 'MM/dd/yyyy hh:mm:ss a', dformatr: '%m/%d/%Y', tformatr: '%m/%d/%Y %I:%M:%S %p' },
    {code: 'fr', name: 'Français', dformat: 'dd.MM.yyyy', tformat: 'dd.MM.yyyy HH:mm:ss',   dformatr: '%d.%m.%Y', tformatr: '%d.%m.%Y %H:%M:%S'    },
]
$debugging_api = true
$expiry_time = 3600
