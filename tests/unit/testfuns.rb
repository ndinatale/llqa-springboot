class Range
  def rndval
    return self.first if self.size < 1
    return rand(self.size)+self.min
  end
end
class Integer
  def rndval
    return self
  end
end
class Array
  def rndval
    return 0 if self.size == 0
    return self[rand(self.size)]
  end
end

def gen_code_and_title(syl,en,de,cn,fr,data)
  code = "#{syl}#{'%02d' * data.size}" % data
  title = {'en' => "#{en} #{('%02d.' * data.size)[0..-2]}" % data, 'de' => "#{de} #{('%02d.' * data.size)[0..-2]}" % data, 'cn' => "#{cn} #{('%02d.' * data.size)[0..-2]}" % data,  'fr' => "#{fr} #{('%02d.' * data.size)[0..-2]}" % data}
  return code,title
end
$stddesc = {'en' => 'Description', 'de' => 'Beschreibung', 'cn' => '???', 'fr' => 'Description'}

def seed_demodata_procedures(numproc, numsteps, nummeasures, numprocfin)
  Measure.delete_all
  Step.delete_all
  Procedure.delete_all
  tooltypes = Tooltype.all.to_a.map { |obj| obj.id }
  numproc.rndval.times do |pi|
    proccode,proctitle = gen_code_and_title('PRC','Procedure','Prozedur','(CN)Proc','Procédure',[pi+1])
    _,_,procedure = Procedure.merge_data({ code: proccode, title: proctitle, metadata: {}, modifiers: {}, description: $stddesc })
    proc_id,proc_rid = procedure.id,procedure.realid
    printf("Created     PRC ID: %5d, RID: %3d = %s\n",proc_id,proc_rid,proccode)
    numsteps.rndval.times do |si|
      stepcode,steptitle = gen_code_and_title('STP','Step','Schritt','(CN)Step','Étape',[pi+1,si+1])
      stepdsc = $stddesc
      _,_,step = Step.merge_data({ procedure_id: proc_id, code: stepcode, title: steptitle, description: stepdsc, metadata: {}, modifiers: {} })
      step_id = step.id
      printf("  Created   STP ID: %5d = %s\n",step_id,stepcode)
      nummeasures.rndval.times do |mi|
        msrcode,msrtitle = gen_code_and_title('MSR','Measure','Messung','(CN)Msr','Mesures',[pi+1,si+1,mi+1])
        msrdesc,msrtype,msrtarget,msrthresh,msrttype = $stddesc, 1, 1.0*mi, 0.1*mi, (rand(100)<30 ? nil : tooltypes[rand(tooltypes.size)])
        msrcalc = { 'optional' => 0, 't1_targetv' => msrtarget, 't1_tholdv' => msrthresh, 't1_targetu' => 'µm', 't1_tholdu' => 'µm' }
        a,b,measure = Measure.merge_data({ step_id: step_id, tooltype: nil, code: msrcode, tooltype_id: msrttype, title: msrtitle, description: msrdesc, measuretype: msrtype, calculation: msrcalc, metadata: {}, modifiers: {} })
        msr_id = measure.id
        printf("    Created MSR ID: %5d = %s\n",msr_id,msrcode)
      end
    end
    numprocfin.rndval.times do |pif|
      proc_id = procedure.id
      procedure = procedure.finalize_trunk
      puts('    (Finalized)')
    end
  end
end

def seed_demodata_tools(numttype, numtunit)
  Measure.delete_all
  Toolunit.delete_all
  Tooltype.delete_all
  numttype.rndval.times do |tti|
    typecode,typetitle = gen_code_and_title('TTY','Tooltype','Werkzeugtyp','(CN)TType','Type d\'outil',[tti+1])
    _,_,tooltype = Tooltype.merge_data({ code: typecode, title: typetitle, description: $stddesc, deleted: false })
    tty_id = tooltype.id
    printf("Created     TTY ID: %5d = %s\n",tty_id,typecode)
    numtunit.rndval.times do |tui|
      unitcode,unittitle = gen_code_and_title('TUN','Tool','Werkzeug','(CN)Tool','Outil',[tti+1,tui+1])
      _,_,toolunit = Toolunit.merge_data({ tooltype_id: tty_id, code: unitcode, comment: 'comment', deleted: false, disabled: false })
      unit_id = toolunit.id
      printf("  Created   TUN ID: %5d = %s\n",unit_id,unitcode)
    end
  end
end

def seed_demodata_devices(numdev, nummodels, nummodfin, numunits, numctype, percactproc, percactctype)
  Activeprocedure.delete_all
  Activechecktype.delete_all
  Checktype.delete_all
  Unit.delete_all
  Model.delete_all
  Devicetype.delete_all
  numctype.rndval.times do |cti|
    ctypcode,ctyptitle = gen_code_and_title('CTY','Checktype','Checktyp','(CN)CTyp', 'Type de contrôle',[cti+1])
    _,_,ctyp = Checktype.merge_data({ code: ctypcode, title: ctyptitle, description: $stddesc })
    ctyp_id = ctyp.id
    printf("Created     CTY ID: %5d = %s\n",ctyp_id,ctypcode)
  end
  checktypes = Checktype.all.to_a.map { |obj| obj.id }
  procedures = Procedure.latest_version_objects.to_a.map { |obj| obj.id }
  numdev.rndval.times do |dti|
    dtypcode,dtyptitle = gen_code_and_title('DTY','Devicetype','Gerätetyp','(CN)DTyp','Type de périphérique',[dti+1])
    _,_,dtyp = Devicetype.merge_data({ code: dtypcode, title: dtyptitle, description: $stddesc })
    dtyp_id = dtyp.id
    printf("Created     DTY ID: %5d = %s\n",dtyp_id,dtypcode)
    nummodels.rndval.times do |mi|
      modcode,modtitle = gen_code_and_title('MOD','Model','Modell','(CN)Mod', 'Modèle',[dti+1,mi+1])
      _,_,model = Model.merge_data({ devicetype_id: dtyp_id, code: modcode, title: modtitle, description: $stddesc })
      mod_id = model.id
      checktypes.each { |ctyp_id| Activechecktype.merge_data({ model_id: mod_id, checktype_id: ctyp_id }) if percactctype > rand(100) }
      procedures.each { |proc_id| Activeprocedure.merge_data({ model_id: mod_id, procedure_id: proc_id }) if percactproc > rand(100) }
      printf("  Created   MOD ID: %5d = %s\n",mod_id,modcode)
      [1,nummodfin.rndval].max.times do |mif|
        mod_id = model.id
        model = model.finalize_trunk
        puts('    (Finalized)')
      end
      numunits.rndval.times do |ui|
        unitcode,unitcmnt = gen_code_and_title('UNT','Unit','Einheit','(CN)Unit', 'Unité',[dti+1,mi+1,ui+1])
        _,_,unit = Unit.merge_data({ model_id: mod_id, code: unitcode, customer: 'ACME', status: 1, comment: 'Comment' })
        unit_id = unit.id
        printf("    Created UNT ID: %5d = %s\n",unit_id,unitcode)
      end
    end
  end

end

def seed_demodata_users
  groups = []
  groups << [  0, Usergroup.create(name: 'Guests', level: 1, description: 'Guests')]
  Grant.add_grants_by_name(groups.last[1],%w())
  groups << [  2, Usergroup.create(name: 'Users1', level: 2, description: 'Employees Site A')]
  Grant.add_grants_by_name(groups.last[1],%w(MNGTOL CRTTOL WFLREG))
  groups << [ 31, Usergroup.create(name: 'Users2', level: 2, description: 'Employees Site B')]
  Grant.add_grants_by_name(groups.last[1],%w(MNGTOL CRTTOL WFLREG))
  groups << [ 60, Usergroup.create(name: 'Users3', level: 2, description: 'Employees Site C')]
  Grant.add_grants_by_name(groups.last[1],%w(MNGTOL CRTTOL WFLREG))
  groups << [ 89, Usergroup.create(name: 'Supervisors', level: 3, description: 'Supervisors')]
  Grant.add_grants_by_name(groups.last[1],%w(MNGALL CRTTOL DELTOL USRMGO WFLMNG WFLREG))
  groups << [ 95, Usergroup.create(name: 'Editors', level: 3, description: 'Editors')]
  Grant.add_grants_by_name(groups.last[1],%w(MNGALL EDTALL CRTALL WFLREG))
  groups << [ 99, Usergroup.create(name: 'Managers', level: 4, description: 'Managers')]
  Grant.add_grants_by_name(groups.last[1],%w(MNGALL EDTALL FINALZ CRTALL DELALL USRMGO GRTPRO WFLMNG WFLREG))
  [["Mr.", "Zelim", "A", "Rushisvili", "Evichad"], ["Mr.", "Kadosa", "P", "Gombos", "Sawran"], ["Mrs.", "Laura", "P", "Goncalves", "Obtainted"], ["Mr.", "Gaudenzio", "R", "Esposito", "Beyound"], ["Dr.", "Gertrudis", "W", "Pomp", "Fincen"], ["Mr.", "Sam", "K", "Sheridan", "Whoser"], ["Mrs.", "Isabel", "A", "Sinclair", "Suicannout"], ["Mr.", "Sandro", "E", "Zobavnik", "Thingaing"], ["Mr.", "Parfait", "B", "Dostie", "Contery"], ["Ms.", "Ebelegbulam", "A", "Ekwueme", "Muccommus"], ["Ms.", "Amelia", "J", "Gibson", "Upprow"], ["Ms.", "Jessika", "M", "Schulze", "Anceent"], ["Mrs.", "Brankica", "V", "Jurić", "Noured"], ["Ms.", "Flora", "I", "Bruno", "Prinaces"], ["Ms.", "Radoslava", "J", "Revinšek", "Eandoins"], ["Mr.", "Olkhazar", "T", "Timayev", "Lairger"], ["Mrs.", "W'Sira", "D", "Paghal", "Liumber"], ["Mr.", "Guo", "M", "She", "Lostremew"], ["Mr.", "Gundolpho", "Y", "Took-Took", "Decough"], ["Ms.", "Alex", "D", "Stel", "Woultries"], ["Ms.", "Marie", "J", "Kollerová", "Nesight"], ["Mr.", "Shing", "N", "Liang", "Youstwou"], ["Mrs.", "Margrét", "Ó", "Sigmarsdóttir", "Thournes"], ["Ms.", "Ramóna", "Z", "Szalai", "Friesess"], ["Ms.", "Belladonna", "W", "Gawkroger", "Boodgme"], ["Mr.", "Tsuyoshi", "K", "Kishimoto", "Theethem"], ["Mrs.", "Ivette", "O", "Rosenbrand", "Begaings"], ["Mrs.", "Мстислава", "М", "Соломина", "Uposing"], ["Mr.", "Edgar", "M", "Ignatiev", "Decough"], ["Mrs.", "Kayleigh", "B", "Ford", "Liticest"], ["Mr.", "Alan", "S", "Persson", "Nossorend"], ["Mr.", "Damian", "A", "Marshall", "Ginsiou"], ["Ms.", "Naru", "K", "Ogino", "Butever"], ["Mr.", "Kasper", "G", "Jabłoński", "Undest"], ["Mr.", "Teppo", "N", "Piirainen", "Withents"], ["Mrs.", "Estella", "B", "Hayward", "Reaterem"], ["Mr.", "Younes", "G", "Abrahamsson", "Hiscirs"], ["Mr.", "Jürgen", "N", "Boeijen", "Thaddetishe"], ["Ms.", "Cerys", "K", "Martin", "Thynand"], ["Mr.", "Lewis", "A", "Bell", "Mantockity"], ["Mr.", "Jared", "A", "Jones", "Pospond"], ["Mrs.", "Elisabeth", "T", "Hyvönen", "Hasked"], ["Mr.", "Alan", "A", "Sjöberg", "Surionted"], ["Ms.", "Samantha", "W", "Lockwood", "Ditind"], ["Ms.", "Othilia", "F", "Wallin", "Thavestoon"], ["Mr.", "Stewart", "J", "Cameron", "Harizzy"], ["Ms.", "Wisal", "T", "Almasi", "Afflumad"], ["Mr.", "Johan", "E", "Petrussen", "Forounduce"], ["Mr.", "Zhen", "Y", "Hsiung", "Busiouty"], ["Mr.", "Hannes", "L", "Tainio", "Takalond"], ["Mr.", "Omar", "S", "Alem", "Hatchat"], ["Mr.", "Marius", "S", "Mortensen", "Brephe"], ["Mr.", "Onésimo", "A", "Guzmán", "Saithe"], ["Ms.", "Fayme", "V", "Margand", "Chimplas"], ["Mr.", "Wang", "J", "Yu", "Lowead"], ["Mr.", "Uwe", "P", "Beckenbauer", "Corsome"], ["Mrs.", "Melota", "K", "Kerpach", "Theireses"], ["Mrs.", "Chỉ", "T", "Bùi", "Parpookin"], ["Mrs.", "Freja", "M", "Paulsen", "Hundpares"], ["Mr.", "Elliot", "K", "Slater", "Hichaveste"], ["Mr.", "Chiemenam", "A", "Azikiwe", "Govery"], ["Mr.", "Tranh", "T", "Hà", "Vardert"], ["Mr.", "Onwughara", "A", "Nkemjika", "Youred"], ["Mr.", "Tølløv", "B", "Tveten", "Becruily"], ["Mrs.", "Kamisa", "U", "Batukayev", "Nessery"], ["Mr.", "Kian", "E", "Holm", "Sontst"], ["Mr.", "Robert", "M", "Novotny", "Bobbles"], ["Mr.", "Ziemowit", "M", "Grabowski", "Whowne"], ["Mrs.", "Najiyah", "H", "Khouri", "Couglag"], ["Mr.", "Yuusei", "S", "Kashiwagi", "Neents"], ["Mrs.", "Emma", "J", "Abakumova", "Agetaing"], ["Mrs.", "Hanna", "I", "Liikanen", "Beldess"], ["Mr.", "Ladomér", "L", "Albert", "Thomed"], ["Mr.", "Avent", "M", "Bourgeau", "Douner"], ["Mr.", "Cai", "Y", "Hsueh", "Lusand"], ["Ms.", "Cantara", "R", "Seif", "Pasuded"], ["Mr.", "Ralph", "S", "Eberhardt", "Gandiziesed"], ["Mrs.", "Kaypa", "S", "Umkhayev", "Succurs"], ["Mrs.", "Riemi", "S", "Aizawa", "Ressen"], ["Mr.", "Tesfalem", "S", "Isaias", "Sirchat"], ["Mrs.", "Aila", "K", "Riipinen", "Younsholve"], ["Mr.", "Aslanbek", "E", "Sultygov", "Hustack"], ["Mr.", "Khumid", "S", "Eldarkhanov", "Hisinee"], ["Mr.", "Robur", "R", "Zaragamba", "Marmuccuself"], ["Mrs.", "Honami", "M", "Sakaguchi", "Succubly"], ["Ms.", "Maria", "G", "Anderson", "Fliatich"], ["Mr.", "Tinna", "E", "Valgeirsson", "Hicarrece"], ["Ms.", "Mia", "B", "O'Neil", "Beadis"], ["Ms.", "Celestina", "O", "Amdam", "Thicithe"], ["Mr.", "Yohannes", "S", "Gebre", "Theirefte"], ["Mr.", "Muhammad", "C", "Russell", "Oppostis"], ["Mr.", "Kasper", "F", "Dahl", "Uniscomen"], ["Mrs.", "Freja", "R", "Koch", "Gessarcidigh"], ["Mr.", "Tetsurou", "S", "Ohara", "Frogivers"], ["Ms.", "Silvana", "K", "Arellano", "Hishisent"], ["Mr.", "Hákon", "J", "Ludvigsson", "Comraced"], ["Ms.", "K'Ellal", "M", "Ondagh", "Wasterem"], ["Ms.", "Danira", "J", "Ferenčič", "Jusbancief"], ["Ms.", "Katie", "C", "Duncan", "Thathe"], ["Mr.", "Urho", "H", "Haatainen", "Hercure"], ["Mrs.", "Urunna", "C", "Chimaraoke", "Tapeon"], ["Mr.", "Thomas", "N", "Olsen", "Rathany"], ["Ms.", "Marigold", "S", "Greenhand", "Anobaciping"], ["Mr.", "Børil", "V", "Hoven", "Milifte"], ["Mrs.", "Julija", "B", "Tabaković", "Andele"], ["Mr.", "Uchenna", "C", "Uchenna", "Amefore"], ["Mr.", "Nazhmuddin", "T", "Panova", "Suchemsess"], ["Mrs.", "Odinakachukwu", "O", "Madukaife", "Succart"], ["Ms.", "Lamees", "D", "Abboud", "Armorthavins"], ["Mr.", "Elías", "H", "Vilbergsson", "Bithate"], ["Ms.", "Jennifer", "D", "Davis", "Whation"], ["Mrs.", "Shukornia", "G", "Tesmi", "Majaus"], ["Mrs.", "Nwando", "I", "Ugonna", "Lepaso"], ["Mr.", "Gundabald", "B", "Hogpen", "Saitte"], ["Mrs.", "Satu", "L", "Gallen-Kallela", "Shaden"], ["Ms.", "Nojood", "H", "Mifsud", "Roseen"], ["Mrs.", "Nasya", "L", "Romero", "Antakedine"], ["Mr.", "Lennon", "M", "Cameron", "Mareake"], ["Mr.", "Johan", "L", "Kjær", "Hinfore"], ["Mr.", "Herman", "E", "Bengtsson", "Cognarl"], ["Mr.", "Jiří", "A", "Jaroš", "Entsion"], ["Mrs.", "Emma", "T", "Bobrova", "Thislumakin"], ["Ms.", "Simone", "N", "Öberg", "Shopplaunt"], ["Mr.", "Elenus", "M", "Grøtterud", "Resits"], ["Ms.", "Elvia", "B", "Trevisano", "Uppossom"], ["Ms.", "Zdeňka", "J", "Buchalová", "Sprety"], ["Mrs.", "Stana", "C", "Kolar", "Addren"], ["Mr.", "Callum", "I", "Johnstone", "Anguareany"], ["Mrs.", "Gerður", "A", "Bjarnadóttir", "Prover"], ["Ms.", "Bồn", "T", "Luong", "Whords"], ["Mr.", "Савелий", "А", "Щербаков", "Duchich"], ["Mrs.", "Elsebet", "S", "Myhr", "Fatinvand"], ["Mrs.", "Dorothy", "E", "White", "Mardst"], ["Ms.", "Rebekka", "L", "Garðarsdóttir", "Ardesclarm"], ["Ms.", "Taymaskha", "Y", "Chichigov", "Lowelies"], ["Mrs.", "Elisa", "E", "Iggi", "Manded"], ["Mr.", "Leonid", "A", "Lazarev", "Hicivy"], ["Mr.", "Trygg", "A", "Lauvås", "Beeltrat"], ["Mrs.", "Lalia", "R", "Button", "Inelisgue"], ["Mrs.", "Thể", "T", "Đoàn", "Riatecand"], ["Ms.", "Demi", "R", "Docherty", "Inupoppeired"]].each do |name|
    grouprnd = rand(100)
    group = nil
    groups.each { |g| group = g[1] if grouprnd >= g[0] }
    user = User.create(username: name[4].downcase, passhash: '*', realname: "#{name[0]} #{name[1]} #{name[3]}")
    Affiliation.create(user: user, usergroup: group)
  end
end

def dump_procedures
  Procedure.all.order(realid: :asc, id: :asc).each do |po|
    printf("PROC: %05d/%03d V%s %s (%s, %s)\n",po.id,po.realid,po.version ? sprintf('%02d',po.version) : '--',po.disabled ? '-' : 'X',po.code,po.title['de'])
    po.steps.each do |so|
      printf(" -> STEP: %05d/%03d V%s %s (%s, %s)\n",so.id,so.realid,so.version ? sprintf('%02d',so.version) : '--',so.disabled ? '-' : 'X',so.code,so.title['de'])
      so.measures.each do |mo|
        printf("    -> MESR: %05d/%03d V%s %s (%s, %s)\n",mo.id,mo.realid,mo.version ? sprintf('%02d',mo.version) : '--',mo.disabled ? '-' : 'X',mo.code,mo.title['de'])
      end
    end
  end
end

def change_procedure_codes(realid,repl,by)
  if !realid
    Procedure.trunk_realids.each { |trid| change_procedure_codes(trid,repl,by) }
  else
    po = Procedure.trunk_object(realid)
    po.code = po.code.gsub(repl ? repl : /\z/, by)
    po.save!
    po.steps.each do |so|
      so.code = so.code.gsub(repl ? repl : /\z/, by)
      so.save!
      so.measures.each do |mo|
        mo.code = mo.code.gsub(repl ? repl : /\z/, by)
        mo.save!
      end
    end
  end
end
