require 'test_helper'
require 'active_record'

require 'net/http'
require 'uri'
require 'json'

class UserTest < ActiveSupport::TestCase
    test "login with a locked user should fail" do

        # preparing header & request
        uri = URI.parse("http://localhost:4567/app/login")
        #header = {'Content-Type': 'text/json'}
        user = {username: 'her', passcode:'abc' } # user 'her' should be locked

        # create the HTTP objects
        http = Net::HTTP.new(uri.host, uri.port)
        request = Net::HTTP::Post.new(uri.request_uri, header)
        request.body = user.to_json

        # send the request
        response = http.request(request)

        assert response['status'] == 'error'
        assert response['code'] == 0
        assert response['messages'][0]['msg'] == 'SERVER.ERROR.LOGIN.STATUSDISABLED'

        # expected: {
        #    "status": "error",
        #    "code": 0,
        #    "stack": [],
        #    "messages": [{
        #        "msg": "SERVER.ERROR.LOGIN.STATUSDISABLED",
        #        "vals": {}
        #    }],
        #    "type": "SERVER.ERROR.LOGIN.TYPE"
        #}
    end

end
